<?php

//if (!defined('INCLUDED_AMEMBER_CONFIG')) 
//  die("Direct access to this location is not allowed");

/*
 *  aMember Pro site customization file
 *
 *  Rename this file to site.php and put your site customizations, 
 *  such as fields additions, custom hooks and so on to this file
 *  This file will not be overwritten during upgrade
 *                                                                               
 */
Am_Di::getInstance()->hook->add(Am_Event::ADMIN_MENU, function (Am_Event $event) {
    $menu = $event->getMenu();
    $menu->addPage(Zend_Navigation_Page::factory(array(
       
            'id' => 'import_csv',
            'uri' => '#',
            'label' => ___('Import CSV'),
            'order' => 1001,
            'pages' => array_filter(array(
               array(
                    'id' => 'import',
                    'controller' => 'import-csv',
                    'label' => ___('Import CSV files'),
                    'resource' => Am_Auth_Admin::PERM_IMPORT_CSV,
                    'class' => 'bold',
                ),
               
             )
        ))
                ));
   
});

class ImportcsvController extends Am_Controller // a Zend_Controller_Action successor 
{

    public function checkAdminPermissions(Admin $admin) {
        return $admin->hasPermission(Am_Auth_Admin::PERM_IMPORT_CSV);
    }

    function indexAction() {

//$user = $this->getDi()->user; //currently authenticated customer or throws exception if no auth

        $this->view->title = "My Page";
        $this->view->content = "Page content";
        $this->view->display('layout.phtml');
    }

}
