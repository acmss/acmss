<?php
/**
 * Plugin Name: WooCommerce Simply Order Export Add-on
 * Description: Downloads order details in csv format
 * Version: 2.2
 * Author: Ankit Gade
 * Author URI: http://sharethingz.com
 * License: GPL2
 */

if( !defined('ABSPATH') ){
	exit;
}

/**
 * Version of plugin
 */
define('WSOE_ADDON_VERSION', '2.2');

define('WSOE_ADDON_BASE', plugin_basename(__FILE__));

if ( in_array( 'woocommerce-simply-order-export/main.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	
	class wsoe_addon {

		/**
		 * Fields to add to export
		 */
		public $fields;

		/*
		 * Class constructor
		 */
		function __construct() {

			$this->fields = array(
				'wc_settings_tab_sku' => __('Product SKU', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__payment_method' => __('Payment Method', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__order_currency' => __('Order Currency', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab_customer_note' => __('Customer Note', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__prices_include_tax' => __('Prices Include Tax (yes/no)', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__customer_ip_address' => __('Customer IP Address', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__customer_user_agent' => __('Customer User Agent', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__customer_user' => __('Customer User', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__order_shipping' => __('Order Shopping', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_country' => __('Billing Country', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_first_name' => __('Billing First Name', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_last_name' => __('Billing Last Name', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_company' => __('Billing Company', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_address' => __('Billing Address', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_city' => __('Billing City', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_state' => __('Billing State', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__billing_postcode' => __('Billing Postcode', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_country' => __('Shipping Country', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_first_name' => __('Shipping First Name', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_last_name' => __('Shipping Last Name', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_company' => __('Shipping Company', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_address' => __('Shipping Address', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_city' => __('Shipping City', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_state' => __('Shipping State', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__shipping_postcode' => __('Shipping Postcode', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab_shipping_method' => __('Shipping Method', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__cart_discount' => __('Cart Discount', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__cart_discount_tax' => __('Cart Discount Tax', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__order_tax' => __('Order Tax', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__order_shipping_tax' => __('Order Shipping Tax', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__paid_date' => __('Paid Date', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab__transaction_id' => __('Transaction ID', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab_coupon' => __('Coupon Code', 'woocommerce-simply-order-export-add-on'),
				'wc_settings_tab_coupon_discount' => __('Coupon Discount Amount', 'woocommerce-simply-order-export-add-on'),
			);

			add_action( 'init', array( $this, 'init' ), 9 );
		}

		/**
		 * Define constants, include files, call enqueue scripts, hook to actions and filters.
		 */
		function init() {

			$this->define();
			$this->includes();
			$this->fields = apply_filters( 'wsoe_filter_fields', $this->fields );
			add_action( 'admin_enqueue_scripts', array( $this, 'script' ) );
			add_filter( 'wpg_order_columns', array( $this, 'add_fields' ) );
			add_filter( 'wc_settings_tab_order_export', array( $this, 'add_Settings_fields' ) );
			add_action( 'wpg_add_values_to_csv', array( $this, 'add_values_to_csv' ), 10, 6 );
			add_action( 'advanced_options_begin', array( $this, 'wsoe_auto_update_email' ), 10 );
			add_action( 'advanced_options_begin', array( $this, 'reorder_options' ), 10 );
			add_action( 'woocommerce_settings_saved', array( $this, 'settings_saved' ) );
			add_filter( 'wpg_export_options_settings', 'wpg_return_reorder_ids' );
		}

		/**
		 * Load locale
		 */
		function load_plugin_textdomain() {

			load_plugin_textdomain( 'woocommerce-simply-order-export-add-on', false, plugin_basename( dirname( __FILE__ ) ) . "/languages" );
		}

		/**
		 * Define constants
		 */
		function define() {
			define( 'WPG_ADDON_IMG', plugins_url('assets/img/', __FILE__) );
		}

		/**
		 * include library functions
		 */
		function includes() {

			// Includes PHP files located in 'lib' folder
			foreach( glob ( dirname(__FILE__). "/includes/*.php" ) as $lib_filename ) {
				require_once( $lib_filename );
			}
		}

		/**
		 * Enqueue scripts and styles
		 */
		public function script() {

			if( (!empty( $_GET['tab'] )&& $_GET['tab'] === 'order_export') ) {

				$custom_fields = wsoe_addon_customs();

				if( !empty( $custom_fields ) ){
					wp_register_script( 'wsoe_addon_customs', plugins_url('assets/js/script.js', __FILE__) );
					wp_localize_script( 'wsoe_addon_customs', 'wsoe_addon_customs_array', $custom_fields );
					wp_enqueue_script( 'wsoe_addon_customs' );
				}

				wp_enqueue_script('jquery-ui-sortable');
				wp_enqueue_script('jquery-ui-autocomplete');
				wp_enqueue_script('wsoe_addon_script', plugins_url('assets/js/script.js', __FILE__), array('jquery', 'jquery-ui-sortable', 'wsoe_addon_customs', 'jquery-ui-autocomplete') );
			}
		}

		function add_fields ( $cols ) {

			foreach( $this->fields as $key=>$field ){
				$cols[$key] = $field;
			}

			return $cols;
		}

		function add_Settings_fields( $settings ) {

			$columns = array_map( 'return_keys', array_flip( $this->fields ) );

			foreach( $columns as $key=>$field ) {

				$settings[$field] = array(
					'name' => __( $key , 'woocommerce-simply-order-export-add-on' ),
					'type' => 'checkbox',
					'desc' => __( $key , 'woocommerce-simply-order-export-add-on' ),
					'id'   => 'wc_settings_tab_'.$field
				);

			}

			return $settings;
		}

		function add_values_to_csv( &$csv_values, $order_details, $key, $fields, $item_id, $current_item ) {

			/**
			 * Check if the current is field from add-on or not.
			 */
			if( empty( $this->fields[$key] ) )
				return;

			/**
			 * Holds keys which are amount, in order to format those values.
			 * Use 'wsoe_price_format_list' filter to add some more keys.
			 */
			$prices_array = apply_filters( 'wsoe_price_format_list', array( '_cart_discount', '_cart_discount_tax', '_order_shipping', '_order_tax', '_order_shipping_tax', '_coupon_discount' ) );

			/**
			 * Holds keys which are not present in wp_postmeta table.
			 */
			$not_in_meta = apply_filters( 'not_in_meta', array( 'coupon', 'sku', 'customer_note', '_paid_date', 'coupon_discount', 'shipping_method', '_billing_address', '_shipping_address' ) );
			$key = str_replace( 'wc_settings_tab_', '', $key);


			if( !in_array( $key, $not_in_meta ) ) {

				if( in_array( $key, $prices_array ) ){

						$formatted_amount = wsoe_formatted_price( self::get_order_meta( $order_details->id, $key ), $order_details );
						array_push( $csv_values, $formatted_amount );
				}
				else{
					array_push( $csv_values, self::get_order_meta( $order_details->id, $key ) );
				}

			}else{

				switch( $key ) {

					case 'coupon':
						array_push( $csv_values, wpg_get_coupons( $order_details ) );
						break;

					case 'coupon_discount':
						array_push( $csv_values, wpg_get_coupons_discount_amt( $order_details ) );
						break;

					case '_billing_address':
						array_push( $csv_values, strip_tags( str_replace( '<br/>', ', ', $order_details->get_formatted_billing_address() ) ) );
						break;

					case '_shipping_address':
						array_push( $csv_values, strip_tags(str_replace( '<br/>', ', ', $order_details->get_formatted_shipping_address())) );
						break;

					case 'shipping_method':
						array_push( $csv_values, $order_details->get_shipping_method() );
						break;

					case '_paid_date':
						array_push( $csv_values, get_the_date('Y-m-d', $order_details->ID) );
						break;

					case 'customer_note':
						array_push( $csv_values, esc_html( $order_details->post->post_excerpt ) );
						break;

					case 'sku':

						if( wc_product_sku_enabled() && array_key_exists( 'wc_settings_tab_product_name', $fields ) ) {

							$product = wc_get_product( $current_item['product_id'] );
							//$id = $current_item['product_id'];
							$sku = $parent_sku = get_post_meta( $current_item['product_id'], '_sku', true );

							if( $product->is_type('variable') ){
								$id = $current_item['variation_id'];
								$sku = get_post_meta( $current_item['variation_id'], '_sku', true );
								// assign parent sku, if variation sku is blank
								if( empty( $sku ) ){
									$sku = $parent_sku;
								}
							}

							array_push( $csv_values,$sku );

						}else{
							array_push( $csv_values, '-' );
						}
						break;

					default :
						do_action_ref_array( 'wsoe_addon_add_to_csv', array( &$csv_values, $order_details, $key, $fields, $item_id, $current_item ) );
						break;
					
				}
			}

		}

		static function get_order_meta( $order_id, $key ) {

			if( empty( $order_id ) || empty( $key ) ){
				return '';
			}

			return get_post_meta( $order_id, $key, true );
		}
		
		function wsoe_auto_update_email() {
			
			$email = get_option('edd-updater-email', ''); ?>

			<tr>
				<th>
					<?php _e( 'Email for auto-update', 'woocommerce-simply-order-export-add-on' ) ?>
					<img class="help_tip" data-tip="<?php _e('Insert the email address you used to purchase this plugin.', 'woocommerce-simply-order-export-add-on') ?>" src="<?php echo OE_IMG; ?>help.png" height="16" width="16">
				</th>
				<td>
					<div>
						<input type="email" name="edd_updater_email" value="<?php echo $email; ?>" />
					</div>
				</td>
			</tr><?php
		}

		function reorder_options() {

			$fields_to_reorder = get_fields_to_reorder( wpg_order_export::get_settings_fields() );

			if( !empty( $fields_to_reorder['id'] ) ) { ?>

				<tr>

					<th><?php _e( 'Reorder Fields', 'woocommerce-simply-order-export-add-on' ) ?></th>
					
					<td>
						
						<div class="reorder-section">

							<ul id="wpg-reorder"><?php

								foreach( $fields_to_reorder['id'] as $key=>$val ) { ?>

									<li class="button wpg-reorder-button">
										<span><?php echo $fields_to_reorder['name'][$key]; ?></span>
										<input type="hidden" name="wpg_reorder[]" value="<?php echo $val; ?>" />
									</li><?php

								} ?>

							</ul>

							<input type="button" value="<?php _e('Save', 'woocommerce-simply-order-export-add-on') ?>" class="button" />
							<img src="<?php echo WPG_ADDON_IMG.'load.gif' ?>" class="hidden" />

						</div>
				
					</td>
					
				</tr><?php
			}

		}

		function settings_saved() {

			if( !empty($_REQUEST['tab']) && $_REQUEST['tab'] === 'order_export' ) {

				/**
				 * Update auto update email for add-on
				 */
				$auto_update_email = !empty( $_POST['edd_updater_email'] ) ? $_POST['edd_updater_email'] : '';

				$setting_fields = wpg_order_export::get_settings_fields();
				$current_fields = array();

				foreach( $setting_fields as $k=>$v ) {

					if( empty( $v['id'] ) )
						continue;

					if( !empty( $_REQUEST[$v['id']] ) ) {
						array_push( $current_fields, $v['id'] );
					}
				}

				$wpg_reorder = empty($_REQUEST['wpg_reorder']) ? array() : $_REQUEST['wpg_reorder'];
				$remove_from_order_array = array_diff( $wpg_reorder, ($current_fields) ); // This will let us know which fields to remove from reorder array
				$add_to_order_array		 = array_diff( $current_fields, $wpg_reorder ); // This will let us know which new fields are added

				/**
				 * Remove elements from $wpg_reorder array.
				 */
				if( !empty( $remove_from_order_array ) ) {

					foreach( $remove_from_order_array as $k=>$v ) {

						if( ($key = array_search( $v, $wpg_reorder )) !== FALSE ) {
							unset($wpg_reorder[$key]);
						}
					}
				}

				/**
				 * Add elements to $wpg_reorder array at end
				 * because we do not have it's order info.
				 */
				if( !empty( $add_to_order_array ) ) {

					foreach( $add_to_order_array as $k=>$v ){
						array_push( $wpg_reorder, $v );
					}
				}

				update_option( 'wpg_reorder', $wpg_reorder );
				update_option( 'edd-updater-email', $auto_update_email );

				do_action('wsoe_addon_settings_saved');
			}
		}

	}

	new wsoe_addon();
}