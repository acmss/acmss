jQuery(document).ready(function(){

	jQuery( "#wpg-reorder" ).sortable();

	jQuery('.reorder-section input[type="button"]').on('click', function(){
		
		var element = jQuery(this);
		var data = jQuery("#wpg-reorder input").serialize();
		var action = 'wpg_reorder';
		var ajaxdata = {
			action: action,
			data: data
		};
		
		jQuery(element).next().removeClass('hidden');
		jQuery.post(ajaxurl, ajaxdata, function(response){
			response = jQuery.parseJSON(response);
			jQuery(element).next().addClass('hidden');
		});
		
	});

	jQuery('body').off('click', '.clone').on('click', '.clone',function(){
		var $clone = jQuery(this).parents('.toclone').clone(false);
		$clone.find('input[type="checkbox"]').prop('checked', false);
		$clone.find('input[type="text"]').val("");
		$clone.appendTo('.clone-wrapper');
		jQuery('.clone-wrapper').trigger('wsoe-customs-adjust');
		wsoe_bind_autocomplete();
	});

	jQuery('body').off('click', '.delete').on('click', '.delete',function(){
		if( jQuery('.clone-wrapper .toclone').length > 1 ) {
			jQuery(this).parents('.toclone').remove();
			jQuery('.clone-wrapper').trigger('wsoe-customs-adjust');
		}
	});

	wsoe_bind_autocomplete();

	/**
	 * Fires when row is added/deleted
	 */
	jQuery('body').off('wsoe-customs-adjust', '.clone-wrapper').on('wsoe-customs-adjust', '.clone-wrapper', function(){

		var count = 0;

		jQuery('.clone-wrapper .toclone').each(function(){
			jQuery(this).find('input[type="checkbox"]').attr('name', 'wsoe_cf_meta['+count+']');
			count++;
		});
	});
	
});

function wsoe_bind_autocomplete() {
	if( typeof wsoe_addon_customs_array !== 'undefined' ) {
		jQuery('.wsoe_cf_key').each(function(){
			jQuery(this).autocomplete({source: wsoe_addon_customs_array});
		});
	}
}