<?php

global $wpdb;

$offset = 0; $keys = array();

do{

	// Fetch 50 orders at a time
	$orders = new WP_Query( array( 'post_type'=> 'shop_order', 'fields'=>'ids', 'post_status'=>'any', 'offset'=> ($offset * 50) ) );

	if( $orders->have_posts() ){

		$post_ids_placeholder = implode( ',', array_fill( 0, count( $orders->posts ), '%d' ) );

		$query = $wpdb->prepare( "SELECT DISTINCT meta_key as mkey FROM ". $wpdb->prefix ."postmeta WHERE post_id IN (". $post_ids_placeholder .")", $orders->posts );
		$results = $wpdb->get_results( $query );

		if( !empty( $results ) ) {

			/**
			 * Collect all the keys in one array
			 */
			foreach ( $results as $result ) {	
				array_push( $keys, $result->mkey );
			}
			//wsoe_addon_update_customs( $keys );
		}
	}

	++$offset;

}while( $orders->have_posts() );

// Sort the array ascending order.
sort( $keys, SORT_STRING );

// Store all unique meta keys, so that it will be available o choose on settings page.
wsoe_addon_update_customs( array_values(array_unique( $keys )) );