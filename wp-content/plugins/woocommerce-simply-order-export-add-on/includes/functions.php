<?php

/**
 * Array mapping function.
 */
function return_keys( $fields ) {

	return str_replace( 'wc_settings_tab_', '', $fields );
}

/**
 * Returns names and IDs of fields in reorder array.
 * This will be used in reorder section under advanced settings.
 */
function get_fields_to_reorder( $setting_fields ) {

	if( !is_array( $setting_fields )  || empty($setting_fields) )
		return null;

	$reorder_array['id'] = $reorder_array['name'] = array();

	$wpg_reorder = get_option( 'wpg_reorder', array() );
	$settings_array = get_settings_assoc($setting_fields);

	if( !empty( $wpg_reorder ) ) {

		foreach( $wpg_reorder as $k=>$v ) {

			if( !empty( $settings_array[$v] ) ) {
				array_push($reorder_array['id'], $v);
				array_push($reorder_array['name'], $settings_array[$v]);
			}
		}

	}

	return $reorder_array;
}

/**
 * Return IDs of reorder array.
 */
function wpg_return_reorder_ids( $settings ) {

	$wpg_reorder = get_option( 'wpg_reorder', array() );

	if( !empty( $wpg_reorder ) ) {

		$settings = array();

		foreach( $wpg_reorder as $k=>$v ) {
			array_push( $settings, array('id'=>$v) );
		}
	}
	
	return $settings;
}

/**
 * Returns associative array of settings
 */
function get_settings_assoc( $settings ) {
	
	$return = array();
	
	foreach( $settings as $k=>$v ) {
		if( !empty( $v['id'] ) && !empty( $v['name'] ) ) {
			$return[$v['id']] = $v['name'];
		}
	}
	
	return $return;
}

/**
 * Saves reordering of fields
 */
function wpg_save_reordering() {

	parse_str(filter_input(INPUT_POST, 'data', FILTER_DEFAULT), $order);
	$order = !empty( $order['wpg_reorder'] ) ? $order['wpg_reorder'] : array();
	update_option( 'wpg_reorder', $order, 'no' );

}
add_action('wp_ajax_wpg_reorder', 'wpg_save_reordering');

/**
 * Gets order related item
 */
function wpg_get_item( $order, $item = array() ) {

	return $order->get_items( $item );
}

/**
 * Get comma separated coupons list
 */
function wpg_get_coupons( $order ) {

	$coupons = wpg_get_item( $order, array('coupon') );

	if( $coupons ) {

		$coupon_codes = array();

		foreach ( $coupons as $coupon ) {
			array_push( $coupon_codes, $coupon['name'] );
		}

		$separator = apply_filters( 'wpg_coupons_separator', ' | ' );
		return $coupon_codes = implode( $separator, $coupon_codes );
	}

	return '';
}

function wpg_get_coupons_discount_amt($order) {

	$coupons = wpg_get_item( $order, array('coupon') );

	if( $coupons ) {

		$dis_amt = 0;
		foreach( $coupons as $coupon ) {
			$dis_amt += ( empty($coupon['discount_amount']) ? 0 : $coupon['discount_amount'] );
		}

		return strip_tags( html_entity_decode( wc_price( $dis_amt , array( 'currency' => $order->get_order_currency() ) ) ) ) ;
	}

	return 0;
}

/**
 * Returns the arrays of custom fields.
 * 
 * @return array $keys Arrays of custom fields
 */
function wsoe_addon_customs() {

	if( $keys = wp_cache_get( 'wsoe_addon_customs', 'wsoe_cache' ) )
		return $keys;

	return $keys = get_option('wsoe_addon_customs', array());
}

/**
 * 
 * @param array $customs Array of custom fields related to orders
 */
function wsoe_addon_update_customs( $customs ) {

	update_option( 'wsoe_addon_customs', $customs, false );

	wp_cache_set( 'wsoe_addon_customs', $customs, 'wsoe_cache' );
}

/**
 * Whenever there will be any new custom field in shop_order post
 * Update the custom field array in database.
 */
function wsoe_update_custom_fields( $meta_id, $object_id, $meta_key, $meta_value ) {

	if( 'shop_order' == get_post_type($object_id) ) {

		$custom_fields = wsoe_addon_customs();

		if( in_array( $meta_key, $custom_fields ) ) {

			array_push( $custom_fields, $meta_key );

			wsoe_addon_update_customs( $custom_fields ); // Update custom fields array
		}
	}
}
add_action( 'updated_postmeta', 'wsoe_update_custom_fields', 12, 4 );
add_action( 'updated_post_meta', 'wsoe_update_custom_fields', 12, 4 );

/**
 * 
 */
function wsoe_custom_field_management_section() {

		$wsoe_customs_data_default['wsoe_cf_keys']  = array(0=>'');
		$wsoe_customs_data_default['wsoe_cf_names'] = array(0=>'');
		$wsoe_customs_data_default['wsoe_cf_meta']  = array(0=>0);

		$custom_fields_data = get_option( 'wsoe_addon_custom_fields', $wsoe_customs_data_default );
		$custom_fields_data = empty( $custom_fields_data['wsoe_cf_keys'] ) ? $wsoe_customs_data_default : $custom_fields_data;
		?>

		<tr>

			<th colspan="4">
				<?php _e( 'Custom Fields', 'woocommerce-simply-order-export') ?>
			</th>

		</tr>

		<tr>
			<td colspan="4">

				<table>
					<thead>
						<tr>
							<th>
								<?php _e( 'Field key', 'woocommerce-simply-order-export' ) ?>
							</th>
							<th>
								<?php _e( 'Field name', 'woocommerce-simply-order-export' ) ?>
							</th>
							<th>
								<?php _e( 'Not in meta', 'woocommerce-simply-order-export' ) ?>
							</th>
							<th colspan="2"></th>
						</tr>
					</thead>

					<tbody class="clone-wrapper"><?php
	
					foreach( $custom_fields_data['wsoe_cf_keys'] as $key => $field_data ){?>
						
						<tr class="toclone">

							<td>
								<input class="wsoe_cf_key" type="text" name="wsoe_cf_key[]" value="<?php echo $custom_fields_data['wsoe_cf_keys'][$key]; ?>" />
							</td>
							
							<td>
								<input type="text" name="wsoe_cf_name[]" value="<?php echo $custom_fields_data['wsoe_cf_names'][$key]; ?>" />
							</td>
							
							<td>
								<input type="checkbox" name="wsoe_cf_meta[<?php echo $key; ?>]" value="1" <?php checked( $custom_fields_data['wsoe_cf_meta'][$key], 1, true ); ?> />
							</td>

							<td>
								<div class="button clone">+</div>
							</td>

							<td>
								<div class="button delete">-</div>
							</td>

						</tr><?php
						
					} ?>

					</tbody>
					<tfoot></tfoot>
				</table>
			</td>
		</tr><?php

}
add_action( 'advanced_options_end', 'wsoe_custom_field_management_section', 8 );

/**
 * Save custome fields data
 */
function wsoe_save_customs_data( ) {

	if( (!empty( $_POST ) && !empty( $_GET['tab'] ) && $_GET['tab'] == 'order_export') && 
		( !empty( $_POST['wsoe_cf_key'] ) && is_array( $_POST['wsoe_cf_key'] ) ) 
	) {

		$wsoe_customs_data['wsoe_cf_keys']  = array();
		$wsoe_customs_data['wsoe_cf_names'] = array();
		$wsoe_customs_data['wsoe_cf_meta']  = array();

		foreach( $_POST['wsoe_cf_key'] as $key=>$val ) {

			if( empty( $val ) ){
				continue;
			}

			$name = empty( $_POST['wsoe_cf_name'][$key] ) ? $val : $_POST['wsoe_cf_name'][$key];
			$meta = empty( $_POST['wsoe_cf_meta'][$key] ) ? 0 : 1; 

			array_push( $wsoe_customs_data['wsoe_cf_keys'], $val );
			array_push( $wsoe_customs_data['wsoe_cf_names'], $name );
			array_push( $wsoe_customs_data['wsoe_cf_meta'], $meta );
		}

		update_option( 'wsoe_addon_custom_fields', $wsoe_customs_data, false );

		/**
		 * Identify referer
		 */
		$ref = false;
		if ( ! empty( $_REQUEST['_wp_http_referer'] ) )
			$ref = wp_unslash( $_REQUEST['_wp_http_referer'] );
		elseif ( ! empty( $_SERVER['HTTP_REFERER'] ) )
			$ref = wp_unslash( $_SERVER['HTTP_REFERER'] );

		// Once we have referer, we can redirect
		if( $ref ) {
			wp_safe_redirect($ref, 301);
			die;
		}

	}
}
add_action( 'wsoe_addon_settings_saved', 'wsoe_save_customs_data', 99 );

function wsoe_start_op_buffer() {
	
	if( (!empty( $_POST ) && !empty( $_GET['tab'] ) && $_GET['tab'] == 'order_export') && 
		( !empty( $_POST['wsoe_cf_key'] ) && is_array( $_POST['wsoe_cf_key'] ) ) 
	) {
		ob_start();
	}
}
add_action( 'load-woocommerce_page_wc-settings', 'wsoe_start_op_buffer' );

/**
 * Add custom fields data using filter
 */
function wsoe_add_custom_fields( $fields ){

	$custom_fields_data	= get_option( 'wsoe_addon_custom_fields', array() );

	if( !empty( $custom_fields_data['wsoe_cf_keys'] ) ) {

		foreach( $custom_fields_data['wsoe_cf_keys'] as $key => $value ){

			$fields['wc_settings_tab_'.$value] =	$custom_fields_data['wsoe_cf_names'][$key];
		}
	}

	return $fields;

}
add_filter( 'wsoe_filter_fields', 'wsoe_add_custom_fields' );

/**
 * Add no meta values to array using filter
 */
function wsoe_add_nometa_values( $no_meta ) {

	$custom_fields_data = get_option( 'wsoe_addon_custom_fields', array() );

	if( !empty( $custom_fields_data['wsoe_cf_keys'] ) ) {

		foreach( $custom_fields_data['wsoe_cf_keys'] as $key => $value ){
			
			if( $custom_fields_data['wsoe_cf_meta'][$key] == 1 ) {
				array_push( $no_meta, $custom_fields_data['wsoe_cf_keys'][$key] );
			}
		}
	}

	return $no_meta;
}
add_filter( 'not_in_meta', 'wsoe_add_nometa_values' );

/**
 * Run plugin update scripts
 */
function wsoe_run_update_scripts() {

	// Run this block if current user is woocommerce manager
	if( is_admin() && wsoe_is_shop_manager() ){

		$db_version		=	get_option( 'wsoe_addon_version', '1.0' ); // Check what was the last version
		$upgrade_ran	=	$upgrade_ran_new = get_option( 'wsoe_upgrade_ran', array() );

		$update_scripts = array('1.9'); // Array of version numbers for which update scripts are present.

		if( version_compare( $db_version, WSOE_ADDON_VERSION, '<' ) ) {

			foreach( $update_scripts as $script ) {

				// Check if this update script has already been executed, if it is, do not execute it again.
				if( (version_compare( $script, WSOE_ADDON_VERSION, '<=' )) && ( !in_array( $script, $upgrade_ran ) ) ) {

					require_once trailingslashit( plugin_dir_path( dirname(__FILE__) ) ).'updater/wsoe-'.$script.'.php';
					array_push( $upgrade_ran_new, $script );
				}
			}

			$diff = array_diff( $upgrade_ran_new, $upgrade_ran );

			if( !empty( $diff ) ){
				update_option( 'wsoe_upgrade_ran', $upgrade_ran_new );
			}

			update_option( 'wsoe_addon_version', WSOE_ADDON_VERSION );
		}
	}
}
add_action( 'wp_loaded', 'wsoe_run_update_scripts' );
