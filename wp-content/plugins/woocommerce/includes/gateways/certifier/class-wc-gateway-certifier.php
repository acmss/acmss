<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Cheque Payment Gateway.
 *
 * Provides a Cheque Payment Gateway, mainly for testing purposes.
 *
 * @class 		WC_Gateway_Certifier
 * @extends		WC_Payment_Gateway
 * @version		2.1.0
 * @package		WooCommerce/Classes/Payment
 * @author 		Kurt Schuster - Web Masters - based on WooCommerce Cheque
 */
class WC_Gateway_Certifier extends WC_Payment_Gateway {

	/**
	 * Constructor for the gateway.
	 */
	public function __construct() {
		$this->id                 = 'certifier';
		$this->icon               = apply_filters( 'woocommerce_cheque_icon', '' );
		$this->has_fields         = false;
		$this->method_title       = _x( 'National Certifier', 'Payment processor for Entities with contracts', 'woocommerce' );
		$this->method_description = __( 'Provides an interface to process orders from business entities with contracts with ACMSS.', 'woocommerce' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions' );

		// Actions
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_thankyou_cheque', array( $this, 'thankyou_page' ) );
		// Filter
		// add_filter( 'woocommerce_payment_complete_order_status', array( $this, 'change_payment_complete_order_status' ), 10, 3 );

		// Customer Emails
		add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
	}

	/**
	 * Initialise Gateway Settings Form Fields.
	 */
	public function init_form_fields() {

		$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable National Certifier processing', 'woocommerce' ),
				'default' => 'no',
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => _x( 'Process certifier signups', 'Certifier processing method', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => __( 'Description', 'woocommerce' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your checkout.', 'woocommerce' ),
				'default'     => __( 'Processes on-boarding of clients from National Certifiers.', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'instructions' => array(
				'title'       => __( 'Instructions', 'woocommerce' ),
				'type'        => 'textarea',
				'description' => __( 'Instructions that will be added to the thank you page and emails.', 'woocommerce' ),
				'default'     => 'Your subscription / membership is activated',
				'desc_tip'    => false,
			),
		);
	}

	/**
	 * Output for the order received page.
	 */
	public function thankyou_page() {
		if ( $this->instructions ) {
			echo wpautop( wptexturize( $this->instructions ) );
		}
	}

	/**
	 * Add content to the WC emails.
	 *
	 * @access public
	 * @param WC_Order $order
	 * @param bool $sent_to_admin
	 * @param bool $plain_text
	 */
	public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		if ( $this->instructions && ! $sent_to_admin && 'certifier' === $order->get_payment_method() && $order->has_status( 'on-hold' ) ) {
			echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
		}
	}

	/**
	 * Process the payment and return the result.
	 *
	 * @param int $order_id
	 * @return array
	 */
	public function process_payment( $order_id ) {

		$order = wc_get_order( $order_id );
		// error_log('certifier gateway :: 115 :: $order ' . print_r($order,true));
		$customerID = $order->get_customer_id();
		// get certifier name
		$certifierName = $_POST['certifierName'];
		// error_log('certifier gateway :: 118 :: certifierName: ' . $certifierName);
		
		// process according to contract terms
		switch($certifierName) {
		case 'CityMD':
			$group_id = 195;
			$order->set_total(0);
			$order->update_status('completed', _x('Account ready','National Certifier','woocommerce'));
			$order->payment_complete();
			break;
		case 'ProScribe':
			$group_id = 286;
			$order->set_total(0);
			$order->update_status('completed', _x('Account ready','National Certifier','woocommerce'));
			$order->payment_complete();
			break;
		case 'ScribeAmerica':
			$group_id = 369;
			$order->update_status( 'on-hold', _x( 'Awaiting payment', 'National Cerifier', 'woocommerce' ) );
			break;
		case 'ACESO':
			$group_id = 453;
			$order->update_status( 'on-hold', _x( 'Awaiting payment', 'National Cerifier', 'woocommerce' ) );
			break;
		case 'FloridaEye':
			$group_id = 290;
			$order->update_status( 'on-hold', _x( 'Awaiting payment', 'National Cerifier', 'woocommerce' ) );
			break;
		case 'ESSEN':
			$group_id = 419;
			$order->update_status( 'on-hold', _x( 'Awaiting payment', 'National Cerifier', 'woocommerce' ) );
			break;
		case 'FairviewClinic':
			$group_id = 439;
			$order->update_status( 'on-hold', _x( 'Awaiting payment', 'National Cerifier', 'woocommerce' ) );
			break;
		
		default:
		
		
		
		}
		
		
		
		
		
		/*

		if ( $order->get_total() > 0 ) {
			// Mark as on-hold (we're awaiting the cheque)
			$order->update_status( 'on-hold', _x( 'Awaiting check payment', 'Check payment method', 'woocommerce' ) );
		} else {
			$order->payment_complete();
		}
		
		*/
		// $order->update_status('completed', _x('Account ready','National Certifier','woocommerce'));
		$order->add_order_note('Order created via Gateway Certifier',0,false);
		// $order->get_customer_order_notes();
		
		if($_POST['certifierName'] != ''){
			Groups_User_Group::create( array( 'user_id' => $customerID, 'group_id' => $group_id ));
			add_user_meta($customerID,'_acmssNationalCertifierName',$_POST['certifierName']);
			add_user_meta($customerID,'_acmssCertifierOtherName',$_POST['certifierOtherName']);

		}

		// Reduce stock levels
		// wc_reduce_stock_levels( $order_id );

		// Remove cart
		WC()->cart->empty_cart();

		// Return thankyou redirect
		return array(
			'result' 	=> 'success',
			'redirect'	=> $this->get_return_url( $order ),
		);
	}
	
	/**
	 * Change payment complete order status to completed for COD orders.
	 *
	 * @since  3.1.0
	 * @param  string $status
	 * @param  int $order_id
	 * @param  WC_Order $order
	 * @return string
	 */
	public function change_payment_complete_order_status( $status, $order_id = 0, $order = false ) {
		if ( $order && 'certifier' === $order->get_payment_method() ) {
			$status = 'completed';
		}
		return $status;
	}	
}
