<?php
	/**
	 * Plugin Name: ACMSS Plugin
	 * Plugin URI: http://www.theacmss.com/plugins/
	 * Description: ACMSS Core functionality
	 * Version: 2.42
	 * Author: Dynamic Array
	 * Revised / Extended: Web Masters http://www-masters.com
	 * Author URI: http://www.dynamicarray.co.uk
	 * Requires at least: 4.0
	 * Tested up to: 4.4
	 */
	
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	define( 'ACMSS_VERSION', '2.42');
	define( 'BOUGHT_BUNDLES', 'bought_bundles');
	define( 'READ_MSCAT_MANUAL', 'read_mscat_manual' );
	define( 'PURCHASED_QUIZZES', 'purchased_quizzes' );
	define( 'ACCESS_ATTESTATION', 'access_attestation' );
	define( 'WEB_DEV', 'john@dynamicarray.co.uk' );
	define( 'WEB_DEV_ID', 3 );

	putenv( 'MAILCHIMP_APIKEY=64282e189044e615bee3629fc605d816-us3' );

	class ACMSS_Core {
		protected $_emails = null;

		protected static $_instance = null;
		protected static $_wc = null;
		protected static $_admin = null;
		protected static $_entitlements = null;
		protected static $_shortcodes = null;
		protected static $_quizzes = null;

		function __construct() {
			if( WP_DEBUG ) {
				error_reporting( E_ALL & ~E_NOTICE );
			}

			$me = __CLASS__;

			self::$_wc = new ACMSS_WooCommerce();
			self::$_admin = new ACMSS_Admin();
			self::$_entitlements = new ACMSS_Entitlements();
			self::$_shortcodes = new ACMSS_Shortcodes();
			self::$_quizzes = new ACMSS_Quizzes();

			$this->emails();

			add_action( 'plugins_loaded', array( 'ACMSS_PageTemplates', 'get_instance' ) );
			//add_action( 'wp', array( __class__, 'schedule_imap' );
			add_action( 'acmss_affidavits_event', array( 'ACMSS_IMAP', 'fire_process_affidavits' ), 1, 1 );
			add_action( 'init', array( $this, 'init' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			// add_action( 'wp_footer', array( $this, 'zendesk_footer') );
			
			add_filter( 'wp_nav_menu_objects', array( $this, 'custom_nav_menu_objects' ), 20, 2 );

			add_filter( 'body_class', array( $this, 'body_class' ) );
			add_filter( 'gettext', array( $this, 'get_text' ), 10, 33 );
		

			add_shortcode( 'cmss-affidavit-prompt', array( $this, 'cmss_affidavit_prompt' ) );

			add_action( 'wp_ajax_member_assign', array( $this, 'member_assign' ) );
			add_action( 'wp_ajax_increment_entitlement', array( $this, 'increment_entitlement' ) );

		}

		public function increment_entitlement() {
			$total = 'error';
			$inc = $_POST['increment'];
			$id = $_POST['object_id'];
			$user_id = $_POST['user_id'];;

			switch ($_POST['type']) {
				case 'product':
					ACMSS_Entitlements::add_assignable( $id, $inc, $user_id );
					$total = ACMSS_Entitlements::user_has_assignable( $id, $user_id );
					break;
				
				case 'entitlement':
					ACMSS_Entitlements::add_entitlement( 'quiz', $id, $inc, $user_id );
					$total = ACMSS_Entitlements::user_has_entitlement( 'quiz', $id, $user_id );
					break;
			}
			
			$response = array( 'total' => $total );

			wp_send_json_success( $response );
		}

		public function member_assign() {
			$user_id = get_current_user_id();
			$product_id = $_POST['product_id'];
			error_log('ACMSS 96 :: product_id: ' . $product_id . ' user_id: ' . $user_id . ' members: ' . print_r($_POST['members'],true));

/*
			switch($item) {
				case 'mscat-quiz':
					$item_count = $this->get_assessment_access( $user_id, 'MSCAT' );
					break;

				case 'hipaa-quiz':
					$item_count = $this->get_assessment_access( $user_id, 'HIPAA' );
					break;

				case 'mscat-manual':
					$item_count = (int) get_user_meta( $user_id, '_mscat_manual_access', true );
					break;

				case 'acmss-membership':
					$item_count = $this->adjust_owned_memberships_count( $user_id );
					break;

				default:
					exit();
			}
*/
			$item_count = ACMSS_Entitlements::user_has_assignable( $product_id, $user_id );
			$members = $_POST['members'];
			$assigned = 0;

			if( $item_count >= count($members) ) {
				foreach( $members as $member_id ) {
					ACMSS_Entitlements::process_purchased_entitlements( $product_id, 1, $member_id );

					if( $product_id == 11959 ){
						error_log('extend subscription / membership for 12 months for memberID: ' . $member_id);
					
						$subscriptions = wcs_get_users_subscriptions($member_id);
						error_log('acmss.php :: 132 :: subscriptionsArray: ' . print_r($subscriptions,true));
						$subscription = array_shift($subscriptions);
						error_log('acmss.php :: 134 :: subscription: ' . print_r($subscription,true));
						$subscriptionID = $subscription->id;
						$subscriptionRenewalDate = get_post_meta($subscriptionID,'_schedule_next_payment',true);
						error_log('acmss.php :: 137 :: subscriptionRenewalDate: ' . $subscriptionRenewalDate);
						
						if(strlen($subscriptionRenewalDate) > 10) {
							$subscriptionRenewalDateArray = explode('-',$subscriptionRenewalDate);
							$subscriptionRenewalDateYearUpdated = $subscriptionRenewalDateArray[0] + 1;
							$subscriptionRenewalDateUpdated = $subscriptionRenewalDateYearUpdated . '-' . $subscriptionRenewalDateArray[1] . '-' . $subscriptionRenewalDateArray[2];
							error_log('acmss.php :: 143 :: subscriptionRenewalDateUpdated: ' . $subscriptionRenewalDateUpdated);
							$returnedValue = update_post_meta($subscriptionID,'_schedule_next_payment',$subscriptionRenewalDateUpdated,$subscriptionRenewalDate);
							// error_log('acmss.php :: 145 :: returnedValue: ' . $returnedValue);
							// ACMSS_Entitlements::use_assignable( $product_id, 1, $user_id );
						}
						else {
							error_log('acmss.php :: 149 :: user_id: ' . $user_id);
							ACMSS_Entitlements::add_assignable(11959,1,$user_id);
						}

					
					}

/*
					switch($item) {
						case 'mscat-quiz':
							$this->add_assessment_access( $member_id, 'MSCAT', 3 );
							$assigned -= 3;
							break;

						case 'hipaa-quiz':
							$this->add_assessment_access( $member_id, 'HIPAA', 3 );
							$assigned -= 3;
							break;

						case 'mscat-manual':
							if( !user_can( $member_id, READ_MSCAT_MANUAL) ) {
								$this->add_user_cap( $member_id, READ_MSCAT_MANUAL );
								$assigned--;
							}
							break;

						case 'acmss-membership':
							$this->emails()->send_email( 'welcome-to-the-acmss', $member_id );
							$assigned--;
					}
					*/
				}
				
				ACMSS_Entitlements::use_assignable( $product_id, count($members) * -1, $user_id );
				$assigned = count($members) * -1;
				
				error_log('acmss.php :: 185 :: just decremented the product # :: product_id: ' . $product_id . ' assigned count: ' . $assigned);
				/*
				if( ($strlen($subscriptionRenewalDate) > 10) && ($product_id == 11959) ) {
					$assigned = (int)$assigned + 1;
				}
				error_log('acmss.php :: 190 :: assigned: ' . $assigned);
				*/


				// Remove the assigned items from the current user's allocation - !!$ASSIGNED WILL BE NEGATIVE!!!
				/*
				switch($item) {
					case 'mscat-quiz':
						$this->add_assessment_access( $user_id, 'MSCAT', $assigned );
						break;

					case 'hipaa-quiz':
						$this->add_assessment_access( $user_id, 'HIPAA', $assigned );
						break;

					case 'mscat-manual':
						update_user_meta( $user_id, '_mscat_manual_access', $item_count + $assigned );
						break;

					case 'acmss-membership':
						$this->adjust_owned_memberships_count( $user_id, $assigned );
						break;
				}			*/

				$list = $this->get_member_list( $user_id, true );
			} else {
				$item_count = -1;
				$message = 'You do not have sufficient items to make those assignments.';
			}

			echo json_encode( array( 'message' => $message, 'item_count' => $item_count + $assigned, 'list' => $list ) );

			exit();
		}

		public function enqueue_scripts() {
			wp_enqueue_script( 'acmss-global', plugins_url('/js/acmss.js', __file__ ), 'jquery', ACMSS_VERSION, true );
		
			wp_localize_script( 'acmss-global', 'acmss_global', array( 'ajax_url' => admin_url('admin-ajax.php'), 'cart_nonce' => wp_create_nonce( 'ajax-cart-request' ) ) );			
		}

		public function get_text( $translation = '', $text = '', $domain = '' ) {
			// error_log('acmss.php :: 232 :: translation: ' . $translation);
			if( $domain == 'woocommerce-subscriptions' ) {
				switch($translation) {
					case '%1$s and a %2$s sign-up fee':
						// error_log('string 1: %1$s string2: %2$s');
						// error_log();
						// $translation = '%1$s and %2$s for assessment materials<BR>MACRA Quarterly Discount'; // was bundles
						$translation = '%1$s plus %2$s, one time, for assessment materials<BR>MACRA Quarterly Discount';
						// error_log('acmss.php :: 239 :: translation: ' . $translation);
						// $translation = '%2$s Initial cost<BR>MACRA Quarterly Discount'; // was bundles
						break;

					//case '%1$s<span class="bundled_subscriptions_price_html" %2$s> now,</br>then %3$s</span>':
					//	$translation = '%1$s<span class="bundled_subscriptions_price_html" %2$s> now, then %3$s</span>';
					//	break;
				}
			}

			return $translation;
		}

		public function body_class( $classes ) {
			if( !is_user_logged_in() ) {
				$classes[] = 'logged-out';
			}

			return $classes;
		}


		public function zendesk_footerOLD() {
				echo <<< EOT
<!-- Start of acmss Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","acmss.zendesk.com");/*]]>*/</script>
<!-- End of acmss Zendesk Widget script -->
EOT;
		}

		public function zendesk_footer() {
				echo <<< EOT
<!-- Start of acmss Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","acmss.zendesk.com");
/*]]>*/</script>
<!-- End of acmss Zendesk Widget script -->
EOT;
		}


		public function init() {
			if( isset($_GET['imap']) && $_GET['imap'] == 'aX42Mi' ) {
				do_action( 'acmss_affidavits_event', true );
				wp_die('Manual execution of IMAP complete.');
			}

			if( current_user_can('administrator') && isset($_GET['code']) && method_exists( 'ACMSS_Testing', $_GET['code'] ) ) {
				$m = $_GET['code'];

				ob_start();

				ACMSS_Testing::$active = true;

				ACMSS_Testing::$m();

				$buffer = ob_get_contents();

				ob_end_clean();

				wp_die( $buffer );
			}

			remove_action( 'wp_head', 'feed_links', 2 );
			remove_action( 'wp_head', 'feed_links_extra', 3 );
			remove_action( 'wp_head', 'rsd_link' );
			remove_action( 'wp_head', 'wlwmanifest_link' );
			remove_action( 'wp_head', 'index_rel_link' );
			remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
			remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
			remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
			remove_action( 'wp_head', 'wp_generator' );
			remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
			remove_action( 'wp_head', 'noindex', 1 );
			
			add_action( 'learndash_quiz_completed', array( $this, 'quiz_completed' ) );
			add_action( 'wp_head', array( $this, 'analytics' ) );

			$this->init_mscat();
			$this->schedule_imap();
		}

		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		public function emails() {
			if ( is_null( $this->_emails ) ) {
				$this->_emails = new ACMSS_Emails();
			}
			return $this->_emails;
		}

		/**
		 * On an early action hook, check if the imap call is scheduled - if not, schedule it.
		 */
		public static function schedule_imap() {
			if ( !wp_next_scheduled( 'acmss_affidavits_event' ) ) {
				wp_schedule_event( time(), 'hourly', 'acmss_affidavits_event');
			}
		}

		public function continuing_ed_list() {
			$a =  array( 
				'1' => 'Health Information Technology',
				'2' => 'Electronic Health Records',
				'3' => 'Reimbursement',
				'4' => 'HIPAA, Privacy & Security',
				'5' => 'Regulations (MU, TJC, CMS)',
				'6' => 'Clinical Medicine',
				'7' => 'Quality Assurance',
				'8' => 'Quality Measures',
				'9' => 'Interoperability',
				'10' => 'Health Information Exchange',
				'11' => 'Clinical Documentation',
				'12' => 'Technology and the Workplace',
				'13' => 'Leadership',
				'14' => 'HIPAA for Medical Scribes video/assessment',
				'15' => 'Emergency Department Best Practices',
				'16' => 'Practice Management',
				'17' => 'Conference',
				'18' => 'Webinar',
				'19' => 'Bloodborne Pathogens/Waste Management (ACMSS approved through 2017)',
				'20' => 'Ergonomics/General Safety (ACMSS approved through 2017)',
				'21' => 'TB Infection Control (ACMSS approved through 2017)',
				'22' => 'HIPAA Privacy Rule (ACMSS approved through 2017)',
				'23' => 'HIPAA Security Rule (ACMSS approved through 2017)',
				'24' => 'Privacy Breach and Identity Verification (ACMSS approved through 2017)',
				'25' => 'Emergency Preparedness (ACMSS approved through 2017)',
				'26' => 'Workplace Violence and Harassment (ACMSS approved through 2017)',
				'27' => 'Influenza Safety/Handwashing (ACMSS approved through 2017)',
				'28' => 'Hazard Communication (ACMSS approved through 2017)',
				'29' => 'OIG Fraud and Abuse Prevention (ACMSS approved through 2017)',
				'30' => 'AAO MOPS / HIPAA (ACMSS approved)',
				'31' => 'ACMSS CSE',
				'32' => 'ACMSS Approved General Education'
			);

			asort( $a );

			return $a;
		}

		public function set_org_name( $org_name, $user_id = null ) {
			if( is_null($user_id) ) $user_id = get_current_user_id();
			
			$org_name = trim($org_name);

			if( !empty($org_name) ) {
				if( update_user_meta( $user_id, '_org_name', $org_name ) ) {
					$this->add_to_corporate_group( $user_id, $org_name );

					return $org_name;
				}
			}
		}


		public function get_org_name( $user_id = null ) {
			if( is_null($user_id) ) $user_id = get_current_user_id();
			error_log('acmss :: 356 :: user_id: ' . $user_id);
			$org_name = get_user_meta( $user_id, '_org_name', true );
			error_log('acmss :: 358 :: org_name: ' . $org_name);
			if( !$org_name ) {	// legacy support
				$org_name = get_user_meta( $user_id, '_cap_org_name', true );
				$this->set_org_name( $org_name, $user_id );
			}

			return trim($org_name);		
		}

		public function add_org_user( $first_name, $last_name, $email ) {
			if( !empty($first_name) && !empty($last_name) && !empty($email) ) {
				if( is_email($email) ) {
					$password = wp_generate_password( 12, true );

					$user_login = $sanitized_user_login = sanitize_user( strtolower( $first_name . substr( $last_name, 0, 1 ) ) );

					while( username_exists($sanitized_user_login) ) {
						$sanitized_user_login = $user_login .'-'. (string) rand( 1, 999 );
					}

					$display_name = "{$first_name} {$last_name}";

					$role = isset($_REQUEST['member_role']) ? $_REQUEST['member_role'] : ( current_user_can('certified_academic_partner') ? 'student_member' : 'acmss_member' );

					$member_id = wp_insert_user( array (
						'user_pass' 		=> $password,
						'user_login' 		=> $sanitized_user_login,
						'user_nicename' 	=> $display_name,
						'user_email' 		=> $email,
						'display_name' 		=> $display_name,
						'first_name' 		=> $first_name,
						'last_name' 		=> $last_name,
						'user_registered'	=> date( 'Y-m-d H:i', time() ),
						'role' 				=> $role
					));

					if( is_wp_error($member_id) ) {
						echo '<div class="messageBox error icon"><span>An error has occurred: '.$member_id->get_error_message().'</span></div>';
					
					} else {
						global $wpdb, $current_user;

						$org_name = $this->get_org_name();

						//wp_new_user_notification( $member_id, $password );

						update_user_meta( $member_id, '_org_account', get_current_user_id() );

						if( $role == 'student_member' ) {
							// Set as academic partner student
							Groups_User_Group::create( array( 'user_id' => $member_id, 'group_id' => 6 ) );
						}

						$this->add_to_corporate_group( $member_id, $org_name );

						//$member_count = $this->adjust_owned_memberships_count( get_current_user_id(), -1 );

						//$welcome_email = ($role == 'student_member') ? 'students-welcome' : 'welcome-to-the-acmss';
						$welcome_email = 'acmss-membership-created';

						$args = array( 'password' => $password, 'login_url' => site_url( 'my-account' ), 'assignor_name' => $current_user->user_email );

						ACMSS()->emails()->send_email( $welcome_email, $member_id, $args );

						
						error_log('acmss.php :: 423 :: member_id: ' . $member_id);
						// add manual access
						ACMSS_Core::add_user_cap($member_id, 'read_mscat_manual' );
						// add mscat / hipaa access
							// remove mscat / hipaa from ??
						
						// add mscat / hipaa x3 to member_id
						ACMSS_Entitlements::add_entitlement('quiz', 1758, 3, $member_id);
						ACMSS_Entitlements::add_entitlement('quiz', 1568, 3, $member_id);
						
						// calculate dates
						$start_date 		= date("Y-m-d H:i:s",time());
						$next_payment_date 	= date("Y-m-d H:i:s",strtotime('+1 year',time()));
						
						// add membership
						// rewritten for WC 3.3.4 May 2018 kls
						
						$subscriptionArgs = array(
							'status' => 'active', 
							'billing_period' => 'year', 
							'billing_interval' => 1, 
							'start_date' => $start_date, 
							'customer_id' => $member_id, 
							'customer_note' => 'Record added through Add New Member on MMA'
						);
						
						$sub = wcs_create_subscription($subscriptionArgs);
						
						$product = wc_get_product(11500);
						
						$args = array(
							'attribute_billng-period' => 'Yearly'
						);
						
						$sub->add_product($product,1,$args);
						
						$sub->calculate_totals();
						$subscriptionID = $sub->id;
						update_post_meta( $subscriptionID, '_schedule_next_payment', $next_payment_date );
						
						return $member_id;
					}
				} else {
					echo '<div class="messageBox error icon"><span>Please enter a real email address.</span></div>';
				}
			}		
		}

		public function add_to_corporate_group( $user_id, $corp_name ) {
			if( class_exists('Groups_Group') ) {
				$group = Groups_Group::read_by_name( $corp_name );

				if( !$group ) {
					$name = $corp_name;
					$creator_id = '3';
					$datetime	= date( 'Y-m-d H:i:s', time() );
					$parent_id = '16';

					if( $group_id = Groups_Group::create( compact( 'creator_id', 'datetime', 'name', 'parent_id' ) ) ) {
						$group = Groups_Group::read( $group_id );
					}
				}
				
				// error_log('acmss.php :: 444 :: group: ' . print_r($group,true));
				
				if( $group ) {
					Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => $group->group_id ) );
					// add subscription 
					
					
				}
			}
		}		
		// $userID is passed from template-assignments2.php 
		// returns HTML snippet for display - formatted as a table
		public function get_subscriptions( $userID ) {
			error_log('get subscriptions :: userID: ' . $userID);
			$adminSubscriptions = wcs_get_users_subscriptions($userID);
			error_log('adminSubscriptions: '. print_r($adminSubscriptions,true));
			$subscriptionsData = '<form method="get"><table><th>ID</th><th>Description</th><th>Renewal</th><th colspan=2>Qty</th><th>Price</th><th>Total</th>';
			// require_once( '../../themes/acmss-2015/dataRequest/lib/woocommerce-api.php' );
			require_once( '/nas/content/live/acmss/wp-content/themes/acmss-2015/dataRequest/lib/woocommerce-api.php' );
			$options = array(
				'debug'           => true,
				'return_as_array' => false,
				'validate_url'    => false,
				'timeout'         => 30,
				'ssl_verify'      => false,
			);
			$client = new WC_API_Client( 'https://theacmss.org', 'ck_d9374ac6271db98513069d1d054f12344287519a', 'cs_5a594c40ef64271c0dae5401cc13c412f9393a7c', $options );
			// error_log('acmss.php :: 560 :: subscription->get(20170): ' . print_r( $client->subscriptions->get( 20170 ), true ));
			foreach ( $adminSubscriptions as $subscriptionId => $subscription ) {
				error_log('subscriptionID: ' . $subscriptionId . ' subscriptionArray: ' . print_r($subscription,true));
				
				if($subscriptionId) {
					$subscriptionObject = $client->subscriptions->get( $subscriptionId );
				
				
					error_log('acmss.php :: 565 :: subscription->get: ' . print_r($subscriptionObject, true ));
					// error_log('acmss.php :: 566 :: quantity: ' . $subscriptionObject->subscription->line_items[0]->quantity . ' total: ' . $subscriptionObject->subscription->line_items[0]->total . ' name: ' . $subscriptionObject->subscription->line_items[0]->name . ' variation: ' . $subscriptionObject->subscription->line_items[0]->meta[0]->value);
				
					$subscriptionQuantity = $subscriptionObject->subscription->line_items[0]->quantity;
					$subscriptionPrice = $subscriptionObject->subscription->line_items[0]->price;
					$subscriptionLineTotal = $subscriptionObject->subscription->line_items[0]->total;
					$subscriptionName = $subscriptionObject->subscription->line_items[0]->name;
					$subscriptionVariation = $subscriptionObject->subscription->line_items[0]->meta[0]->value;
				
					$subscriptionRenewalDateString = $subscriptionObject->subscription->billing_schedule->next_payment_at;
					$subscriptionRenewalDateArray = explode('-',$subscriptionRenewalDateString);
					$subscriptionRenewalDateYear 	= $subscriptionRenewalDateArray[0];
					$subscriptionRenewalDateMonth 	= $subscriptionRenewalDateArray[1];
					$subscriptionRenewalDateDate 	= substr($subscriptionRenewalDateArray[2],0,2);
					$subscriptionRenewalDate = $subscriptionRenewalDateMonth . '/' . $subscriptionRenewalDateDate . '/' . $subscriptionRenewalDateYear;
				
					$subscriptionQuantityAdjustment = "<input type='hidden' name=" . $subscriptionId . "-oldQuantity value=" . $subscriptionQuantity . "><input style='width: 55px;' type='number' name=" . $subscriptionId . "-newQuantity value='". $subscriptionQuantity . "' min='0' max='99'>";
				
					error_log('acmss.php :: 572 :: quantity: ' . $subscriptionQuantity . ' total: ' . $subscriptionLineTotal . ' name: ' . $subscriptionName . ' variation: ' . $subscriptionVariation . ' Renewal Date: ' . $subscriptionRenewalDate);
				
					$subscriptionsData .= '<tr><td>' . $subscriptionId . '</td><td>' . $subscriptionName . ' ' . $subscriptionVariation . '</td><td>' . $subscriptionRenewalDate . '</td><td>' . $subscriptionQuantity .  '</td><td>' . $subscriptionQuantityAdjustment . '</td><td>' . $subscriptionPrice . '</td><td>' . $subscriptionLineTotal . '</td></tr>';
				
				}
			}
			
			$subscriptionsData .= '</table>';
			$subscriptionsData .= '<BR>';
			$subscriptionsData .= '<input type="submit">';
			$subscriptionsData .= '</form>';
			return $subscriptionsData;
		}
		
		public function subscriptionsToUpdate( $subscriptionsToUpdate ) {
			error_log('acmss.php :: 597 :: subscriptionsToUpdate: ' . print_r($subscriptionsToUpdate,true));
			require_once( '/nas/content/live/acmss/wp-content/themes/acmss-2015/dataRequest/lib/woocommerce-api.php' );
			$options = array(
				'debug'           => true,
				'return_as_array' => false,
				'validate_url'    => false,
				'timeout'         => 30,
				'ssl_verify'      => false,
			);
			$client = new WC_API_Client( 'https://theacmss.org', 'ck_d9374ac6271db98513069d1d054f12344287519a', 'cs_5a594c40ef64271c0dae5401cc13c412f9393a7c', $options );
			
			for($pointer = 0; $pointer < count($subscriptionsToUpdate); $pointer += 2) {
				$currentSubscriptionID = $subscriptionsToUpdate[$pointer];
				$currentSubscriptionQuantity = $subscriptionsToUpdate[$pointer + 1];
				
				if($currentSubscriptionQuantity == 0) {
					error_log('acmss.php :: 613 LL subscription quantity is zero!');
				}
				
				$returnedSubscriptionObject = $client->subscriptions->get( $currentSubscriptionID );
				$subscriptionObjectID = $returnedSubscriptionObject->subscription->line_items[0]->id;
				error_log('acmss.php :: 614 :: subscriptionObjectID: ' . $subscriptionObjectID);
				
				$data = array(
					"subscription" => array(
						"line_items" => array(
							array(
								"id"			=> $subscriptionObjectID,
								"product_id"	=> 1743,
								"quantity" 		=> $currentSubscriptionQuantity
							)
						)
					)
				);
				
				try {
					$client->subscriptions->update( $currentSubscriptionID, $data );
				}
				catch(Exception $e) {
					echo "<div style='margin: 25px; border: 1px solid black; padding: 20px; font-size: 24px; font-weight: bold; line-height: 1.2em; color: red; border-radius: 7px;'>Message: " . $e->getMessage() . "</div>";
				}	

			}
			/*
			$returnedSubscriptionObject = $client->subscriptions->get( 29968 );
			$subscriptionObjectID = $returnedSubscriptionObject->subscription->line_items[0]->id;
			error_log('acmss.php :: 610 :: subscriptionObjectID: ' . $subscriptionObjectID);
			
			$subscription_id = 29968;
			
			$data = array(
				"subscription" => array(
					"line_items" => array(
						array(
							"id"			=> $subscriptionObjectID,
							"product_id"	=> 1743,
							"quantity" 		=> 15
						)
					)
				)
			);
			error_log('acmss.php :: 624 :: dataArray: ' . print_r($data,true));
			$returnedData2 = $client->subscriptions->update( $subscription_id, $data );
			error_log('acmss.php :: 626 :: returnedData2: ' . print_r($returnedData2,true ) );
			*/
		}
		
		public function get_member_list_expired( $user_id, $inc_self = true ) {
			global $wpdb;
			$org_name = $this->get_org_name( $user_id );
			if( !empty($org_name) ) {
				$limitStatement = '';
				$user_ids = $wpdb->get_col( "SELECT DISTINCT g1.user_id FROM wp_groups_user_group g1 WHERE g1.group_id IN (SELECT group_id FROM wp_groups_group WHERE name = '$org_name') $limitStatement;");
				$fName = get_user_meta($user_id, 'first_name', true);
				$lName = get_user_meta($user_id, 'last_name', true);
				
				
				$html = '<BR><span style="font-weight: bold; font-size: 24px;">Welcome back ' . $fName . ',</span>'; // . ' ' . $lName;
				$html .= '<BR><BR>';
				// $html .= 'The following members of your practice are expired or coming up for renewal:<BR><BR>';
				
				if( count($user_ids) > 0 ) {
					$args = array(
						'blog_id'		=> $GLOBALS['blog_id'],
						'include'		=> $user_ids,
						'orderby'		=> 'login',
						'order'			=> 'ASC',
						'fields'		=> array( 'ID', 'display_name' )
					);
					$members = get_users( $args );
					
					// $html1 .= '<BR><BR>';
					$html1 .= 'The following members of your practice are expired or coming up for renewal:<BR><BR>';
					
					foreach ($members as $member) {
						$memberSubscriptions = wcs_get_users_subscriptions($member->ID);
						$memberCurrentSubscription = array_shift($memberSubscriptions);
						$subscriptionID = $memberCurrentSubscription->id;
						$subscriptionRenewalDateString = get_post_meta($subscriptionID,'_schedule_next_payment',true);
						$subscriptionRenewalDateAsUnixTime = strtotime($subscriptionRenewalDateString);
						$currentUnixTime = time();
						if($subscriptionRenewalDateAsUnixTime < $currentUnixTime) {
							$html2 .= $member->display_name;
							$html2 .= ' is expired.<BR>';
						}
						elseif($subscriptionRenewalDateAsUnixTime < $currentUnixTime + (24 * 60 * 60 * 30) ) {
							$html2 .= $member->display_name;
							$html2 .= ' is coming up for renewal in the next 30 days.<BR>';
						}
						else {
							// $html2 .= 'All members are Active.<BR>';
						}
						if( ($memberCounter > 15) && (strlen($html2) > 10) ) {
							$html2 .= '<BR><BR> And many more.';
							break;
						}
						$memberCounter ++;
					}
					
					if(strlen($html2) < 1) {
						$html .= 'All members are Active.';
					}
					else {
						$html .= $html1 . $html2;
					}

				}			
				else {
					$html .= 'All members are Active.';
				}
			}
			return $html;
		}
		
		public function get_member_list_plain( $user_id, $inc_self = true ) {
			global $wpdb;
			error_log('acmss :: 453 :: user_id: ' . $user_id);
			$org_name = $this->get_org_name( $user_id );
			if( !empty($org_name) ) {
				$user_ids = $wpdb->get_col( "SELECT DISTINCT g1.user_id FROM wp_groups_user_group g1 WHERE g1.group_id IN (SELECT group_id FROM wp_groups_group WHERE name = '$org_name');");
				$args = array(
					'blog_id'		=> $GLOBALS['blog_id'],
					'include'		=> $user_ids,
					'orderby'		=> 'login',
					'order'			=> 'ASC',
					'fields'		=> array( 'ID','display_name')
				);
				$members = get_users( $args );
				// print_r($members);
				return $members;
			}
		}

		public function get_member_list( $user_id, $inc_self = false, $offset = 0, $recordsToRetrieve ) {
			global $wpdb;
			error_log('acmss :: 472 :: user_id: ' . $user_id . ' offset: ' . $offset);
			$org_name = $this->get_org_name( $user_id );
			error_log('acmss :: 474 :: org_name: ' . $org_name);
			
			$limitStatement = '';
			if($user_id == 5627) {
				// $recordsToRetrieve = $recordsToRetrieve ?: 500;
				$limitStatement = 'LIMIT ' . $offset . ', ' . $recordsToRetrieve;
				error_log('acmss :: 617 :: limit: ' . $limitStatement);
			}
			
			$limitStatement = 'LIMIT ' . $offset . ', ' . $recordsToRetrieve;
			error_log('acmss :: 621 :: limit: ' . $limitStatement);
			
			if( !empty($org_name) ) {
				$user_ids = $wpdb->get_col( "SELECT DISTINCT g1.user_id FROM wp_groups_user_group g1 WHERE g1.group_id IN (SELECT group_id FROM wp_groups_group WHERE name = '$org_name') $limitStatement;");
				error_log('acmss.php :: 477 :: inc_self: ' . $inc_self . ' user_ids: ' . print_r($user_ids,true));
				
				$fName = get_user_meta($user_id, 'first_name', true);
				$lName = get_user_meta($user_id, 'last_name', true);
				error_log('acmss.php :: 481 :: fName: ' . $fName . ' lName: ' . $lName);
				
				$subscriptions = wcs_get_users_subscriptions($user_id);
				$subscription = array_shift($subscriptions);
				error_log('acmss.php :: 485 :: subscription array: ' . print_r($subscription,true));
				$subscriptionID = $subscription->id;
				error_log('acmss.php :: 487 :: subscriptionID: ' . $subscriptionID);
				$subscriptionRenewalString = get_post_meta($subscriptionID,'_requires_manual_renewal',true);
				$subscriptionRenewalBool = $subscriptionRenewalString == 'false' ? false : true;
				error_log('acmss.php :: 490 :: subscriptionRenewalBool: ' . $subscriptionRenewalBool);
				$subscriptionRenewalMethod = $subscriptionRenewalBool == false ? 'Automatic Renewal' : 'Manual Renewal' ;
				
				$html = !$inc_self ? '' : sprintf ( '<li data-member-id="%d" class="self" style="width: 90%%; font-weight: bold;">Assign to yourself<br/>(Manual & Membership only)<BR>Practice Administrator: <span style="color: black;">%s %s -- %s</span> <a onclick="openWindow2(%d);" class="dashicons dashicons-edit"></a></li>', $user_id, $fName, $lName, $subscriptionRenewalMethod, $user_id );

				if( count($user_ids) > 0 ) {
					$args = array(
						'blog_id'		=> $GLOBALS['blog_id'],
						'include'		=> $user_ids,
						'orderby'		=> 'login',
						'order'			=> 'ASC',
						'fields'		=> array( 'ID', 'display_name' )
					);

					$members = get_users( $args );
					/*
					$subscriptions = wcs_get_users_subscriptions($user_id);
					error_log('acmss.php :: 478 :: subscriptions array: ' . print_r($subscriptions,true));
					
					$subscription = array_shift($subscriptions);
					$subscriptionRenewalMethod = $subscription->is_manual();
					error_log('acmss.php :: 482 :: subscription renewal method: ' . $subscriptionRenewalMethod);
					*/
					error_log('acmss.php :: 516 :: members array: ' . print_r($members,true));

					if( count($members) > 0 ) {
						// usort( $members, create_function( '$a, $b', 'return strnatcasecmp($a->first_name, $b->first_name);' ) );
						usort( $members, create_function( '$a, $b', 'return strnatcasecmp($a->display_name, $b->display_name);' ) );

						foreach ($members as $member) {
							$assigns = '';
							
							$assigns .=  sprintf( '<span class="assign orange">%d</span>',
								ACMSS_Entitlements::user_has_entitlement( 'quiz', MSCAT_QUIZ_ID, $member->ID )
								);
							$assigns .=  sprintf( '<span class="assign blue">%d</span>', 
								ACMSS_Entitlements::user_has_entitlement( 'quiz', HIPAA_QUIZ_ID, $member->ID )
								);
							$assigns .=  sprintf( '<span class="assign yellow">%s</span>', user_can( $member->ID, READ_MSCAT_MANUAL ) ? 'Y' : 'N' );
							
							
							
							$currentUserID = get_current_user_id();
							error_log('86 :: template assignments currentUserID: ' . $currentUserID);

							// if( ($currentUserID == 5627) || ($currentUserID == 5813) ) {
								$assigns .= sprintf( '<span class="assign cseOphthma1Color ">%d</span>', 
								ACMSS_Entitlements::user_has_entitlement( 'quiz', 11836, $member->ID )
								);
								
								$assigns .= sprintf( '<span class="assign cseOphthma2Color ">%d</span>', 
								ACMSS_Entitlements::user_has_entitlement( 'quiz', 11837, $member->ID )
								);
								
								$assigns .= sprintf( '<span class="assign csereportinHIPAAColor ">%d</span>', 
								ACMSS_Entitlements::user_has_entitlement( 'quiz', 11839, $member->ID )
								);
								
								$assigns .= sprintf( '<span class="assign cseEMColor ">%d</span>', 
								ACMSS_Entitlements::user_has_entitlement( 'quiz', 12213, $member->ID )
								);

								
							// }	
							// $subscriptionRenewalBool = false;
							$subscriptions = wcs_get_users_subscriptions($member->ID);
							error_log('acmss.php :: 606 :: subscriptionsArray: ' . print_r($subscriptions,true));
							$subscription = array_shift($subscriptions);
							error_log('acmss.php :: 607 :: subscription: ' . print_r($subscription,true));
							$subscriptionID = $subscription->id;
							// $membershipStatus = $subscription->get_status();
							error_log('acmss.php :: 609 :: membershipStatus: ' . $membershipStatus);
							$subscriptionRenewalString = get_post_meta($subscriptionID,'_requires_manual_renewal',true);
							$subscriptionRenewalDate   = get_post_meta($subscriptionID,'_schedule_next_payment',true);
							$subscriptionRenewalDate = array_shift(explode(' ', $subscriptionRenewalDate));	
							
							$subscriptionRenewalDateAsUnixTime = strtotime($subscriptionRenewalDate);
							error_log('acmss.php :: 664 :: subscriptionRenewalDateAsUnixTime: ' . $subscriptionRenewalDateAsUnixTime);
							$currentUnixTime = time();
							
							$subscriptionRenewalDate = strlen($subscriptionRenewalDate) > 5 ? $subscriptionRenewalDate : 'No Date Set' ;
							
							
							
							if($subscriptionRenewalDate == 'No Date Set'){
								$subscriptionRenewalDate = '<span style="color: red;">' . $subscriptionRenewalDate . '</span>';
							}
							
							elseif($subscriptionRenewalDateAsUnixTime < $currentUnixTime) {
								$subscriptionRenewalDate = '<span style="color: red;">' . $subscriptionRenewalDate . '</span>';
							}
							
							elseif($subscriptionRenewalDateAsUnixTime < $currentUnixTime + (24 * 60 * 60 * 30)) {
								$subscriptionRenewalDate = '<span style="color: orange;">' . $subscriptionRenewalDate . '</span>';
							}
							/*
							else{}
							*/
										
							error_log('acmss.php :: 561 :: subscriptionRenewalDate: ' . $subscriptionRenewalDate);
							$subscriptionRenewalBool = $subscriptionRenewalString == 'false' ? false : true ;
							error_log('acmss.php :: 563 :: subscriptionRenewalBool: ' . $subscriptionRenewalBool . ' userID: ' . $member->ID . ' subscriptionID: ' . $subscriptionID);
							
							$subscriber = $subscriptionID ? true : false ;
							// error_log('acmss.php :: 566 :: subscriber: ' , $subscriber);
							
							if($subscriber == false) {
								$subscriptionRenewalMethod = '<div style="display: inline-block; width: 20px; height: 20px; border-radius: 10px; background-color: black; color: white; padding-left: 5px;">N</div>';
							}
							elseif( (isset($subscriptionRenewalBool)) && ($subscriptionRenewalBool == false) ) {
								$subscriptionRenewalMethod = '<div style="display: inline-block; width: 20px; height: 20px; border-radius: 10px; background-color: green; color: white; padding-left: 5px;">A</div>';
							}
							else {
								$subscriptionRenewalMethod = '<div style="display: inline-block; width: 20px; height: 20px; border-radius: 10px; background-color: red; color: white; padding-left: 5px;">M</div>';
							}

							
							// $subscriptionRenewalMethod = !$subscriptionRenewalBool ? '<div style="display: inline-block; width: 20px; height: 20px; border-radius: 10px; background-color: green; color: white; padding-left: 5px;">A</div>' : '<div style="display: inline-block; width: 20px; height: 20px; border-radius: 10px; background-color: red; color: white; padding-left: 5px;">M</div>' ;

							$html .= sprintf ( '<li data-member-id="%d"> Renewal Date: %s %s <span style="font-weight: bold; color: black;">%s</span> %s</li>', $member->ID, $subscriptionRenewalDate,  $subscriptionRenewalMethod,  $member->display_name, $assigns );
							// $html .= sprintf ( '<li data-member-id="%d"><a onclick="window.open(document.URL,\'_blank\',location=yes,height= 500,width=500,scrollbars=yes,status=yes);">%s</a> %s</li>', $member->ID, $member->display_name, $assigns );
						}
					}
				}

				return empty($html) ? '' : "<ul id=\"member-list\" data-d=\"4\">$html</ul>";
			}
		}
/*
		public function add_bundles( $user_id, $variation_id, $qty, $user_type = '_memberships' ) {
			if( !in_array( $variation_id, array( 2292, 2304, 2305, 2306, 2307, 2308 ) ) ) {
				$this->adjust_owned_memberships_count( $user_id, $qty, $user_type );
				$this->add_user_cap( $user_id, BOUGHT_BUNDLES );			
			}

			// non-members only get x1 of each
			$factor = in_array( $variation_id, array( 2292 ) ) ? 1 : 3;

			// MSCAT assessment
			if( in_array( $variation_id, array( 1946, 2293, 1948, 2292, 2296, 2304, 2307, 2308, 2309, 2312, 2313 ) ) ) {
				$this->add_assessment_access( $user_id, 'MSCAT', $factor * $qty );
				$this->add_user_cap( $user_id, BOUGHT_BUNDLES );
			}

			// HIPAA Assessment
			if( in_array( $variation_id, array( 1948, 1948, 2292, 2294, 2296, 2305, 2307, 2308, 2310, 2312, 2313 ) ) ) {
				$this->add_assessment_access( $user_id, 'HIPAA', $factor * $qty );
				$this->add_user_cap( $user_id, BOUGHT_BUNDLES );
			}

			// MSCAT Manual
			if( in_array( $variation_id, array( 1948, 2292, 2295, 2296, 2306, 2308, 2311, 2313 ) ) ) {
				$count = intval( $qty ) + (int) get_user_meta( $user_id, '_mscat_manual_access', true );
				update_user_meta( $user_id, '_mscat_manual_access', $count );
				$this->add_user_cap( $user_id, BOUGHT_BUNDLES );
			}
		}
*/
		public function adjust_owned_memberships_count( $user_id, $adjustment = 0, $member_type = '_memberships' ) {
			$owned_memberships = (int) get_user_meta( $user_id, $member_type, true );

			//if( user_can( $user_id, 'administrator' ) && $owned_memberships < 1 && $adjustment < 1  ) {
			//	$owned_memberships = 7;
			//	$adjustment = 0;
			//}

			if( $adjustment != 0 )  {
				$owned_memberships += intval( $adjustment );

				update_user_meta( $user_id, $member_type, $owned_memberships );	
			}

			return $owned_memberships;
		}
		
		public function adjust_membershipRenewal_count( $user_id, $adjustment = 0 ) {
			// error_log('acmss.php :: 702 :: user_id: ' . $user_id . ' adjustment: ' . $adjustment);
			// error_log('acmss.php :: 703 :: can access renewal: ' . ACMSS_Entitlements::user_can_access_quiz( 11959, $user_id ));
		}
/*
		public function add_assessment_access( $user_id, $assessment, $count = 1 ) {
			$assessments = (array) get_user_meta( $user_id, '_assessment_access', true );

			if( isset( $assessments[$assessment] ) )  {
				$count += intval( $assessments[$assessment] );
			}

			$assessments[$assessment] = $count;

			update_user_meta( $user_id, '_assessment_access', $assessments );

			$this->add_user_cap( $user_id, PURCHASED_QUIZZES );
		}
*/
		public function get_assessment_access( $user_id, $assessment, $assessments = null ) {
			if( !$assessments ) {
				$assessments = (array) get_user_meta( $user_id, '_assessment_access', true );
			}
			return isset( $assessments[$assessment] ) ? intval( $assessments[$assessment] ) : 0;
		}

		public function add_manual_access_count( $user_id, $count = 1 ) {
			$current = $this->get_manual_access_count( $user_id );

			update_user_meta( $user_id, '_mscat_manual_access', $current + $count );
		}

		public function get_manual_access_count( $user_id ) {
			$count = get_user_meta( $user_id, '_mscat_manual_access', true );

			return isset( $count )  ?  intval( $count ) : 0;
		}

		public function distributed_products_owned( $user_id ) {
			if( $this->get_assessment_access( $user_id, 'HIPAA') > 3 ) return true;
			if( $this->get_assessment_access( $user_id, 'MSCAT') > 3 ) return true;
			if( $this->get_manual_access_count( $user_id ) > 0 ) return true;

			return false;
		}

		public function analytics() {
			echo <<< EOT
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-403733-26', 'theacmss.org');
	  ga('send', 'pageview');
	</script>
EOT;
		}

		public function quiz_completed( $quiz_data ) {
			$user_id = isset($quiz_data['user_id']) ? $quiz_data['user_id'] : get_current_user_id();

			switch( (int) $quiz_data['quiz']->ID ) {
				case 4954:	// Primary Care
				case 4955:	// Internal Medicine
				case 2546:	// Urgent Care
				case 2547:	// Vascular
				case 2548:	// Dermatology
				case 2336: 	// Ophthalmology
				case 1758: 	// Emergency	
				case 10651:	// Oncology					
					if( $quiz_data['pass'] == 1 ) {
						$this->certify_user( true, null, $user_id );
					}

					// UPDATE TO ENTITLEMENTS
					//$this->add_assessment_access( $user_id, 'MSCAT', -1 );
					break;
				
				case 1568:
					if( $quiz_data['pass'] == 1 ) {
						Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 18 ) );
					}

					// UPDATE TO ENTITLEMENTS
					//$this->add_assessment_access( $user_id, 'HIPAA', -1 );
					break;
			}
		}

		public function certify_user( $mscat = null, $affidavit = null, $user_id = null ) {
			$user_id = !is_null( $user_id ) ? $user_id : get_current_user_id();

			if( $mscat === true ) {
				update_user_meta( $user_id, '_completed_mscat_test', true );
			}

			if( $affidavit === true || $affidavit === 'CMSS' ) {
				update_user_meta( $user_id, '_signed_mscat_affidavit', true );

				if( $affidavit === 'CMSS' ) {
					update_user_meta( $user_id, '_signed_cmss_affidavit', true );
				} else {
					delete_user_meta( $user_id, '_signed_cmss_affidavit', true );
				}
			}

			$t = get_user_meta( $user_id, '_completed_mscat_test', true );
			$a = get_user_meta( $user_id, '_signed_mscat_affidavit', true );
			$c = get_user_meta( $user_id, '_signed_cmss_affidavit', true );

			if( !empty($a) && !empty($t) ) {
				if( user_can( $user_id, 'acmss_member' ) ) {
					if( !empty($c) ) {
						$this->add_user_cap( $user_id, 'certified_member' );
						$this->remove_user_cap( $user_id, 'apprenticed_member' );
						Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 2 ) );
						Groups_User_Group::delete( array( 'user_id' => $user_id, 'group_id' => 17 ) );
					} else {
						$this->remove_user_cap( $user_id, 'certified_member' );
						$this->add_user_cap( $user_id, 'apprenticed_member' );
						Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 17 ) );
						Groups_User_Group::delete( array( 'user_id' => $user_id, 'group_id' => 2 ) );
					}

					$this->emails()->send_email( 'cmss-congratulations', $user_id );
				} else {
					if( !empty($c) ) {
						$user = new WP_User( $user_id );
						$user->set_role('certified_non_member');
					} else {
						$this->add_user_cap( $user_id, 'apprenticed_non_member' );
					}
				}
			}
		}

		function init_mscat() {
			$args = array(
				'labels' =>  array(
						'name'               => 'MSCAT Page',
						'singular_name'      => 'Slide',
						'add_new'            => 'Add New Page',
						'add_new_item'       => 'Add New Page',
						'edit_item'          => 'Edit Page',
						'new_item'           => 'New Page',
						'all_items'          => 'All Pages',
						'view_item'          => 'View Page',
						'search_items'       => 'Search Pages',
						'not_found'          => 'No Pages found',
						'not_found_in_trash' => 'No Pages found in Trash',
						'parent_item_colon'  => '',
						'menu_name'          => 'MSCAT Pages'
					),
				'public'			=> true,
				'publicly_queryable'=> true,
				'show_ui'			=> true,
				'show_in_menu'		=> true,
				'menu_position'		=> 20,
				'query_var'			=> true,
				'rewrite'			=> array('slug' => 'mscat-manual', 'with_front' => false ),
				'has_archive'		=> false,
				'hierarchical'		=> true,
				'supports' 			=> array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes' )
			);
			register_post_type( 'mscat-page', $args );

			$args = array (
						'label' 			=> 'events',
						'labels' 			=> array (
							'name'			=> 'Events',
							'singular_name' => 'Event',
							'all_items' 	=> 'All Events',
							'add_new_item' 	=> 'Add New Event',
							'edit_item' 	=> 'Edit Event',
							'new_item' 		=> 'New Event',
							'view_item' 	=> 'View Event',
							'search_items'	=> 'Search Events',
							'not_found' 	=> 'No events found',
							'not_found_in_trash' => 'No events found in Trash',
							'menu_name' 	=> 'Events'
						),
						'rewrite' 		=> array( 'slug' => 'events', 'with_front' => false ),
						'has_archive' 		=> true,
						'public' 		=> true,
						'show_ui' 		=> true,
						'show_in_menu' 		=> true,
						'supports' 		=> array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail' )
					);
			register_post_type( 'events', $args );

			// Add the manual reading ability to appropriate roles
			foreach ( array( 'administrator', 'editor' ) as $rolename ) {
				if( $role = get_role( $rolename ) ) {
					foreach( array( READ_MSCAT_MANUAL, PURCHASED_QUIZZES, 'acmss_member', 'read', 'read_mscat_manual', 'read_private_mscat_manuals','edit_mscat_manuals','edit_private_mscat_manuals','edit_published_mscat_manuals','edit_others_mscat_manuals','publish_mscat_manuals','delete_mscat_manuals','delete_private_mscat_manuals','delete_published_mscat_manuals', 'delete_others_mscat_manuals' ) as $cap ) {
						$role->add_cap( $cap ); 
					}
				}
			}

			register_nav_menu( 'mscat-manual', 'Table of Contents for the MSCAT Manual' );
		}

		public function user_has_signed_mscat_affidavit( $user_id = null ) {
			$user_id = $user_id ? $user_id : get_current_user_id();

			$a = get_user_meta( $user_id, '_signed_mscat_affidavit', true );

			// If they have signed it, $a will NOT be empty
			return empty($a) ? false : true;
			
			
			// return true;  // added Nov 21,2016 making submission of MSCAT affidavit disabled
		}

		public function get_file_save_dir( $folder ) {
			$uploads = wp_upload_dir();

			$directory = $uploads['basedir'] . '/' . $folder;

			if( !file_exists($directory) ) {
				$r =  mkdir( $directory, 0777, true );
			}

			return $directory;
		}

		public function remove_user_cap( $user_id, $cap ) {
			// error_log('acmss == user_id: ' . $user_id . ' capability: ' . $cap);
			if( $user = new WP_User( $user_id ) ) {
				$user->remove_cap( $cap );
			}

			return get_class($user);
		}

		public function add_user_cap( $user_id, $cap ) {
			if( $user = new WP_User( $user_id ) ) {
				$user->add_cap( $cap );
			}

			return get_class($user);
		}

		public function has_user_cap( $user_id, $cap ) {
			if( $user = new WP_User( $user_id ) ) {
				return $user->has_cap( $cap );
			}

			return false;
		}

		public function is_assignor( $user_id = null ) {
			$user_id = $user_id ?: get_current_user_id();
			// error_log('acmss.php :: 1086 :: user_id: ' . $user_id);

			if( user_can( $user_id, 'certified_academic_partner_administrator') || user_can( $user_id, 'corporate_partner_administrator') || user_can( $user_id, 'administrator') ) {
				return true;
			} 

			return false;
		}

		public function is_corporate_partner( $user_id = null ) {
			$user_id = $user_id ?: get_current_user_id();

			if( 
				user_can( $user_id, 'corporate_practice')
				|| user_can( $user_id, 'corporate_partner')
				|| user_can( $user_id, 'corporate_partner_administrator')
				|| user_can( $user_id, 'administrator')
				) {
				return true;
			} 

			return false;
		}
		
		public function can_access_secure_docs( $user_id = null ) {
			$user_id = $user_id ? $user_id : get_current_user_id();
			
			if( user_can( $user_id, '') || user_can( $user_id, '') ) {
				return true;
			}
			return false;
		}
		
		public function membershipStatus($memberID) {
			error_log('acmss.php :: 978 :: membershipStatus :: memberID: ' . $memberID );
			$nextPaymentDatesArray = array();
			$subscriptions = wcs_get_users_subscriptions($memberID);
			
			foreach($subscriptions as $subscriptionID => $subscription) {
				$nextPaymentDate = strtotime($subscription->get_date_to_display( 'next_payment' ));
				$subscriptionStatus = $subscription->get_status();
				// error_log('acmss.php :: 984 :: get_status: ' . $subscriptionStatus);
				$nextPaymentDatesArray[$nextPaymentDate] = $subscriptionStatus;
	
			}
			// error_log('acmss.php :: 989 :: nextPaymentDatesArray: ' . print_r($nextPaymentDatesArray,true));
			ksort($nextPaymentDatesArray);
			// error_log('acmss.php :: 991 :: nextPaymentDatesArray: ' . print_r($nextPaymentDatesArray,true));
			$keyValue = '';
			$membershipStatus= '';
			foreach($nextPaymentDatesArray as $key => $value ){
				$keyValue = $key;
				$membershipStatus = $value;
				// error_log('my-account :: 39 :: key: ' . $keyValue . ' value: ' . $membershipStatus); 
			}

			// error_log('acmss.php :: 1000 :: keyValue: ' . $keyValue . ' status: ' . $membershipStatus);
			
			return $membershipStatus;
		}
		

		public function custom_nav_menu_objects( $items, $args = null ){
			if( !is_admin() ) { 
				// error_log('acmss.php :: 1278 :: $items: ' . print_r($items,true));
				// Only add item to the main navigation menu
				if ( $args->theme_location == 'primary' ) {
					$menu_order = 0;
					$my_account_idx = 0;
					$my_account_id = 0;				

					foreach ( $items as $key => $post ) {
						// error_log('acmss.php :: 1286 :: $key: ' . $key . ' -- $post: ' . $post);
						$menu_order++;

						if( strpos( $post->url, '/my-account' ) > 0 ) {
							$my_account_idx	= $key;
							
							$my_account_id = $post->ID;

							unset( $items[$my_account_idx + 1] );

							break;
						}
					}

					if( is_user_logged_in() ) {
						$user_id = get_current_user_id();
						$items[ $my_account_idx ]->classes[] = 'menu-item-has-children';
						$new_links = array();
						
						// capture login of lapsed member
						$userID = get_current_user_id();
						// error_log('acmss.php :: 1003 :: userID: ' . $userID . ' user_id: ' . $user_id);

						$lapsedMember = ( user_can( $userID, 'lapsed_member' ) ) ? 1 : 0 ;
						// error_log('acmss.php :: 860 lapsedMember: ' . $lapsedMember);
						
						// removed Oct 11, 2016 - 
						
						if( $lapsedMember == true ) {
							// error_log('acmss.php :: 858 Got Ya');
							// wp_logout();
							// wp_safe_redirect( site_url('store/membership-types') );
							
							// exit;
							// uncomment the 'return' below to not show nav menus
							// return;
						}
						
						

						$links = array( 
							'my-account'											=> 'My Account',
							'assessment-instructions'								=> 'MSCAT Instructions',
							'my-account/memberships-assignment-2'					=> 'Manage CMSS Assignments',
							'job-dashboard'											=> 'Career Dashboard',
							/* 'membership-and-certification-assignment'				=> 'Manage Member Assignments', temp page */
							'certified-medical-scribe-specialist-cmss-crosswalk' 	=> 'CMS Clinical Skills',
							'confidential-documents' 								=> 'National Confidential Blueprint',
							'mscat-manual/foreward'									=> 'Read CMSS Clinical Manual',
							/* 'mscat-manual-maintenance'								=> 'Read the MSCAT Manual', */
							'quizzes/hipaa-assessment'								=> 'Watch HIPAA Video',
							'certification-assessment'								=> 'Take CMSS Certification',
							'account-details/continuing-scribe-education'			=> 'Continuing Scribe Education'
						);
						
						// unset($links['job-dashboard']);

						if( !$this->is_corporate_partner() ) {
							unset($links['certified-medical-scribe-specialist-cmss-crosswalk']);
							unset($links['confidential-documents']);
						}
						
						error_log('acmss.php :: 1071 :: user_has_assignables: ' . ACMSS_Entitlements::user_has_assignables());

						if( !current_user_can('certified_academic_partner') && !current_user_can('corporate_partner') && !current_user_can('administrator') && !ACMSS_Entitlements::user_has_assignables() ) {

							unset($links['my-account/memberships-assignment-2']);
						}

						$r = ACMSS_Entitlements::user_has_entitlement( 'quiz', HIPAA_QUIZ_ID, $user_id );
						if( $r > 0 ) {
							$links['quizzes/hipaa-assessment'] .= " ($r)";
						} else {
							unset($links['quizzes/hipaa-assessment']);
						}

						$r = ACMSS_Entitlements::user_has_entitlement( 'quiz', MSCAT_QUIZ_ID, $user_id );
						if( $r > 0 ) {
							$links['certification-assessment'] .= " ($r)";
						} else {
							unset($links['certification-assessment']);
						}  

						if( !current_user_can('acmss_member') && !current_user_can('administrator') ) {
							unset($links['my-account/continuing-scribe-education']);
						}

						if( !current_user_can( READ_MSCAT_MANUAL ) ) {
							// unset( $links['mscat-manual/foreward'] );
						}
						
						$membershipStatus = $this->membershipStatus($user_id);
						error_log('acmss.php :: 1101 :: membershipStatus: ' . $membershipStatus . ' user_id: ' . $user_id);

						error_log('acmss.php :: 1067 :: lapsedMember: ' . $lapsedMember);
						if( ($lapsedMember == true) || ($membershipStatus != 'active') ) {
							error_log('acmss.php :: 1069 :: trying to unset menu items');
							// $this->unset_menu_item( $items, 'my-account' );
							//	$menuName = '10733';
							// $returnValue = wp_delete_nav_menu($menuName);
							// error_log('acmss.php :: 1085 :: returnValue: ' . $returnValue);
							unset($links['my-account/memberships-assignment-2']);
							unset($links['certified-medical-scribe-specialist-cmss-crosswalk']);
							unset($links['confidential-documents']);
							unset($links['mscat-manual/foreward']);
							unset($links['quizzes/hipaa-assessment']);
							unset($links['certification-assessment']);
							unset($links['account-details/continuing-scribe-education']);
						}
						
						// for displaying the special MSCAT preview page - set userID
						if( get_current_user_id() == 7729 ) {
							// unset($links['mscat-manual/foreward']);
							// unset($links['account-details/continuing-scribe-education']);
							// $links['mscat-preview'] = 'MSCAT Preview';
							// array_unshift($links, 'mscat-preview' => 'MSCAT Preview');
						}
	

						foreach( $links as $link => $caption ) {
							$new_links[] = $this->create_nav_menu_item( $caption, site_url($link), $menu_order, $my_account_id ); 
							$menu_order++;
						}
						array_splice( $items, $my_account_idx, 0, $new_links );

						//$this->unset_menu_item( $items, 'purchase-certifications-memberships' );

						if( $this->is_corporate_partner() ) {
//							$this->unset_menu_item( $items, 'add-individual-products' );
						} else {
//							$this->unset_menu_item( $items, 'add-volume-bundles' );
						}
						
					
						
						error_log('acmss.php :: 1419 :: items: ' . print_r($items,true));
						
						$this->unset_menu_item( $items, 'my-account');

					} else {
						//unset( $items[$my_account_idx] );

						//$this->unset_menu_item( $items, 'add-individual-products' );
						$this->unset_menu_item( $items, 'add-volume-bundles' );
						// error_log('items menu: ' . print_r($items,true));
						$this->unset_menu_item( $items, 'membership-renewal' );
						//$this->unset_menu_item( $items, 'learning-modules' );
						//$this->unset_menu_item( $items, 'purchase-certifications-memberships' );
					}
				}
			}
			return $items;
		}

		private function unset_menu_item( &$items, $item_slug ) {
			foreach ( $items as $key => $post ) {
				if( $post->post_name == $item_slug ) {
					unset( $items[$key] );
					break;
				}
			}
		}

		public function cmss_affidavit_prompt( $atts = null ) {
			extract( shortcode_atts( array(
				'user_id'		=> get_current_user_id()
				), $atts ) );	

			if(!user_can( $user_id, 'certified_member' )) {
				return sprintf( '<p>As soon as you are in a position to do so, you should sign our <a href="%s">CMSS Affidavit</a> to qualify for full CMSS accreditation.</p>', site_url('cmss-affidavit') );
			}
		}



		/**
		* Simple helper function for make menu item objects
		* 
		* @param $title      - menu item title
		* @param $url        - menu item url
		* @param $order      - where the item should appear in the menu
		* @param int $parent - the item's parent item
		* @return \stdClass
		*/ 
		private function create_nav_menu_item( $title, $url, $order, $parent = 0 ){
		    
			$id = 1000000 + $order;
			$item = new stdClass();

			$item->ID = $id;
			$item->object_id = $id;
			$item->db_id = $id;

			$item->title = $title;
			$item->url = $url;

			$item->menu_order = $order;
			$item->menu_item_parent = $parent;

			$item->type = 'custom';
			$item->object = 'custom';

			$item->classes = array( '', 'menu-item', 'menu-item-type-custom', 'menu-item-object-custom' );

			$item->xfn = null;
            $item->current = null;
            $item->current_item_ancestor = null;
            $item->current_item_parent = null;
   
			return $item;
		}		
	}

	spl_autoload_register(function ($class) {
		if( !class_exists($class) ) {
			$class_file = __DIR__ . '/classes/' . strtolower( str_replace( '_', '-', $class ) ) . '.php';

			if( is_file($class_file) ) require_once( $class_file );
			 return;
		}			
	});

	/**
	 * Returns the main instance of ACMSS to prevent the need to use globals.
	 *
	 */
	function ACMSS() {
		return ACMSS_Core::instance();
	}

	ACMSS();