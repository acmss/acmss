<?php

	class ACMSS_Groups {
		static public function group_table( ) {
			if( function_exists('_groups_get_tablename') ) {
				return _groups_get_tablename( 'group' );
			}
		}

		static public function assignable_groups_parent_id() {
			if( $group = Groups_Group::read_by_name( 'Assignable Group' ) ) {
				return $group->group_id;
			}
		}

		static public function child_groups( $parent_id ) {
			global $wpdb;

			if( $group_table = self::group_table() ) {
				return $wpdb->get_results( "SELECT group_id, name, description FROM $group_table WHERE parent_id = $parent_id" );
			}
		}
	}