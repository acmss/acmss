<?php

	class ACMSS_Shortcodes {

		public function __construct() {
			$me = __CLASS__;

			add_shortcode( 'cmss-affidavit-prompt', "$me::cmss_affidavit_prompt" );
			add_shortcode( 'acmss-certification-level', "$me::certification_level" );
			add_shortcode( 'events-map', "$me::events_map" );
			add_shortcode( 'acmss-quiz-list', "$me::quiz_list" );
			//add_shortcode( 'memberships-management', "$me::memberships_management" );
			add_shortcode( 'my_account_links', "$me::account_links" );
			add_shortcode( 'acmss-user-info', "$me::user_info" );
			add_shortcode( 'terminology_search', "$me::terminology_search" );
			add_shortcode( 'acmss-login-form', "$me::login_form_shortcode" );
			add_shortcode( 'acmss-top-buttons', "$me::top_buttons" );
			add_shortcode( 'acmss-message-box', "$me::message_box" );

			add_shortcode( 'acmss-membership-types', array( __CLASS__, 'membership_types') );
		}

		static public function message_box( $atts = null, $content = '' ) {
			extract( shortcode_atts( array(
				'type'		=> 'info'
				), $atts ) );

			$html = sprintf( '<div class="alert alert-%s">%s</div>', $type, $content );

			return $html;
		}

		// Apicona theme-dependant - could be removed if theme changes
		static public function top_buttons( $atts = null ) {
			
			$userID = get_current_user_id();
			
			$membershipStatus = ACMSS()->membershipStatus( $userID );
			error_log('acmss-shortcodes.php :: 39 :: membershipStatus: ' . $membershipStatus);

			$lapsedMember = ( user_can( $userID, 'lapsed_member' ) ) ? 1 : 0 ;
			// error_log('acmss-shortcodes.php :: 39 lapsedMember: ' . $lapsedMember);
			
			if( !is_user_logged_in() ) {
				$html = <<< EOT
<li><a href="/my-account/"><button class="top-button">Sign In</button></a></li>
<li><a href="/store/acmss-volume-packages/"><button class="top-button">Become Certified</button></a></li>
<li><a href="/store/acmss-volume-packages/"><button class="top-button">Certification</button></li>
EOT;
			} 
			
			elseif( ($lapsedMember == true) || ($membershipStatus != 'active') ) {
				// error_log('acmss-shortcodes :: 50 member lapsed');
				$html = '<li><a href="/my-account/"><button class="top-button">Account Details</button></a></li>
				<li class="hiddxen-xs hxidden-sm"><a href="/my-account/customer-logout/"><button class="top-button">Log Out</button></a></li>';				
			
			}
			
			else {
				$html = <<< EOT
<li><a href="/my-account/"><button class="top-button">Account Details</button></a></li>
<li><a href="/certification-assessment/"><button class="top-button">Certification Assessment</button></li>
<li class="hiddxen-xs hxidden-sm"><a href="/my-account/customer-logout/"><button class="top-button">Log Out</button></a></li>
EOT;
			}

			if( current_user_can('administrator') ) {
				$html .= '<li class="hidxden-xs hiddxen-sm"><a href="/wp-admin/"><button class="top-button">Dashboard</button></a></li>';
			}

			return $html;
		}

		static public function membership_types( $atts = null ) {
			$types = array( 'Dermatology', 'Emergency Medicine', 'General', 'Internal Medicine', 'Oncology', 'Ophthalmology', 'Primary Care', 'Urgent Care', 'Vascular Care' );

			$list = '';
			foreach( $types as $key => $type ) {
				$list .= sprintf( '<li><img class="alignnone size-full" src="%1$s" alt="%2$s" /><span class="span-text">%2$s</span></li>', plugins_url( 'images/specialities' . str_replace( ' ', '-', strtolower($type) ), __FILE__ ), $type);
			}

 			$html = <<< EOT
<div class="speciality_img">
	<h4 class="product_title entry-title">CMS Recognizes Certified Scribes in ALL Specialities</h4> 
	<ul class="inline_b">
		$list
	</ul>
</div>
EOT;
			return $html;
		}

		static public function cmss_affidavit_prompt( $atts = null ) {
			extract( shortcode_atts( array(
				'user_id'		=> get_current_user_id()
				), $atts ) );

			if( !user_can( $user_id, 'certified_member' ) ) {
				return sprintf( '<p>As soon as you are in a position to do so, you should sign our <a href="%s">CMSS Affidavit</a> to qualify for full CMSS accreditation.</p>', site_url('cmss-affidavit') );
			}
		}

		static public function certification_level( $atts ) {
			extract( shortcode_atts( array(
				'user_id'		=> get_current_user_id(),
				'with_comma'	=> 'Yes',
				'in_full'		=> false
				), $atts, 'acmss_email' ) );	

			$comma = $with_comma == 'Yes' ? ', ' : '';

			if( is_null($user_id) ) $user_id = get_current_user_id();

			if( ( user_can( $user_id, 'certified_member' ) || user_can( $user_id, 'certified_non_member' ) ) && !user_can( $user_id, 'apprenticed_member')) {
				if( $in_full ) {
					return $comma.'Certified Medical Scribe Specialist';
				} else {
					return $comma.'CMSS';
				}

			} else if( user_can( $user_id, 'apprenticed_member' ) || user_can( $user_id, 'apprenticed_non_member' ) ) {
				if( $in_full ) {
					return $comma.'Certified Medical Scribe Apprentice';
				} else {
					return $comma.'CMSA';
				}
			}
		}

		static public function events_map( $atts ) {
			wp_enqueue_script( 'acmss-maps' );

			$query = new WP_Query( array( 'post_type' => 'events', 'posts_per_page' => -1 ) );
			$events = array();

			foreach ($query->posts as $event) {
				$meta = get_post_meta( $event->ID );

				$events[] = array( 'title' => $event->post_title, 'lat' => $meta['_eventLat'][0], 'lng' => $meta['_eventLng'][0], 'date' => date( 'F j, Y', $meta['_eventDate'][0] ) );
			}

			wp_localize_script( 'acmss-maps', 'acmss_events', $events );

			return '<div id="gmap_canvas" style="width: 600px; height: 450px; margin: 5px;"></div>';
		}


		static public function quiz_list( $atts ) {
			return '<div id="assessment_list"><h2>Currently, you have no assessments available.</h2></div>';
		}

/*
		static public function memberships_management( $atts ) {
			if( current_user_can('certified_academic_partner') ) {
				if( false == get_user_meta( get_current_user_id(), '_signed_cap_affidavit', true ) ) {
					return '<p><strong>Please <a href="//theacmss.org/cap-affidavit">sign the CAP Affidavit</a> before adding students to your account.</strong></p><div class="messageBox alert icon"><span>Please note that it may take several hours for your signed affidavit to be recieved and processed. You will receive a <em>"Next Steps"</em> email from the ACMSS on successful receipt.</span></div>';
				}
			}
	/ *		
			if( current_user_can('administrator') ) {
				acmss_add_bundles( get_current_user_id(), 2303, 1 );

				echo '@'.	(int) get_user_meta( $user_id, $member_type, true ) . '@';	

				acmss_add_bundles( get_current_user_id(), 2303, 1, '_memberships' );
			}
	* /		
			$member_count = (int) ACMSS()->adjust_owned_memberships_count( get_current_user_id() );
			
			$org_name = ACMSS()->get_org_name();

			if( isset($_POST['org_name']) ) {
				$org_name = trim($_POST['org_name']);

				update_user_meta( get_current_user_id(), '_org_name', $org_name );

				ACMSS()->add_to_corporate_group( get_current_user_id(), $org_name );
			}

			ACMSS()->add_org_user();

			if( empty($org_name) ) {
				$org_form = <<< EOT
			<p><strong>Before adding members, please set the name under which your members will be identified.<strong><em> Note, once set, this cannot be changed</em>.</p>

			<form action="#" method="post">
				<label>Organization Name:</label>
				<input type="text" name="org_name" class="member_input" style="width: 250px;" />
				<input type="submit" value="Save" class="btn green" />			
			</form>
EOT;
			} else {
				$org_form = "<p>Your organization name is set as <strong>$org_name</strong>. This can only be changed on request.</p>";

				$mscat_count = ACMSS()->get_assessment_access( get_current_user_id(), 'MSCAT' );
				$hipaa_count = ACMSS()->get_assessment_access( get_current_user_id(), 'HIPAA' );
				$manual_count = (int) get_user_meta( get_current_user_id(), '_mscat_manual_access', true );

				$add_new_members = <<< EOT
					<div class="std_disp"><span class="count">$member_count</span><span>Memberships</span><a href="/store/scribe-bundles" title="Purchase more memberships">+</a><button class="btn small blue member-assign-button" data-item="acmss-membership" style="display: none;">Assign</button></div>

					<div class="std_disp green"><span class="count">$mscat_count</span><span>MSCAT Assessments</span><a href="/store/scribe-bundles" title="Purchase further MSCAT certification access">+</a><button class="btn small blue assign-button" data-item="mscat-quiz">Assign</button></div>

					<div class="std_disp blue"><span class="count">$hipaa_count</span><span>HIPAA Assessments</span><a href="/store/scribe-bundles" title="Purchase further HIPAA certification access">+</a><button class="btn small blue assign-button" data-item="hipaa-quiz">Assign</button></div>

					<div class="std_disp orange"><span class="count">$manual_count</span><span>MSCAT Manuals</span><a href="/store/scribe-bundles" title="Purchase further MSCAT Manual access">+</a><button class="btn small blue assign-button" data-item="mscat-manual">Assign</button></div>
EOT;

				if( $member_count > 0 ) {
					$add_new_members .= <<< EOT
				<form action="#" method="post" style="clear:both; margin-top: 30px;">
					<label for="first_name">First Name:</label>
					<input type="text" name="first_name" class="member_input" />

					<label for="last_name">Last Name:</label>
					<input type="text" name="last_name" class="member_input" />

					<label for="user_email">Email Address:</label>
					<input type="text" name="user_email" class="member_input" />

					<input type="submit" value="Save" class="btn green" />
				</form>
EOT;
				} 
				
				$add_new_members .= '<p style="clear:both; font-weight: bold;">You can purchase further memberships or certification products in <a href="/store/scribe-bundles">the ACMSS Store</a>.</p>';
			}


			$existing = ACMSS()->get_member_list( get_current_user_id(), true );
			if( empty($existing) ) {
				$existing = '<p>You have not yet created any memberships.</p>';
			}

			$html = <<< EOT
			<h2>Add new member</h2>

			$org_form

			$add_new_members

			<hr/>

			<h2>Existing Memberships</h2>

			<p>To assign the manual or assessments to individual members, select the member(s) by clicking on their name and then click the relevant <strong>Assign</strong> button above.</p>

			<p>If you have any questions on assignments please email <a href="mailto:assessments@theacmss.org">assessments@theacmss.org</a>.</p>

			<div id="existing_members">
				$existing
			</div>
EOT;

			return $html;
		}
*/
		static public function account_links( $atts ) {
			$links = array( 
					'my-account' 								=> 'Account details', 
					'specialist-crosswalk'						=> 'CMS Specialist Attestation',
					'my-account/continuing-scribe-education'	=> 'Continuing scribe education',
					'my-account/edit-account' 					=> 'Change password', 
					'my-account/edit-address/billing' 			=> 'Edit postal address', 
					'my-account/customer-logout'				=> 'Logout',
					'my-account/memberships-assignment'			=> 'Manage member assignments'
				);

			$html = '';

			if( !current_user_can('certified_academic_partner') && !current_user_can('administrator') 
				&& ACMSS()->adjust_owned_memberships_count( get_current_user_id() ) == 0
				&& ACMSS()->distributed_products_owned( get_current_user_id() ) == false
				) {
				unset($links['my-account/memberships-assignment']);
				unset($links['specialist-crosswalk']);
			}

			if( !current_user_can('acmss_member') && !current_user_can('administrator') ) {
				unset($links['my-account/continuing-scribe-education']);
			}

			foreach ($links as $link => $caption) {
				$html .= sprintf( '<li><div class="icon16 iconSymbol check"></div><a href="%s">%s</a></li>', site_url($link), $caption );
			}

			return '<ul class="account-links icon-list">'.$html.'</ul>';
		}

		static public function user_info( $atts ) {
			if ( !is_user_logged_in() )	return '';

			extract( shortcode_atts( array(
				'field' 	=> 'display_name',
				'user_id'	=> isset($_GET['user_id']) ? $_GET['user_id'] : get_current_user_id()
				), $atts ) );

			
			if( is_null($user_id) ) $user_id =  get_current_user_id();

			switch( $field ) {
				case 'hex_id':
					$id = dechex( $user_id + 100001 );
					return strtoupper( str_pad( $id, 8, '0', STR_PAD_LEFT ) );
					break;

				default:
					$user = new WP_User( $user_id );
					return $user->$field;
			}
		}


		static public function terminology_search() {
			wp_enqueue_script('terminology-search');

			return '<div id="term_results"></div>';
		}

		static public function login_form_shortcode() {
			if ( is_user_logged_in() )
				return '';

			return wp_login_form( array(  'redirect' => site_url(), 'form_id' => 'slidedown_loginform','echo' => false ) );
		}
}