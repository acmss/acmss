<?php
	/**
	 * Custmisations and overrides for the WooCommerce plugin used for the ACMMS store
	 */
	define( 'STUDENT_MEMBERSHIP_PRODUCT_ID', 1656 );
	define( 'MSCAT_CERTIFICATE_PRODUCT_ID', 1508 );
//	define( 'MSCAT_OPTHALMOLOGY_PRODUCT_ID', 2445 );	
	define( 'HIPAA_CERTIFICATE_PRODUCT_ID', 1550 );
	define( 'MSCAT_MANUAL_PRODUCT_ID', 1555 );
	define( 'INDIVIDUAL_MEMBERSHIP_PRODUCT_ID', 1641 );
	define( 'CORPORATE_MEMBERSHIP_PRODUCT_ID', 1743 );
	define( 'ACADEMIC_MEMBERSHIP_PRODUCT_ID', 1644 );
	define( 'FACILITY_ACCREDITATION_PRODUCT_ID', 1748 );
	define( 'NON_SCRIBE_BUNDLES_PRODUCT_ID', 2015 );
	define( 'SCRIBE_BUNDLES_PRODUCT_ID', 2301 );
	define( 'ACMSS_MEMBERSHIP_PRODUCT_ID', 11500 );


	class ACMSS_WooCommerce {
		public function __construct() {
			$me = __CLASS__;

			add_action( 'init', "$me::init" );
		}

		static public function init() {
			$me = __CLASS__;

			//add_action( 'acmss_order_complete', "$me::mark_order_as_complete" );
			add_action( 'woocommerce_after_main_content', "$me::donation" );
			//add_action( 'woocommerce_before_main_content', "$me::store_buy_assessments", 25 );
			// add_action( 'woocommerce_calculate_totals', "$me::apply_discounts", 10, 1 );
			add_action( 'woocommerce_cart_totals_before_order_total', "$me::cart_totals_before_order_total" );
			add_action( 'woocommerce_checkout_init', "$me::checkout_init" );	
			//add_action( 'woocommerce_order_status_changed', "$me::completed_order", 5, 3 );
			add_action( 'woocommerce_order_status_completed', "$me::completed_order" );
			add_action( 'woocommerce_proceed_to_checkout', "$me::proceed_to_checkout" );
			//add_action( 'woocommerce_review_order_before_order_total', "$me::cart_totals_before_order_total" );

			add_action( 'woocommerce_checkout_init', "$me::maybe_add_coupon" );

			// removed 4/15/16 removes 'how did you hear about us' box from checkout
			// add_action( 'woocommerce_after_checkout_billing_form', "$me::hear_about_us_field", 10 );
			// add_action( 'woocommerce_checkout_process',  "$me::hear_about_checkout_field_process" );
			// add_action( 'woocommerce_checkout_update_order_meta',  "$me::hear_about_us_field_update_order_meta" );

			add_action( 'woocommerce_thankyou', "$me::thankyou_redirect" ); 

			add_filter( 'add_to_cart_text', "$me::custom_cart_button_text" );
			add_filter( 'single_add_to_cart_text', "$me::custom_cart_button_text" );
			add_filter( 'woocommerce_add_cart_item_data', "$me::add_cart_item_data", 10, 2 );
			//add_filter( 'woocommerce_available_variation', "$me::hide_zero_savings", 99, 3 );			
			add_filter( 'woocommerce_get_catalog_ordering_args', "$me::get_catalog_ordering_args" );
			add_filter( 'woocommerce_params', "$me::woocommerce_params" );
			add_filter( 'woocommerce_product_tabs', "$me::remove_reviews_tab", 98);
			
			// add_filter('woocommerce_get_price', "$me::customized_price", 10, 2);

			add_shortcode( 'product_calculator', "$me::product_calculator" );

			//if( !is_user_logged_in() ) {
				//add_filter( 'woocommerce_sale_price_html', "$me::member_prices", 1, 2 );
				//add_filter( 'woocommerce_price_html', "$me::member_prices", 1, 2 );
				//add_filter( 'woocommerce_empty_price_html', "$me::member_prices", 1, 2 );
			//}

			remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
		}
		
		static public function customized_price($price, $product) {
			error_log('acmss-woocommerce :: 71');
			if( !is_user_logged_in() ) return $price;
			
			if( has_term( 'webinars', 'product_cat', $product->ID)) {
				$price = 15;
			}
			return $price;
		}

		static public function maybe_add_coupon() {
			error_log('acmss-woocommerce :: 81 :: maybe_add_coupon -- ');
			$available = array(
				'corporate_platinum_plus'	=> 'platinum plus',
				'corporate_platinum'		=> 'platinum 50',
				'corporate_diamond' 		=> 'diamond 40',
				'corporate_gold' 			=> 'gold 30',
				'corporate_silver' 			=> 'silver 20',
				'corporate_bronze' 			=> 'bronze 10',
				'corporate_copper' 			=> 'copper 50',
				'none'						=> 'none'
			);
			
			foreach ($available as $cap => $coupon ) {
				error_log('acmss-woocommerce :: 93 :: cap: ' . $cap . ' current_user_can: ' . current_user_can( $cap ) . ' admin: ' . current_user_can( 'administrator' ) . ' coupon: ' .  $coupon . ' discount:  ' .  WC()->cart->has_discount( $coupon ));
				error_log('acmss-woocommerce :: 94 :: coupons: ' . WC()->cart->get_applied_coupons()[0] );
				error_log('acmss-woocommerce :: 95 :: cap: ' . $cap . ' current_user_can($cap): ' . current_user_can( $cap ));
				if( current_user_can( $cap ) || current_user_can( 'administrator' ) ) {
					if( !WC()->cart->has_discount( $coupon ) ) {
						// WC()->cart->add_discount( $coupon );
						if(WC()->cart->get_applied_coupons()[0]) {
							error_log('have incoming coupon');
						}
						else {
							error_log('do not have incoming coupon -- coupon added: ' . $coupon );
							// WC()->cart->add_discount( $coupon ); // to address error generated at checkout 1/3/17
							
						}
						return;
					}
				} 
				else { // remove it if they've used it - they are no supposed to
					if( WC()->cart->has_discount( $coupon ) ) {
						WC()->cart->remove_coupon( $coupon );
					}
				}
			}
			error_log(" **** bundle check ****");
			$products = array();
			foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
				$_product = $values['data'];
			
				if( in_array( $_product->id, array( 
					INDIVIDUAL_MEMBERSHIP_PRODUCT_ID, 
					MSCAT_CERTIFICATE_PRODUCT_ID, 
					HIPAA_CERTIFICATE_PRODUCT_ID,
					MSCAT_MANUAL_PRODUCT_ID,
					) ) ) {
					$products[$_product->id] = intval($products[$_product->id]) + 1;
				error_log(' **** yes ' . $_product->id . ' ****');
				} else {
					error_log(' **** no ' . $_product->id . ' ****');
				}
			}			
			
			$full_bundle = ( count($products) == 4 );
			$coupon = 'full bundle discount';
			if( $full_bundle ) {
				if( !WC()->cart->has_discount( $coupon ) ) {
					WC()->cart->add_discount( $coupon );
				}
			} else {
				if( WC()->cart->has_discount( $coupon ) ) {
					WC()->cart->remove_coupon( $coupon );
				}
			}
		}
		
		/**
		 * Outputs a radio button form field
		 */
		public static function woocommerce_form_field_radio( $key, $args, $value = '' ) {
			global $woocommerce;

			$defaults = array(
				'type' 			=> 'radio',
				'label' 		=> '',
				'placeholder' 	=> '',
				'required' 		=> false,
				'class' 		=> array(),
				'label_class' 	=> array(),
				'return' 		=> false,
				'options' 		=> array()
			);
			
			$args = wp_parse_args( $args, $defaults );
	
			if( isset($args['clear']) && $args['clear'] ) {
				$after = '<div class="clear"></div>';

			} else {
				$after = '';
				$required = ( $args[ 'required' ] ) ? ' <abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>' : '';

				switch( $args['type'] ) {
					case 'select':
						$options = '';
						if( !empty($args['options']) ) {
							$html_structure = '<input type="radio" name="%1$s" id="%1$s" value="%2$s" %3$s class="select"> %4$s';

							foreach ( $args[ 'options' ] as $option_key => $option_text ) {								$options .= sprintf( $html_structure, $key, $option_key, selected( $value, $option_key, true ), $option_text );
							}

							$field = sprintf( '<p class="form-row %1$s" id="%2$s_field">
								<label for="%2$s" class="%3$s">%4$s</label>%5$s</p>%6$s', implode( ' ', $args[ 'class' ] ), $key, implode( ' ', $args['label_class'] ), $args[ 'label' ] . $required, $options, $after );
						}
						break;
				}
				
				if( $args['return'] ) {
					return $field;	
				} else {
					echo $field;
				}
			}
		}
		/**
		 * Add the field to the checkout
		 **/

		public static function hear_about_us_field( $checkout ) {
			echo '<div id="hear_about_us_field"><h4>' . __( 'How did you hear about us?' ) . '</h4>';

			self::woocommerce_form_field_radio( 'hear_about_us', array(
				 'type' => 'select',
				'class' => array(
								 'here-about-us form-row-wide'
				),
				'label' => __( '' ),
				'placeholder' => __( '' ),
				'required' => false,
				'options' => array(
								'Google' 		=> 'Google<br/>',
								'Other Search' 	=> 'Other Search Engine<br/>',
								'Colleague' 	=> 'Colleague<br/>',
								'ASOA' 			=> 'ASOA<br/>',
								'Other' 		=> 'Other<br/>'
				)
				), $checkout->get_value( 'hear_about_us' ) );
			
			echo '</div>';
		}

		/**
		 * Process the checkout
		 **/
		public static function hear_about_checkout_field_process( ) {
			// Check if set, if its not set add an error.
			if ( !$_POST['hear_about_us'] ) {
				wc_add_notice( __( 'Please let us know how you found out about the ACMSS.' ), 'error' );
			}
		}
		
		/**
		 * Update the order meta with field value
		 **/
		public static function hear_about_us_field_update_order_meta( $order_id ) {
			if ( $_POST[ 'hear_about_us' ] )
							update_post_meta( $order_id, 'How did you hear about us?', esc_attr( $_POST[ 'hear_about_us' ] ) );
		}
/*
		public static function acmss_vary_woo_template( $template, $slug, $name ) {
			if( $slug == 'content' && $name = 'single-product' ) {
				add_filter( 'woocommerce_product_single_add_to_cart_text', function( $title ) { return 'Calculate Total Cost'; }  );
				add_filter( 'woocommerce_quantity_input_args', function( $args, $product = null ) { $args['input_value'] = $_POST['quantity']; return $args; }  );

				add_filter( 'gettext', 
					function($translated, $text, $domain) {
						switch ($text) {
							case 'Cart Totals':
								return 'Calculated Totals';
								break;

							case 'Cart Subtotal':
								return 'Standard Cost';
								break;

							case 'Order Total':
								return 'Discounted Cost';
								break;

							default:
								return $translated;
								break;
						}
					}
					, 10, 3 );

				$template = get_stylesheet_directory() . '/woocommerce/calculator-content-single-product.php';
			}
			return $template;
		}

		static public function product_calculator( $atts ) {
			$id = $atts['id'];

			add_filter( 'wc_get_template_part', array( 'ACMSS_WooCommerce', 'vary_woo_template' ), 10, 3 );
			
			$html = '<div class="transparency-calculator">';
			$html .= do_shortcode( "[product_page id='{$id}']" );

			if( count(WC()->cart->get_cart()) > 0 ) {
				$html .= do_shortcode( '[woocommerce_cart]' );
			}
			
			$html .= '</div>';

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				WC()->cart->set_quantity( $cart_item_key, 0 );	
			}		

			return $html;
		}
		
	

		static public function vary_woo_template( $template, $slug, $name ) {
			if( $slug == 'content' && $name = 'single-product' ) {
				add_filter( 'woocommerce_product_single_add_to_cart_text', function( $title ) { return 'Calculate Total Cost'; }  );
				add_filter( 'woocommerce_quantity_input_args', function( $args, $product = null ) { $args['input_value'] = $_POST['quantity']; return $args; }  );

				add_filter( 'gettext', 
					function($translated, $text, $domain) {
						switch ($text) {
							case 'Cart Totals':
								return 'Calculated Totals';
								break;

							case 'Cart Subtotal':
								return 'Standard Cost';
								break;

							case 'Order Total':
								return 'Discounted Cost';
								break;

							default:
								return $translated;
								break;
						}
					}
					, 10, 3 );

				$template = get_stylesheet_directory() . '/woocommerce/calculator-content-single-product.php';
			}
			return $template;
		}*/

		// Showing "You Save $0.00" is a bit pointless, so let's remove it from the price html
		/*static public function hide_zero_savings(  $variation = '', $obj = '' , $variation_obj  = '' ) {
			$variation['price_html'] = str_replace( '<br><span class="price normal_price savings">You Save <span class="amount"><span class="amount">&#36;0.00</span></span></span>', '', $variation['price_html'] );

			return $variation;
		}*/


		static public function cart_totals_before_order_total() {
			error_log('321 :: acmss-woocommerce discount_total: ' . WC()->cart->discount_total);
			if( isset( WC()->cart->discount_total ) && WC()->cart->discount_total > 0 ) {
				$total_discount = '<em>&ndash; '. wc_price( WC()->cart->discount_total ) . '</em>';

				$html = <<< EOT
				<tr class="discount-total">
					<th style="white-space: nowrap;">Customer Discount</th>
					<td>$total_discount</td>
				</tr>
EOT;
				echo $html;
			}
		}


		static public function apply_discounts( $cart ) {
			$membership = new WC_Product( INDIVIDUAL_MEMBERSHIP_PRODUCT_ID );
			$membership_price = $membership->price;
// echo("<!-- '327 :: membership_price: ' . $membership_price -->");
			foreach( $cart->get_cart() as $cart_item_key => $item ) {
				if( isset($item['variation']) && isset($item['variation']['attribute_bundles']) ) {
					$membership_price =  $item['variation']['attribute_membership'] == 'Without Membership' ? 0 : $membership->price;

					foreach( self::membership_discounts() as $threshold => $discount ) {
						if( ( $item['quantity'] >= $threshold ) ) {
							$cart->discount_total += ($item['quantity'] * $membership_price) * ($discount / 100);
							break;
						}
					}

					foreach( self::product_discounts() as $threshold => $discount ) {
						if( ( $item['quantity'] >= $threshold ) ) {
							$cart->discount_total += ($item['line_total'] - ($item['quantity'] * $membership_price)) * ($discount / 100);
							break;
						}
					}
				}
			}
		}
/*
		static public function membership_discounts() {
			return array( 1600 => 81.25, 800 => 80, 400 => 78.75, 200 => 77.5, 100 => 76.25, 25 => 75, 5 => 50 );
		}
		
		static public function product_discounts() {
			return array( 500 => 50, 450 => 45, 400 => 40, 350 => 35, 300 => 30, 250 => 25, 200 => 20, 150 => 15, 100 => 10, 50 => 5 );
		}
*//*
		static public function store_buy_assessments() {
			if( isset($_GET['add_assessments']) ) {
				$html = <<< EOT
	<div class="messageBox alert icon" style="margin: 20px 0">
		<p><strong>You have no access to the {$_GET['add_assessments']} Assessment<strong>. You can purchase further access right here, right now!</p>
	</div>
EOT;
				echo $html;			
			}
		}*/

/*
		static public function store_promotion() {
			if( !is_archive() ) return;

			$html = <<< EOT
	<div class="messageBox info icon">
		<h2>New Year's Celebration kick off!</h2>
		<p>For a limited time, receive $5 off any purchase. Enter coupon code: <em>Certified Scribe</em>.</p>
	</div>
EOT;
			echo $html;
		}
*/		//add_action( 'woocommerce_before_main_content', 'acmss_store_promotion' );


	/*
		static public function woocommerce_product_query( $q, $this ) {
			if( current_user_can( 'certified_academic_partner' ) == false && current_user_can( 'administrator' ) == false ) {
				$q->query_vars['post__not_in'][] = STUDENT_MEMBERSHIP_PRODUCT_ID;
			}

			if( current_user_can( 'corporate_partner_administrator' ) == false && current_user_can( 'administrator' ) == false ) {
				$q->query_vars['post__not_in'][] = SCRIBE_BUNDLES_PRODUCT_ID;
			}

			if( current_user_can( 'corporate_partner_administrator' ) == true && current_user_can( 'administrator' ) == false ) {
				$q->query_vars['post__not_in'][] = NON_SCRIBE_BUNDLES_PRODUCT_ID;
			}
		}
		//add_action( 'woocommerce_product_query', 'acmss_woocommerce_product_query', 10, 2 );
	*/

		static public function proceed_to_checkout() {
			// add padding around button
			echo '<a href="/store" class="button btn alt">Continue Shopping</a>';
		}

/*
		static public function member_prices( $price, $product ) {
			global $woocommerce_tiered_pricing;

			if( $product->id != 2015 ) {
				$role = 'ignite_level_52b062edeab25';

				if( $woocommerce_tiered_pricing->settings['show_savings'] == 'yes' ) { 
					$meta = get_post_custom( $product->id );

					if( isset( $meta["_{$role}_price"] ) ) {
						$reg_price = $meta['_regular_price'][0];

						$price = woocommerce_price( $reg_price );

						$role_price = $meta["_{$role}_price"][0];

						if ( floatval( $role_price ) < floatval( $reg_price ) ) {
							$price .= '<br><span class="price normal_price savings">Member Price: <span class="amount">' . woocommerce_price( $role_price ) . '</span></span>';
						}
					}
				}
			} 

			return $price;
		}
*/

		static public function completed_order( $order_id ) {
			global $wpdb;
			$order = new WC_Order( $order_id );
			error_log('acmss-woocommerce.php :: 463 :: order: ' . print_r($order,true) . ' order_id: ' . $order_id);
			
			$userID = $order->customer_id;
			error_log('acmss-woocommerce.php :: 466 :: userID: ' . $userID);
			
			foreach( $order->get_items() as $item_id => $item ) {
				$product_id = (int) $item['variation_id'] != 0 && !is_null( $item['variation_id'] ) ? (int) $item['variation_id'] : $item['product_id'];
 
				error_log('acmss-woocommerce.php :: 467 variation_id: ' . (int) $item['variation_id']);
				error_log('acmss-woocommerce.php :: 468 product_id: ' . $product_id);
				if( $product_id == 11586 ) {
					error_log('copper 1');
					// $product_id = 11500;
				}
				
				// returns true/false
				$assigned = ACMSS_Entitlements::process_purchased_entitlements( $product_id, $item['qty'], $userID );
				// $assigned = ACMSS_Entitlements::process_purchased_entitlements( $product_id, $item['qty'], $order->user_id );
				error_log('acmss-woocommerce.php :: 476 :: assigned: ' . $assigned);
				// if( ACMSS()->is_assignor( $order->user_id ) ) {
				if( ACMSS()->is_assignor( $userID ) ) {
					$assignable = $item['qty'] - ( $assigned ? 1 : 0 );

					if( $assignable > 0 ) {
						// ACMSS_Entitlements::add_assignable( $product_id, $assignable, $order->user_id );
						ACMSS_Entitlements::add_assignable( $product_id, $assignable, $userID );
					}
				}

				if( $item['product_id'] == 1743  ) {
					$freebies = 0;

					switch( (int) $item['variation_id'] ) {
						case 10813: // Platinum+
							$freebies = 100;
							break;

						case 10812: // Platinum
							$freebies = 50;
							break;

						case 10811: // Diamond
							$freebies = 35;
							break;
						
						case 33189: // Gold+
							$freebies = 100;
							break;
							
						case 10810: // Gold
							$freebies = 20;
							break;

						case 10809: // Silver
							$freebies = 10;
							break;

						case 10808: // Bronze
							$freebies = 5;
							break;
							
						case 14010: // Copper (4)
							$freebies = 4;
							break;						
						
						case 14007: // Copper (3)
							$freebies = 3;
							break;
						
						case 14006: // Copper (2)
							$freebies = 2;
							break;	

						case 11586: // Copper(1)
							$freebies = 1;
							break;
						
						// found product removed by KH 
						/*	
						case 14870: // Admin Only
							$freebies = 0;
							break;
						*/		
					}
					
					error_log('acmss-woocommerce.php :: 537 :: freebies: ' . $freebies);
					error_log('acmss-woocommerce.php :: 538 :: is_assignor: ' . ACMSS()->is_assignor( $userID ) );
					if( ($freebies > 1) || (ACMSS()->is_assignor( $order->user_id )) ) {
						foreach( array( 
								// MSCAT_CERTIFICATE_PRODUCT_ID, 
								// HIPAA_CERTIFICATE_PRODUCT_ID, 
								//  MSCAT_MANUAL_PRODUCT_ID, 
								ACMSS_MEMBERSHIP_PRODUCT_ID
							) as $product_id ) {
							ACMSS_Entitlements::add_assignable( $product_id, $freebies, $userID );
						}
					}
					else {
						// add entitlements
						// ACMSS_Entitlements::add_entitlement();
						error_log('purchased copper(1)');
						ACMSS_Entitlements::add_entitlement( 'quiz',1758,3,$userID );
						ACMSS_Entitlements::add_entitlement( 'quiz',1568,3,$userID );
						// ACMSS_Entitlements::add_entitlement( 'quiz',1555,1,$order->user_id );
						ACMSS()->add_user_cap( $order->user_id, 'read_mscat_manual' );
						// init membership / subscription :: one year
						// done by product purchase
					}
					
					/*
					error_log('acmss-woocommerce :: 547 :: is_assignor: ' . ACMSS()->is_assignor());
					
					if( $freebies > 0 ) {
						foreach( array( 
								MSCAT_CERTIFICATE_PRODUCT_ID, 
								HIPAA_CERTIFICATE_PRODUCT_ID, 
								MSCAT_MANUAL_PRODUCT_ID, 
								ACMSS_MEMBERSHIP_PRODUCT_ID
							) as $product_id ) {
							ACMSS_Entitlements::add_assignable( $product_id, $freebies, $order->user_id );
						}
					}
					else {
						// update_user_meta( $order->user_id, '_admin_only_membership', 1 );
						error_log('purchased copper(0)');
						ACMSS_Entitlements::add_entitlement( 'quiz',1758,3,$order->user_id );
						ACMSS_Entitlements::add_entitlement( 'quiz',1568,3,$order->user_id );
						// ACMSS_Entitlements::add_entitlement( 'quiz',1555,1,$order->user_id );
						ACMSS()->add_user_cap( $order->user_id, 'read_mscat_manual' );
					}
					*/
				}
				// global $wpdb;
				
				if( ($product_id == 42942) || 
					($product_id == 42941) || 
					($product_id == 42940) || 
					($product_id == 42925) || 
					($product_id == 42924) ||
					($product_id == 43584) ) 
				{
					error_log('acmss-woocommerce.php :: 547 :: ');
					$freebies = 2;
					$returnedValue = wc_paid_listings_give_user_package( $userID, 42942 );
					error_log('acmss-woocommerce.php :: 605 :: returnedValue: ' . $returnedValue);
					// WC_Paid_Listings_Orders::order_paid( $order_id );
					$tableUpdateProcessed = $wpdb->update(
						'wp_posts',
						array(
							'post_author' => $userID,
							'post_status' => 'publish',
							'post_type'   => 'job_listing',
							'post_password' => ''
						),
						array(
							'ID' => $order_id
						)
					);
					error_log('acmss-woocommerce.php :: 620 :: tableUpdateProcessed: ' . $tableUpdateProcessed);
				}
					
			}
		}


		static public function mark_order_as_complete( $order_id ) {
			$order = new WC_Order( $order_id );
			$order->update_status( 'completed' );
		}


		static public function get_catalog_ordering_args( $args ) {
			$orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

			if ( 'random_list' == $orderby_value ) {
				$args['orderby'] = 'rand';
				$args['order'] = '';
				$args['meta_key'] = '';
			}

			return $args;
		}

		static public function custom_cart_button_text() {
			$pid = get_the_ID();

			foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
				$_product = $values['data'];
			
				if( $pid == $_product->id  ) {
					return __( 'Add Again?', 'woocommerce' );
				}
			}

			return __( 'Add to cart', 'woocommerce' );
		}

		static public function donation() {
			if( !is_archive() ) return;

			$contribute_url = plugins_url( 'images/contribute-button.png', dirname(__FILE__) );

			$html = <<< EOT
			<div class="new-div row">
	<p class="contributer">Contribute to the responsible growth of the medical scribe industry. Foster the professional development of Certified Medical Scribe Specialists to improve operational efficiency of healthcare.  Contributors recognized.</p>
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
		<input type="hidden" name="cmd" value="_donations">
		<input type="hidden" name="business" value="store@theacmss.org">
		<input type="hidden" name="lc" value="US">
		<input type="hidden" name="item_name" value="The American College of Medical Scribe Specialists">
		<input type="hidden" name="no_note" value="0">
		<input type="hidden" name="currency_code" value="USD">
		<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_LG.gif:NonHostedGuest">
		<input type="image" src="{$contribute_url}" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
		<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>
	</div>
	<p>Our <a href="/about-us/privacy-policy">notice of privacy practices</a> is available for you here.</p>
	<p><em>ACMSS Refund Policy is 30 days.</em> In the event that you are dissatisfied, please send inquiries to <a href="mailto:support@acmss.zendesk.com">support@acmss.zendesk.com</a>. Thank you.</p>
	
EOT;
			echo $html;
		}

		static public function has_user_bought( $user_id, $product_id ) {
			$ordered_products = acmss_get_user_orders( $user_id );

			foreach ( (array) $ordered_products as $p_id ) {
				if( $p_id == $product_id ) return true;
				
				$product = new WC_Product($product_id);
			}

			return false;
		}


		static public function get_user_orders( $user_id, $status = 'completed') {
			if( !$user_id ) return false;

			$orders = array();//order ids

			$args = array(
				'posts_per_page'  => -1,
				'meta_key'        => '_customer_user',
				'meta_value'      => $user_id,
				'post_type'       => 'shop_order',
				'post_status'     => 'all',
				'tax_query' => array(
					array(
						'taxonomy'  => 'shop_order_status',
						'field'     => 'slug',
						'terms'     => $status
					)
				)
			);

			if( $posts = new WP_Query($args) ) {
				if( $orders = wp_list_pluck( $posts->get_posts(), 'ID' ) ) {
					return $orders;	    	
				}
			}

			if( current_user_can('administrator') ) {
				global $wpdb; 
				echo $wpdb->last_query;
				echo 'orders';
				print_r($args);
			}
		}

		static function thankyou_redirect( $order_id ) {
			$order = wc_get_order( $order_id );

			foreach( $order->get_items() as $item ) {
				if ( $item['product_id'] == 8044 ) {
					wp_safe_redirect( site_url('acmss-non-profit-partner-sponsorship') . "?order_id=$order_id" );
				} 
			}
		}

		static public function remove_reviews_tab( $tabs ) {
			unset( $tabs['reviews'] );
			return $tabs;
		}


		static public function add_cart_item_data( $cart_item_meta, $product_id ) {
			if( $product_id == STUDENT_MEMBERSHIP_PRODUCT_ID ) {
				$cart_item_meta['student_count'] = 1;

				if( isset($_POST['student_count']) ) {
					$cart_item_meta['student_count'] = (int) $_POST['student_count'];
				}
			}

			return $cart_item_meta;
		}

		static public function register_at_checkout() {
			foreach( WC()->cart->cart_contents as $cart_item ) {
				error_log('acmss-woocommerce 670 :: product_id: ' . $cart_item['product_id'] . ' price: ' . get_post_meta($cart_item['product_id'], '_price', true));
				if( has_term( 'must-register', 'product_tag', $cart_item['product_id'] ) ) {
					return true;
				}
			}
		
			return false;		
		}

		/** Force uses to create an account when certain products are purchased **/
		static public function woocommerce_params( $woocommerce_params ) {
			if( self::register_at_checkout() ) {
				$woocommerce_params['option_guest_checkout'] = 'no';
			}
			$woocommerce_params['ajax_url'] = admin_url('admin-ajax.php');

			return $woocommerce_params;
		}

		static public function checkout_init( $checkout ) {
			if( self::register_at_checkout() ) {
				$checkout->must_create_account = true;
				$checkout->enable_guest_checkout = false;
			}
		} 
	}