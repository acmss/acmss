<?php

	class ACMSS_Testing {
		static public $active = false;
		
		static public function log( $msg ) {
			if( self::$active ) {
				echo "<pre>$msg</pre>";
			}
		}

		static public function shortcode() {
			self::log( do_shortcode( '[email field="site_url"]woocommerce/page[/email]' ) );
		}


		static public function upgrade_assignments() {
			$user_id = $_GET['user_id'];
			
			ACMSS_Entitlements::convert_assignments( $user_id );
		}


		static public function process_order() {
			ACMSS_WooCommerce::completed_order( $_GET['order_id'] );
		}

		static public function certify_user() {
			$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
			$affidavit = isset($_GET['affidavit']) ? $_GET['affidavit'] : '';
			$mscat = isset($_GET['mscat']) ? $_GET['mscat'] : null;

			if( $user_id > 0 ) {
				$text = 'Is Member: '.	user_can( $user_id, 'acmss_member' );
				ACMSS()->certify_user( $mscat, $affidavit, $user_id );

				$user = new WP_User( $user_id );

				$text .= "\nCertified {$user->user_email}";

				$text .= "\nComplete MSCAT: ". get_user_meta( $user_id, '_completed_mscat_test', true );
				$text .= "\nMSCAT Aff: ". get_user_meta( $user_id, '_signed_mscat_affidavit', true );
				$text .= "\nCMSS Aff: ". get_user_meta( $user_id, '_signed_cmss_affidavit', true );
				wp_die( $text );
			}
		}

		static public function dump_order() {
			$order = new WC_Order( $_GET['order_id'] );

			$html = '';
			foreach( $order->get_items() as $item_id => $item ) {
				$html .= "<br><br>#$item_id: {$item['product_id']}, {$item['variation_id']}, {$item['qty']}<br><br>";
			}

			wp_die($html);
		}
	}