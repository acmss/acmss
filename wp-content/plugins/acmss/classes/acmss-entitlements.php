<?php
	define( 'ENTITLEMENTS_KEY', '_entitlements' );
	define( 'ASSIGNABLE_ENTITLEMENTS_KEY', '_assignable' );

	class ACMSS_Entitlements {
		public function __construct() {
			$me = __CLASS__;

			add_action( 'init', "$me::init" );
		}

		static public function init() {
			$me = __CLASS__;

			add_action( 'template_redirect', "$me::template_redirect" );
		}

		static public function template_redirect() {
			if( is_singular('sfwd-quiz') ) {
				if( !self::user_can_access_quiz() ) {
					wp_safe_redirect( site_url('need-quiz-access') );
					exit();
				}

				global $post;

				if( $post->ID !== 1568 && !ACMSS()->user_has_signed_mscat_affidavit() && $post->ID !== 11837 && $post->ID !== 11836 && $post->ID !== 11839 && $post->ID !== 12213  ) {
					wp_safe_redirect( site_url('mscat-affidavit?sign_before_exam=true') );
					exit();
				}
			}
		}

		static public function convert_assignments( $user_id ) {
			error_log('acmss-entitlements.php :: 35 :: user_id: ' . $user_id);
			$mscat_count = (int) ACMSS()->get_assessment_access( $user_id, 'MSCAT' );
			$hipaa_count = (int) ACMSS()->get_assessment_access( $user_id, 'HIPAA' );
			$manual_count = (int) get_user_meta( $user_id, '_mscat_manual_access', true );
			$member_count = (int) ACMSS()->adjust_owned_memberships_count( $user_id );
			$memberRenewal_count = (int) ACMSS()->adjust_membershipRenewal_count( $user_id );
			error_log('acmss-entitlements :: 41 :: membershipRenewal count: ' . $memberRenewal_count);


			//ACMSS_Testing::log( "$user_id:  $mscat_count, $hipaa_count, $manual_count, $member_count" );

			//ACMSS_Testing::log( print_r( self::user_entitlements( $user_id, ASSIGNABLE_ENTITLEMENTS_KEY ), true ) );

			if( $mscat_count > 3 ) {
				self::add_entitlement( 'quiz', 1758, 3, $user_id );
				self::add_assignable( 1508, $mscat_count - 3, $user_id );

			} else {
				self::add_entitlement( 'quiz', 1758, $mscat_count, $user_id );	
			}

			if( $hipaa_count > 3 ) {
				self::add_entitlement( 'quiz', 1568, 3, $user_id );
				self::add_assignable( 1550, $hipaa_count - 3, $user_id );

			} else {
				self::add_entitlement( 'quiz', 1568, $hipaa_count, $user_id );
			}

			if( $manual_count > 1 ) {
				self::add_assignable( 1555, $manual_count, $user_id );
			}

			self::add_assignable( 11500, $member_count, $user_id );

			delete_user_meta( $user_id, '_mscat_manual_access' );
			delete_user_meta( $user_id, '_assessment_access' );
			delete_user_meta( $user_id, '_memberships' );

			//ACMSS_Testing::log( print_r( self::user_entitlements( $user_id, ASSIGNABLE_ENTITLEMENTS_KEY ), true ) );			
		}

		static public function user_entitlements( $user_id = null, $key = ENTITLEMENTS_KEY ) {
			$user_id = is_null($user_id) ? get_current_user_id() : $user_id;
		
			$entitlements = get_user_meta( $user_id, $key, true );

			if( !$entitlements ) {
				$entitlements = array();
			}

			return $entitlements;
		}

		static public function process_purchased_entitlements( $product_id, $quantity = 1, $user_id ) {
			error_log('Entitlements 86 :: product_id: ' . $product_id . ' -- quantity: ' . $quantity . ' -- user_id: ' . $user_id);
			if( is_null($user_id) || $user_id < 1 ) {
				return;
			}

			$assigned = false;

			$entitlements = get_post_meta( $product_id, ENTITLEMENTS_KEY, true );
			// the following is probably incorrect since incoming quantity could be >1 hence
			if( ACMSS()->is_assignor( $user_id ) ) {
				$quantity = 1;
			}

			foreach ( (array) $entitlements as $obj ) {
				switch( $obj->entitlement_class ) {
					case 'email':
						ACMSS()->emails()->send_email( (int) $obj->entitlement_id, $user_id );
						ACMSS_Testing::log( "\nEmail Sent {$obj->entitlement_id} x $quantity\n" );
						break;

					case 'group':
						if( class_exists('Groups_User_Group') ) {
							$group = new Groups_User( $user_id );

							if( !$group->is_member( (int) $obj->entitlement_id ) ) {
								Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => (int) $obj->entitlement_id ) );

								ACMSS_Testing::log( "\nGroup Added {$obj->entitlement_id} x $quantity\n" );

								$assigned = true;
							}
						}
						break;

					case 'quiz':
						error_log('121 :: entitlement_class: ' . $obj->entitlement_class . ' entitlement_id: ' . $obj->entitlement_id);
						
						// add if(quantity > 1) then add to assignables else 
						
						self::add_entitlement( $obj->entitlement_class, (int) $obj->entitlement_id, $quantity, $user_id );
						
						ACMSS_Testing::log( "\nQuiz Set {$obj->entitlement_id} x $quantity\n" );
						$assigned = true; // added Apr 13 kls
						break;

					case 'cap':
						$user = new WP_User( $user_id );
						if( !$user->has_cap( $obj->entitlement_id ) ) {
							$user->add_cap( $obj->entitlement_id );
							
							ACMSS_Testing::log( "\nCap Set {$obj->entitlement_id} x $quantity\n" );

							$assigned = true;
						}
				}
			}

			return $assigned;
		}

		static public function user_has_assignables( $user_id = null ) {
			// routine rewritten to correctly return whether a user has assignable items
			
			$entitlementsArray = self::user_entitlements( $user_id, ASSIGNABLE_ENTITLEMENTS_KEY );
			// error_log('acmss-entitlements.php :: 149 :: entitlementsArray: ' . print_r($entitlementsArray,true));
			
			$returnFlag = 0;
			
			foreach($entitlementsArray as $value) {
				error_log('acmss-entitlements.php :: 152 :: entitlement value: ' . $value);
				if($value != 0) {
					$returnFlag = 1;
				} 
			}
			
			// error_log('acmss-entitlements.php :: 160 :: returnFlag: ' . $returnFlag);
			
			$returnValue = '';
			if($returnFlag != 0) {
				$returnValue = $entitlementsArray;
			}
			
			// error_log('acmss-entitlements.php :: 170 :: returnValue: ' . print_r($returnValue,true));
			
			// return !empty( self::user_entitlements( $user_id, ASSIGNABLE_ENTITLEMENTS_KEY ) ); removed 1/12/17
			
			return $returnValue;
		}

		static public function user_has_assignable( $product_id, $user_id = null ) {
			$assignables = self::user_entitlements( $user_id, ASSIGNABLE_ENTITLEMENTS_KEY );

			if( isset($assignables[$product_id]) ) {
				return intval( $assignables[$product_id] );
			}

			return 0;
		}

		static public function use_assignable( $product_id, $count = -1, $user_id ) {
			self::add_assignable( $product_id, $count, $user_id );
		}

		static public function add_assignable( $product_id, $count, $user_id ) {
			if( $count == 0 ) return;

			$assignables = self::user_entitlements( $user_id, ASSIGNABLE_ENTITLEMENTS_KEY );
			// error_log('acmss-entitlements :: 168 :: assignables array: ' . print_r($assignables,true));
			if( !isset($assignables[$product_id]) ) {
				$assignables[$product_id] = array();
			}
			// error_log('acmss-entitlements :: 172 :: assignables[product_id] = ' . $assignables[$product_id]);
			$assignables[$product_id] = intval( $assignables[$product_id] ) + $count;

			$returnValue = update_user_meta( $user_id, ASSIGNABLE_ENTITLEMENTS_KEY, $assignables );
			// error_log('acmss-entitlements :: 176 :: true/false from update_user_meta: ' . $returnValue);
		}

		static public function use_entitlement( $type, $id, $user_id = null ) {
			self::add_entitlement( $type, $id, -1, $user_id );
		}

		static public function add_entitlement( $type, $id, $count, $user_id = null, $key = ENTITLEMENTS_KEY ) {
			global $post;
			
			error_log('acmss-entitlements.php :: 211 :: type: ' . $type . ' id: ' . $id . ' count: ' . $count . ' user_id: ' . $user_id . ' key: ' . $key);

			$id = is_null($id) ? $post->ID : $id;

			$entitlements = self::user_entitlements( $user_id, $key );

			if( !isset($entitlements[$type]) ) {
				$entitlements[$type] = array();
			}

			$entitlements[$type][$id] = intval($entitlements[$type][$id]) + $count;

			update_user_meta( $user_id, $key, $entitlements );
		}

		static public function user_can_access_quiz( $quiz_id = null, $user_id = null ) {
			return self::user_has_entitlement( 'quiz', $quiz_id, $user_id );
		}


		static public function user_has_entitlement( $type, $id = null, $user_id = null ) {
			global $post;
			error_log('Entitlements 199 :: type: ' . $type . ' id: ' . $id . ' user_id: ' . $user_id);
			self::convert_assignments( get_current_user_id() );

			$id = is_null($id) ? $post->ID : $id;

			$entitlements = self::user_entitlements( $user_id );
			
			error_log('acmss-entitlements :: 212 id: ' . $id . ' entitlementsArray: ' . print_r($entitlements,true));

			if( isset( $entitlements[$type] ) ) {
				if( isset( $entitlements[$type][$id] ) ) {
					return $entitlements[$type][$id];
				}
			} 

			if( $type == 'quiz' ) {
				// Legacy access
				switch( $id ) {
					case 4954: // Primary Care
					case 4955: // Internal Medicine
					case 2546: // Urgent Care
					case 2547: // Vascular
					case 2548: // Dermatology
					case 2336: // Ophthalmology
					case 1758: // General
					case 2614: // Emergency Medicine
					// case 8237: // Oncology -- wrong postID
					case 10651: // Oncology
						if( isset( $entitlements[$type] ) ) {
							if( isset( $entitlements[$type][1758] ) ) {
								return $entitlements[$type][1758];
							}
						}
						
						//return ACMSS()->get_assessment_access( $user_id, 'MSCAT' );
						break;
					
					case 1568:
						//return ACMSS()->get_assessment_access( $user_id, 'HIPAA' );
						break;
					
					/*
					case 11837: // continue on because access granted on Continuing Scribe Education Page
						return 1;
						break;
					case 11836:
						return 1;
						break;
					*/									
				}
			}

			return 0;
		}
	}		