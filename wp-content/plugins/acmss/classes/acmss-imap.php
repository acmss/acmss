<?php

	class ACMSS_IMAP {
		private $_imap;
//		private $_server = '{imap.secureserver.net:993/imap/ssl}';
//		private $_server = '{secure.emailsrvr.com:143}';
//		private $_server = '{imap.gmail.com:993/imap/ssl}';
//		private $_server = '{imap.gmail.com:993/imap/ssl}';
		private $_server = '{www-masters.com:143}';

		public function __construct( $console = false ) {
			// $this->_imap = imap_open( $this->_server.'INBOX', 'affidavits@theacmss.net', 'Czpr6lI46ZTc69', OP_DEBUG || OP_SECURE, 5 );
			// $this->_imap = imap_open( $this->_server.'INBOX', 'affidavits@theacmss.net', 'Czpr6lI46ZTc69' );
			// $this->_imap = imap_open( $this->_server.'INBOX', 'affidavits@theacmss.net', '0^uKKgz&RX!er', OP_DEBUG || OP_SECURE, 5 );
			// $this->_imap = imap_open( $this->_server.'INBOX', 'developer@theacmss.org', 'HappyHeart!' );
			// $this->_imap = imap_open( $this->_server.'INBOX', '1masterprogrammer@gmail.com', 'fit@@guy' );
			$this->_imap = imap_open( $this->_server.'INBOX', 'kschuster', '1fitguy1' );
			
			error_log('acmss-imap :: 15 :: this->_imap: ' . $this->_imap);
			
			if( $console && isset($_GET['mailboxes']) ) {
				$mailboxes = imap_list( $this->_imap, $this->_server, '*' );
				
				wp_die( '<pre>'.print_r( $mailboxes, true ).'</pre>' );
			}
			
			$msgs = imap_sort( $this->_imap, SORTDATE, 1, SE_UID );
			foreach ($msgs as $msguid) {
				$msgno = imap_msgno( $this->_imap, $msguid );
				$header = imap_header( $this->_imap, $msgno );
				error_log('header: ' . $header);
			}
			
			if( $this->_imap ) {
				// Getting all should cache for use by functions
				$headers = imap_headers( $this->_imap );
			}
		}

		public function process_affidavits( $console = false ) {
			if( $console ) echo '<h1>Processing Emails</h1>';

			$count = 64;

			if( $this->_imap  ) {
				$uids = array();

				if( $console ) echo "<p>Creating Folders...";

				$this->create_processed_folder( $console );

				$msgs = imap_sort( $this->_imap, SORTDATE, 1, SE_UID );

				foreach( $msgs as $msguid ) {
					if( $console ) echo "<p>Processing $msguid...";

					$msgno = imap_msgno( $this->_imap, $msguid );

					$header = imap_header( $this->_imap, $msgno );

					if( $affidavit_type = $this->is_affidavit( $header, $console )  ) {
						if( $user_email = $this->affidavit_to_email( $header ) ) {
	
							$user = get_user_by( 'email', $user_email );

							if( $user ) {
								if( $console ) echo "...$user_email exists...";
								
								$affidavit_type_found = true;
								
								switch( $affidavit_type ) {
									case 'mscat':
										ACMSS()->certify_user( null, true, $user->ID );
										break;

									case 'cmss: two step':
										ACMSS()->certify_user( null, 'CMSA', $user->ID );
										break;

									case 'cmss: one step':
										ACMSS()->certify_user( null, 'CMSS', $user->ID );
										break;

									case 'volunteer':
										update_user_meta( $user->ID, '_signed_volunteer_affidavit', true );
										break;

									case 'cap':
										update_user_meta( $user->ID, '_signed_cap_affidavit', true );
										break;

									default:
										if( $console ) echo "...<strong>affidavit type not found</strong>...";

										$affidavit_type_found = false;
										break;
								}

								if( $affidavit_type_found ) {
									$this->save_pdf( $user->ID, $affidavit_type, $msgno );

									ACMSS()->emails()->send_email( "$affidavit_type-affidavit-received", $user->ID );

								}
							} else {
								if( $console ) echo "...$user_email <em>doesn't exist</em>";

								if( time() - $header->udate > (60 * 60 * 24 * 21) ) {
									if( $console ) echo "...deleting";
									imap_delete( $this->_imap, $msgno );
								}
							}
						}
					} else {
						if( $console ) echo " header match failed.";						
					}

					$uids[] = imap_uid( $this->_imap, $msgno );

					if( $console ) echo "</p>";

					$count--;

					if( $count == 0 ) break;
				}

				foreach( (array) $uids as $uid ) {
					$r = imap_mail_move( $this->_imap, $uid, 'Processed', CP_UID );

					if( $console ) echo "<p>$uid: moving to Processed: ". ($r ? 'succeeded' : 'failed') ."</p>";
				}

		 		imap_expunge( $this->_imap );

				imap_close( $this->_imap, CL_EXPUNGE );
			} else {
				if( $console ) {
					echo "<p>Failed to open IMAP connection....because:";
						
					foreach (imap_errors() as $msg) {
						echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;$msg";
					}
					
				}
			}

			if( $console ) echo "<p>IMAP complete.</p>";
		}


		public function list_pending_affidavits() {

			if( $this->_imap  ) {
				$pending = array();

				$msgs = imap_sort( $this->_imap, SORTDATE, 1, SE_UID );

				foreach ($msgs as $msguid) {
					$msgno = imap_msgno( $this->_imap, $msguid );

					$header = imap_header( $this->_imap, $msgno );

					if( $affidavit_type = $this->is_affidavit( $header )  ) {

						if( !is_array( $pending[$affidavit_type] ) ) {
							$pending[$affidavit_type]  = array();
						}
						
						$pending[$affidavit_type][] = $this->affidavit_to_email( $header );
					}
				}

				imap_close( $this->_imap );

				if( count($pending) > 0 ) {
					foreach( $pending as $key => $emails ) {
						echo sprintf( '<h4>%s</h4>', strtoupper($key) );
						
						echo '<ul>';
						foreach ($emails as $email) {
							echo sprintf( '<li>%s</li>', strtolower($email) );	
						}
						echo '</ul>';
					}
				} else {
					echo '<h4>No pending affidavits</h4>';
				}
			} else {
				echo '<h4>Unable to connect to mail server!</h4>';
			}
		}

		private function affidavit_to_email( $imap_header ) {
			foreach ( $imap_header->to as $to_field ) {
				$mail_obj = $to_field;
				if( $mail_obj->host != 'theacmss.net' ) break;
			}

			if( $mail_obj->host != 'theacmss.net' ) {
				return $mail_obj->mailbox .'@'. $mail_obj->host;
			}
		}

		private function is_affidavit( $imap_header, $console = false ) {
			if( $console ) echo "...checking from address...";

			if( strpos( $imap_header->fromaddress, 'echosign@echosign.com' ) !== false ) {
				if( $console ) echo "...checking subject...";				
				
				if( preg_match( '/^Eligibility Affidavit - (.*) between American College of Medical Scribe Specialists/', $imap_header->subject, $matches ) ) {
					if( $console ) echo "...header matches...";

					$name = array_pop($matches);

					return strtolower( $name );
				} else {
					if( $console ) echo "...no match for <strong>$imap_header->subject</strong>";
				}
			} else {
				if( $console ) echo "...wrong from address....";
			}

			$uid = imap_uid( $this->_imap, $imap_header->Msgno );

			imap_setflag_full( $this->_imap, $uid, '\\Seen \\Flagged' );

			$r = imap_mail_move( $this->_imap, $uid, 'NonAffidavit', CP_UID );

			if( $console ) echo "... $r moved.";

			return false;
		}

		private function create_processed_folder( $console = false ) {
			// Create a Folder to put the processed ones in and the unwanted ones

			foreach( array( 'Processed', 'NonAffidavit' ) as $folder ) {
				if( $list = imap_getmailboxes( $this->_imap, $this->_server, $folder ) ) {
					if( !is_array($list) || count($list) < 1 ) {
						@imap_createmailbox( $this->_imap, imap_utf7_encode( $this->_server . $folder ) );
					} else {
						if( $console ) echo "<p>$folder folder exists.</p>";
					}
				}
			}
		}

		private function get_message_count( $console ) {
			$n_msgs = imap_num_msg( $this->_imap );

			if( $console ) echo "<p>$n_msgs messages to process.</p>";

			return $n_msgs;
		}

		private function save_pdf( $user_id, $affidavit_type, $msgno, $console = false ) {
			// Save PDF file and store
			$structure = imap_fetchstructure( $this->_imap, $msgno );

			$attachments = array();

			if( isset($structure->parts) && count($structure->parts) ) {
				for( $x = 0; $x < count($structure->parts); $x++ ) {
					$attachments[$x] = array(
						'is_attachment' => false,
						'filename' => '',
						'name' => '',
						'attachment' => ''
					);

					if( $structure->parts[$x]->ifdparameters ) {
						foreach($structure->parts[$x]->dparameters as $object) {
							if(strtolower($object->attribute) == 'filename') {
								$attachments[$x]['is_attachment'] = true;
								$attachments[$x]['filename'] = $object->value;
							}
						}
					}

					if($structure->parts[$x]->ifparameters) {
						foreach($structure->parts[$x]->parameters as $object) {
							if(strtolower($object->attribute) == 'name') {
								$attachments[$x]['is_attachment'] = true;
								$attachments[$x]['name'] = $object->value;
							}
						}
					}

					if($attachments[$x]['is_attachment']) {
						$attachments[$x]['attachment'] = imap_fetchbody( $this->_imap, $msgno, $x+1 );
						if($structure->parts[$x]->encoding == 3) { // 3 = BASE64
							$attachments[$x]['attachment'] = base64_decode($attachments[$x]['attachment']);
						}
						elseif($structure->parts[$x]->encoding == 4) { // 4 = QUOTED-PRINTABLE
							$attachments[$x]['attachment'] = quoted_printable_decode($attachments[$x]['attachment']);
						}
					}
				}
			}

			foreach ($attachments as $attachment) {
				if( $attachment['is_attachment'] == 1 ) {
					$filename = ACMSS()->get_file_save_dir("affidavits/{$affidavit_type}") ."/{$user_id} - {$attachment['filename']}.pdf";

					$contents = $attachment['attachment'];
					
					file_put_contents( $filename, $contents );

					if( $console ) echo "...attachment saved...";
				}
			}			
		}

		public static function fire_process_affidavits( $console = false ) {
			$imap = new ACMSS_IMAP( $console );

			$imap->process_affidavits( $console );

			unset($imap);

			if( $console ) wp_die( '', 'IMAP processing complete' );			
		}

		public static function fire_pending_affidavits() {
			$imap = new ACMSS_IMAP();

			$imap->list_pending_affidavits();

			unset($imap);
		}
	}