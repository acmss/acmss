<?php

	class ACMSS_Admin {
		public function __construct() {
			$me = __CLASS__;

			add_action( 'init', "$me::init" );
		}

		static public function init() {
			$me = __CLASS__;

			add_action( 'admin_menu', "$me::create_admin_menus" );
			// add_action( 'admin_init', "$me::admin_init", 10, 1 );
			add_action( 'edit_user_profile_update', "$me::profile_personal_options_update" );
			add_action( 'personal_options_update', "$me::profile_personal_options_update" );
			add_action( 'edit_user_profile', "$me::profile_personal_options" );
			add_action( 'show_user_profile', "$me::profile_personal_options" );	
			add_action( 'admin_notices', "$me::admin_update_password" );
			add_filter( 'manage_users_columns', "$me::user_columns", 50, 1 );
			add_action( 'manage_users_custom_column', "$me::user_column_values", 10, 3 );
			add_action( 'wp_dashboard_setup', "$me::remove_dashboard_widgets" );

			add_action( 'admin_enqueue_scripts', "$me::enqueue_scripts" );
		}

		static public function enqueue_scripts() {
			$acmss = array(
				'ajax_url' 		=> admin_url( 'admin-ajax.php' )
			);

			wp_enqueue_script( 'acmss-admin', plugin_dir_url(__FILE__).'../js/acmss-admin.js', array( 'jquery' ), ACMSS_VERSION, true );

			wp_localize_script( 'acmss-admin', 'acmss_global', $acmss );

			wp_enqueue_style( 'acmss-admin', plugin_dir_url(__FILE__).'../js/acmss-admin.css', ACMSS_VERSION, true );
		}
	
		static public function create_admin_menus() {
			$me = __CLASS__;

			add_submenu_page( 'edit.php?post_type=product', 'Product Entitlements', 'Product Entitlements', 'manage_options', 'product-entitlements', "$me::product_entitlements" );
		}

		static public function product_entitlements() {
			error_reporting(E_ERROR | E_WARNING | E_PARSE);

			include_once( plugin_dir_path( __FILE__ ) . "../admin-boxes/admin-box-product-entitlements.php" );
		}

		static public function admin_init() {
			$me = __CLASS__;
			remove_filter( 'views_users', array( 'Groups_Admin_Users', 'views_users' ) );
			add_filter( 'views_users', "$me::user_views", 10, 1 );
		}

		static public function add_user_cap( $user_id, $cap ) {
			if( $user = new WP_User( $user_id ) ) {
				$user->add_cap( $cap );
			}

			return get_class($user);
		}		

		static public function user_views( $views ) {
			$screen = get_current_screen();

			if ( $screen->id == 'users' ) {
				global $wpdb;

				$group_table = _groups_get_tablename( 'group' );

				$user_group_table = _groups_get_tablename( 'user_group' );

				$groups = $wpdb->get_results( "SELECT gt.group_id, gt.name, COUNT(ugt.user_id) as user_count FROM $group_table gt INNER JOIN $user_group_table ugt ON ugt.group_id = gt.group_id GROUP BY gt.group_id ORDER BY name" );
				$options = '';

				foreach( $groups as $group ) {
					$options .= sprintf( '<option value="%s"%s>%s (%s)</option>', $group->group_id, selected( $group->group_id, $_REQUEST['group'], false ), $group->name, $group->user_count );
				}
				
				$views[] = sprintf( '<form action="" method="post"><select name="group" onchange="this.form.submit()"><optgroup label="Filter list by group..."><option>All Groups</option>%s</optgroup></select></form>', $options );
			}

			return $views;
		}

		static public function profile_personal_options_update( $user_id ) {
			if( !empty($_POST) ) {
				if( current_user_can('administrator') ) {
					if( isset($_POST['convert_to_cmss']) && !empty($_POST['convert_to_cmss']) ) {
						ACMSS()->add_user_cap( $user_id, 'certified_'.$_POST['convert_to_cmss'] );

						Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 2 ) );
						Groups_User_Group::delete( array( 'user_id' => $user_id, 'group_id' => 17 ) );
					}

					if( isset($_POST['affidavit_received']) ) {
						update_user_meta( $user_id, '_signed_mscat_affidavit', true );
					}
					
					error_log('acmss-admin :: 102 cmss_affidavit_received: ' . $_POST['cmss_affidavit_received']);
					if( isset($_POST['cmss_affidavit_received']) ) {
						update_user_meta( $user_id, '_signed_corporate_member_affidavit', true );
					}
					if($_POST['cmss_affidavit_received'] == '') {
						error_log('cmss_affidavit_received EMPTY');
						delete_user_meta( $user_id, '_signed_corporate_member_affidavit', true );
					}
					
					// corporate logo received
					if( isset($_POST['cmss_logo_received']) ) {
						update_user_meta( $user_id, '_corporate_logo_received', true );
					}
					if($_POST['cmss_logo_received'] == '') {
						error_log('cmss_logo_received EMPTY');
						delete_user_meta( $user_id, '_corporate_logo_received', true );
					}
										
					// corporate curriculum received
					if( isset($_POST['cmss_curriculum_received']) ) {
						update_user_meta( $user_id, '_corporate_curriculum_received', true );
					}
					if($_POST['cmss_curriculum_received'] == '') {
						error_log('cmss_curriculum_received EMPTY');
						delete_user_meta( $user_id, '_corporate_curriculum_received', true );
					}
					
					// corporate curriculum approved
					if( isset($_POST['cmss_curriculum_approved']) ) {
						update_user_meta( $user_id, '_corporate_curriculum_approved', true );
					}
					if($_POST['cmss_curriculum_approved'] == '') {
						error_log('cmss_curriculum_approved EMPTY');
						delete_user_meta( $user_id, '_corporate_curriculum_approved', true );
					}
					
					// access MSCAT manual 
					// error_log('' . $_POST);
					if( isset($_POST['userCanReadManual']) ) {
						// set wp_cabilities to 'read_mscat_manual'
						error_log('set wp_capabilites to read_mscat_manual' . ' == userID: ' . $user_id);
						ACMSS()->add_user_cap( $user_id, 'read_mscat_manual' );
					}
					if( $_POST['userCanReadManual'] == '' ) {
						// remove 'read_mscat_manual' from wp_capabilities 
						error_log('unset read_mscat_manual in wp_capabilites');
						ACMSS()->remove_user_cap( $user_id, 'read_mscat_manual' );
					}						
									
/*
					if( isset($_POST['add_access']) ) {
						if ( strpos( $_POST['add_access'], 'Memberships' ) > 0 ) {
							// UPDATE TO ENTITLEMENTS
							//ACMSS()->adjust_owned_memberships_count( $user_id, intval( $_POST['add_memberships'])  );

						} else if ( strpos( $_POST['add_access'], 'Manual' ) > 0 ) {
							$inc = strpos( $_POST['add_access'], 'Remove' ) === false ? 1 : -1;

							// UPDATE TO ENTITLEMENTS
							//ACMSS()->add_manual_access_count( $user_id, $inc * self::increment_count('manual') );

						} else {
							if( strpos( $_POST['add_access'], 'MSCAT Certification' ) > 0 ) {
								$quiz = 'MSCAT';
							} else {
								$quiz = 'HIPAA';
							}
							
							$inc = strpos( $_POST['add_access'], 'Remove' ) === 0 ? -1 : 1;

							// UPDATE TO ENTITLEMENTS
							//ACMSS()->add_assessment_access( $user_id, $quiz, $inc * self::increment_count( $quiz ) );
						}

						add_action( 'admin_notices', "ACMSS::assessment_admin_notice" );
					}
*/
				}
			}
		}

		static public function assessment_admin_notice() {
			echo sprintf( '<div class="updated"><p>Additional access to %s added!</p></div>', $quiz );
		}	

		static public function increment_count( $field ) {
			$count_field = strtolower( "add_{$field}s" );

			if( isset($_POST[$count_field]) && !empty($_POST[$count_field]) ) {
				$increment = intval($_POST[$count_field]);
			} else {
				$increment = 1;
			}		

			return $increment;
		}

		static public function profile_personal_options( $user ) {
			if( current_user_can('administrator') ) {
				ACMSS_Entitlements::convert_assignments( $user->ID );

				$mscat_id = MSCAT_CERTIFICATE_PRODUCT_ID;
				$hipaa_id = HIPAA_CERTIFICATE_PRODUCT_ID;
				$membership_id = ACMSS_MEMBERSHIP_PRODUCT_ID;
				$membershipRenewal_id = 11959;
				$manual_id = MSCAT_MANUAL_PRODUCT_ID;
				
				$cse_Opthma1_id 	= 12205;
				$cse_Opthma2_id 	= 12204;
				$cse_compliance_id 	= 12206;
				$cse_em_id 			= 13602;
				$cse_ehipaa_id		= 1550;

				$mscat_count = ACMSS_Entitlements::user_has_assignable( MSCAT_CERTIFICATE_PRODUCT_ID, $user->ID );
				$hipaa_count = ACMSS_Entitlements::user_has_assignable( HIPAA_CERTIFICATE_PRODUCT_ID, $user->ID );
				$memberships = ACMSS_Entitlements::user_has_assignable( ACMSS_MEMBERSHIP_PRODUCT_ID, $user->ID );
				$manual_count = ACMSS_Entitlements::user_has_assignable( MSCAT_MANUAL_PRODUCT_ID, $user->ID );
				$membershipRenewals = ACMSS_Entitlements::user_has_assignable( 11959, $user->ID );
				
				$cse_Opthma1_count 		= ACMSS_Entitlements::user_has_assignable( 12205, $user->ID );
				$cse_Opthma2_count 		= ACMSS_Entitlements::user_has_assignable( 12204, $user->ID );
				$cse_compliance_count 	= ACMSS_Entitlements::user_has_assignable( 12206, $user->ID );
				$cse_em_count 			= ACMSS_Entitlements::user_has_assignable( 13602, $user->ID );
				$cse_ehipaa_count 		= ACMSS_Entitlements::user_has_assignable( 1550, $user->ID );

				if( ( user_can($user->ID, 'apprenticed_member') || user_can($user->ID, 'apprenticed_non_member') ) && !( user_can($user->ID, 'certified_member') || user_can( $user->ID, 'certified_non_member') ) ) {
					$member_state = user_can( $user->ID, 'apprenticed_member') ? 'member' : 'non_member';

					$convert_cmsa = <<< EOT
	<tr>
		<th colspan="2"><h3>Advance CMSA to CMSS</h3></th>
	</tr>
	<tr>
		<th><label>Advance user</label></th>
		<td><input type="checkbox" name="convert_to_cmss" value="$member_state"/> Check this box to advance the certification status of this user.</td>
	</tr>
EOT;
				} else {
					$convert_cmsa = '';
				}
				
				
				$cmssAffidavitChecked = checked( true, get_user_meta( $user->ID, '_signed_corporate_member_affidavit', true ), false );
				error_log('acmss-admin :: 211 cmssAffidavitChecked: ' . $cmssAffidavitChecked);
				$cmssLogoChecked = checked( true, get_user_meta( $user->ID, '_corporate_logo_received', true ), false );
				$cmssCurriculumSubmitted = checked( true, get_user_meta( $user->ID, '_corporate_curriculum_received', true), false );
				$cmssCurriculumApproved  = checked( true, get_user_meta( $user->ID, '_corporate_curriculum_approved', true), false );
				
				// add conditional for CMSS affidavit prompt
				if( user_can($user->ID, 'corporate_partner') ) {
					error_log('acmss-admin :: 202 userID: ' . $user->ID . ' corporate partner -- yes');
					$additionalCorporateMemberItems = ' 
						<tr>
						<th><label>CMSS Affidavitt</label></th>
						<td><input type="checkbox" name="cmss_affidavit_received" value="cmss_affidavit_received" ' . $cmssAffidavitChecked . ' /> 
							Check to mark this account as having an CMSS affidavit. (Checked indicates affidavit received)
						</td>
						</tr>
						<tr>
						<th><label>CMSS Logo</label></th>
						<td>
							<input type="checkbox" name="cmss_logo_received" value="cmss_logo_received" ' . $cmssLogoChecked . ' />
							Check to mark this account as having a CMSS logo.
						</td>
						</tr>
						<tr>
						<th><label>CMSS Curriculum plan submitted</label></th>
						<td>
							<input type="checkbox" name="cmss_curriculum_received" value="cmss_curriculum_received" ' . $cmssCurriculumSubmitted . ' />
							Check to mark this account as having submitted a CMSS Curriculum plan.
						</td>
						</tr>
						<tr>
						<th><label>CMSS Curriculum plan Approved</label></th>
						<td>
							<input type="checkbox" name="cmss_curriculum_approved" value="cmss_curriculum_approved" ' . $cmssCurriculumApproved . ' />
							Check to mark this account as having a Board Approved CMSS Curriculum plan.
						</td>
						</tr>
						
						
						';
				}
				else {
					error_log('acmss-admin :: 205 userID: ' . $user->ID . ' corporate partner -- no');
					$additionalCorporateMemberItems = '';
				}
				
				$certificationLevel = 'none';
				if( user_can($user->ID,'certified_member') || user_can($user->ID,'certified_non_member')) {
					error_log('CMSS');
					$certificationLevel = 'CMSS';
				}
				elseif( user_can($user->ID,'apprenticed_member') ) {
					error_log('CMSA');
					$certificationLevel = 'CMSA';
				}
				error_log('certification level: ' . $certificationLevel);

				$checked = checked( true, get_user_meta( $user->ID, '_signed_mscat_affidavit', true ), false );
				// $cmssAffidavitChecked = checked( true, get_user_meta( $user->ID, '_signed_corporate_member_affidavit', true ), false );

				$html = <<< EOT
				<table id="adjust-assignable" class="form-table">
					<tr>
						<th colspan="2">
							<h3>Assignable Products</h3>
							<p>Use these settings to allow $user->display_name <em>to assign products to other users</em>.</p>
						</th>
					</tr>
					<tr>
						<th class="right"><label>MSCAT Certification</label></th>
						<td>
							<button class="minus button" data-product-id="$mscat_id">-</button>
							<span class="current-total">$mscat_count</span>
							<button class="plus button button-primary" data-product-id="$mscat_id">+</button>
						</td>
					</tr>
					<tr>
						<th class="right"><label>HIPAA</label></th>
						<td>
							<button class="minus button" data-product-id="$hipaa_id">-</button>
							<span class="current-total">$hipaa_count</span>
							<button class="plus button button-primary" data-product-id="$hipaa_id">+</button>
						</td>
					</tr>
					<tr>
						<th class="right"><label>MSCAT Manual</label></th>
						<td>
							<button class="minus button" data-product-id="$manual_id">-</button>
							<span class="current-total">$manual_count</span>
							<button class="plus button button-primary" data-product-id="$manual_id">+</button>
						</td>
					</tr>
					<tr>
						<th class="right"><label>Memberships</label></th>
						<td>
							<button class="minus button" data-product-id="$membership_id">-</button>
							<span class="current-total">$memberships</span>
							<button class="plus button button-primary" data-product-id="$membership_id">+</button>
						</td>
					</tr>

					<tr>
						<th class="right"><label>Membership Renewals</label></th>
						<td>
							<button class="minus button" data-product-id="$membershipRenewal_id">-</button>
							<span class="current-total">$membershipRenewals</span>
							<button class="plus button button-primary" data-product-id="$membershipRenewal_id">+</button>
						</td>
					</tr>

					
					<tr>
						<th class="right"><label>CSE Opthamology Level 1</label></th>
						<td>
							<button class="minus button" data-product-id="$cse_Opthma1_id">-</button>
							<span class="current-total">$cse_Opthma1_count</span>
							<button class="plus button button-primary" data-product-id="$cse_Opthma1_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>
					
					<tr>
						<th class="right"><label>CSE Opthamology Level 2</label></th>
						<td>
							<button class="minus button" data-product-id="$cse_Opthma2_id">-</button>
							<span class="current-total">$cse_Opthma2_count</span>
							<button class="plus button button-primary" data-product-id="$cse_Opthma2_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>
					
					<tr>
						<th class="right"><label>CSE Compliance, Reporting, Documentation & HIPAA</label></th>
						<td>
							<button class="minus button" data-product-id="$cse_compliance_id">-</button>
							<span class="current-total">$cse_compliance_count</span>
							<button class="plus button button-primary" data-product-id="$cse_compliance_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>
					
					<tr>
						<th class="right"><label>CSE E&M </label></th>
						<td>
							<button class="minus button" data-product-id="$cse_em_id">-</button>
							<span class="current-total">$cse_em_count</span>
							<button class="plus button button-primary" data-product-id="$cse_em_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>
					
					<tr>
						<th class="right"><label>CSE e-HIPAA for Scribes</label></th>
						<td>
							<button class="minus button" data-product-id="$cse_ehipaa_id">-</button>
							<span class="current-total">$cse_ehipaa_count</span>
							<button class="plus button button-primary" data-product-id="$cse_ehipaa_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>										
				</table>
EOT;
				echo $html;
				
				$mscat_count = ACMSS_Entitlements::user_has_entitlement( 'quiz', MSCAT_QUIZ_ID, $user->ID );
				$hipaa_count = ACMSS_Entitlements::user_has_entitlement( 'quiz', HIPAA_QUIZ_ID, $user->ID );
				$manual_count = ACMSS_Entitlements::user_has_entitlement( 'group', MSCAT_MANUAL_PRODUCT_ID, $user->ID );
				
				$cse_Opthma1_count		= ACMSS_Entitlements::user_has_entitlement( 'quiz', 11837, $user->ID );
				$cse_Opthma2_count		= ACMSS_Entitlements::user_has_entitlement( 'quiz', 11836, $user->ID );
				$cse_compliance_count	= ACMSS_Entitlements::user_has_entitlement( 'quiz', 11839, $user->ID );
				$cse_em_count			= ACMSS_Entitlements::user_has_entitlement( 'quiz', 12213, $user->ID );
				

				$m_id = MSCAT_QUIZ_ID;
				$h_id = HIPAA_QUIZ_ID;
				
				$cse_Opthma1_id		= 11837;
				$cse_Opthma2_id		= 11836;
				$cse_compliance_id	= 11839;
				$cse_em_id			= 12213;

				$html = <<< EOT
				<table id="adjust-owned" class="form-table">
					$convert_cmsa
							
					<tr>
						<th><label>MSCAT Affidavitt</label></th>
						<td><input type="checkbox" name="affidavit_received" value="affidavit_received" $checked /> Check to mark this account as having an MSCAT affidavit. (Checked indicates affidavit already received)</td>
					</tr>
					<!--
					<tr>
						<th><label>CMSS Affidavitt</label></th>
						<td><input type="checkbox" name="cmss_affidavit_received" value="cmss_affidavit_received" $cmssAffidavitChecked /> Check to mark this account as having an CMSS affidavit. (Checked indicates affidavit already received)</td>
					</tr>					
					-->
					$additionalCorporateMemberItems		
					<tr>
						<th colspan="2">
							<h3>Individual Access</h3>
							<p>Use these settings to allow $user->display_name <em>to access these products themself</em>.</p>
						</th>
					</tr>
					<tr>
						<th class="right"><label>Access MSCAT Certification</label></th>
						<td>
							<button class="minus button" data-entitlement-id="$m_id">-</button>
							<span class="current-total">$mscat_count</span>
							<button class="plus button button-primary" data-entitlement-id="$m_id">+</button>
						</td>
					</tr>
					<tr>
						<th class="right"><label>Access HIPAA</label></th>
						<td>
							<button class="minus button" data-entitlement-id="$h_id">-</button>
							<span class="current-total">$hipaa_count</span>
							<button class="plus button button-primary" data-entitlement-id="$h_id">+</button>
						</td>
					</tr>
					
					<tr>
						<th class="right"><label>Access CSE Opthmaology Level 1</label></th>
						<td>
							<button class="minus button" data-entitlement-id="$cse_Opthma1_id">-</button>
							<span class="current-total">$cse_Opthma1_count</span>
							<button class="plus button button-primary" data-entitlement-id="$cse_Opthma1_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>					
					
					<tr>
						<th class="right"><label>Access CSE Opthmaology Level 2</label></th>
						<td>
							<button class="minus button" data-entitlement-id="$cse_Opthma2_id">-</button>
							<span class="current-total">$cse_Opthma2_count</span>
							<button class="plus button button-primary" data-entitlement-id="$cse_Opthma2_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>
					
					<tr>
						<th class="right"><label>Access Compliance, Reporting, Documentation & HIPAA</label></th>
						<td>
							<button class="minus button" data-entitlement-id="$cse_compliance_id">-</button>
							<span class="current-total">$cse_compliance_count</span>
							<button class="plus button button-primary" data-entitlement-id="$cse_compliance_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>
					
					<tr>
						<th class="right"><label>Access CSE E&M</label></th>
						<td>
							<button class="minus button" data-entitlement-id="$cse_em_id">-</button>
							<span class="current-total">$cse_em_count</span>
							<button class="plus button button-primary" data-entitlement-id="$cse_em_id">+</button> &nbsp;&nbsp;
						</td>
					</tr>
					
					<!--
					<tr>
						<th class="right"><label>Access MSCAT Manual</label></th>
						<td>
							<button class="minus button" data-entitlement-id="$manual_id">-</button>
							<span class="current-total">$manual_count</span>
							<button class="plus button button-primary" data-entitlement-id="">+</button>
						</td>
					</tr>
					-->
				</table>
EOT;
				echo $html;
				
				$userCanReadManualChecked = checked( true, user_can($user->ID, 'read_mscat_manual' ), false );
				// echo 'current user ID: ' . get_current_user_id();
				/*
				if(get_current_user_id() == 5627) {
					// echo 'Hello - It\'s me.<BR>';
					echo 'user can read mscat manual: ' . user_can($user->ID, 'read_mscat_manual') . '<BR>';
					echo '<input type="checkbox" name="userCanReadManual" value="userCanReadManual" ' . $userCanReadManualChecked . ' /> ';
					echo 'If checked, this user has access to the MSCAT Manual.<BR>';
				}
				*/
				echo '<input type="checkbox" name="userCanReadManual" value="userCanReadManual" ' . $userCanReadManualChecked . ' /> ';
				echo '<h2 style="display: inline-block;">If checked, this user has access to the MSCAT Manual.</h2><BR>';
				
				// get continuing ed from user_meta
				$userContinuingEd = get_user_meta( $user->ID, '_continuing_ed', true );
				error_log('383 :: ' . print_r($userContinuingEd,true));
				error_log('384 :: hours: ' . $userContinuingEd[0]['hours']);
				echo "<BR><h1>Continuing Education List</h1><BR>";
				$list = ACMSS()->continuing_ed_list();
				
				// display user input Continuing Education Hours
				if( $ce = get_user_meta( $user->ID, '_continuing_ed', true ) ) {
					echo '<table id="user_ce" style="width: 80%;">';
					echo '<thead><tr><th>Date</th><th>Topic</th><th>Hours</th><th></th></tr></thead>';

					$i = 0;
					foreach( $ce as $record ) {
						echo sprintf( '<tr><td>%s</td><td>%s</td><td>%s</td><td><!--<form method="post"><input type="hidden" name="ce_nonce" value="%s" /><input type="hidden" name="delete_id" value="%s" /><input class="delete_id" type="submit" value="Delete" /></form>--></tr>', $record['date'], $list[$record['topic']], $record['hours'], $nonce, $i );
						$i++;
					}
					echo '</table>';
				} else {
					echo '<p style="margin-top: 5px;">No Continuing Education recorded</p>';
				}
				echo "<BR><BR>";
			}
		}

		static public function admin_update_password() {

			if( isset( $_GET['new_pass'] ) ) {
				global $acmss_emails;

				$user_id = (int) $_GET['new_pass'];

				$password = wp_generate_password( 12, false );
				
				wp_set_password( $password, $user_id );
				
				$acmss_emails->send_email( 'acmss-login-notice', $user_id, array( 'password' => $password, 'login_url' => wp_login_url() ) );

				echo <<< EOT
		<div class="updated">
			<p>A new password has been created and sent to the user</p>
		</div>
EOT;
			}
		}

		static public function user_columns( $columns ) {
			if ( ! current_user_can( 'administrator' ) )
				return $columns;

			unset( $columns['role'] );
			unset( $columns['posts'] );
			unset( $columns['name'] );
			unset( $columns['groups_user_groups'] );
			unset( $columns['woocommerce_shipping_address'] );
			unset( $columns['woocommerce_paying_customer'] );
			
			$columns['acmss_name'] = 'Name';
			$columns['acmss_role'] = 'Role/Group';
			$columns['acmss_pdfs'] = 'Affidavits';
			$columns['acmss_pwd'] = 'New Password';

			return $columns;
		}

		static public function user_column_values( $value, $column_name, $user_id ) {
			switch ($column_name) {
				case 'acmss_pwd':
					$user_object = get_userdata( (int) $user_id );
					$value = sprintf( '<a href="?new_pass=%d" class="btn">Email new password</a>', $user_id );
					break;

				case 'acmss_name':
					$user_object = get_userdata( (int) $user_id );
					
					error_log('acmss-admin :: 550 user_registered: ' . $user_object->user_registered);
					$dateRegistered = $user_object->user_registered != '1970-01-01 00:00:00' ? date( 'M d, Y', strtotime($user_object->user_registered) ) : 'Unknown';
					// $dateRegistered = date( 'd M Y', strtotime($user_object->user_registered) );
					
					// get subscription status 
					// $returnedFromWcsFunction = wcs_user_has_subscription($userID);
					// error_log('returned by wcs_user_has_subscription: ' . $returnedFromWcsFunction);
					
					// get subscriptions
					$nextPaymentDatesArray = array();
					$subscriptions = wcs_get_users_subscriptions($user_id);
					// error_log('acmss-admin :: 568 subscriptions: ' . print_r($subscriptions,true));
					foreach($subscriptions as $subscriptionID => $subscription) {
						// error_log('acmss-admin :: 599 :: subscriptionArray: ' . print_r($subscription,true));
						// error_log('acmss-admin :: 570 orderNumber: ' . $subscription->get_order_number());
						// error_log('acmss-admin :: 571 nextPaymentDate: ' . $subscription->get_date_to_display( 'next_payment' ));
						$nextPaymentDate = strtotime($subscription->get_date_to_display( 'next_payment' ));
						// error_log('acmss-admin :: 574 nextPaymentDate timestamp: ' . $nextPaymentDate);
						$subscriptionStatus = $subscription->get_status();
						error_log('acmss-admin :: 604 :: get_status: ' . $subscriptionStatus);
						// array_push($nextPaymentDatesArray,$nextPaymentDate,$subscriptionStatus);
						$nextPaymentDatesArray[$nextPaymentDate] = $subscriptionStatus;
						
					}
					error_log('acmss-admin :: 610 :: nextPaymentDatesArray: ' . print_r($nextPaymentDatesArray,true));
					ksort($nextPaymentDatesArray);
					error_log('acmss-admin :: 611 :: nextPaymentDatesArray: ' . print_r($nextPaymentDatesArray,true));
					$keyValue = '';
					$status= '';
					foreach($nextPaymentDatesArray as $key => $value ){
						$keyValue = $key;
						$status = $value;
						error_log('acmss-admin :: 616 :: key: ' . $keyValue . ' value: ' . $status); 
					}
					
					error_log('acmss-admin :: 620 :: keyValue: ' . $keyValue . ' status: ' . $status);
					
					$nextPaymentDateSortedString = date("M j, Y",$keyValue);
					
					switch($status) {
						case 'active':
							$status = 'Renews ' . $nextPaymentDateSortedString ;
							break;
						case 'cancelled':
							$status = 'Cancelled';
							break;
						case 'pending-cancel':
						case 'pending':
							$status = 'Pending';
							break;
						case 'expired':
							$status = 'Expired';
							break;
						case 'on-hold':
						case 'suspend':
							$status = 'On Hold';
							break;
						case 'failed':
							$status = 'Failed';
							break;				
						default:
							$status = '?';	
					
					}
										
					// $renewalDate = $status . ' ' . $nextPaymentDateSortedString;
					
					$value = "$user_object->first_name $user_object->last_name<br><small style=\"font-style:italic;\">registered ". $dateRegistered;
					$value .="<BR>" . $status;
					$value .= "</small>";
					
					break;

				case 'acmss_role':
					$elements = array();

					$user = new WP_User( $user_id );
					if ( !empty( $user->roles ) ) {
						$elements[] = implode( ', ', (array) $user->roles );
					}

					$user = new Groups_User( $user_id );
					$elements[] = implode( ', ', array_map( create_function( '$o', 'return $o->name;'), $user->groups ) );

					$value = implode( ' / ', $elements );
					break;

				case 'acmss_pdfs':
					$files = array();

					foreach( array( 'mscat', 'volunteer', 'cap') as $folder ) {
						foreach ( glob( ACMSS()->get_file_save_dir("affidavits/{$folder}") . "/{$user_id} -*.pdf" ) as $filename ) {
							$files[] = array( $folder => str_replace( ABSPATH, '', $filename ) );
						}					
					}

					foreach ( $files as $file ) {
						foreach( $file as $key => $path) {
							$value .= sprintf( '<a href="/%s">%s</a>&nbsp;', $path, strtoupper($key) );
						}
					}
					break;
			}

			return $value;
		}

		static public function remove_dashboard_widgets(){
			remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   	// Right Now
			remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
			remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   	// Plugins
			remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  	// Quick Press
			remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
			remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   		// WordPress blog
			remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );		// Other WordPress News

			/*
			// removed Connecting to email server widget from admin dashboard 
			if( current_user_can('administrator') ) {
				wp_add_dashboard_widget( 'acmss_affidavit_pending_widget', 'Pending Affidavits', array( 'ACMSS_IMAP', 'fire_pending_affidavits' ) );
			}
			*/
			
		}
	}