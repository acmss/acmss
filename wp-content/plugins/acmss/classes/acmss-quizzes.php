<?php
	define( 'MSCAT_QUIZ_ID', 1758 );
	define( 'HIPAA_QUIZ_ID', 1568 );

	class ACMSS_Quizzes {
		static $me = __class__;

		public function __construct() {
			add_action( 'add_meta_boxes', self::$me.'::add_quiz_metabox' );
			add_action( 'save_post', self::$me.'::save_quiz_metaboxes' );			
			add_action( 'template_redirect', self::$me.'::template_redirect' );
		}

		static public function add_quiz_metabox() {
			add_meta_box( 'acmss-quiz-metabox', 'Auto-Pass Users', self::$me.'::display_quiz_metabox', 'sfwd-quiz', 'advanced', 'high' );
		}

		static public function save_quiz_metaboxes( $post_id ) {
			if ( !wp_verify_nonce( $_POST['fld_noncename'], plugin_basename(__FILE__) )) {
				return;
			}

			if ( !current_user_can( 'edit_post', $post_id ) ) {
				return;
			}

			// make sure meta is added to the post, not a revision
			if ( $the_post = wp_is_post_revision($post_id) ) {
				 $post_id = $the_post;
			}

			if( isset($_POST['auto_pass']) && !empty($_POST['auto_pass']) ) {
				$auto = $_POST['auto_pass'];

				$user_id = 0;

				if( !empty($auto['user']) && !empty($auto['pass_mark']) ) {
					if( is_numeric($auto['user']) ) {
						$user_id = intval($auto['user']);
					} else {
						if( $user = get_user_by( 'email', $auto['user'] ) ) {
							$user_id = $user->ID;
						}
					}

					if( $user_id > 0 ) {
						if( class_exists('LD_QuizPro') ) {
							$quizdata = array(
								'quiz' 			=> $post_id,
								'score' 		=> $auto['pass_mark'],
								'count' 		=> $auto['question_count'],
								'rank' 			=> true,
								'time' 			=> time(),
								'pro_quizid' 	=> $_POST['sfwd-quiz_quiz_pro'],
								'pass' 			=> 1,
								'points' 		=> $auto['pass_mark'],
								'total_points' 	=> $auto['question_count'],
								'percentage' 	=> $auto['pass_mark'],
								'timespent' 	=> null
							);

							$usermeta = get_user_meta( $user_id, '_sfwd-quizzes', true );
							$usermeta = (array) maybe_unserialize( $usermeta );
							$usermeta[] = $quizdata;

							update_user_meta( $user_id, '_sfwd-quizzes', $usermeta );
							
							// if user passed quiz
							// get user meta _continuing_education
							// add to new data
							// save / update user meta
							
						}
					}
				}
			}
		}


		static public function display_quiz_metabox() {
			global $post;

			wp_enqueue_script( 'acmss-admin' );

			$nonce = wp_create_nonce( plugin_basename(__FILE__) );

			$quiz = get_post_meta( $post->ID, '_sfwd-quiz', true );

			echo <<< EOT
<input type="hidden" name="fld_noncename" id="fld_noncename" value="$nonce" />
<table style="width:100%;">
	<tr>
		<td><label for="_user_id">User ID or email address</label><td>
		<td><input id="_user_id" name="auto_pass[user]" style="width:100%;"></td>
	</tr>
	<tr>
		<td><label for="_pass_mark">Score eg 78 (as in <strong>78</strong> out of 105) </label><td>
		<td><input id="_pass_mark" name="auto_pass[pass_mark]" style="width:100%;"></td>
	</tr>
	<tr>
		<td><label for="_question_count">Total Question Count eg 105 (as in 78 out of <strong>105</strong>) </label><td>
		<td><input id="_question_count" name="auto_pass[question_count]" style="width:100%;"></td>
	</tr>
</table>
EOT;
		}

		static public function template_redirect() {
			if( is_singular('sfwd-quiz') ) {
				if( !self::user_can_access_quiz() ) {

				}
			}
		}

		static public function user_can_access_quiz( $quiz_id = null, $user_id = null) {
			global $post;

			$quiz_id = is_null($quiz_id) ? $post->ID : $quiz_id;
			$user_id = is_null($user_id) ? get_current_user_id() : $user_id;
			error_log('Quizzes 115 :: quiz_id: ' . $quiz_id . ' user_id: ' . $user_id);
			$entitlements = get_user_meta( $user_id, '_entitlements', true );

			if( isset( $entitlements['quizzes'] ) ) {
				return isset( $entitlements['quizzes'][$quiz_id] ) && count($entitlements['quizzes'][$quiz_id]) > 0;
			} 

			return false;
		}
	}		