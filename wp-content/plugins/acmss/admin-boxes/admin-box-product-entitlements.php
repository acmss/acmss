	<?php 
		if( isset($_POST['entitlements']) ) {
			foreach( (array) json_decode( stripslashes($_POST['entitlements']) ) as $p ) {
				update_post_meta( intval( $p->product_id ), ENTITLEMENTS_KEY, $p->entitlements );
			}
		}

		wp_enqueue_script('jquery-ui-droppable'); 
	?>

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />
	<style>
		.entitlement {
			border-radius: 4px;
			float: left;
			border: 1px solid #CCC;
			background-color: #003D6D;
			color: #fff;
			padding: 5px;
			margin: 5px;
			white-space: nowrap;
			cursor: pointer;	
		}

		.entitlement.group {
			background-color: #0065B5;
		}

		.entitlement.cap {
			background-color: #459DE9;
		}

		.entitlement.email {
			background-color: #80b9ea;
		}

		.product-box .entitlement i::before {
			content: '\f00d';
		}

		.product-box { 
			width: 40%;
			min-height: 120px;
			border: 1px solid #CCC;
			background-color: #FEFEFE;
			padding: 5px;
			margin: 10px;
			border-radius: 4px;
			float: left;
		}
	
		.product-box.active {
			background-color: #DEDEFE;
		}

		.product-box h2 {
			font-size: 1.1em;
			padding: 0;
			font-weight: bold;
			line-height: 1.1em;			
		}

		#entitlements.fixed {
			position: fixed;
			top: 20px;
		}

		#entitlements h3 {
			clear: both;
			margin: 20px 0 0 0;
		}

		#entitlements h3:not(:first-child) {
			padding-top: 20px;
		}

		#entitlements-table td {
			vertical-align: top;
		}

		.fixedsticky {
		    position: -webkit-sticky;
		    position: -moz-sticky;
		    position: -ms-sticky;
		    position: -o-sticky;
		    position: sticky;
		}
		/* When position: sticky is supported but native behavior is ignored */
		.fixedsticky-withoutfixedfixed .fixedsticky-off,
		.fixed-supported .fixedsticky-off {
		    position: static;
		}
		.fixedsticky-withoutfixedfixed .fixedsticky-on,
		.fixed-supported .fixedsticky-on {
		    position: fixed;
		}
		.fixedsticky-dummy {
		    display: none;
		}
		.fixedsticky-on + .fixedsticky-dummy {
		    display: block;
		}

		#entitlements.fixedsticky { top: 60px; }
		#entitlements.fixedsticky-on { width: 35%; }

	</style>
	<div class="wrap">
		<h1>Product Entitlements</h1>

		<p>Use the page to select which entitlements (access to quizzes/services etc) each product or variation allows.</p>

		<p>To set, drag each entitlement into the approriate product box and then save.</p>

		<form id="entitlements-form" method="post" action="">
			<table id="entitlements-table">
				<tr>
					<td style="width: 60%">
						<?php
							$products = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1 ) );
							$current_val = array();

							function get_current_entitlements( $post_id ) {
								$meta = get_post_meta( $post_id, ENTITLEMENTS_KEY, true );
								
								if( $meta && !empty($meta) && count( (array) $meta ) > 0 ) {
									return array( 'product_id' => $post_id, 'entitlements' => $meta );
								}
							}
							
							while ( $products->have_posts() ) {
								$products->the_post();

								$product = wc_get_product( get_the_ID() );

								if( $product->has_child() ) { 
									global $variation;

									$variations = $product->get_available_variations();

									foreach( $variations as $variation ) {
										if( $c = get_current_entitlements( $variation['variation_id'] ) ) {
											$current_val[] = $c;
										}

										get_template_part( 'product-box', 'variation' );
									}
								} else {
									if( $c = get_current_entitlements( get_the_ID() ) ) {
										$current_val[] = $c;
									}

									get_template_part( 'product-box', 'product' );
								}
							}

							wp_reset_postdata();

							function draw_entitlement( $id, $class, $caption, $data = null ) {
								echo sprintf( '<span data-entitlement-id="%1$s" data-entitlement-class="%2$s" class="entitlement %2$s"><i class="fa fa-hand-paper-o"></i>&nbsp;%3$s</span>', $id, strtolower($class), ucwords( str_replace( '_', ' ', $caption ) )  );
							}
						?>
					</td>
					<td>
						<div id="entitlements" class="fixedsticky">
							<h3>Quiz Access</h3>
							<?php
								$quizzes = new WP_Query( array( 'post_type' => 'sfwd-quiz', 'posts_per_page' => -1 ) );

								while ( $quizzes->have_posts() ) {
									$quizzes->the_post();

									draw_entitlement( get_the_ID(), 'quiz', get_the_title() );
								}

								wp_reset_postdata();
							?>
							<h3>Group Membership</h3>
							<?php
								$groups = ACMSS_Groups::child_groups( ACMSS_Groups::assignable_groups_parent_id() );

								foreach ( $groups as $group ) {
									draw_entitlement( $group->group_id, 'group', $group->name );
								}
							?>
							<h3>Email to send on purchase</h3>
							<?php
								$emails = new WP_Query( array( 'post_type' => 'emails', 'posts_per_page' => -1 ) );

								while ( $emails->have_posts() ) {
									$emails->the_post();

									draw_entitlement( get_the_ID(), 'email', get_the_title() );
								}

								wp_reset_postdata();
							?>							
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="submit" class="button-primary" value="<?php esc_attr_e('Save'); ?>" />
					</td>
				</tr>
			</table>
			<input type="hidden" name="entitlements" value="<?php echo urlencode(json_encode($current_val)); ?>" />
		</form>
	</div>