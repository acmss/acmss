<?php

	require_once('mailchimp/Mailchimp.php');

	define( 'ACADEMIC_GROUP_ID', 4, true );
	define( 'CORPORATE_GROUP_ID', 5, true );
	define( 'STUDENT_GROUP_ID', 6, true );


	if( get_current_user_id() == 3 && $_GET['ml'] == true) {
		$ml = new ACMSS_Mailing_list();

		switch ($_GET['ml']) {
			case 'uml':
				echo '<p>Updating member list</p>';
				$ml->update_members_list();
				break;
			
			default:
				# code...
				break;
		}

		wp_die();
	}

	class ACMSS_Mailing_List {
		protected $_api;
		protected $_groups;

		public function __construct() {
			global $wpdb;

			set_time_limit( 300 );

			$this->_api = new Mailchimp();

			$this->_groups = array();

			$user_group_table = _groups_get_tablename( 'user_group' );

			foreach( array( ACADEMIC_GROUP_ID, CORPORATE_GROUP_ID, STUDENT_GROUP_ID ) as $group_id ) {
				$this->_groups[] = $wpdb->get_col( "SELECT user_id FROM $user_group_table WHERE group_id = $group_id" );
			}
		}

		public function update_members_list() {
			$batch = array();

			$list_id = $this->get_list_id('ACMSS Newsletter');

			foreach( get_users( array( 'role' => 'acmss_member' ) ) as $user ) {
				$batch[] = $this->get_subscribe_struct( $user );
			}

			$this->_api->call( 'lists/batch-subscribe', array( 'id' => $list_id, 'double_optin' => false, 'update_existing' => true, 'batch' => $batch ) );
		}

		public function subscribe_user( $user_id, $optin = true ) {
			$struct = $this->get_subscribe_struct( new WP_User( $user_id ) );

			$struct['id'] = $this->get_list_id('ACMSS Newsletter');
			$struct['double_optin'] = false;
			$struct['update_existing'] = true;

			$this->_api->call( 'lists/subscribe', $struct );
		}

		public function mail_subscription_renewals( $within_days = 30 ) {
			global $wpdb, $acmss_emails;

			$ids = $wpdb->get_col( "SELECT id FROM $wpdb->users WHERE DAYOFYEAR(user_registered) - DAYOFYEAR(CURDATE( )) BETWEEN 1 AND $within_days"); 

			foreach( $ids as $id ) {
				if( user_can( $id, 'acmss_member' ) ) {
					$acmss_emails->send_email( 'subscription-renewal-reminder', $id );
				}
			}
		}

		private function get_subscribe_struct( $user ) {
			$struct = array();
			$struct['merge_vars'] = array();

			$struct['email'] = array( 'email' => $user->user_email );
			$struct['email_type'] = 'html';
			$struct['merge_vars']['FNAME'] = $user->first_name;
			$struct['merge_vars']['LNAME'] = $user->last_name;
			$struct['merge_vars']['GROUPINGS'] = $this->get_user_membership_type( $user->ID );

			return $struct;
		}

		private function get_user_membership_type( $user_id ) {
			if( in_array( $user_id, $this->_groups[0] ) ) {
				$group_name = 'Academic Member';

			} else if( in_array( $user_id, $this->_groups[1] ) ) {
				$group_name = 'Corporate Member';

			} else if( in_array( $user_id, $this->_groups[2] ) ) {
				$group_name = 'Student Member';

			} elseif( user_can( $user_id, 'acmss_member' ) ) {
				$group_name = 'Individual Member';
			}

			if( $group_name )	{
				return array( array( 'name' => 'Membership Type', 'groups' => array($group_name) ) );
			}
		}

		private function get_list_id( $list_name ) {
			$lists = $this->_api->call( 'lists/list' );

			foreach( (array) $lists['data']  as $list ) {
				if( $list['name'] == $list_name ) {
					return $list['id'];
				}
			}

			return false;
		}
	}


	function acmss_update_mailing_subscription( $user_id, $old_user_data = null ) {
		$ml = new ACMSS_Mailing_List();

		$ml->subscribe_user( $user_id, false );
	}
	add_action( 'profile_update', 'acmss_update_mailing_subscription', 10, 2 );

	function acmss_add_mailing_subscription( $user_id ) {
		$ml = new ACMSS_Mailing_List();

		$ml->subscribe_user( $user_id );
	}
	add_action( 'user_register', 'acmss_add_mailing_subscription', 10, 1 );