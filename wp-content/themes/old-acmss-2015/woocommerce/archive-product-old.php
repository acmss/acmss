<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>

<div class="container acmss-store">
	<div class="row">
		<div class="col-md-9 col-lg-9 col-xs-12">
			<?php do_action( 'woocommerce_before_main_content' ); ?>

			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

				<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

			<?php endif; ?>

			<?php do_action( 'woocommerce_archive_description' ); ?>

			<?php 
				global $product, $variation;

				if ( have_posts() ) :

					do_action( 'woocommerce_before_shop_loop' );
					
					woocommerce_product_loop_start();

					woocommerce_product_subcategories();

					while ( have_posts() ) : 
						the_post();

						if( $product->has_child() ) { 
							foreach( $product->get_available_variations() as $variation ) {
								wc_get_template_part( 'content', 'variation' );
							}
						} else {
							wc_get_template_part( 'content', 'product' );
						}
					endwhile;
					
					woocommerce_product_loop_end();

					do_action( 'woocommerce_after_shop_loop' );
				
				elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) :
				
					wc_get_template( 'loop/no-products-found.php' );

				endif;
			
				do_action( 'woocommerce_after_main_content' );
			?>
		</div>
		<?php do_action( 'woocommerce_sidebar' ); ?>
	</div>
</div>

<?php get_footer( 'shop' );