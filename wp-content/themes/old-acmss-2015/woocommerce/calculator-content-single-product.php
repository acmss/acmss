<?php
/**
 * The template for displaying product content in the cart calculator
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php do_action( 'woocommerce_single_product_summary' ); ?>
</div>
