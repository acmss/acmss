<?php if( current_user_can('acmss_member') || current_user_can('certified_academic_partner') || current_user_can('corporate_partner') ) { ?>
<?php
	foreach( array( '_member_org', '_member_tel' ) as $key ) {
		if( isset( $_POST[$key] ) ) {
			update_user_meta( (int) get_current_user_id(), $key, (string) $_POST[$key] );
		} 
	}

	$meta = get_metadata( 'user', get_current_user_id() );

	global $wpdb;
	wp_enqueue_script( 'jquery-ui-autocomplete' );
	$group_table = _groups_get_tablename( 'group' );
	$org_groups = $wpdb->get_results( "SELECT group_id, name FROM $group_table WHERE parent_id = 16" );
?>

	<hr/>

	<script type="text/javascript">var acmss_groups = <?php echo json_encode($org_groups); ?>;</script>

	<style type="text/css">
		/* highlight results */
		.ui-autocomplete span.hl_results {
		    background-color: #ffff66;
		}
		 
		/* loading - the AJAX indicator */
		.ui-autocomplete-loading {
		    background: white url('../img/ui-anim_basic_16x16.gif') right center no-repeat;
		}
		 
		/* scroll results */
		.ui-autocomplete {
		    max-height: 250px;
		    overflow-y: auto;
		    /* prevent horizontal scrollbar */
		    overflow-x: hidden;
		    /* add padding for vertical scrollbar */
		    padding-right: 5px;
		}
		 
		.ui-autocomplete li {
		    font-size: 16px;
		}
		 
		/* IE 6 doesn't support max-height
		* we use height instead, but this forces the menu to always be this tall
		*/
		* html .ui-autocomplete {
		    height: 250px;
		}
	</style>

	<h2>ACMSS Membership Details</h2>

	<form method="post" action="">
		<div class="editfield">
			<label for="_member_org">Name of Organization or Employer (required)</label>
			<input name="_member_org" id="txtMemberOrg" value="<?php echo $meta['_member_org'][0]; ?>" style="width: 250px;" type="text" />
			<p class="description"></p>
		</div>

		<div class="editfield">
			<label for="_member_tel">Contact Telephone Number</label>
			<input name="_member_tel" id="txtMemberPhone" value="<?php echo $meta['_member_tel'][0]; ?>" style="width: 250px;" type="text">
			<p class="description"></p>
		</div>

		<input type="submit" value="Update" />
	</form>
        
        
        
        <style>
            
            .box {
               /* width: 20%;*/
                margin: 0 auto;
                background: rgba(255,255,255,0.2);
             /*   padding: 35px;
                border: 2px solid #fff;
                border-radius: 20px/50px;
                background-clip: padding-box;*/
                text-align: center;
              }
/*
              .button {
                font-size: 1em;
                padding: 10px;
                color: #fff;
                border: 2px solid orange;
                border-radius: 20px/50px;
                text-decoration: none;
                cursor: pointer;
                transition: all 0.3s ease-out;
              }
              .button:hover {
                background: orange;
              }
*/
              .overlay {
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                background: rgba(0, 0, 0, 0.7);
                transition: opacity 500ms;
                visibility: hidden;
                opacity: 0;
              }
              .overlay:target {
                visibility: visible;
                opacity: 1;
              }

          /*    .popup {
    background: #fff none repeat scroll 0 0;
    border-radius: 5px;
    left: 400px;
    padding: 20px;
    position: absolute;
    top: 750px;
    transition: all 5s ease-in-out 0s;
    width: 30%;
}*/
              .popup {
    background: #fff none repeat scroll 0 0;
    border-radius: 5px;
    margin:70px auto; /*738px 0 0 403px;*/
    padding: 20px;
    position: relative;
    transition: all 5s ease-in-out 0s;
    width: 33%;
}

              .popup h2 {
                margin-top: 0;
                color: #333;
                font-family: Tahoma, Arial, sans-serif;
              }
              .popup .close {
                position: absolute;
                top: 20px;
                right: 30px;
                transition: all 200ms;
                font-size: 30px;
                font-weight: bold;
                text-decoration: none;
                color: #333;
              }
              .popup .close:hover {
                color: orange;
              }
              .popup .content {
                max-height: 30%;
                overflow: auto;
              }
            
        </style>
        
        
<?php }
 
	if( $results = get_user_meta( get_current_user_id(), '_sfwd-quizzes', true ) )  
                {
		$quizzes = array();
		foreach( $results as $result ) {
			if( !in_array( $result['quiz'], $quizzes) ) {
				if( $result['pass'] == 1 ) {
					$link = learndash_certificate_details( $result['quiz'] );
					$post = get_post($result['quiz']);

					$links .= sprintf( '<p><a href="%s">Click to print your %s</a></p>', $link['certificateLink'], $post->post_title );
					$quizzes[] = $result['quiz'];
				}
			}
		}


		if( empty($links) ) {
			$links = '<p>You have not passed any assessments yet.</p>';
		}

		$update_pathway = do_shortcode('[cmss-affidavit-prompt]');

		echo "<hr/><h2>Assessment Certificates</h2>$update_pathway$links";
	}

	 /* Get order id , user belong to -- Osiyatech*/
	$customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_order_statuses() ),
         ) );
	 
	 /* Get Variation id , that product belong to -- Osiyatech*/
        /////////////////////////////////////////////////////////////////////////////////////////
         $customer_orders = get_posts( array(
                'numberposts' => $order_count,
                'meta_key'    => '_customer_user',
                'meta_value'  => get_current_user_id(),
                'post_type'   => wc_get_order_types('view-orders'),
                'post_status' => 'wc-completed'//array_keys(wc_get_order_statuses())
     ));
    
   // echo "<pre>";
     $membership = '';
     foreach($customer_orders as $key=> $order)
	 {
	     $order = new WC_Order($order->ID);
             $items = $order->get_items();
	     // print_r($items);
	     //echo $items[$key][name];
		 foreach($items as $key2=>$item2) 
		  { 
  		     $item2['package'];
		     $membership=$items[$key2]['name'];
		  } 
	 }/////////////////////////////////////////////////////////////////////////////////
        
	 //echo "<pre>";
	 //print_r($customer_orders);
	foreach($customer_orders as $key=> $order)
	 {
	     $order = new WC_Order($order->ID);
             $items = $order->get_items();
		 //print_r($items);
		 //echo $items[$key][name];
		 foreach($items as $key2=>$item2) 
		  { 
  		       //$item2['package'];
			   $item2['varation'];
		       echo $membership=$items[$key2]['name'];
		  } 
	 }
	 //echo "<pre>";
	 //  print_r($items); 
	 /* Get product class -- Osiyatech*/
	 global $product;  
         $current_user = wp_get_current_user();
         $flag=0;
	 /*
	  1743 = Corporate Membership 
	  10661 = Certified Academic Partner
	  10660 = Practice Membership
	  10667 = Non-Profit Partner
	  10664 = Vendor Sponsorship
	  
	 */
	 
	 /* Check  , package with product  -- Osiyatech*/ 
	  $prouct_arr =array(0=>1743,1=>10661,2=>10660,3=>8044,4=>10664);
	  for($i=0;$i<=count($prouct_arr);$i++)
	  {
	    if(wc_customer_bought_product( $current_user->user_email, $current_user->ID, $prouct_arr[$i]))
		{
                
		   if($membership=='Corporate Membership')
			    {$mylogo='partner-logo-corporate.png';$flag=1; }
		   if($membership=='Non-Profit Partner')
			    {$mylogo='partner-logo-non-profit-partner.png';$flag=1; }
		   if($membership=='Practice Membership')		
		        {$mylogo='Practice-Member.jpg';$flag=1; }
		   if($membership=='Vendor Sponsorship')		
		        {$mylogo='Vendor-Sponsorship.jpg';$flag=1;}
                    if($membership=='Certified Academic Partner')	
                        {$mylogo='Certified-Academic-Partner-(CAP)-Logo.jpg';$flag=1;}	
		   //echo $item2['package']; 	
		   switch($item2['varation'])
			 {
			 
			   case 'Bronze': $mylogo='Bronze-Corporate-Member.jpg';$flag=1; break;
			   case 'Silver': $mylogo='Silver-Corporate-Member.jpg';$flag=1; break;
			   case 'Gold': $mylogo='Gold-Corporate-Member.jpg';$flag=1; break;
			   case 'Diamond': $mylogo='Diamond-Corporate-Member.jpg'; $flag=1;break;
			   case 'Platinum': $mylogo='Platinum-Corporate-Member.jpg';$flag=1; break;
			   case 'Plantinum+': $mylogo='Platinum-Corporate-Member.jpg';$flag=1; break;
			   
			 } 
			
			  
		}
		
		 
	  }
          
          /* Show Coupon With membership */
          
            $customer_orders = get_posts( array(
                'numberposts' => $order_count,
                'meta_key'    => '_customer_user',
                'meta_value'  => get_current_user_id(),
                'post_type'   => wc_get_order_types('view-orders'),
                'post_status' => 'wc-completed'//array_keys(wc_get_order_statuses())
            ));
    
            foreach($customer_orders as $key=> $order)
            {
                 $order = new WC_Order($order->ID);
                 $items = $order->get_items();
                     foreach($items as $key2=>$item2) 
                      { 
                         $item2['package'];
                         $membership=$items[$key2]['name'];
                      } 
             }
             
           //echo "<pre>"  ;
           //print_r($items);
             
          $args = array(
            'posts_per_page'   => -1,
            'orderby'          => 'title',
            'order'            => 'asc',
            'post_type'        => 'shop_coupon',
            'post_status'      => 'publish',
            );
    
            $coupons = get_posts( $args );
            //$coupon_arr=array();
            //$i=0;
            //echo "<pre>";
            // print_r($coupons);
            echo '<hr/><h2>Eligible Coupons</h2>'; 
            foreach($coupons as $key=>$coupon)
            {
                
                if (stristr($coupon->post_title,$item2['package']) !== false)
                    echo '<p>'.$coupon->post_title.'</p>';  
                  //$coupon_arr[$i]= $coupon->post_title;
                //$i++;
                
            } 
                    
            //$coupon_arr[$i]='15%120715';
            //echo "<pre>";
            // print_r($coupons);            
            /* Show Coupon With membership */
          
          if($flag==1)
          { 
            echo sprintf( '<hr/><h2>Partner Benefits</h2><p>As a valued partner of the ACMSS, we welcome you to display the following logo on your website.</p><p><img src="%s/images/'.$mylogo.'" />
              <div class="box left-box clearfix">
               <a class="button" href="'.get_stylesheet_directory_uri().'/images/'.$mylogo.'" target="_blank" download>
Download Image </a>
           	    <a class="button" href="#popup1">Get Code</a>
                  </div></p>', get_stylesheet_directory_uri(), $logo );
          }
          
        ?>
        
	<hr/>
        <?php  
		if($flag==1)
          {  ?>
        <div id="popup1" class="overlay">
            <div class="popup">
                  <a class="close" href="#">×</a>
                    <div>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/></div>
                    <div class="content">
                        <code>&lt;a href='http://theacmss.org/wp-content/<br/>themes/acmss-2015/images<br/>/<?php echo $mylogo; ?>'&gt; <br/>&lt;img src='http://theacmss.org<br/>/wp-content/themes/acmss-2015/images/<?php echo $mylogo; ?>'&gt;<br/> &lt;/a&gt;</code>
                    </div>
            </div> 
        </div> 
        <?php  } ?>