<?php
	/*
 	 *	Template Name: ACMSS Non-Profit Partner Sponsorship
 	 */

	if( !isset( $_GET['order_id'] ) ) wp_safe_redirect( home_url() );

	get_header();

	echo do_shortcode( '[contact-form-7 id="8151" title="Sponsoring the ACMSS"]' );
?>
	<div class="content-area col-md-12 col-lg-12 col-sm-12 col-xs-12">
		<p><img src="http://theacmss.staging.wpengine.com/wp-content/themes/apicona/images/sign1.png" /></p>
		
		<h5><strong>Kristin Hagen, President/Chief Executive Officer</strong></h5>
		<p>ACMSS, a 501(c)6 non-profit corporation</p>
		<p><span>Corporate Office</span>: <span style="color:blue">657-888-2158</span></p>
		<p><span style="color:#F00">"Revolutionizing clinical care one certified scribe at a time." theacmss.org</span></p>

		<div class="social-box">
			<a href="https://twitter.com/theacmss"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png" title="Twitter" alt="Twitter"></a>
			<a href="https://www.facebook.com/TheACMSS.org"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb.png" title="Facebook" alt="Facebook"></a>
			<a href="https://www.linkedin.com/in/theacmss"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkdin.png" title="LinkedIn" alt="LinkedIn"></a>
		</div>

		<p>NOTICE: This message is confidential, intended for the named recipient(s) and may contain information that is (i) proprietary to the sender, and/or, (ii) privileged, confidential and/or otherwise exempt from disclosure under applicable state and federal law, including, but not limited to, privacy standards imposed pursuant to the federal Health Insurance Portability and Accountability Act of 1996 ("HIPAA"). Receipt by anyone other than the named recipient(s) is not a waiver of any applicable privilege. Thank you in advance for your compliance with this notice.</p>
	</div>

<?php get_footer();