<?php
/**
 * Checkout coupon form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! WC()->cart->coupons_enabled() ) {
	return;
}



$current_user = wp_get_current_user();

//if((wc_customer_bought_product( $current_user->user_email, $current_user->ID, 1641)|| wc_customer_bought_product( $current_user->user_email, $current_user->ID, 1743)|| wc_customer_bought_product( $current_user->user_email, $current_user->ID, 1555)||wc_customer_bought_product( $current_user->user_email, $current_user->ID, 1644))) 
{  

$info_message = apply_filters( 'woocommerce_checkout_coupon_message', __( 'Have a coupon?', 'woocommerce' ) . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>' );
wc_print_notice( $info_message, 'notice' );
?>

<form id="couponform" class="checkout_coupon" method="post" style="display:none">

	<p class="form-row form-row-first">
		<input type="text" name="coupon_code" class="input-text coupon_code" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
	</p>

	<p class="form-row form-row-last">
		<input type="submit" class="button coupon-button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />
		
	</p>
    <span id="error_span" style="color:red;float: left;"></span> 
	<div class="clear"></div> 
</form>
<?php 
}
?>

<script>
    
   jQuery(document).ready(function(){
       jQuery('#coupon_code').change(function(){
   jQuery('.coupon-button').attr('disabled','disabled');
   jQuery('#error_span').html('');
   
           var code = jQuery('#coupon_code').val();
           var a = Math.random();
           var url = ajaxurl+"?action=check_coupon&coupon="+code+"&a="+a; 
           jQuery.get(url,function(response){
		 // alert(response);   
               if(response==0)
               {
                   jQuery('#error_span').html('<br>Coupon Not applied');
                   jQuery('#coupon_code').val('');
               }
               if(response==2)
               {
                   jQuery('#error_span').html('<br>Membership required for   coupon');
                   jQuery('#coupon_code').val('');
               }
               if(response==3)
               {
                   jQuery('#error_span').html('<br>Practice Membership required 1 bundle at a time');
                   jQuery('#coupon_code').val('');
               }
      jQuery('.coupon-button').removeAttr('disabled');
           });
       }) ;
    });  
    
</script>  

