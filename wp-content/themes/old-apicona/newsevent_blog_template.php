<?php
/**
 * Template name:News And Event Blog
 */

get_header(); ?>


	<div id="primary" class="content-area <?php echo $primaryclass; ?>">
		<div id="content" class="site-content" role="main">
<div class="kwayy-blog-boxes col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="kwayy-blog-boxes-inner row kwayy-items-wrapper ">
<header class="kwayy-heading-wrapper kwayy-heading-wrapper-align-center"><div class="kwayy-heading-wrapper-inner"><h2 class="kwayy-heading-align-center"><span>LATEST NEWS</span></h2></div></header>
<?php 

 $args=array(
 'post_type'      => 'post', 
 'category__not_in' => array(93),
 'category__in' => array( 87),
 'order'          => 'DESC',
 'posts_per_page' => 1
 );
 $args2=array(
 'post_type'      => 'post', 
 'category__not_in' => array(93),
 'category__in' => array( 57),
 'order'          => 'DESC',
 'posts_per_page' => 1
 );
 $args3=array(
 'post_type'      => 'post', 
 'category__not_in' => array(93),
 'category__in' => array( 94),
 'order'          => 'DESC',
 'posts_per_page' => 1
 );
 $args4=array(
 'post_type'      => 'post', 
 'category__not_in' => array(93),
 'category__in' => array( 95),
 'order'          => 'DESC',
 'posts_per_page' => 1
 );
//'category__in' => array( 133),
//$blogpost->query("cat=10&author='. $post->post_author .'&order=DESC");
$blogpost = new WP_Query( $args );
 $blogpost2 = new WP_Query( $args2 );
 $blogpost3 = new WP_Query( $args3 );
 $blogpost4 = new WP_Query( $args4 );
 while ( $blogpost->have_posts() ) : $blogpost->the_post(); 

		?>
		<article class="post-box col-lg-4 col-sm-6 col-md-4 col-xs-12 kwayy-blogbox-format-standard ">
			<div class="post-item">
				<div class="post-item-thumbnail">
					<div class="post-item-thumbnail-inner">
						<div class="kwayy-postbox-small-date"><div class="kwayy-post-box-date-wrapper"><div class="kwayy-entry-date-wrapper">
								<span class="kwayy-entry-date">
									<time class="entry-date" >
										<span class="entry-date"><?php echo the_date('j'); ?> </span> 
										<span class="entry-month"><?php echo get_the_time('M'); ?></span> 
										<span class="entry-year"><?php echo get_the_time('Y'); ?></span> 
									</time>
								</span>
							</div></div></div>
											<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
 $thumb_url[0];
 if( $thumb_url[0]=='https://acmss.wpengine.com/wp-includes/images/media/default.png'){
?>

<a href="<?php echo get_category_link( 87 ); ?> "><div class="kwayy-proj-noimage"><i class="kwicon-fa-image"></i></div></a><?php } else { ?>
		
				<a href="<?php echo get_category_link( 87 ); ?>"><img width="370" height="202" src="<?php echo $thumb_url[0]; ?>" class="attachment-portfolio-three-column wp-post-image" alt="ACMSS"></a>
				<?php }  ?>
						
					</div>
					<div class="overthumb"></div>
					
				</div>
				<div class="item-content">
				<h4><a href="<?php echo get_category_link( 87 ); ?>"> <?php echo get_the_title(); ?> </a></h4>
					
					<div class="kwayy-meta-details"><div class="kwayy-post-user"><span class="author vcard"><i class="kwicon-fa-user"></i> <a class="url fn n" href="" title="View all posts by " rel="author"></a></span></div>	<?php $categories_list = get_the_category_list( __( ', ', 'apicona' ) ); ?>
		<span class="categories-links"><i class="kwicon-fa-folder-open"></i>  <?php echo $categories_list; ?></span></div>
					<div class="kwayy-blogbox-desc"></div>
				</div>
			</div>
		</article>
 <?php endwhile; 
  	 
		  wp_reset_query();?> 
		  <!-- second post-->
		  <?php while ( $blogpost2->have_posts() ) : $blogpost2->the_post(); 

		?>
		<article class="post-box col-lg-4 col-sm-6 col-md-4 col-xs-12 kwayy-blogbox-format-standard ">
			<div class="post-item">
				<div class="post-item-thumbnail">
					<div class="post-item-thumbnail-inner">
						<div class="kwayy-postbox-small-date"><div class="kwayy-post-box-date-wrapper"><div class="kwayy-entry-date-wrapper">
								<span class="kwayy-entry-date">
									<time class="entry-date" >
										<span class="entry-date"><?php echo the_date('j'); ?> </span> 
										<span class="entry-month"><?php echo get_the_time('M'); ?></span> 
										<span class="entry-year"><?php echo get_the_time('Y'); ?></span> 
									</time>
								</span>
							</div></div></div>
											<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
 $thumb_url[0];
 if( $thumb_url[0]=='https://acmss.wpengine.com/wp-includes/images/media/default.png'){
?>

<a href="<?php echo get_category_link( 57 ); ?>"><div class="kwayy-proj-noimage"><i class="kwicon-fa-image"></i></div></a><?php } else { ?>
		
				<a href="<?php echo get_category_link( 57 ); ?>"><img width="370" height="202" src="<?php echo $thumb_url[0]; ?>" class="attachment-portfolio-three-column wp-post-image" alt="ACMSS"></a>
				<?php }  ?>
						
					</div>
					<div class="overthumb"></div>
					
				</div>
				<div class="item-content">
				<h4><a href="<?php echo get_category_link( 57 ); ?>"> <?php echo get_the_title(); ?> </a></h4>
					
					<div class="kwayy-meta-details"><div class="kwayy-post-user"><span class="author vcard"><i class="kwicon-fa-user"></i> <a class="url fn n" href="" title="View all posts by " rel="author"></a></span></div>	<?php $categories_list = get_the_category_list( __( ', ', 'apicona' ) ); ?>
		<span class="categories-links"><i class="kwicon-fa-folder-open"></i>  <?php echo $categories_list; ?></span></div>
					<div class="kwayy-blogbox-desc"></div>
				</div>
			</div>
		</article>
 <?php endwhile; 
  	 
		  wp_reset_query(); ?>
		   <?php while ( $blogpost3->have_posts() ) : $blogpost3->the_post(); 

		?>
		<article class="post-box col-lg-4 col-sm-6 col-md-4 col-xs-12 kwayy-blogbox-format-standard ">
			<div class="post-item">
				<div class="post-item-thumbnail">
					<div class="post-item-thumbnail-inner">
						<div class="kwayy-postbox-small-date"><div class="kwayy-post-box-date-wrapper"><div class="kwayy-entry-date-wrapper">
								<span class="kwayy-entry-date">
									<time class="entry-date" >
										<span class="entry-date"><?php echo the_date('j'); ?> </span> 
										<span class="entry-month"><?php echo get_the_time('M'); ?></span> 
										<span class="entry-year"><?php echo get_the_time('Y'); ?></span> 
									</time>
								</span>
							</div></div></div>
											<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
 $thumb_url[0];
 if( $thumb_url[0]=='https://acmss.wpengine.com/wp-includes/images/media/default.png'){
?>

<a href="<?php echo get_category_link( 94 ); ?>"><div class="kwayy-proj-noimage"><i class="kwicon-fa-image"></i></div></a><?php } else { ?>
		
				<a href="<?php echo get_category_link(94 ); ?>"><img width="370" height="202" src="<?php echo $thumb_url[0]; ?>" class="attachment-portfolio-three-column wp-post-image" alt="ACMSS"></a>
				<?php }  ?>
						
					</div>
					<div class="overthumb"></div>
					
				</div>
				<div class="item-content">
				<h4><a href="<?php echo get_category_link( 94 ); ?>"> <?php echo get_the_title(); ?> </a></h4>
					
					<div class="kwayy-meta-details"><div class="kwayy-post-user"><span class="author vcard"><i class="kwicon-fa-user"></i> <a class="url fn n" href="" title="View all posts by " rel="author"></a></span></div>	<?php $categories_list = get_the_category_list( __( ', ', 'apicona' ) ); ?>
		<span class="categories-links"><i class="kwicon-fa-folder-open"></i>  <?php echo $categories_list; ?></span></div>
					<div class="kwayy-blogbox-desc"></div>
				</div>
			</div>
		</article>
 <?php endwhile; 
  	 
		  wp_reset_query(); ?>
		<h2 class="kwayy-heading-align-center"><span>LATEST EVENTS</span></h2>
		   <?php while ( $blogpost4->have_posts() ) : $blogpost4->the_post(); 

		?>
		<article class="post-box col-lg-4 col-sm-6 col-md-4 col-xs-12 kwayy-blogbox-format-standard ">
			<div class="post-item">
				<div class="post-item-thumbnail">
					<div class="post-item-thumbnail-inner">
						<div class="kwayy-postbox-small-date"><div class="kwayy-post-box-date-wrapper"><div class="kwayy-entry-date-wrapper">
								<span class="kwayy-entry-date">
									<time class="entry-date" >
										<span class="entry-date"><?php echo the_date('j'); ?> </span> 
										<span class="entry-month"><?php echo get_the_time('M'); ?></span> 
										<span class="entry-year"><?php echo get_the_time('Y'); ?></span> 
									</time>
								</span>
							</div></div></div>
											<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
 $thumb_url[0];
 if( $thumb_url[0]=='https://acmss.wpengine.com/wp-includes/images/media/default.png'){
?>

<a href="<?php echo get_category_link( 95 ); ?>"><div class="kwayy-proj-noimage"><i class="kwicon-fa-image"></i></div></a><?php } else { ?>
		
				<a href="<?php echo get_category_link( 95 ); ?>"><img width="370" height="202" src="<?php echo $thumb_url[0]; ?>" class="attachment-portfolio-three-column wp-post-image" alt="ACMSS"></a>
				<?php }  ?>
						
					</div>
					<div class="overthumb"></div>
					
				</div>
				<div class="item-content">
				<h4><a href="<?php echo get_category_link( 95 ); ?>"> <?php echo get_the_title(); ?> </a></h4>
					
					<div class="kwayy-meta-details"><div class="kwayy-post-user"><span class="author vcard"><i class="kwicon-fa-user"></i> <a class="url fn n" href="" title="View all posts by " rel="author"></a></span></div>	<?php $categories_list = get_the_category_list( __( ', ', 'apicona' ) ); ?>
		<span class="categories-links"><i class="kwicon-fa-folder-open"></i>  <?php echo $categories_list; ?></span></div>
					<div class="kwayy-blogbox-desc"></div>
				</div>
			</div>
		</article>
 <?php endwhile; 
  	 
		  wp_reset_query(); ?>
	</div></div>

		</div><!-- #content -->
	</div><!-- #primary -->
	

<?php get_footer(); ?>