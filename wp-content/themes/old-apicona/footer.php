<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */
 global $apicona;
?>
<?php /*if( !is_page() ){
		echo '</div>';
	}*/ ?>
				
		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
        	<div class="footer footer-text-color-<?php echo $apicona['footerwidget_color']; ?>">
			<?php /* ?>
              <div class="footersocialicon">
                  <div class="container">
                        <div class="row">
                          <div class="col-xs-12"><?php echo kwayy_get_social_links(); ?></div>
                        </div>                
                  </div>                
              </div>
			  <?php */ ?>
				<div class="container">
					<div class="row">
						<?php get_sidebar( 'footer' ); ?>
					</div>
				</div>
            </div>
			<div class="site-info footer-info-text-color-<?php echo $apicona['footertext_color']; ?>">
                <div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 copyright">
							<span class="kwayy_footer_text"><?php global $apicona; echo do_shortcode($apicona['copyrights']); ?></span> 
						</div><!--.copyright -->
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 kwayy_footer_menu">
							<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'footer-nav-menu', 'container' => false ) ); ?>
					  </div>
                    </div><!--.row -->
				</div><!-- .site-info -->
			</div><!-- .container -->
		</footer><!-- #colophon -->
	</div><!-- #page -->
	
	</div><!-- .main-holder.animsition -->

    <a id="totop" href="#top" style="display: inline;"><i class="kwicon-fa-angle-up"></i></a>
    
	<?php
	if( isset($apicona['custom_js_code']) && trim($apicona['custom_js_code'])!='' ){
		echo '<script type="text/javascript"> /* Custom JS Code */ '.$apicona['custom_js_code'].'</script>';
	}
	?>
	<script type="text/javascript">
        jQuery(function($) {
        
            //set active state for store sidebar products
			$('.postid-1641').find('ul.sidebarProd li').eq(0).addClass('pActive pActive1')
            $('.postid-1743').find('ul.sidebarProd li').eq(1).addClass('pActive pActive2')
            $('.postid-10661').find('ul.sidebarProd li').eq(2).addClass('pActive pActive3')
            $('.postid-10660').find('ul.sidebarProd li').eq(3).addClass('pActive pActive4')
			
			//swap out product photo on product detail page
			$('#product-1641 .images a').attr('href','<?php echo get_template_directory_uri(); ?>/images/ACM_Ind2.jpg')
			$('#product-1743 .images a').attr('href','<?php echo get_template_directory_uri(); ?>/images/ACM_Corp2.jpg')
			$('#product-10661 .images a').attr('href','<?php echo get_template_directory_uri(); ?>/images/ACM_Acedemic.jpg')
			$('#product-10660 .images a').attr('href','<?php echo get_template_directory_uri(); ?>/images/ACM_Practice_r3.jpg')
			
			$('#product-1641 .images a img').attr('src','<?php echo get_template_directory_uri(); ?>/images/ACM_Ind2.jpg')
			$('#product-1743 .images a img').attr('src','<?php echo get_template_directory_uri(); ?>/images/ACM_Corp2.jpg')
			$('#product-10661 .images a img').attr('src','<?php echo get_template_directory_uri(); ?>/images/ACM_Acedemic.jpg')
			$('#product-10660 .images a img').attr('src','<?php echo get_template_directory_uri(); ?>/images/ACM_Practice_r3.jpg')
			//change package name on store landing
			$('.post-1641 h3').html('Individual Package')
			$('.post-1743 h3').text('Corporate Package')
			$('.post-10661 h3').text('Academic Package')
			$('.post-10660 h3').text('Practice Package')
			
			
				$('.post-type-archive .breadcrumbs').html('<span class="homeLink">Home</span><span><a rel="v:url" property="v:title" title="Go to Products." href="/store/" class="post post-page">Store/</a></span><span><a rel="v:url" property="v:title" title="Go to Products." href="/store/select-a-specialty/membership-types" class="post post-page current-item">Membership Types</a></span>')
				
				$('.single-product .breadcrumbs span:nth-child(3) a').addClass('post post-page')
				$('.single-product .breadcrumbs span:nth-child(4) a').addClass('current-item')
			
			
			
        })//end jdoc
    
    </script>
	<?php wp_footer(); ?>
    
</body>
</html>
