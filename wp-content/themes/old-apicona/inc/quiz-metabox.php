<?php

	function acmss_add_quiz_metabox() {
		add_meta_box( 'acmss-quiz-metabox', 'Auto-Pass Users', 'acmss_display_quiz_metabox', 'sfwd-quiz', 'advanced', 'high' );
	}
	add_action( 'add_meta_boxes', 'acmss_add_quiz_metabox' );

	function acmss_save_quiz_metaboxes( $post_id ) {
		if ( !wp_verify_nonce( $_POST['fld_noncename'], plugin_basename(__FILE__) )) {
			return;
		}

		if ( !current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// make sure meta is added to the post, not a revision
		if ( $the_post = wp_is_post_revision($post_id) ) {
			 $post_id = $the_post;
		}

		if( isset($_POST['auto_pass']) && !empty($_POST['auto_pass']) ) {
			$auto = $_POST['auto_pass'];

			$user_id = 0;

			if( !empty($auto['user']) && !empty($auto['pass_mark']) ) {
				if( is_numeric($auto['user']) ) {
					$user_id = intval($auto['user']);
				} else {
					if( $user = get_user_by( 'email', $auto['user'] ) ) {
						$user_id = $user->ID;
					}
				}

				if( $user_id > 0 ) {
					if( class_exists('SFWD_SlickQuiz') ) {
						//ob_start();

						//$quiz = new SFWD_SlickQuiz();

						$_POST['score'] = $auto['pass_mark'];
						$_POST['userID'] = $user_id;
						$_POST['postID'] = $post_id;
						$_POST['questionCount'] = $auto['question_count'];


						$usermeta = get_user_meta( $user_id, '_sfwd-quizzes', true );
						$usermeta = maybe_unserialize( $usermeta );
						if ( !is_array( $usermeta ) ) $usermeta = array();
						
						$quiz = get_post_meta( $_POST['postID'], '_sfwd-quiz', true);

						$passingpercentage = intVal($quiz['sfwd-quiz_passingpercentage']);

						$pass = ( $_POST['score'] * 100 / $_POST['questionCount'] >= $passingpercentage ) ? 1 : 0;

						$quizdata = array( 'quiz' =>  $_POST['postID'], 'score' => $_POST['score'], 'count' => $_POST['questionCount'], 'pass' => $pass, 'rank' => $_POST['levelRank'], 'time' => time() );

						$usermeta[] = $quizdata;

						$quizdata['quiz'] = get_post( $_POST['postID']);
						$courseid = learndash_get_course_id( $_POST['postID']);
						$quizdata['course'] = get_post($courseid);
						$quizdata['user_id'] = $user_id; // Used to force passing and emails to correct person
						
						do_action( 'learndash_quiz_completed', $quizdata ); //Hook for completed quiz
						
						update_user_meta( $user_id, '_sfwd-quizzes', $usermeta );						

						//$quiz->finish_quiz();

//						ob_end_clean();
					}
				}
			}
		}
	}
	add_action( 'save_post', 'acmss_save_quiz_metaboxes' );

	function acmss_display_quiz_metabox() {
		global $post;

		wp_enqueue_script( 'acmss-admin' );

		$nonce = wp_create_nonce( plugin_basename(__FILE__) );

		$quiz = get_post_meta( $post->ID, '_sfwd-quiz', true );

		echo <<< EOT
	<input type="hidden" name="fld_noncename" id="fld_noncename" value="$nonce" />
	<table style="width:100%;">
		<tr>
			<td><label for="_user_id">User ID or email address</label><td>
			<td><input id="_user_id" name="auto_pass[user]" style="width:100%;"></td>
		</tr>
		<tr>
			<td><label for="_pass_mark">Score eg 78 (as in <strong>78</strong> out of 105) </label><td>
			<td><input id="_pass_mark" name="auto_pass[pass_mark]" style="width:100%;"></td>
		</tr>
		<tr>
			<td><label for="_question_count">Total Question Count eg 105 (as in 78 out of <strong>105</strong>) </label><td>
			<td><input id="_question_count" name="auto_pass[question_count]" style="width:100%;"></td>
		</tr>
	</table>
EOT;
	}