<?php
	/**
	**	Functions related to the woocommerce plugin
	**/

	define( 'STUDENT_MEMBERSHIP_PRODUCT_ID', 1656 );
	define( 'MSCAT_CERTIFICATE_PRODUCT_ID', 1508 );
//	define( 'MSCAT_OPTHALMOLOGY_PRODUCT_ID', 2445 );	
	define( 'HIPAA_CERTIFICATE_PRODUCT_ID', 1550 );
	define( 'MSCAT_MANUAL_PRODUCT_ID', 1555 );
	define( 'INDIVIDUAL_MEMBERSHIP_PRODUCT_ID', 1641 );
	define( 'CORPORATE_MEMBERSHIP_PRODUCT_ID', 1743 );
	define( 'ACADEMIC_MEMBERSHIP_PRODUCT_ID', 1644 );
	define( 'FACILITY_ACCREDITATION_PRODUCT_ID', 1748 );
	define( 'NON_SCRIBE_BUNDLES_PRODUCT_ID', 2015 );
	define( 'SCRIBE_BUNDLES_PRODUCT_ID', 2301 );
	

	function acmss_vary_woo_template( $template, $slug, $name ) {
		if( $slug == 'content' && $name = 'single-product' ) {
			add_filter( 'woocommerce_product_single_add_to_cart_text', function( $title ) { return 'Calculate Total Cost'; }  );
			add_filter( 'woocommerce_quantity_input_args', function( $args, $product = null ) { $args['input_value'] = $_POST['quantity']; return $args; }  );

			add_filter( 'gettext', 
				function($translated, $text, $domain) {
					switch ($text) {
						case 'Cart Totals':
							return 'Calculated Totals';
							break;

						case 'Cart Subtotal':
							return 'Standard Cost';
							break;

						case 'Order Total':
							return 'Discounted Cost';
							break;

						default:
							return $translated;
							break;
					}
				}
				, 10, 3 );

			$template = get_stylesheet_directory() . '/woocommerce/calculator-content-single-product.php';
		}
		return $template;
	}



	// Showing "You Save $0.00" is a bit pointless, so let's remove it from the price html
	function acmss_hide_zero_savings(  $variation = '', $obj = '' , $variation_obj  = '' ) {
		$variation['price_html'] = str_replace( '<br><span class="price normal_price savings">You Save <span class="amount"><span class="amount">&#36;0.00</span></span></span>', '', $variation['price_html'] );

		return $variation;
	}
	add_filter( 'woocommerce_available_variation', 'acmss_hide_zero_savings', 99, 3 );

	function acmss_cart_totals_before_order_total() {
		global $woocommerce;

		if( isset($woocommerce->cart->discount_total) && $woocommerce->cart->discount_total > 0 ) {
			$total_discount = '<em>&ndash; '. wc_price( $woocommerce->cart->discount_total ) . '</em>';


			$html = <<< EOT
			<tr class="discount-total">
				<th style="white-space: nowrap;">Customer Discount</th>
				<td>$total_discount</td>
			</tr>
EOT;
			echo $html;
		}
	}
	add_action( 'woocommerce_cart_totals_before_order_total', 'acmss_cart_totals_before_order_total' );
	add_action( 'woocommerce_review_order_before_order_total', 'acmss_cart_totals_before_order_total' );



	function acmss_apply_discounts( $cart ) {
		$membership = new WC_Product( INDIVIDUAL_MEMBERSHIP_PRODUCT_ID );
		$membership_price = $membership->price;

		foreach( $cart->get_cart() as $cart_item_key => $item ) {
			if( isset($item['variation']) && isset($item['variation']['attribute_bundles']) ) {
				$membership_price =  $item['variation']['attribute_membership'] == 'Without Membership' ? 0 : $membership->price;

				foreach( acmss_membership_discounts() as $threshold => $discount ) {
					if( ( $item['quantity'] >= $threshold ) ) {
						$cart->discount_total += ($item['quantity'] * $membership_price) * ($discount / 100);
						break;
					}
				}

				foreach( acmss_product_discounts() as $threshold => $discount ) {
					if( ( $item['quantity'] >= $threshold ) ) {
						$cart->discount_total += ($item['line_total'] - ($item['quantity'] * $membership_price)) * ($discount / 100);
						break;
					}
				}
			}
		}
	}
	add_action( 'woocommerce_calculate_totals', 'acmss_apply_discounts', 10, 1 );

	function acmss_membership_discounts() {
		//return array( 1600 => 81.25, 800 => 80, 400 => 78.75, 200 => 77.5, 100 => 76.25, 25 => 75, 5 => 50 );
	}
	
	function acmss_product_discounts() {
		//return array( 500 => 50, 450 => 45, 400 => 40, 350 => 35, 300 => 30, 250 => 25, 200 => 20, 150 => 15, 100 => 10, 50 => 5 );
	}

	function acmss_store_buy_assessments() {
		if( isset($_GET['add_assessments']) ) {
			$html = <<< EOT
<div class="messageBox alert icon" style="margin: 20px 0">
	<p><strong>You have no access to the {$_GET['add_assessments']} Assessment<strong>. You can purchase further access right here, right now!</p>
</div>
EOT;
			echo $html;			
		}
	}
	add_action( 'woocommerce_before_main_content', 'acmss_store_buy_assessments', 25 );

	function acmss_store_promotion() {
		if( !is_archive() ) return;

		$html = <<< EOT
<div class="messageBox info icon">
	<h2>New Year's Celebration kick off!</h2>
	<p>For a limited time, receive $5 off any purchase. Enter coupon code: <em>Certified Scribe</em>.</p>
</div>
EOT;
		echo $html;
	}
	//add_action( 'woocommerce_before_main_content', 'acmss_store_promotion' );


/*
	function acmss_woocommerce_product_query( $q, $this ) {
		if( current_user_can( 'certified_academic_partner' ) == false && current_user_can( 'administrator' ) == false ) {
			$q->query_vars['post__not_in'][] = STUDENT_MEMBERSHIP_PRODUCT_ID;
		}

		if( current_user_can( 'corporate_partner_administrator' ) == false && current_user_can( 'administrator' ) == false ) {
			$q->query_vars['post__not_in'][] = SCRIBE_BUNDLES_PRODUCT_ID;
		}

		if( current_user_can( 'corporate_partner_administrator' ) == true && current_user_can( 'administrator' ) == false ) {
			$q->query_vars['post__not_in'][] = NON_SCRIBE_BUNDLES_PRODUCT_ID;
		}
	}
	//add_action( 'woocommerce_product_query', 'acmss_woocommerce_product_query', 10, 2 );
*/

	function acmss_proceed_to_checkout() {
		echo '<a href="/store" class="button btn alt">Continue Shopping</a>';
	}
	add_action( 'woocommerce_proceed_to_checkout', 'acmss_proceed_to_checkout' );


	function acmss_member_prices( $price, $product ) {
		global $woocommerce_tiered_pricing;

		if( $product->id != 2015 ) {
			$role = 'ignite_level_52b062edeab25';

			if( $woocommerce_tiered_pricing->settings['show_savings'] == 'yes' ) { 
				$meta = get_post_custom( $product->id );

				if( isset( $meta["_{$role}_price"] ) ) {
					$reg_price = $meta['_regular_price'][0];

					$price = woocommerce_price( $reg_price );

					$role_price = $meta["_{$role}_price"][0];

					if ( floatval( $role_price ) < floatval( $reg_price ) ) {
						$price .= '<br><span class="price normal_price savings">Member Price: <span class="amount">' . woocommerce_price( $role_price ) . '</span></span>';
					}
				}
			}
		} 

		return $price;
	}
	if( !is_user_logged_in() ) {
		add_filter( 'woocommerce_sale_price_html', 'acmss_member_prices', 1, 2 );
		add_filter( 'woocommerce_price_html', 'acmss_member_prices', 1, 2 );
		add_filter( 'woocommerce_empty_price_html', 'acmss_member_prices', 1, 2 );
	}

	function acmss_free_hipaa( ) {
		global $woocommerce;

		$cap_bundle = sanitize_text_field( 'cap bundle' ) ;

   		if ( $woocommerce->cart->applied_coupons ) {
			foreach ( $woocommerce->cart->applied_coupons as $index => $code ) {
				if( $code == $cap_bundle ) {
					unset( $woocommerce->cart->applied_coupons[$index] );
					break;
				}
			}
		}
		
		if( !$woocommerce->cart->add_discount( $cap_bundle ) ) {
			$woocommerce->show_messages();
		}
	}	
	//add_action( 'woocommerce_before_cart_table', 'acmss_free_hipaa' );

	function acmss_completed_order( $order_id, $old_status = '', $new_status = '' ) {
		global $woocommerce, $acmss_emails;

		if( $new_status != 'processing' ) return;

		$order = new WC_Order( $order_id );
		$user_id = $order->user_id;

		foreach( $order->get_items() as $item_id => $item ) {
			if( in_array( $item['product_id'], array(INDIVIDUAL_MEMBERSHIP_PRODUCT_ID, CORPORATE_MEMBERSHIP_PRODUCT_ID, ACADEMIC_MEMBERSHIP_PRODUCT_ID, FACILITY_ACCREDITATION_PRODUCT_ID ) ) ) {
				acmss_add_user_cap( $user_id, 'acmss_member' );
			}

			switch( $item['product_id'] ) {
				case NON_SCRIBE_BUNDLES_PRODUCT_ID:
					acmss_add_bundles( $user_id, (int) $item['variation_id'], (int) $item['qty'] );
					break;

				case SCRIBE_BUNDLES_PRODUCT_ID:
					acmss_add_bundles( $user_id, (int) $item['variation_id'], (int) $item['qty'] );
					break;

				case STUDENT_MEMBERSHIP_PRODUCT_ID:
					acmss_add_bundles( $user_id, (int) $item['variation_id'], (int) $item['qty'] );
					break;

				case INDIVIDUAL_MEMBERSHIP_PRODUCT_ID:
					$acmss_emails->send_email( 'welcome-to-the-acmss', $user_id );
					break;

				case CORPORATE_MEMBERSHIP_PRODUCT_ID:
					Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 5 ) );
					$acmss_emails->send_email( 'welcome-to-the-acmss', $user_id );
					break;

				case ACADEMIC_MEMBERSHIP_PRODUCT_ID:
					Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 4 ) );
					$acmss_emails->send_email( 'cap-welcome-to-the-acmss', $user_id );
					break;

				case FACILITY_ACCREDITATION_PRODUCT_ID:
					Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 7 ) );
					$acmss_emails->send_email( 'welcome-to-the-acmss', $user_id );
					break;

				case MSCAT_MANUAL_PRODUCT_ID:
					acmss_add_user_cap( $user_id, READ_MSCAT_MANUAL );
					$acmss_emails->send_email( 'acmss-mscat-manual', $user_id );
					break;

				case HIPAA_CERTIFICATE_PRODUCT_ID:
					$count = is_acmss_member( $user_id ) ? 3 : 1;

					acmss_add_assessment_access( $user_id, 'HIPAA', $count * $item['qty'] );			

					$acmss_emails->send_email( 'accessing-the-hipaa-assessment', $user_id );
					break;

				case MSCAT_CERTIFICATE_PRODUCT_ID:
					$count = is_acmss_member( $user_id ) ? 3 : 1;

					acmss_add_assessment_access( $user_id, 'MSCAT', $count * $item['qty'] );

					$acmss_emails->send_email( 'accessing-the-mscat-certification', $user_id );
					break;
			}
		}

		wp_schedule_single_event( time() + 30, 'acmss_order_complete', array( $order_id ) );
	}
	add_action( 'woocommerce_order_status_changed', 'acmss_completed_order', 5, 3 );
 

	function acmss_mark_order_as_complete( $order_id ) {
		$order = new WC_Order( $order_id );
		$order->update_status( 'completed' );
	}
	add_action( 'acmss_order_complete', 'acmss_mark_order_as_complete' );

	function amcss_get_catalog_ordering_args( $args ) {
	  $orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	 
		if ( 'random_list' == $orderby_value ) {
			$args['orderby'] = 'rand';
			$args['order'] = '';
			$args['meta_key'] = '';
		}
	 
		return $args;
	}
	add_filter( 'woocommerce_get_catalog_ordering_args', 'amcss_get_catalog_ordering_args' );

	function acmss_woo_custom_cart_button_text() {
		global $woocommerce;

		$pid = get_the_ID();

		foreach( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
			$_product = $values['data'];
		
			if( $pid == $_product->id  ) {
				return __( 'Add Again?', 'woocommerce' );
			}
		}

		return __( 'Add to cart', 'woocommerce' );
	}
	add_filter( 'single_add_to_cart_text', 'acmss_woo_custom_cart_button_text' );
	add_filter( 'add_to_cart_text', 'acmss_woo_custom_cart_button_text' );


	function acmss_donation() {
		if( !is_archive() ) return;

		$html = <<< EOT
<p>Contribute to the responsible growth of the medical scribe industry. Foster the professional development of Certified Medical Scribe Specialists to improve operational efficiency of Emergency Departments.  Contributors recognized.</p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
	<input type="hidden" name="cmd" value="_donations">
	<input type="hidden" name="business" value="store@theacmss.org">
	<input type="hidden" name="lc" value="US">
	<input type="hidden" name="item_name" value="The American College of Medical Scribe Specialists">
	<input type="hidden" name="no_note" value="0">
	<input type="hidden" name="currency_code" value="USD">
	<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_LG.gif:NonHostedGuest">
	<input type="image" src="http://108.167.189.27/~avyat/wp-content/themes/acmss-2015/images/contribute-button.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
<p>Our <a href="/about-us/privacy-policy">notice of privacy practices</a> is available for you here.</p>
<p><em>ACMSS Refund Policy is 30 days.</em> In the event that you are dissatisfied, please send inquiries to <a href="mailto:store@theacmss.org">store@theacmss.org</a>. Thank you.</p>
EOT;
		echo $html;
	}
	add_action( 'woocommerce_after_main_content', 'acmss_donation' );


	function test( $query ) {
		return str_replace( 'AND 0 = 1', '', $query );
	}
	add_filter( 'posts_where', 'test' );


	function acmss_has_user_bought( $user_id, $product_id ) {
		$ordered_products = acmss_get_user_orders( $user_id );

		foreach ( (array) $ordered_products as $p_id ) {
			if( $p_id == $product_id ) return true;
			
			$product = new WC_Product($product_id);
		}

		return false;
	}

/*if( current_user_can('administrator') ) {
	acmss_has_user_bought( 455, 21 );
}*/
	function acmss_get_user_orders( $user_id, $status = 'completed') {
		if( !$user_id ) return false;

		$orders = array();//order ids

		$args = array(
			'posts_per_page'  => -1,
			'meta_key'        => '_customer_user',
			'meta_value'      => $user_id,
			'post_type'       => 'shop_order',
			'post_status'     => 'all',
			'tax_query' => array(
				array(
					'taxonomy'  => 'shop_order_status',
					'field'     => 'slug',
					'terms'     => $status
				)
			)
		);

		if( $posts = new WP_Query($args) ) {
			if( $orders = wp_list_pluck( $posts->get_posts(), 'ID' ) ) {
				return $orders;	    	
			}
		}

if( current_user_can('administrator') ) {
	global $wpdb; echo $wpdb->last_query;
	echo 'orders';
		print_r($args);
	}
	}


	function acmss_remove_reviews_tab($tabs) {
		unset($tabs['reviews']);
		return $tabs;
	}
	add_filter( 'woocommerce_product_tabs', 'acmss_remove_reviews_tab', 98);

	function acmss_order_complete( $order_id ) {
		$order = new WC_Order( $order_id );

		if ( $order->user_id > 0 ) {
			//update_user_meta( $order->user_id, 'paying_customer', 1 );

			//$old_count = absint( get_user_meta( $order->user_id, '_order_count', true ) );
			//update_user_meta( $order->user_id, '_order_count', $old_count + 1 );
		}
	}
	add_action( 'woocommerce_order_status_completed', 'acmss_order_complete' );

	function acmss_add_cart_item_data( $cart_item_meta, $product_id ) {
		if( $product_id == STUDENT_MEMBERSHIP_PRODUCT_ID ) {
			$cart_item_meta['student_count'] = 1;

			if( isset($_POST['student_count']) ) {
				$cart_item_meta['student_count'] = (int) $_POST['student_count'];
			}
		}

		return $cart_item_meta;
	}
	add_filter( 'woocommerce_add_cart_item_data', 'acmss_add_cart_item_data', 10, 2 );


	function acmss_register_at_checkout() {
		global $woocommerce;

		foreach( $woocommerce->cart->cart_contents as $cart_item ) {
			if( has_term( 'must-register', 'product_tag', $cart_item['product_id'] ) ) {
				return true;
			}
		}
	
		return false;		
	}


	/** Force uses to create an account when certain products are purchased **/
	function acmss_woocommerce_params( $woocommerce_params ) {
		global $woocommerce;

		if( acmss_register_at_checkout() ) {
			$woocommerce_params['option_guest_checkout'] = 'no';
		}
		$woocommerce_params['ajax_url'] = admin_url('admin-ajax.php');

		return $woocommerce_params;
	}
	add_filter( 'woocommerce_params', 'acmss_woocommerce_params' );


	function acmss_checkout_init( $checkout ) {
		if( acmss_register_at_checkout() ) {
			$checkout->must_create_account = true;
			$checkout->enable_guest_checkout = false;
		}
	} 
	add_action( 'woocommerce_checkout_init', 'acmss_checkout_init' );	
