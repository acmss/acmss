<?php

	function acmss_definitions_list() {
		$terms = new WP_Query( array( 'post_parent' => 1581, 'post_type' => 'mscat-page', 'posts_per_page' => -1 ) );
		
		while( $terms->have_posts() ) {
			$terms->next_post();
			echo str_replace( array('<dl>', '</dl>'), array( '', '' ), $terms->post->post_content );
		}

		exit();
	}
	add_action( 'wp_ajax_term_search', 'acmss_definitions_list' );

	function acmss_get_assessment_list() {
		$assessments = 0;
		$strings = array();

		foreach( array( 'HIPAA' => '/quizzes/hipaa-assessment', 'MSCAT' => '/start-mscat-certification' ) as $key => $link )  {
			$a = acmss_get_assessment_access( get_current_user_id(), $key );			

			if( $a > 0 ) {
				$assessments++;

				$strings[] = sprintf( '<h3><a href="%s">Access the %s Assessment</a> <em>%d %s remaining</em></h3>', $link, $key, $a, $a == 1 ? 'attempt' : 'attempts' );
			} else {
				if( in_array( $key, array( 'HIPAA', 'MSCAT') ) ) {
					$strings[] = sprintf( '<h3>You currently do not have access to the %s Assessment. <a href="/store">Purchase access here.</a></h3>', $key );
				}
			}
		}

		echo json_encode( array( 'user_id' => get_current_user_id(), 'assessments' => $assessments, 'html' => '<h2>Your Assessment access</h2>' . implode( '', $strings ) ) );

		exit();
	}
	add_action( 'wp_ajax_get_assessment_list', 'acmss_get_assessment_list' );
	add_action( 'wp_ajax_nopriv_get_assessment_list', 'acmss_get_assessment_list' );

	function acmss_member_assign() {
		$user_id = get_current_user_id();
		$item = $_POST['product_item'];

		switch($item) {
			case 'mscat-quiz':
				$item_count = acmss_get_assessment_access( $user_id, 'MSCAT' );
				break;

			case 'hipaa-quiz':
				$item_count = acmss_get_assessment_access( $user_id, 'HIPAA' );
				break;

			case 'mscat-manual':
				$item_count = (int) get_user_meta( $user_id, '_mscat_manual_access', true );
				break;

			case 'acmss-membership':
				$item_count = acmss_adjust_owned_memberships_count( $user_id );
				break;

			default:
				exit();
		}

		$members = $_POST['members'];
		$assigned = 0;

		if( ($item == 'acmss-membership' && $item_count >= 1 ) || ($item == 'mscat-manual' && $item_count >= count($members)) ||  ($item != 'mscat-manual' && ($item_count / 3) >= count($members)) ) {

			foreach( $members as $member_id ) {
				switch($item) {
					case 'mscat-quiz':
						acmss_add_assessment_access( $member_id, 'MSCAT', 3 );
						$assigned -= 3;
						break;

					case 'hipaa-quiz':
						acmss_add_assessment_access( $member_id, 'HIPAA', 3 );
						$assigned -= 3;
						break;

					case 'mscat-manual':
						if( !user_can( $member_id, READ_MSCAT_MANUAL) ) {
							acmss_add_user_cap( $member_id, READ_MSCAT_MANUAL );
							$assigned--;
						}
						break;

					case 'acmss-membership':
						global $acmss_emails;
						$acmss_emails->send_email( 'welcome-to-the-acmss', $member_id );
						$assigned--;
				}
			}

			// Remove the assigned items from the current user's allocation - !!$ASSIGNED WILL BE NEGATIVE!!!
			switch($item) {
				case 'mscat-quiz':
					acmss_add_assessment_access( $user_id, 'MSCAT', $assigned );
					break;

				case 'hipaa-quiz':
					acmss_add_assessment_access( $user_id, 'HIPAA', $assigned );
					break;

				case 'mscat-manual':
					update_user_meta( $user_id, '_mscat_manual_access', $item_count + $assigned );
					break;

				case 'acmss-membership':
					acmss_adjust_owned_memberships_count( $user_id, $assigned );
					break;
			}			

			$list = acmss_get_member_list( $user_id, true );
		} else {
			$item_count = -1;
			$message = 'You do not have sufficient items to make those assignments.';
		}

		echo json_encode( array( 'message' => $message, 'item_count' => $item_count + $assigned, 'list' => $list ) );

		exit();
	}
	add_action( 'wp_ajax_member_assign', 'acmss_member_assign' );
	add_action( 'wp_ajax_nopriv_member_assign', 'acmss_member_assign' );	