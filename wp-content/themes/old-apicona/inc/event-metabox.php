<?php

	function acmss_add_event_metabox() {
		add_meta_box( 'acmss-metaboxes', 'Event Details', 'acmss_display_event_metabox', 'events', 'advanced', 'high' );
	}
	add_action( 'add_meta_boxes', 'acmss_add_event_metabox' );

	function acmss_save_event_metaboxes( $post_id ) {
		if ( !wp_verify_nonce( $_POST['fld_noncename'], plugin_basename(__FILE__) )) {
			return;
		}

		if ( !current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// make sure meta is added to the post, not a revision
		if ( $the_post = wp_is_post_revision($post_id) ) {
			 $post_id = $the_post;
		}

		update_post_meta( $post_id, '_location', $_POST['_location'] );
		update_post_meta( $post_id, '_eventLat', $_POST['_eventLat'] );
		update_post_meta( $post_id, '_eventLng', $_POST['_eventLng'] );
		update_post_meta( $post_id, '_eventDate', strtotime( $_POST['_eventDate'] ) );
	}
	add_action( 'save_post', 'acmss_save_event_metaboxes' );

	function acmss_display_event_metabox() {
		global $post;

		wp_enqueue_script( 'acmss-maps' );
		wp_enqueue_script( 'acmss-admin' );

		$meta = get_post_meta( $post->ID );
		$nonce = wp_create_nonce( plugin_basename(__FILE__) );
		$date = date( 'Y-m-d', $meta['_eventDate'][0] );

		echo <<<EOT
	<input type="hidden" name="fld_noncename" id="fld_noncename" value="$nonce" />
	<table style="width:100%;">
		<tr>
			<td><label for="_location">Location</label><td>
			<td><textarea rows="4" id="_location" name="_location" style="width:100%;">{$meta['_location'][0]}</textarea></td>
			<td rowspan="2">
				<div id="gmap_canvas" style="width: 450px; height: 420px; margin: 25px;"></div>

				<input type="button" value="Search for current location" id="btnGMapsSearch" />

				<p>...or click on the map to select a location. The marker can be dragged around once you've clicked or searched.</p>

				<input type="hidden" name="_eventLat" id="_eventLat" value="{$meta['_eventLat'][0]}"/>
				<input type="hidden" name="_eventLng" id="_eventLng" value="{$meta['_eventLng'][0]}" />
			</td>
		</tr>
		<tr>
			<td><label for="_eventDate">Event Date</label><td>
			<td><input type="text" name="_eventDate" style="width:100%;" maxlength="45" value="{$date}"><br/><span class="description">Enter in YYYY-MM-DD format.</span></td>
		</tr>
	</table>
EOT;

	}