<?php
	class ACMSS_Emails {
		protected $target_user;

		public function __construct() {
			add_action( 'init', array( $this, 'init' ) );
			add_shortcode( 'email', array( $this, 'email_shortcode' ) );
			add_filter( 'shortcode_atts_acmss_email', array( $this, 'shortcode_atts' ) );
		}


		public function shortcode_atts( $out = null, $pairs = null, $atts  = null ) {
			if( isset( $out['user_id'] ) ) {
				$out['user_id'] = $this->target_user->ID;
			}

			return $out;
		}

		public function email_shortcode( $atts, $content = '' ) {
			extract( shortcode_atts( array(
        			'field' 	=> 'first_name',
    		), $atts ) );

    		switch( $field ) {
    			default:
    				if( isset($this->target_user->$field) ) {
    					return $this->target_user->$field;
    				}
    		}

			return '';
		}

		public function init() {
			$args = array (
						'label' 			=> 'emails',
						'labels' 			=> array (
							'name'			=> 'Emails',
							'singular_name' => 'Email',
							'all_items' 	=> 'All Emails',
							'add_new_item' 	=> 'Add New Email',
							'edit_item' 	=> 'Edit Email',
							'new_item' 		=> 'New Email',
							'view_item' 	=> 'View Email',
							'search_items'	=> 'Search Emails',
							'not_found' 	=> 'No emails found',
							'not_found_in_trash' => 'No emails found in Trash',
							'menu_name' 	=> 'Emails'
						),
						'rewrite' 			=> array( 'slug' => 'emails', 'with_front' => false ),
						'has_archive' 		=> true,
						'public' 			=> true,
						'show_ui' 			=> true,
						'show_in_menu' 		=> true,
						'supports' 			=> array( 'title', 'editor' )
					);
			register_post_type( 'emails', $args );
		}

		public function send_email( $email_name, $user_id, $args = null ) {
			if( empty($user_id) ) {
				$user_id = get_current_user_id();
			}

			if( is_integer($user_id) ) {
				$this->target_user = new WP_User( $user_id );
			} else {	// Allow an email address to be passed in
				$this->target_user = get_user_by( 'email', $user_id );
			}

			if( !is_null($args) ) {
				foreach ($args as $key => $value) {
					$this->target_user->$key = $value;
				}
			}

			add_filter( 'wp_mail_content_type', array( $this, 'wp_mail_type') );
			add_filter( 'wp_mail_from', array( $this, 'wp_mail_from' ) );

			$user_email = $this->target_user->data->user_email;

			if( get_class($this->target_user) == 'WP_User' ) {
				$email = $this->get_email_post( $email_name );

				if( $email != false  ) {
					$subject = do_shortcode( $email->post_title );
					$message = wpautop( do_shortcode( $email->post_content ) );

//					wp_mail( WEB_DEV, 'Email content', "U: $user_email, S: $subject, M: $message." );

					$r = wp_mail( $user_email, $subject, $message );

					if( $r = false  ) {
						error_log ( "Failed to send $email_name to $user_email." );
					} else {
						error_log ( "Email $email_name sent to $user_email." );
					}
				} else {
					wp_mail( WEB_DEV, 'Email send failed', "Could not locate page $email_name." );
				}

			} else {
				wp_mail( WEB_DEV, 'Email send failed', get_class($this->target_user) );
			}

			remove_filter( 'wp_mail_from', array( $this, 'wp_mail_from' ) );
			remove_filter( 'wp_mail_content_type', array( $this, 'wp_mail_type') );

			unset($this->target_user);
		}

		public function wp_mail_type() {
			return 'text/html';
		}

		public function wp_mail_from( $original_email_address ) {
			return 'no-reply@theacmss.org';
		}		

		private function get_email_post( $email_name ) {
			global $wpdb;

			if( empty($email_name) ) return false;

			$r = $wpdb->get_row( "SELECT * FROM $wpdb->posts WHERE post_name = '$email_name' AND post_type = 'emails'" );

			error_log ( ( is_null($r) ? 'Failed to find' : 'Found' ) . " $email_name: " . get_current_user_id() );

			return is_null($r) ? false : $r;
		}
	}

	global $acmss_emails;
	$acmss_emails = new ACMSS_Emails();