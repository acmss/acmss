<?php

	class ACMSS_IMAP {
		private $_imap;
		//private $_server = '{imap.secureserver.net:993/imap/ssl}';
		private $_server = '{secure.emailsrvr.com:143}';

		public function __construct() {
			//$this->_imap = imap_open( $this->_server.'INBOX', 'affidavits@theacmss.org', 'Li8It1UV6Mvd', OP_DEBUG, 5 );
			$this->_imap = imap_open( $this->_server.'INBOX', 'admin@theacmss.net', 'xlb306*WSwb%9*', OP_DEBUG, 5 );

			if( $this->_imap ) {
				// Getting all should cache for use by functions
				imap_headers( $this->_imap );
			}
		}

		public function process_affidavits( $console = false ) {
			if( $console ) echo '<h1>Processing Emails</h1>';

			$count = 64;

			if( $this->_imap  ) {
				$uids = array();

				$this->create_processed_folder( $console );

				$msgs = imap_sort( $this->_imap, SORTDATE, 1, SE_UID );
				
				foreach ($msgs as $msguid) {
					if( $console ) echo "<p>Processing $msguid...";

					$msgno = imap_msgno( $this->_imap, $msguid );

					$header = imap_header( $this->_imap, $msgno );

					if( $affidavit_type = $this->is_affidavit( $header, $console )  ) {
						$user_email = $this->affidavit_to_email( $header );

						$user = get_user_by( 'email', $user_email );

						if( $user ) {
							if( $console ) echo "...$user_email exists...";
							
							$affidavit_type_found = true;
							
							switch( $affidavit_type ) {
								case 'mscat':
									acmss_certify_user( null, true, $user->ID );
									break;

								case 'cmss: two step':
									acmss_certify_user( null, 'CMSS', $user->ID );
									break;

								case 'cmss: one step':
									acmss_certify_user( null, 'CMSS', $user->ID );
									break;

								case 'volunteer':
									update_user_meta( $user->ID, '_signed_volunteer_affidavit', true );
									break;

								case 'cap':
									update_user_meta( $user->ID, '_signed_cap_affidavit', true );
									break;

								default:
									if( $console ) echo "...<strong>affidavit type not found</strong>...";

									$affidavit_type_found = false;
									break;
							}

							if( $affidavit_type_found ) {
								$this->save_pdf( $user->ID, $affidavit_type, $msgno );

								global $acmss_emails;

								$acmss_emails->send_email( "$affidavit_type-affidavit-received", $user->ID );

								$uids[] = imap_uid( $this->_imap, $msgno );
							}
						} else {
							if( $console ) echo "...$user_email <em>doesn't exist</em>";
							if( time() - $header->udate > (60 * 60 * 24 * 80) ) {
								if( $console ) echo "...deleting";
								imap_delete( $this->_imap, $msgno );
							}
						}
					} else {
						if( $console ) echo " header match failed.";						
					}

					if( $console ) echo "</p>";

					$count--;

					if( $count == 0 ) break;
				}

				foreach( $uids as $uid ) {
					$r = imap_mail_move( $this->_imap, $uid, 'Processed', CP_UID );

					if( $console ) echo "<p>$uid: moving to Processed: ". ($r ? 'succeeded' : 'failed') ."</p>";
				}

		 		imap_expunge( $this->_imap );

				imap_close( $this->_imap, CL_EXPUNGE );
			} else {
				if( $console ) {
					echo "<p>Failed to open IMAP connection....because:";
						
					foreach (imap_errors() as $msg) {
						echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;$msg";
					}
					
				}
			}


			if( $console ) echo "<p>IMAP complete.</p>";
		}


		public function list_pending_affidavits() {

			if( $this->_imap  ) {
				$pending = array();

				$msgs = imap_sort( $this->_imap, SORTDATE, 1, SE_UID );

				foreach ($msgs as $msguid) {
					$msgno = imap_msgno( $this->_imap, $msguid );

					$header = imap_header( $this->_imap, $msgno );

					if( $affidavit_type = $this->is_affidavit( $header )  ) {

						if( !is_array( $pending[$affidavit_type] ) ) {
							$pending[$affidavit_type]  = array();
						}
						
						$pending[$affidavit_type][] = $this->affidavit_to_email( $header );
					}
				}

				imap_close( $this->_imap );

				if( count($pending) > 0 ) {
					foreach( $pending as $key => $emails ) {
						echo sprintf( '<h4>%s</h4>', strtoupper($key) );
						
						echo '<ul>';
						foreach ($emails as $email) {
							echo sprintf( '<li>%s</li>', strtolower($email) );	
						}
						echo '</ul>';
					}
				} else {
					echo '<h4>No pending affidavits</h4>';
				}
			} else {
				echo '<h4>Unable to connect to mail server</h4>';
			}
		}

		private function affidavit_to_email( $imap_header ) {
			foreach ( $imap_header->to as $to_field ) {
				$mail_obj = $to_field;
				if( $mail_obj->host != 'theacmss.net' ) break;
			}

			return $mail_obj->mailbox .'@'. $mail_obj->host;
		}

		private function is_affidavit( $imap_header, $console = false ) {
			if( $console ) echo "...checking subject...";

			if( $imap_header->fromaddress == 'EchoSign <echosign@echosign.com>' ) {
				if( preg_match( '/^(Eligibility Affidavit - (.*) \()|(Eligibility Affidavit - (.*) between American College of Medical Scribe Specialists)|(.*has signed Eligibility Affidavit - (.*)$)/', $imap_header->subject, $matches ) ) {
					if( $console ) echo "...header matches...";

					$name = array_pop($matches);

					return strtolower( $name );
				} else {
					if( $console ) echo "...no match for <strong>$imap_header->subject</strong>";
				}
			} else {
				if( $console ) echo "...wrong from address....";

				$uid = imap_uid( $this->_imap, $imap_header->Msgno );

				$r = imap_mail_move( $this->_imap, $uid, 'Non_Affidavit', CP_UID );

				if( $console ) echo "... $r moved.";
			}

			return false;
		}

		private function create_processed_folder( $console = false ) {
			// Create a Folder to put the processed ones in and the unwanted ones

			foreach( array( 'Processed', 'Non_Affidavit' ) as $folder ) {
				if( $list = imap_getmailboxes( $this->_imap, $this->_server, $folder ) ) {
					if( !is_array($list) || count($list) < 1 ) {
						@imap_createmailbox( $this->_imap, imap_utf7_encode( $this->_server . $folder ) );
					} else {
						if( $console ) echo "<p>$folder folder exists.</p>";
					}
				}
			}
		}

		private function get_message_count( $console ) {
			$n_msgs = imap_num_msg( $this->_imap );

			if( $console ) echo "<p>$n_msgs messages to process.</p>";

			return $n_msgs;
		}

		private function save_pdf( $user_id, $affidavit_type, $msgno, $console = false ) {
			// Save PDF file and store
			$structure = imap_fetchstructure( $this->_imap, $msgno );

			$attachments = array();

			if( isset($structure->parts) && count($structure->parts) ) {
				for( $x = 0; $x < count($structure->parts); $x++ ) {
					$attachments[$x] = array(
						'is_attachment' => false,
						'filename' => '',
						'name' => '',
						'attachment' => ''
					);

					if( $structure->parts[$x]->ifdparameters ) {
						foreach($structure->parts[$x]->dparameters as $object) {
							if(strtolower($object->attribute) == 'filename') {
								$attachments[$x]['is_attachment'] = true;
								$attachments[$x]['filename'] = $object->value;
							}
						}
					}

					if($structure->parts[$x]->ifparameters) {
						foreach($structure->parts[$x]->parameters as $object) {
							if(strtolower($object->attribute) == 'name') {
								$attachments[$x]['is_attachment'] = true;
								$attachments[$x]['name'] = $object->value;
							}
						}
					}

					if($attachments[$x]['is_attachment']) {
						$attachments[$x]['attachment'] = imap_fetchbody( $this->_imap, $msgno, $x+1 );
						if($structure->parts[$x]->encoding == 3) { // 3 = BASE64
							$attachments[$x]['attachment'] = base64_decode($attachments[$x]['attachment']);
						}
						elseif($structure->parts[$x]->encoding == 4) { // 4 = QUOTED-PRINTABLE
							$attachments[$x]['attachment'] = quoted_printable_decode($attachments[$x]['attachment']);
						}
					}
				}
			}

			foreach ($attachments as $attachment) {
				if( $attachment['is_attachment'] == 1 ) {
					$filename = acmss_get_file_save_dir("affidavits/{$affidavit_type}") ."/{$user_id} - {$attachment['filename']}.pdf";

					$contents = $attachment['attachment'];
					
					file_put_contents( $filename, $contents );

					if( $console ) echo "...attachment saved...";
				}
			}			
		}

		public static function fire_process_affidavits( $console = false ) {
			$imap = new ACMSS_IMAP();

			$imap->process_affidavits( $console );

			unset($imap);

			if( $console ) wp_die( '', 'IMAP processing complete' );			
		}


		public static function fire_pending_affidavits() {
			$imap = new ACMSS_IMAP();

			$imap->list_pending_affidavits();

			unset($imap);
		}
	}

	/**
	 * On an early action hook, check if the imap call is scheduled - if not, schedule it.
	 */
	function acmss_schedule_imap() {
		if ( !wp_next_scheduled( 'acmss_affidavits_event' ) ) {
			wp_schedule_event( time(), 'hourly', 'acmss_affidavits_event');
		}
	}
	add_action( 'wp', 'acmss_schedule_imap' );
	add_action( 'acmss_affidavits_event', array( 'ACMSS_IMAP', 'fire_process_affidavits' ) );

	if( $_GET['imap'] == 'aX42Mi' ) {
		do_action( 'acmss_affidavits_event', true );
		wp_die();
	}