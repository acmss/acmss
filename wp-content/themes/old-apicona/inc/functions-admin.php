<?php

	function acmss_admin_init() {
		remove_filter( 'views_users', array( 'Groups_Admin_Users', 'views_users' ) );
	}
	add_action( 'admin_init', 'acmss_admin_init' );

/*
	function acmss_admin_users_filter( $query ){
		global $pagenow, $wp_query, $wpdb;

		if ( is_admin() && $pagenow == 'users.php' && isset($_GET['abc']) && !empty($_GET['abc']) ) {
			$query->search_term = urldecode( $_GET['abc'] );

			if (!is_null($query->search_term)) {
				$query->query_from .= " INNER JOIN {$wpdb->usermeta} ON " . 
				"{$wpdb->users}.ID={$wpdb->usermeta}.user_id AND " .
				"{$wpdb->usermeta}.meta_key='meta_key_name_here' AND "."{$wpdb->usermeta}.meta_value LIKE '%{$query->search_term}%'";
			}    
		}
	}
	add_filter( 'pre_user_query', 'acmss_admin_users_filter' );
*/
	function acmss_profile_personal_options_update( $user_id ) {
		if( !empty($_POST) ) {
			if( current_user_can('administrator') ) {
				if( isset($_POST['convert_to_cmss']) && !empty($_POST['convert_to_cmss']) ) {
					acmss_add_user_cap( $user_id, 'certified_'.$_POST['convert_to_cmss'] );

					Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => 2 ) );
					Groups_User_Group::delete( array( 'user_id' => $user_id, 'group_id' => 17 ) );
				}


				if( isset($_POST['affidavit_received']) ) {
					update_user_meta( $user_id, '_signed_mscat_affidavit', true );
				}

				if( isset($_POST['add_access']) ) {
					if ( strpos( $_POST['add_access'], 'Memberships' ) > 0 ) {
						acmss_adjust_owned_memberships_count( $user_id, intval( $_POST['add_memberships'])  );

					} else if ( strpos( $_POST['add_access'], 'Manual' ) > 0 ) {
						$inc = strpos( $_POST['add_access'], 'Remove' ) === false ? 1 : -1;

						acmss_add_manual_access_count( $user_id, $inc * acmss_increment_count('manual') );

					} else {
						if( strpos( $_POST['add_access'], 'MSCAT Certification' ) > 0 ) {
							$quiz = 'MSCAT';
						} else {
							$quiz = 'HIPAA';
						}
						
						$inc = strpos( $_POST['add_access'], 'Remove' ) === 0 ? -1 : 1;

						acmss_add_assessment_access( $user_id, $quiz, $inc * acmss_increment_count( $quiz ) );
					}

					add_action( 'admin_notices', 'acmss_assessment_admin_notice' );
				}
			}
		}
	}
	add_action( 'edit_user_profile_update', 'acmss_profile_personal_options_update' );
	add_action( 'personal_options_update', 'acmss_profile_personal_options_update' );

	function acmss_assessment_admin_notice() {
		echo sprintf( '<div class="updated"><p>Additional access to %s added!</p></div>', $quiz );
	}	

	function acmss_increment_count( $field ) {
		$count_field = strtolower( "add_{$field}s" );

		if( isset($_POST[$count_field]) && !empty($_POST[$count_field]) ) {
			$increment = intval($_POST[$count_field]);
		} else {
			$increment = 1;
		}		

		return $increment;
	}

	function acmss_profile_personal_options( $user ) {
		if( current_user_can('administrator') ) {
			$mscat_count = acmss_get_assessment_access( $user->ID, 'MSCAT' );
			$hipaa_count = acmss_get_assessment_access( $user->ID, 'HIPAA' );
			$memberships = acmss_adjust_owned_memberships_count( $user->ID );
			$manual_count = acmss_get_manual_access_count( $user->ID );

			if( ( user_can($user->ID, 'apprenticed_member') || user_can($user->ID, 'apprenticed_non_member') ) && !( user_can($user->ID, 'certified_member') || user_can( $user->ID, 'certified_non_member') ) ) {
				$member_state = user_can( $user->ID, 'apprenticed_member') ? 'member' : 'non_member';

				$convert_cmsa = <<< EOT
<tr>
	<th colspan="2"><h3>Advance CMSA to CMSS</h3></th>
</tr>
<tr>
	<th><label>Advance user</label></th>
	<td><input type="checkbox" name="convert_to_cmss" value="$member_state"/> Check this box to advance the certification status of this user.</td>
</tr>
EOT;

			} else {
				$convert_cmsa = '';
			}

			$checked = checked( true, get_user_meta( $user->ID, '_signed_mscat_affidavit', true ), false );

			$html = <<< EOT
			
			<table class="form-table">
				$convert_cmsa
				<tr>
					<th colspan="2"><h3>Add Assessment Access</h3></th>
				</tr>					
				<tr>
					<th><label>Mark Affidavit as received</label></th>
					<td><input type="checkbox" name="affidavit_received" value="affidavit_received" $checked /> Check to mark this account as having an affidavit. (Checked indicates affidavit already received)</td>
				</tr>
				<tr>
					<th colspan="2"><h3>Certifications etc</h3><p>Just clicking add or remove for each of the following will add/remove a single item. To add/remove multiple items, enter the total number is the associated text box and then click add/remove.</p></th>
				<tr>				
				<tr>
					<th><label>MSCAT</label></th>
					<td><input type="text" name="add_mscats">&nbsp;<input type="submit" name="add_access" value="Add access to MSCAT Certification" class="button button-primary" />&nbsp;<input type="submit" name="add_access"value="Remove access to MSCAT Certification" class="button" />&nbsp;(<em>Currently $mscat_count</em>)</td>
				</tr>
				<tr>
					<th><label>MSCAT Manual</label></th>
					<td><input type="text" name="add_manuals">&nbsp;<input type="submit" name="add_access" value="Add access to MSCAT Manual" class="button button-primary" />&nbsp;<input type="submit" name="add_access"value="Remove access to MSCAT Manual" class="button" />&nbsp;(<em>Currently $manual_count</em>)</td>
				</tr>
				<tr>
					<th><label>HIPAA</label></th>
					<td><input type="text" name="add_hipaas">&nbsp;<input type="submit" name="add_access" value="Add access to HIPAA Assessment" class="button button-primary" />&nbsp;<input type="submit" name="add_access"value="Remove access to HIPAA Assessment" class="button" />&nbsp;(<em>Currently $hipaa_count</em>)</td>
				</tr>
				<tr>
					<th colspan="2"><h3>Members</h3></th>
				<tr>
					<th><label>Memberships</label></th>
					<td><input type="text" name="add_memberships">&nbsp;<input type="submit" name="add_access" value="Add Memberships" class="button button-primary" />&nbsp;<input type="submit" name="add_access" value="Remove Memberships" class="button" />&nbsp;(<em>Currently $memberships</em>)</td>
				</tr>
			</table>
EOT;
			echo $html;
		}
	}
	add_action( 'edit_user_profile', 'acmss_profile_personal_options' );
	add_action( 'show_user_profile', 'acmss_profile_personal_options' );	

	function admin_update_password() {

		if( isset( $_GET['new_pass'] ) ) {
			global $acmss_emails;

			$user_id = (int) $_GET['new_pass'];

			$password = wp_generate_password( 12, false );
			
			wp_set_password( $password, $user_id );
			
			$acmss_emails->send_email( 'acmss-login-notice', $user_id, array( 'password' => $password, 'login_url' => wp_login_url() ) );

			$html = <<< EOT
	<div class="updated">
		<p>A new password has been created and sent to the user</p>
	</div>
EOT;
		}

		echo $html;
	}
	add_action( 'admin_notices', 'admin_update_password' );

	function acmss_user_columns( $columns ) {
		if ( ! current_user_can( 'administrator' ) )
			return $columns;

		unset( $columns['role'] );
		unset( $columns['posts'] );
		unset( $columns['name'] );
		unset( $columns['groups_user_groups'] );
		unset( $columns['woocommerce_shipping_address'] );
		unset( $columns['woocommerce_paying_customer'] );
		
		$columns['acmss_name'] = 'Name';
		$columns['acmss_role'] = 'Role/Group';
		$columns['acmss_pdfs'] = 'Affidavits';
		$columns['acmss_pwd'] = 'New Password';

		return $columns;
	}
	add_filter( 'manage_users_columns', 'acmss_user_columns', 50, 1 );


	function acmss_user_column_values( $value, $column_name, $user_id ) {
		switch ($column_name) {
			case 'acmss_pwd':
				$user_object = get_userdata( (int) $user_id );
				$value = sprintf( '<a href="?new_pass=%d" class="btn">Email new password</a>', $user_id );
				break;

			case 'acmss_name':
				$user_object = get_userdata( (int) $user_id );
				$value = "$user_object->first_name $user_object->last_name<br><small style=\"font-style:italic;\">registered ". date( 'd M Y', strtotime($user_object->user_registered) ) . " </small>";
				break;

			case 'acmss_role':
				$elements = array();

				$user = new WP_User( $user_id );
				if ( !empty( $user->roles ) ) {
					$elements[] = implode( ', ', (array) $user->roles );
				}

				$user = new Groups_User( $user_id );
				$elements[] = implode( ', ', array_map( create_function( '$o', 'return $o->name;'), $user->groups ) );

				$value = implode( ' / ', $elements );
				break;

			case 'acmss_pdfs':
				$files = array();

				foreach( array( 'mscat', 'volunteer', 'cap') as $folder ) {
					foreach ( glob( acmss_get_file_save_dir("affidavits/{$folder}") . "/{$user_id} -*.pdf" ) as $filename ) {
						$files[] = array( $folder => str_replace( ABSPATH, '', $filename ) );
					}					
				}

				foreach ( $files as $file ) {
					foreach( $file as $key => $path) {
						$value .= sprintf( '<a href="/%s">%s</a>&nbsp;', $path, strtoupper($key) );
					}
				}
				break;
		}

		return $value;
	}
	add_action( 'manage_users_custom_column', 'acmss_user_column_values', 10, 3 );

	function acmss_remove_dashboard_widgets(){
		remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   	// Right Now
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
		remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   	// Plugins
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  	// Quick Press
		remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   		// WordPress blog
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );		// Other WordPress News

		if( current_user_can('administrator') ) {
			wp_add_dashboard_widget( 'acmss_affidavit_pending_widget', 'Pending Affidavits', array( 'ACMSS_IMAP', 'fire_pending_affidavits' ) );
		}
	}
	add_action( 'wp_dashboard_setup', 'acmss_remove_dashboard_widgets' );	