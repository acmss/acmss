<?php

	function cmss_affidavit_prompt( $atts = null ) {
		extract( shortcode_atts( array(
			'user_id'		=> get_current_user_id()
			), $atts ) );	

		if( !user_can( $user_id, 'certified_member' ) ) {
			return '<p>As soon as you are in a position to do so, you should sign our <a href="https://theacmss.org/cmss-affidavit">CMSS Affidavit</a> to qualify for full CMSS accreditation.</p>';
		}
	}
	add_shortcode( 'cmss-affidavit-prompt', 'cmss_affidavit_prompt' );

	function acmss_certification_level( $atts ) {

		extract( shortcode_atts( array(
			'user_id'		=> get_current_user_id(),
			'with_comma'	=> 'Yes',
			'in_full'		=> false
			), $atts, 'acmss_email' ) );	

		$comma = $with_comma == 'Yes' ? ', ' : '';

		if( is_null($user_id) ) $user_id = get_current_user_id();

		if( user_can( $user_id, 'certified_member' ) || user_can( $user_id, 'certified_non_member' ) ) {
			if( $in_full ) {
				return $comma.'Certified Medical Scribe Specialist';
			} else {
				return $comma.'CMSS';
			}

		} else if( user_can( $user_id, 'apprenticed_member' ) || user_can( $user_id, 'apprenticed_non_member' ) ) {
			if( $in_full ) {
				return $comma.'Certified Medical Scribe Apprentice';
			} else {
				return $comma.'CMSA';
			}
		}
	}
	add_shortcode( 'acmss-certification-level', 'acmss_certification_level' );

	function acmss_events_map( $atts ) {
		wp_enqueue_script( 'acmss-maps' );

		$query = new WP_Query( array( 'post_type' => 'events', 'posts_per_page' => -1 ) );
		$events = array();

		foreach ($query->posts as $event) {
			$meta = get_post_meta( $event->ID );

			$events[] = array( 'title' => $event->post_title, 'lat' => $meta['_eventLat'][0], 'lng' => $meta['_eventLng'][0], 'date' => date( 'F j, Y', $meta['_eventDate'][0] ) );
		}

		wp_localize_script( 'acmss-maps', 'acmss_events', $events );

		return '<div id="gmap_canvas" style="width: 600px; height: 450px; margin: 5px;"></div>';
	}
	add_shortcode( 'events-map', 'acmss_events_map' );


	function acmss_quiz_list( $atts ) {
		return '<div id="assessment_list"><h2>Currently, you have no assessments available.</h2></div>';
	}
	add_shortcode( 'acmss-quiz-list', 'acmss_quiz_list' );

	// ?first_name=john&last_name=Test&user_email=test1@dynamicarray.co.uk&member_role=acmss_member
	function acmss_add_org_user() {
		if( isset($_REQUEST['first_name']) && isset($_REQUEST['last_name']) && isset($_REQUEST['user_email']) ) {
			if( is_email($_REQUEST['user_email']) ) {
				$password = wp_generate_password( 12, true );

				$user_login = $sanitized_user_login = sanitize_user( strtolower( $_REQUEST['first_name'] . substr( $_REQUEST['last_name'], 1, 1 ) ) );

				while( username_exists($sanitized_user_login) ) {
					$sanitized_user_login = $user_login .'-'. (string) rand( 1, 999 );
				}

				$display_name = "{$_REQUEST['first_name']} {$_REQUEST['last_name']}";

				$role = isset($_REQUEST['member_role']) ? $_REQUEST['member_role'] : ( current_user_can('certified_academic_partner') ? 'student_member' : 'acmss_member' );

				$member_id = wp_insert_user( array (
					'user_pass' 		=> $password,
					'user_login' 		=> $sanitized_user_login,
					'user_nicename' 	=> $display_name,
					'user_email' 		=> $_REQUEST['user_email'],
					'display_name' 		=> $display_name,
					'first_name' 		=> $_REQUEST['first_name'],
					'last_name' 		=> $_REQUEST['last_name'],
					'user_registered'	=> date( 'Y-m-d', $reg_time  ),
					'role' 				=> $role
				));

				if( is_wp_error($member_id) ) {
					echo '<div class="messageBox error icon"><span>An error has occurred: '.$member_id->get_error_message().'</span></div>';
				
				} else {
					global $acmss_emails, $wpdb, $current_user;

					$org_name = acmss_get_org_name();

					//wp_new_user_notification( $member_id, $password );

					update_user_meta( $member_id, '_org_account', get_current_user_id() );

					if( $role == 'student_member' ) {
						// Set as academic partner student
						Groups_User_Group::create( array( 'user_id' => $member_id, 'group_id' => 6 ) );
					}

					acmss_add_to_corporate_group( $member_id, $org_name );

					$member_count = acmss_adjust_owned_memberships_count( get_current_user_id(), -1 );

					$welcome_email = ($role == 'student_member') ? 'students-welcome' : 'welcome-to-the-acmss';
					$welcome_email = 'acmss-membership-created';

					$args = array( 'password' => $password, 'login_url' => site_url( 'my-account' ), 'assignor_name' => $current_user->user_email );

					$acmss_emails->send_email( $welcome_email, $member_id, $args );
				}
			} else {
				echo '<div class="messageBox error icon"><span>Please enter a real email address.</span></div>';
			}
		}		
	}

	function acmss_memberships_management( $atts ) {
		if( current_user_can('certified_academic_partner') ) {
			if( false == get_user_meta( get_current_user_id(), '_signed_cap_affidavit', true ) ) {
				return '<p><strong>Please <a href="//theacmss.org/cap-affidavit">sign the CAP Affidavit</a> before adding students to your account.</strong></p><div class="messageBox alert icon"><span>Please note that it may take several hours for your signed affidavit to be recieved and processed. You will receive a <em>"Next Steps"</em> email from the ACMSS on successful receipt.</span></div>';
			}
		}
/*		
		if( current_user_can('administrator') ) {
			acmss_add_bundles( get_current_user_id(), 2303, 1 );

			echo '@'.	(int) get_user_meta( $user_id, $member_type, true ) . '@';	

			acmss_add_bundles( get_current_user_id(), 2303, 1, '_memberships' );
		}
*/		
		$member_count = (int) acmss_adjust_owned_memberships_count( get_current_user_id() );
		
		$org_name = acmss_get_org_name();

		if( isset($_POST['org_name']) ) {
			$org_name = trim($_POST['org_name']);

			update_user_meta( get_current_user_id(), '_org_name', $org_name );

			acmss_add_to_corporate_group( get_current_user_id(), $org_name );
		}

		acmss_add_org_user();

		if( empty($org_name) ) {
			$org_form = <<< EOT
		<p><strong>Before adding members, please set the name under which your members will be identified.<strong><em> Note, once set, this cannot be changed</em>.</p>

		<form action="#" method="post">
			<label>Organization Name:</label>
			<input type="text" name="org_name" class="member_input" style="width: 250px;" />
			<input type="submit" value="Save" class="btn green" />			
		</form>
EOT;
		} else {
			$org_form = "<p>Your organization name is set as <strong>$org_name</strong>. This can only be changed on request.</p>";

			$mscat_count = acmss_get_assessment_access( get_current_user_id(), 'MSCAT' );
			$hipaa_count = acmss_get_assessment_access( get_current_user_id(), 'HIPAA' );
			$manual_count = (int) get_user_meta( get_current_user_id(), '_mscat_manual_access', true );

			$add_new_members = <<< EOT
				<div class="std_disp"><span class="count">$member_count</span><span>Memberships</span><a href="/store/scribe-bundles" title="Purchase more memberships">+</a><button class="btn small blue member-assign-button" data-item="acmss-membership" style="display: none;">Assign</button></div>

				<div class="std_disp green"><span class="count">$mscat_count</span><span>MSCAT Assessments</span><a href="/store/scribe-bundles" title="Purchase further MSCAT certification access">+</a><button class="btn small blue assign-button" data-item="mscat-quiz">Assign</button></div>

				<div class="std_disp blue"><span class="count">$hipaa_count</span><span>HIPAA Assessments</span><a href="/store/scribe-bundles" title="Purchase further HIPAA certification access">+</a><button class="btn small blue assign-button" data-item="hipaa-quiz">Assign</button></div>

				<div class="std_disp orange"><span class="count">$manual_count</span><span>MSCAT Manuals</span><a href="/store/scribe-bundles" title="Purchase further MSCAT Manual access">+</a><button class="btn small blue assign-button" data-item="mscat-manual">Assign</button></div>
EOT;

			if( $member_count > 0 ) {
				$add_new_members .= <<< EOT
			<form action="#" method="post" style="clear:both; margin-top: 30px;">
				<label for="first_name">First Name:</label>
				<input type="text" name="first_name" class="member_input" />

				<label for="last_name">Last Name:</label>
				<input type="text" name="last_name" class="member_input" />

				<label for="user_email">Email Address:</label>
				<input type="text" name="user_email" class="member_input" />

				<input type="submit" value="Save" class="btn green" />
			</form>
EOT;
			} 
			
			$add_new_members .= '<p style="clear:both; font-weight: bold;">You can purchase further memberships or certification products in <a href="/store/scribe-bundles">the ACMSS Store</a>.</p>';
		}


		$existing = acmss_get_member_list( get_current_user_id(), true );
		if( empty($existing) ) {
			$existing = '<p>You have not yet created any memberships.</p>';
		}

		$html = <<< EOT
		<h2>Add new member</h2>

		$org_form

		$add_new_members

		<hr/>

		<h2>Existing Memberships</h2>

		<p>To assign the manual or assessments to individual members, select the member(s) by clicking on their name and then click the relevant <strong>Assign</strong> button above.</p>

		<p>If you have any questions on assignments please email <a href="mailto:assessments@theacmss.org">assessments@theacmss.org</a>.</p>

		<div id="existing_members">
			$existing
		</div>
EOT;

		return $html;
	}
	add_shortcode( 'memberships-management', 'acmss_memberships_management' );

/*
	function acmss_student_memberships( $atts ) {
		if( false == get_user_meta( get_current_user_id(), '_signed_cap_affidavit', true ) ) {
			return '<p><strong>Please <a href="//theacmss.org/cap-affidavit">sign the CAP Affidavit</a> before adding students to your account.</strong></p><div class="messageBox alert icon"><span>Please note that it may take several hours for your signed affidavit to be recieved and processed. You will receive a <em>"Next Steps"</em> email from the ACMSS on successful receipt.</span></div>';
		}

		$student_count = (int) acmss_adjust_owned_memberships_count( get_current_user_id() );
		
		$org_name = get_user_meta( get_current_user_id(), '_cap_org_name', true );

		if( isset($_POST['org_name']) ) {
			$org_name = trim($_POST['org_name']);

			update_user_meta( get_current_user_id(), '_cap_org_name', $org_name );

			acmss_add_to_corporate_group( get_current_user_id(), $org_name );
		}

		if( isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['user_email']) ) {
			if( is_email($_POST['user_email']) ) {
				$password = wp_generate_password( 12, true );

				$user_login = $sanitized_user_login = sanitize_user( strtolower( $_POST['first_name'] . substr( $_POST['last_name'], 1, 1 ) ) );

				while( username_exists($sanitized_user_login) ) {
					$sanitized_user_login = $user_login . (string) rand( 1, 999 );
				}

				$display_name = "{$_POST['first_name']} {$_POST['last_name']}";

				$student_id = wp_insert_user( array (
					'user_pass' 		=> $password,
					'user_login' 		=> $sanitized_user_login,
					'user_nicename' 	=> $display_name,
					'user_email' 		=> $_POST['user_email'],
					'display_name' 		=> $display_name,
					'first_name' 		=> $_POST['first_name'],
					'last_name' 		=> $_POST['last_name'],
					'user_registered'	=> date( 'Y-m-d', $reg_time  ),
					'role' 				=> 'student_member'
				));

				if( is_wp_error($student_id) ) {
					echo '<div class="messageBox error icon"><span>An error has occurred: '.$student_id->get_error_message().'</span></div>';
				
				} else {
					global $acmss_emails, $wpdb;

					wp_new_user_notification( $student_id, $password );

					update_user_meta( $student_id, '_cap_account', get_current_user_id() );

					// Set as academic partner student
					Groups_User_Group::create( array( 'user_id' => $student_id, 'group_id' => 6 ) );

					acmss_add_to_corporate_group( $student_id, $org_name );

					$student_count = acmss_adjust_owned_memberships_count( get_current_user_id(), -1 );

					$acmss_emails->send_email( 'students-welcome', $student_id );
				}
			} else {
				echo '<div class="messageBox error icon"><span>Please enter a real email address</span></div>';
			}
		}


		if( empty($org_name) ) {
			$org_form = <<< EOT
		<p><strong>Before adding students, please set the name under which your students will be identified.<strong><em> Note, once set, this cannot be changed</em>.</p>

		<form action="#" method="post">
			<label>Organization Name:</label>
			<input type="text" name="org_name" class="student_input" style="width: 250px;" />
			<input type="submit" value="Save" class="btn green" />			
		</form>
EOT;
		} else {
			$org_form = "<p>Your organization name is set as <strong>$org_name</strong>. This can only be changed on request.</p>";

			if( $student_count > 0 ) {
				$mscat_count = acmss_get_assessment_access( get_current_user_id(), 'MSCAT' );
				$hipaa_count = acmss_get_assessment_access( get_current_user_id(), 'HIPAA' );
				$manual_count = (int) get_user_meta( get_current_user_id(), '_mscat_manual_access', true );

				$add_new_students = <<< EOT
			<style>
			 .student_input { width: 160px; margin-right: 35px !important; }
			</style>

			<div class="std_disp"><span class="count">$student_count</span><span>Memberships</span><a href="/store/student-membership" title="Purchase more student memberships">+</a></div>

			<div class="std_disp green"><span class="count">$mscat_count</span><span>MSCAT Assessments</span><a href="/store/mscat-certification" title="Purchase further MSCAT certification access">+</a><button class="btn small blue assign-button" data-item="mscat-quiz">Assign</button></div>

			<div class="std_disp blue"><span class="count">$hipaa_count</span><span>HIPAA Assessments</span><a href="/store/hipaa-for-scribes" title="Purchase further HIPAA certification access">+</a><button class="btn small blue assign-button" data-item="hipaa-quiz">Assign</button></div>

			<div class="std_disp orange"><span class="count">$manual_count</span><span>MSCAT Manuals</span><a href="/store/mscat-studytraining-e-manual" title="Purchase further MSCAT Manual access">+</a><button class="btn small blue assign-button" data-item="mscat-manual">Assign</button></div>

			<form action="#" method="post" style="clear:both; margin-top: 30px;">
				<label for="first_name">First Name:</label>
				<input type="text" name="first_name" class="student_input" />

				<label for="last_name">Last Name:</label>
				<input type="text" name="last_name" class="student_input" />

				<label for="user_email">Email Address:</label>
				<input type="text" name="user_email" class="student_input" />

				<input type="submit" value="Save" class="btn green" />
			</form>
EOT;
			} else {
				$add_new_students = '<p>You need to purchase further student memberships. <a href="/store/student-membership">Visit the ACMSS Store</a>.</p>';
			}
		}


		$existing = acmss_get_student_list( get_current_user_id() );
		if( empty($existing) ) {
			$existing = '<p>You have no existing memberships.</p>';
		}

		$html = <<< EOT
		<h2>Add new student</h2>

		$org_form

		$add_new_students

		<hr/>

		<h2>Existing Student Memberships</h2>

		<p>To assign the manual or assessments to individual students, select the student(s) by clicking on their name and then click the relevant <strong>Assign</strong> button above.</p>

		<p>If you have any questions on assignments please email <a href="mailto:assessments@theacmss.org">assessments@theacmss.org</a>.</p>

		<div id="existing_students">
			$existing
		</div>
EOT;

		return $html;	
	}
	add_shortcode( 'student-memberships', 'acmss_student_memberships' );
*/
	function acmss_account_links( $atts ) {
		$links = array( 
				'my-account' 								=> 'Account details', 
				'specialist-crosswalk'						=> 'CMS Specialist Attestation',
				'my-account/continuing-scribe-education'	=> 'Continuing scribe education',
				'my-account/edit-account' 					=> 'Change password', 
				'my-account/edit-address/billing' 			=> 'Edit postal address', 
				'my-account/customer-logout'				=> 'Logout',
				'my-account/memberships-assignment'			=> 'Manage member assignments'
			);

		$html = '';

		if( !current_user_can('certified_academic_partner') && !current_user_can('administrator') 
			&& acmss_adjust_owned_memberships_count( get_current_user_id() ) == 0
			&& acmss_distributed_products_owned( get_current_user_id() ) == false
			&& !current_user_can( BOUGHT_BUNDLES )
			) {
			unset($links['my-account/memberships-assignment']);
			unset($links['specialist-crosswalk']);
		}

		if( !current_user_can('acmss_member') && !current_user_can('administrator') ) {
			unset($links['my-account/continuing-scribe-education']);
		}

		foreach ($links as $link => $caption) {
			$html .= sprintf( '<li><div class="icon16 iconSymbol check"></div><a href="%s">%s</a></li>', site_url($link), $caption );
		}

		return '<ul class="account-links icon-list">'.$html.'</ul>';
	}
	add_shortcode( 'my_account_links', 'acmss_account_links' );

	function acmss_user_info( $atts ) {
		if ( !is_user_logged_in() )	return '';

		extract( shortcode_atts( array(
			'field' 	=> 'display_name',
			'user_id'	=> isset($_GET['user_id']) ? $_GET['user_id'] : get_current_user_id()
			), $atts ) );

		
		if( is_null($user_id) ) $user_id =  get_current_user_id();

		switch( $field ) {
			case 'hex_id':
				$id = dechex( $user_id + 100001 );
				return strtoupper( str_pad( $id, 8, '0', STR_PAD_LEFT ) );
				break;

			default:
				$user = new WP_User( $user_id );
				return $user->$field;
		}
	}
	add_shortcode( 'acmss-user-info', 'acmss_user_info' );


	function acmss_terminology_search() {
		wp_enqueue_script('terminology-search');

		return '<div id="term_results"></div>';
	}
	add_shortcode( 'terminology_search', 'acmss_terminology_search' );

	function acmss_login_form_shortcode() {
		if ( is_user_logged_in() )
			return '';

		return wp_login_form( array(  'redirect' => site_url(), 'form_id' => 'slidedown_loginform','echo' => false ) );
	}
	add_shortcode( 'acmss-login-form', 'acmss_login_form_shortcode' );		