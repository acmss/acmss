	var acmss = {};

	( function( window, $ ) {
		function ajax_post( data, callback, type ) {
			jQuery.post( acmss_global.ajax_url, data, callback, type );
		}

		$(function() {
                    // alert('Hello Osiya');
			ajax_post( { 'action': 'get_assessment_list' }, function( response ) {
				if( response.assessments > 0 ) {
					$('#assessment_list').html( response.html );
				}
			}, 'json' );

			if( $.isFunction($.fn.datepicker)  ) {
				$('.datepicker').datepicker();
			}

			if( $.isFunction($.fn.autocomplete) && typeof acmss_groups !== 'undefined' ) {
				$('#txtMemberOrg').autocomplete({
					minLength: 	0,
					source: 	acmss_groups,
					focus: 		function( event, ui ) {
						$( '#txtMemberOrg' ).val( ui.item.name );
						return false;
					},
					select: 	function( event, ui ) {
						$('#txtMemberOrg').val( ui.item.name );
						$('#MemberOrgID').val( ui.item.group_id );
						return false;
					}
				});
			}

			if( $.isFunction($.fn.fancybox)  ) {
				$('body.single-sfwd-quiz').on( 'ajaxComplete', function() {
					$('.wpProQuiz_question_text a').filter('[href$=".png"],[href$=".jpg"]').fancybox();
				});
			}	


			$('body').on( 'click', '#existing_members li', function() {
                            alert('Hello');
				$(this).toggleClass('selected_member');
				$('.assign-button').toggle( $('.selected_member').length > 0 );

				$('.member-assign-button').toggle( $('.selected_member').length == 1 && $('.self.selected_member').length  == 1 );
			});

			$('.assign-button, .member-assign-button').click( function() {
				var product_item = $(this).data('item');
				var count_span = $(this).siblings('span.count:first');
				var members = [];

				$('.selected_member').each( function() {
					members.push( $(this).data('member-id') );
				});

				ajax_post( { 'action': 'member_assign', 'product_item': product_item, 'members': members }, function( response ) {
					if( response.item_count > -1 ) {
						$('#existing_members').html( response.list );
						count_span.html(response.item_count);

						$('.assign-button, .member-assign-button').toggle( false );
					}
				}, 'json' );
			});

			$('body').on( 'change', '#product-2015 input.qty', function() {
				var $cart = $(this).closest('.cart');
				$cart.trigger('acmss-bundle-update');
			});

			$( 'body' ).on( 'wc_variation_form', function() {
			});

			if( $.isFunction($.fn.pagewalkthrough)  ) {
				if( typeof wt_steps != 'undefined' ) {
					$('.prev-step').live('click', function(e){
						$.pagewalkthrough('prev',e);
					});

					$('.next-step').live('click', function(e){
						$.pagewalkthrough('next',e);
					});

					$('.restart-step').live('click', function(e){
						$.pagewalkthrough('restart',e);
					});

					$('.close-step').live('click', function(e){
						$.pagewalkthrough('close');
					});

					$('#walkthrough').pagewalkthrough( { steps: wt_steps, name: 'ACMSS_Walkthrough', onLoad: true });
					$.pagewalkthrough( 'show', 'walkthrough' );
				}
			}
		});
	}) ( jQuery( window ), jQuery );