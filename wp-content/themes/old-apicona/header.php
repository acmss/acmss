<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */
global $apicona;
$stickyHeaderClass = ($apicona['stickyheader']=='y') ? 'masthead-header-stickyOnScroll' : '' ; // Check if sticky header enabled

$search_title = ( isset($apicona['search_title']) && trim($apicona['search_title'])!='' ) ? trim($apicona['search_title']) : "Just type and press 'enter'" ;
$search_input = ( isset($apicona['search_input']) && trim($apicona['search_input'])!='' ) ? trim($apicona['search_input']) : "WRITE SEARCH WORD..." ;
$search_close = ( isset($apicona['search_close']) && trim($apicona['search_close'])!='' ) ? trim($apicona['search_close']) : "Close search" ;



?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/helveticanu/stylesheet.css">
<?php wp_head(); ?>




</head>

<body <?php body_class(); ?>>
<!-- <div id="pageoverlay"></div> -->
<div class="main-holder animsition">
<div id="page" class="hfeed site">
<header id="masthead" class="site-header  header-text-color-<?php echo $apicona['header_text_color']; ?>" role="banner">
  <div class="headerblock">
	<?php kwayy_floatingbar(); ?>
	<?php kwayy_topbar(); ?>
    <div id="stickable-header" class="header-inner <?php echo $stickyHeaderClass; ?>">
      <div class="container">
        <div class="headercontent">
          <div class="headerlogo kwayy-logotype-<?php echo $apicona['logotype']; ?>">
				<h1 class="site-title">
					<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					
						<?php if( $apicona['logotype'] == 'image' ){ ?>
							<?php /* ?>
							<?php if( $kwayy_retina_logo=='on' ){ ?>
								<img class="kwayy-logo-img retina" src="<?php echo $apicona['logoimg_retina']["url"]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="<?php echo round(($apicona['logoimg']["width"])/2); ?>" height="<?php echo round(($apicona['logoimg']["height"])/2); ?>">
							<?php } else { ?>
								<img class="kwayy-logo-img standard" src="<?php echo $apicona['logoimg']["url"]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="<?php echo $apicona['logoimg']["width"]; ?>" height="<?php echo $apicona['logoimg']["height"]; ?>">
							<?php }; ?>
							<?php */ ?>
							
							<img class="kwayy-logo-img standardlogo" src="<?php echo $apicona['logoimg']["url"]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="<?php echo $apicona['logoimg']["width"]; ?>" height="<?php echo $apicona['logoimg']["height"]; ?>">
							
						<?php } else { ?>
							
							<?php if( trim($apicona['logotext'])!='' ){ echo $apicona['logotext']; } else { bloginfo( 'name' ); }?>
							
						<?php } ?>
						
					</a>
				</h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		  </div>
		  
		  <?php
		  $navbarClass = '';
		  if( isset($apicona['header_search']) &&  $apicona['header_search']=='1'){
			$navbarClass = ' class="k_searchbutton"';
		  }
		  ?>
			
		  
		  
          <div id="navbar"<?php echo $navbarClass; ?>>
            <nav id="site-navigation" class="navigation main-navigation" role="navigation">
              <h3 class="menu-toggle">
                <?php _e( '<span>Toggle menu</span><i class="kwicon-fa-navicon"></i>', 'apicona' ); ?>
              </h3>
              <a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'apicona' ); ?>">
              <?php _e( 'Skip to content', 'apicona' ); ?>
              </a>
              <?php
					   //if ( has_nav_menu( 'primary' ) ){
						//wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' , 'walker' => new kwayy_custom_menus_walker ) );
					   //} else {
						wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'container_class' => 'menu-main-menu-container nav-menu-wrapper' ) );
					   //}
      			 ?>
              <?php /*?><?php get_search_form(); ?><?php */?>
              
			  
			  <?php if( isset($apicona['header_search']) &&  $apicona['header_search']=='1'): ?>
              <div class="k_flying_searchform"> <span class="k_searchlink"><a href="javascript:void(0);"><i class="kwicon-fa-search"></i></a></span>
                <div class="k_flying_searchform_wrapper">
                  <form method="get" id="flying_searchform" action="<?php echo home_url(); ?>">
                    <div class="w-search-form-h">
                      <div class="w-search-form-row">
                        <div class="w-search-label">
                          <label for="s"> <?php _e($search_title, 'apicona'); ?></label>
                        </div>
                        <div class="w-search-input">
                          <input type="text" class="field searchform-s" name="s" id="searchval" placeholder="<?php _e($search_input, 'apicona' ); ?>">
                        </div>
                        <a class="w-search-close" href="javascript:void(0)" title="<?php _e($search_close, 'apicona' ); ?>"></a> </div>
                    </div>
                    <div class="sform-close-icon"><i class="icon-remove"></i></div>
                  </form>
                </div>
              </div><!-- .k_flying_searchform --> 
              <?php endif; ?>
              
            </nav>
            <!-- #site-navigation --> 
          </div>
          <!-- #navbar --> 
        </div>
        <!-- .row --> 
      </div>
    </div>
  </div>
  <?php kwayy_header_titlebar(); ?>
  <?php kwayy_header_slider(); ?>

  
</header>
<!-- #masthead -->

<?php
  /*$category_id = get_cat_ID('Advancing Care Across the Specialities');
 $category_id1 = get_cat_ID('Certified Medical Scribe Specialists');
 
if($category_id == 111 OR $category_id1 == 57) {   echo "fdsffd";
  ?>
<style>aside#nav_menu-6 {
    display: none !important;
}</style>
<?php }*/
/* for banner image start */
$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
$pagename=$segments[2]; 
$pagename1 = $segments[3];   
 
 
if($pagename == "membership-types" OR $pagename1 == "membership-types" OR $pagename=="membership") {
	
 ?>
 
 <div class="speciality_img">
<h4 class="product_title entry-title">CMS Recognizes Certified Scribes in ALL Specialities</h4> 
  <ul class="inline_b">
<li class="cen"><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/dermatology.gif" alt="ACMSS-slider-1" /><span>Dermatology</span></li>
<li><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/emergency_med.gif" alt="ACMSS-slider-1" /><span class="span-text">Emergency Medicine</span></li>
<li><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/general.gif" alt="ACMSS-slider-1" /><span>General</span></li>
<li><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/internal_medicine.gif" alt="ACMSS-slider-1" /><span>Internal Medicine</span> </li>
<li><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/oncology.jpg" alt="ACMSS-slider-1" /><span>Oncology</span></li>
<li><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/opthamology.gif" alt="ACMSS-slider-1" /><span>Ophthalmology</span></li>
<li><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/primary_care.gif" alt="ACMSS-slider-1" /><span>Primary Care</span></li>
<li><img class="alignnone wp-image-6652 size-full" src="/wp-content/themes/apicona/images/urgent_care.gif" alt="ACMSS-slider-1" /><span class="urg_txt">Urgent Care</span></li>
<li class="mar"><img class="alignnone wp-image-6652 size-full vasc" src="/wp-content/themes/apicona/images/vascular-care.gif" alt="ACMSS-slider-1" /><span>Vascular Care</span></li>
</ul>
 </div>

<?php }
/* for banner image end */  

 ?>


<div id="main" class="site-main">
<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>
