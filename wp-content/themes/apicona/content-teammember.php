<?php
/**
 * Team Member box
 *
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */

global $apicona;
$teamcat_column = ( isset($apicona['teamcat_column']) && trim($apicona['teamcat_column'])!='' ) ? trim($apicona['teamcat_column']) : 'three' ;

?>
<?php echo kwayy_teammemberbox($teamcat_column); ?>
