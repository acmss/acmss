<?php
/**
 * The template for displaying Portfolio Category
 *
 * Used to display tm_portfolio_category with a unique design.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */
global $apicona;
global $wp_query;
$tax   = $wp_query->get_queried_object();


$pfcat_column = ( isset($apicona['pfcat_column']) && trim($apicona['pfcat_column'])!='' ) ? trim($apicona['pfcat_column']) : 'two' ;


get_header(); ?>

<div class="container">
	<div class="row">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
			
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
					<!-- Left Sidebar -->
					<div class="tm-taxonomy-left col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="tm-taxonomy-left-wrapper">
							<?php
							$termList = get_terms( $tax->taxonomy, array('hide_empty'=>false) );
							if( is_array($termList) && count($termList)>0 ){
								echo '<div class="tm-taxonomy-term-list"><ul>';
								foreach( $termList as $term ){
									$active = ($tax->slug == $term->slug) ? ' class="thememount-active" ' : '';
									echo '<li'.$active.'><a href="'.get_term_link( $term ).'">'.$term->name.'</a></li>';
								}
								echo '</ul></div>';
							}
							?>
						</div>
					</div>
					
					<!-- Right Content Area -->
					<div class="tm-taxonomy-right col-lg-9 col-md-9 col-sm-12 col-xs-12">
						
						<?php
						/*
						 * Category description
						 */
						echo '<div class="tm-term-desc">';
							echo do_shortcode('[heading text="'.$tax->name.'"]');
							echo do_shortcode(nl2br($tax->description));
						echo '</div>';
						?>
						
						
						<?php /* The loop */ ?>
						
<?php
function tm_number_of_posts_on_pcat($query){
	global $apicona;
	$pfcat_show = ( isset($apicona['pfcat_show']) && trim($apicona['pfcat_show'])!='' ) ? trim($apicona['pfcat_show']) : '8' ;

	if( is_tax( 'portfolio_category' ) && $query->is_main_query() ){
		$query->set('posts_per_page', $pfcat_show);
	}
	return $query;
}
add_filter('pre_get_posts', 'tm_number_of_posts_on_pcat');
?>
						
						<div class="row multi-columns-row">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php echo kwayy_portfoliobox($pfcat_column) ?>
								<?php //get_template_part( 'content', 'portfolio' ); ?>
								<?php comments_template(); ?>
							<?php endwhile; ?>
						</div><!-- .row -->
						
						<?php apicona_paging_nav(); ?>
						
					</div>
				
				</article>

			</div><!-- #content -->
		</div><!-- #primary -->
	
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>