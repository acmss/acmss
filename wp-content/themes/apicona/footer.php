<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */
 global $apicona;
?>
<?php /*if( !is_page() ){
		echo '</div>';
	}*/ ?>
				
		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
        	<div class="footer footer-text-color-<?php echo $apicona['footerwidget_color']; ?>">
			<?php /* ?>
              <div class="footersocialicon">
                  <div class="container">
                        <div class="row">
                          <div class="col-xs-12"><?php echo kwayy_get_social_links(); ?></div>
                        </div>                
                  </div>                
              </div>
			  <?php */ ?>
				<div class="container">
					<div class="row">
						<?php get_sidebar( 'footer' ); ?>
					</div>
				</div>
            </div>
    <style>
    #placement_298306_1 { background-color: #202020; }
    #placement_298306_1 img { display: block; margin: 0 auto; }
    </style>        
	<!-- ACMSS [javascript] -->
<script type="text/javascript">
var rnd = window.rnd || Math.floor(Math.random()*10e6);
var pid298306 = window.pid298306 || rnd;
var plc298306 = window.plc298306 || 0;
var abkw = window.abkw || '';
var absrc = 'https://servedbyadbutler.com/adserve/;ID=165731;size=728x90;setID=298306;type=js;sw='+screen.width+';sh='+screen.height+';spr='+window.devicePixelRatio+';kw='+abkw+';pid='+pid298306+';place='+(plc298306++)+';rnd='+rnd+';click=CLICK_MACRO_PLACEHOLDER';
document.write('<scr'+'ipt src="'+absrc+'" type="text/javascript"></scr'+'ipt>');
</script>            
            
			<div class="site-info footer-info-text-color-<?php echo $apicona['footertext_color']; ?>">
                <div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 copyright">
							<span class="kwayy_footer_text"><?php
							global $apicona;
							if( isset($apicona['copyrights']) && trim($apicona['copyrights'])!='' ){
								//$tm_footer_copyright = apply_filters('the_content', $apicona['copyrights']);
								//echo $tm_footer_copyright;
								echo do_shortcode( nl2br($apicona['copyrights']) );
							}
							?>
							</span> 
						</div><!--.copyright -->
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 kwayy_footer_menu">
							<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'footer-nav-menu', 'container' => false ) ); ?>
					  </div>
                    </div><!--.row -->
				</div><!-- .site-info -->
			</div><!-- .container -->
		</footer><!-- #colophon -->
	</div><!-- #page -->
	
	<!-- <a class="button" style="position: fixed; bottom: 0; left: 0; height: 50px; width: 150px; background: #2759a8; color: white; border-radius: 10px; text-align: center; font-size: 20px; font-weight: bold; padding: 13px 0 0 0; text-shadow: 2px 2px 1px #000000; z-index: 100000; display: none;" href="https://calendly.com/acmsssupport/">Support</a> -->
	

	
	</div><!-- .main-holder.animsition -->

	
	<div id="supportButton" style="position: fixed; bottom: 55px; left: 13px; height: 40px;">
	<a class="button" style="background: #2759a8; color: white; border-radius: 25px; text-align: center; font-size: 16px; font-weight: bold; padding: 12px 20px; text-shadow: 2px 2px 1px #000000; box-shadow: 1px 1px 1px #ffffff;" href="https://calendly.com/acmsssupport/" target="_blank">LIVE Support</a>
	</div>
	

    <a id="totop" href="#top" style="display: inline;"><i class="kwicon-fa-angle-up"></i></a>
    <div id="trainingMessage" style="display: none; width: 60%; position: absolute; top: 200px; left: 200px; z-index: 10000;">
    <span style="font-weight: bold; font-size: 48px; color: red;">Hello</span>
    </div>
	<?php
	if( isset($apicona['custom_js_code']) && trim($apicona['custom_js_code'])!='' ){
		echo '<script type="text/javascript"> /* Custom JS Code */ '.$apicona['custom_js_code'].'</script>';
	}
	?>
	

	
	<?php wp_footer(); ?>
	
</body>
</html>
