<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */
global $apicona;
$stickyHeaderClass = ($apicona['stickyheader']=='y') ? 'masthead-header-stickyOnScroll' : '' ; // Check if sticky header enabled

$search_title = ( isset($apicona['search_title']) && trim($apicona['search_title'])!='' ) ? trim($apicona['search_title']) : "Just type and press 'enter'" ;
$search_input = ( isset($apicona['search_input']) && trim($apicona['search_input'])!='' ) ? trim($apicona['search_input']) : "WRITE SEARCH WORD..." ;
$search_close = ( isset($apicona['search_close']) && trim($apicona['search_close'])!='' ) ? trim($apicona['search_close']) : "Close search" ;
$logoseo      = ( isset($apicona['logoseo']) && trim($apicona['logoseo'])!='' ) ? trim($apicona['logoseo']) : "h1homeonly" ;

// Logo tag for SEO
$logotag = 'h1';
if( $logoseo=='h1homeonly' && !is_front_page() ){
	$logotag = 'span';
}



$stickyLogo = 'no';
if( isset($apicona['logoimg_sticky']["url"]) && trim($apicona['logoimg_sticky']["url"])!='' ){
	$stickyLogo = 'yes';
}


/*
 * This will override the default "skin color" set in the page directly.
 */
if( is_page() ){
	global $post;
	global $apicona;
	$skincolor = trim( get_post_meta( $post->ID, '_kwayy_page_customize_skincolor', true ) );
	if($skincolor!=''){
		$apicona['skincolor']=$skincolor;
	}
}


// header('X-Frame-Options: GOFORIT'); // added to address x-orginin issue in iFrame - doesn't seem to work

// header_remove('X-Frame-Options');

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
<?php wp_head(); ?>
<style>
.woocommerce ul.products li.product, .woocommerce-page ul.products li.product {
	padding: 30px 0px !important;
}
.woocommerce ul.products li.post-1743, .woocommerce-page ul.products li.post-1743{
    border-right: 2px solid #f2f0f0 !important;
}
.sponsor-grid{
	float:left;
	padding: 30px 40px;
}
.grid-clear{
	clear: both;
}
</style>
</head>

<body <?php body_class(); ?>>

<?php
/* Custom HTML code */
if( isset($apicona['customhtml_bodystart']) && trim($apicona['customhtml_bodystart'])!='' ){
	echo $apicona['customhtml_bodystart'];
}
?>

<div class="main-holder animsition">
<div id="page" class="hfeed site">

<header id="masthead" class="site-header  header-text-color-<?php echo $apicona['header_text_color']; ?>" role="banner">
  <div class="headerblock">
	
<style>
#placement_298306_0 { width: 100%; }
#placement_298306_0 img { display: block; margin: 0 auto; }
</style>
<!-- ACMSS [javascript] -->
<script type="text/javascript">
var rnd = window.rnd || Math.floor(Math.random()*10e6);
var pid298306 = window.pid298306 || rnd;
var plc298306 = window.plc298306 || 0;
var abkw = window.abkw || '';
var absrc = 'https://servedbyadbutler.com/adserve/;ID=165731;size=728x90;setID=298306;type=js;sw='+screen.width+';sh='+screen.height+';spr='+window.devicePixelRatio+';kw='+abkw+';pid='+pid298306+';place='+(plc298306++)+';rnd='+rnd+';click=CLICK_MACRO_PLACEHOLDER';
document.write('<scr'+'ipt src="'+absrc+'" type="text/javascript"></scr'+'ipt>');
</script>		

	
	<?php kwayy_floatingbar(); ?>
	<?php kwayy_topbar(); ?>
	<div id="stickable-header" class="header-inner <?php echo $stickyHeaderClass; ?>">
	

	
	<div class="container">
		<div class="headercontent">
		<div class="headerlogo kwayy-logotype-<?php echo $apicona['logotype']; ?> tm-stickylogo-<?php echo $stickyLogo; ?>">
				<<?php echo $logotag; ?> class="site-title">
					<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					
						<?php if( $apicona['logotype'] == 'image' ){ ?>
							<?php /* ?>
							<?php if( $kwayy_retina_logo=='on' ){ ?>
								<img class="kwayy-logo-img retina" src="<?php echo $apicona['logoimg_retina']["url"]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="<?php echo round(($apicona['logoimg']["width"])/2); ?>" height="<?php echo round(($apicona['logoimg']["height"])/2); ?>">
							<?php } else { ?>
								<img class="kwayy-logo-img standard" src="<?php echo $apicona['logoimg']["url"]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="<?php echo $apicona['logoimg']["width"]; ?>" height="<?php echo $apicona['logoimg']["height"]; ?>">
							<?php }; ?>
							<?php */ ?>
							
							<img class="kwayy-logo-img standardlogo" src="<?php echo $apicona['logoimg']["url"]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="<?php echo $apicona['logoimg']["width"]; ?>" height="<?php echo $apicona['logoimg']["height"]; ?>">
							
							<?php if( isset($apicona['logoimg_sticky']["url"]) && trim($apicona['logoimg_sticky']["url"])!='' ): ?>
							<img class="kwayy-logo-img stickylogo" src="<?php echo $apicona['logoimg_sticky']["url"]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" width="<?php echo $apicona['logoimg_sticky']["width"]; ?>" height="<?php echo $apicona['logoimg_sticky']["height"]; ?>">
							<?php endif; ?>
							
						<?php } else { ?>
							
							<?php if( trim($apicona['logotext'])!='' ){ echo $apicona['logotext']; } else { bloginfo( 'name' ); }?>
							
						<?php } ?>
						
					</a>
				</<?php echo $logotag; ?>>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</div>
		
		<?php
		$navbarClass = '';
		if( isset($apicona['header_search']) &&  $apicona['header_search']=='1'){
			$navbarClass = ' class="k_searchbutton"';
		}
		?>
			
		
		
		<div id="navbar"<?php echo $navbarClass; ?>>
			<nav id="site-navigation" class="navigation main-navigation" role="navigation">
			<h3 class="menu-toggle">
				<?php _e( '<span>Toggle menu</span><i class="kwicon-fa-navicon"></i>', 'apicona' ); ?>
			</h3>
			<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'apicona' ); ?>">
			<?php _e( 'Skip to content', 'apicona' ); ?>
			</a>
			<?php
				wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'container_class' => 'menu-main-menu-container nav-menu-wrapper' ) );
			?>

			<?php if( isset($apicona['header_search']) &&  $apicona['header_search']=='1'): ?>
			<div class="k_flying_searchform"> <span class="k_searchlink"><a href="javascript:void(0);"><i class="kwicon-fa-search"></i></a></span>
				<div class="k_flying_searchform_wrapper">
				<form method="get" id="flying_searchform" action="<?php echo home_url(); ?>">
					<div class="w-search-form-h">
					<div class="w-search-form-row">
						<div class="w-search-label">
						<label for="s"> <?php _e($search_title, 'apicona'); ?></label>
						</div>
						<div class="w-search-input">
						<input type="text" class="field searchform-s" name="s" id="searchval" placeholder="<?php _e($search_input, 'apicona' ); ?>">
						</div>
						<a class="w-search-close" href="javascript:void(0)" title="<?php _e($search_close, 'apicona' ); ?>"></a> </div>
					</div>
					<div class="sform-close-icon"><i class="icon-remove"></i></div>
				</form>
				</div>
			</div>
			<?php endif; ?>
			
			</nav>
		</div>
		</div>
	</div>
	</div>
  </div>
	<?php kwayy_header_titlebar(); ?>
	<?php kwayy_header_slider(); ?>

</header>
<!-- ACMSS 2015 Edtion -->

<div id="main" class="site-main">
	<?php 
		if( function_exists('bcn_display') ) {
			echo '<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">';
			bcn_display();
			echo '</div>';
		}
	?>
<style>
	/* iframe#launcher { display: none; } */
	p.price { display: none; }
	.price { border: none !important; }
	
	#buyButton a {
		background-color: red; 
		border-radius: 7px; 
		color: white; 
		font-weight: bold; 
		text-shadow: 1px 1px #222; 
		box-shadow: 1px 1px 1px #333; 
		text-align: center; 
		line-height: 1.2em; 
		margin: 0; 
		padding: 8px 10px; 
		max-width: 150px; 
		display: block;
		position: fixed;
		top: 140px;
		right: 20px;
		text-transform: capitalize;
	}
	/* iPhone 5 Portrait */
	@media only screen and (min-device-width: 320px) and (max-device-width: 568px) and (orientation: portrait) {
		#buyButton a {
			top: 30px;
			right: 5px;
		}
	}
	/* iPhone 5 Landscape */
	@media only screen and (min-device-width: 320px) and (max-device-width: 568px) and (orientation: landscape) {
		#buyButton a {
			top: 110px;
			right: 130px;
		}
	}
	/* iPhone 6 Portrait */
	@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (orientation: portrait) {
		#buyButton a {
			top: 100px;
			right: 5px;
		}
	}
	/* iPhone 6 Landscape */
	@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (orientation: landscape) {
		#buyButton a {
			top: 70px;
			right: 170px;
		}
	}
	/* iPhone 6+ Portrait */
	@media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (orientation: portrait) {

	}
	/* iPhone 6+ Landscape */
	@media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (orientation: landscape) {

	}
	/* iPad Portrait */
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {
		#buyButton a {
			top: 110px;
			right: 105px;
		}
	}
	/* iPad Landscape */
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) {
		#buyButton a {
			top: 70px;
			right: 125px;
		}
	}	
</style>
<script>
	jQuery(window).bind('scroll',function() {
		if (jQuery(window).scrollTop() > 100) {
			jQuery('#buyButton').hide();
		}
		else {
			jQuery('#buyButton').show();
		}
	});

</script>	
	<div id="buyButton">
		<a href="https://theacmss.org/store/acmss-volume-packages" class="button">National<BR>Certification &<BR>Credentialing</a>
	</div>