<?php
/*
	Template Name: Affidavit Template
*/

	the_post();	

	get_header();
?>
	<div id="primary" class="content-area col-md-12 col-lg-12 col-sm-12 col-xs-12">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="the-post-container">
				<header class="entry-header">
					<h1 class="entry-title" title="<?php echo the_title_attribute('echo=0'); ?>">
						<?php the_title(); ?>
					</h1>
				</header>
				
				<div class="entry-content clearfix">
					<?php if( $_GET['sign_before_exam'] ) { ?>
						<h2>Please sign the MSCAT Affidavit prior to taking the exam.</h2>

						<!-- <p>Once you have signed the affidavit, you can <a href="<?php echo site_url('certification-assessment'); ?>">begin the certification here</a>.</p> -->
					<?php } ?>

					<!-- <div class="alert alert-warning"><span>Please note that it may take several hours for your signed affidavit to be received and processed. You will receive a confirmation email from the ACMSS on successful receipt.</span></div> -->
					<style>
					div.alert-warning span{ color: #03071c !important; font-size: 18px; font-weight: bold; }
					</style>
					<br/>
					
					<div class="alert alert-warning"><span>It is essential to sign this affidavit with the same email address you have used for signing up to this website.</span></div>

					<br/>
					
					<?php the_content(); ?>
				</div>
	
				<footer class="postFooter">
					<?php edit_post_link('Edit','<p class="postEdit">','</p>'); ?>
				</footer>
			</div>
		</article>
	</div>

<?php
	get_footer();