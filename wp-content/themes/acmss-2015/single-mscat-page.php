<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */

	get_header();

	the_post();
?>
<div class="container">
	<div class="row">	
		<div id="primary" class="content-area col-md-9 col-lg-9 col-sm-12 col-xs-12">
			<div id="content" class="site-content" role="main">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="postcontent">
						<header class="entry-header">
							<h2 class="entry-title"><?php the_title(); ?></h2>
						</header>
						<?php 
							if( current_user_can('read_mscat_manual') ) {
								the_content();

							} else {
								echo wp_trim_words( get_the_content(), 40, '...' );

								echo '<h4 style="margin-top: 50px;">You need to purchase access to this content</h4><p>You can purchase this <a href="/store">via our store</a></p>';
							}
						?>
						<footer class="entry-meta">
							<div class="footer-entry-meta">
								<?php edit_post_link( __( 'Edit', 'apicona' ), '<span class="edit-link">', '</span>' ); ?>
							</div>
						</footer>
					</div>
					<div class="clearfix"></div>
				</article>
			</div>
		</div>
		<aside id="sidebar-right" class="col-md-3 col-lg-3 col-sm-12 col-xs-12 sidebar" style="margin-top: 0; padding-top: 0;">
				<?php wp_nav_menu( 
					array( 
						'theme_location'  => 'mscat-manual',
						'items_wrap'      => '<ul class="%2$s" style="list-style-type: none; padding-left: 0;">%3$s</ul>', ) ); ?> 
		</aside>		
	</div>
</div>

<?php get_footer();