

document.addEventListener('DOMContentLoaded', function(event){
	// console.log('attach listener -- really');
	
	var radio_training 				= document.getElementById('radio_training');
	var radio_scholarship			= document.getElementById('radio_scholarship');
	var radio_aaucm 				= document.getElementById('radio_aaucm');
	var radio_asoa 					= document.getElementById('radio_asoa');
	var radio_caahep 				= document.getElementById('radio_caahep');
	var radio_citymd 				= document.getElementById('radio_citymd');
	var radio_essen 				= document.getElementById('radio_essen');
	var radio_fairview 				= document.getElementById('radio_fairview');
	var radio_floridaeye 			= document.getElementById('radio_floridaeye');
	var radio_houstonecommcollege 	= document.getElementById('radio_houstoncommcollege');
	var radio_mds 					= document.getElementById('radio_mds');
	var radio_proscribe 			= document.getElementById('radio_proscribe');
	var radio_scribeamerica 		= document.getElementById('radio_scribeamerica');
	var radio_aceso 				= document.getElementById('radio_aceso');
	var radio_other 				= document.getElementById('radio_other');
	
	// radio_stripe 				= document.getElementById('payment_method_stripe');
	// radio_certifier 			= document.getElementById('payment_method_certifier');
	
	// var continueToCheckoutButton	= document.getElementById('place_order');
	
	radio_training.addEventListener('change', processChange);
	radio_scholarship.addEventListener('change', processChange);
	radio_aaucm.addEventListener('change', processChange);
	radio_asoa.addEventListener('change', processChange);
	
	radio_caahep.addEventListener('change', processChange);
	radio_citymd.addEventListener('change', processChange);
	radio_essen.addEventListener('change', processChange);
	radio_fairview.addEventListener('change', processChange);
	radio_floridaeye.addEventListener('change', processChange);
	radio_houstonecommcollege.addEventListener('change', processChange);
	radio_mds.addEventListener('change', processChange);
	radio_proscribe.addEventListener('change', processChange);
	radio_scribeamerica.addEventListener('change', processChange);
	radio_aceso.addEventListener('change', processChange);
	radio_other.addEventListener('change', processChange); 
	
	// radio_stripe.addEventListener('change', processChange);
	// radio_certifier.addEventListener('change', processChange);
	
	// continueToCheckoutButton.addEventListener('click', validateAcknowledgement);

});

function processChange(event){
	
	var radio_stripe = document.getElementById('payment_method_stripe');
	var radio_certifier = document.getElementById('payment_method_certifier');
	
	radio_stripe.addEventListener('change', processChange);
	radio_certifier.addEventListener('change', processChange);
	
	// var acknowledgementCheckbox = document.getElementById('acknowledgementCheckbox');
	// var acknowledgementLabel = document.getElementById('acknowledgementLabel');
	var stripeDescription = document.getElementsByClassName('payment_box payment_method_stripe');
	var certifierDescription = document.getElementsByClassName('payment_box payment_method_certifier');
	
	var radio = event.target;
	
	if(radio.checked){
		console.log('radio.value: ' + radio.value);
		if( (radio.value == 'CityMD') || (radio.value == 'ProScribe') ){
			console.log('set certifier processing');
			radio_certifier.checked = true;
			acknowledgementCheckboxWithPolicies.style.display = 'none';
			acmssPolicies.style.display = 'block';
			stripeDescription.item(0).style.display = 'none';
			certifierDescription.item(0).style.display = 'block';
		}
		else if( (radio.value == 'certifier') && (radio.value != 'CityMD' || radio.value != 'ProScribe') ){
			console.log('set stripe payment1');
			radio_certifier.checked = false;
			radio_stripe.checked = true;
			acknowledgementCheckboxWithPolicies.style.display = 'block';
			acmssPolicies.style.display = 'none';
			stripeDescription.item(0).style.display = 'block';
			certifierDescription.item(0).style.display = 'none';			
		}
		else {
			console.log('set stripe payment2');
			radio_stripe.checked = true;
			acknowledgementCheckboxWithPolicies.style.display = 'block';
			acmssPolicies.style.display = 'none';
			stripeDescription.item(0).style.display = 'block';
			certifierDescription.item(0).style.display = 'none';
			
			
		}
	}
}

/*
function validateAcknowledgement(){
	console.log('validateAcknowledgement');	
}


var certifierRadioControl = document.getElementById('payment_method_certifier');
certifierRadioControl.addEventListener('change', processCheckoutButton);

function processCheckoutButton(){
	console.log('checkoutButton clicked')
}
*/