<?php
	
	function acmss_init() {
		register_sidebar( array(
			'name' 			=> __( 'News & Events', 'acmss' ),
			'id' 			=> 'blognews',
			'description' 	=> __( 'This is custom widget area for the ACMSS 2015 Theme.', 'acmss' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</aside>',
			'before_title' 	=> '<h3 class="widget-title">',
			'after_title' 	=> '</h3>',
		) );
	}
	add_action( 'init', 'acmss_init', 8 );
	
	// remove showing results functionality site-wide
	function woocommerce_result_count() {
		return;
	}
	
	// remove order by drop down
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
	
	function register_custom_menu_page() {
		add_menu_page('View Members', 'Manage Members', 'administrator', 'custompage', '_custom_menu_page', null, 21);
	}
	add_action('admin_menu', 'register_custom_menu_page');
	
	function _custom_menu_page() {
		
		echo '<script>
		function openWindow(groupID) {
								
					//var groupID = 0;
					console.log("groupID: " + groupID);			
					window.open("http://theacmss.org/wp-content/themes/acmss-2015/dataRequest/getUsers.php?groupID="+groupID,"businessEntity","left=50,top=50,height=600,width=600,status=no,scrollbars=yes,titlebar=no,menubar=no,location=no");
						}';
		echo '</script>';
		echo '<div id="myDIV" style="/* border: 1px solid red; */">';
		echo '<H1>Manage Members</H1>';
		echo '<BR>';
		
		global $wpdb;
		$businessEntities = $wpdb->get_results("SELECT group_id, name FROM wp_groups_group WHERE parent_id = 16 ORDER BY name");
		
		
		function removeUnwanted($value) {
			$returnValue = true;
			
			// $position = stripos($value->name, "ACMSS");
			if(preg_match('/ACMSS/',$value->name)) {
				//echo 'found it ';
				$returnValue = false;
			}
			//echo 'name: ' . $value->name . '<BR>';
			
			if( ($value->group_id == 25) || ($value->group_id == 381) ){
				$returnValue = false;
			}
			if(preg_match('/Dynamic/',$value->name)) {
				$returnValue = false;
			}
			if(preg_match('/Test/',$value->name)) {
				$returnValue = false;
			}
			if(preg_match('/N\/A/',$value->name) ) {
				$returnValue = false;
			}
			if(preg_match('/surewhnot/',$value->name) ) {
				$returnValue = false;
			}
			return $returnValue;
		}
		
		
		
		$businessEntities = array_filter($businessEntities,"removeUnwanted");
		
		
		
		
		foreach($businessEntities as $businessEntity) {
			echo '<a style= "font-weight: bold; line-height: 1.25em; font-size: 16px;" onclick="openWindow(' . $businessEntity->group_id . ')">' . $businessEntity->name . '</a>' . '<BR> ';
			// echo $businessEntity->group_id . ' ' . $businessEntity->name . "<BR>";
		}
		
		echo '</div>';
	}
	
	
	// Make Company name required in checkout
	add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

	// Our hooked in function - $address_fields is passed via the filter!
	function custom_override_checkout_fields( $fields ) {
     	// error_log('functions.php :: 96 :: fieldsArray: ' . print_r($fields,true));
     	// $fields['order']['order_comments']['label'] = 'Another label';
     	// $fields['order']['order_comments']['placeholder'] = 'Enter name(s) to apply this purchase.';
     	// $fields['order']['order_comments']['required'] = true;
     	$fields['billing']['billing_company']['required'] = true;
     	
     	// error_log('functions.php :: 101 :: fieldsArray: ' . print_r($fields,true));

     	return $fields;
	}
	
	// remove Order Notes from checkout field in Woocommerce
	add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields' );
	function alter_woocommerce_checkout_fields( $fields ) {
    	unset($fields['order']['order_comments']);
    	return $fields;
	}
	
	
	// WooCommerce Rename Checkout Field
	add_filter( 'woocommerce_checkout_fields' , 'custom_rename_wc_checkout_fields' );

	// Change placeholder and label text
	function custom_rename_wc_checkout_fields( $fields ) {
		// $fields['billing']['billing_first_name']['placeholder'] = 'Wonka';
		$fields['billing']['billing_company']['label'] = 'Employer Name';
		return $fields;
	}
	
	
	// remove Choose an Option from varible product selection
	add_filter( 'woocommerce_dropdown_variation_attribute_options_args','mmx_remove_select_text');
	function mmx_remove_select_text( $args ) {
		global $product;
		if( $product->id == 1743 ) {
			$args['show_option_none'] = 'Select Number to Certify';
			return $args;
		}
		else {
			return $args;
		}	
	}
	
	
	// add extra functionality to checkout form
	add_action('woocommerce_review_order_before_payment','addCheckoutCheckbox',7);
	add_action('woocommerce_review_order_before_payment','addPracticeInfo',9);
	add_action('woocommerce_review_order_after_order_total','addCheckoutScript',10);
	add_action('woocommerce_review_order_before_payment', 'addReturnToStoreButton', 10);
	add_action('woocommerce_review_order_before_payment', 'validateAcknowledgement');
	add_action('woocommerce_review_order_before_payment', 'loadModalContent',5);
	
	function addCheckoutScript(){
		wp_enqueue_script('checkoutScript', get_stylesheet_directory_uri() . '/js/checkoutScript.js');
	}
	function addReturnToStoreButton(){
		echo '<a class="button" style="margin-bottom: 25px;"  href="https://theacmss.org/newstore">Return to Store</a>';
	
	}
	
	function validateAcknowledgement(){
		error_log('validateAcknowledgement');
	}
	
	function loadModalContent(){
		error_log('loadModalContent');
		echo '<div id="specialModal" style="display: none; position: fixed; top: 5px; left: 5px; z-index: 100000;">';
		echo 'some stuff';
		echo '<object width="600" height="800" data="https://theacmss.org/wp-content/uploads/2018/05/Certification-Re-Certification-Compliance-Personnel-Terms-Conditions.pdf"></object>';
		echo '</div>';
	}
	
	function addCheckoutCheckbox() {
		
		/*
		echo '<div style="margin: -20px auto 30px;">';
		echo 'Note: If under the National Certification CMSS Partnership (NCCP) and compliance program with ACMSS, individuals are covered by written employer contract and policy guidelines. Consult employer for any questions or details.';
		echo '</div>';
		*/		
		
		echo '<p style="color: black;" class=""><a href="https://www.guidestar.org/profile/82-3245967/" target="_blank"> Click Here to make a 501(c)(3) Charitable donation to the ACCDO for the responsible growth of the Certified Medical Scribe Specialists through the American College of Clinical Documentation Outcomes initiatives and cause.</a>';
		echo '<BR><BR></p>';
		
		echo '<p id="acknowledgementCheckboxWithPolicies" class="form-row terms">';
		echo '<input type="checkbox" class="input_checkbox" name="acknowledgeRenewal" id="acknowledgeRenewal" value="acknowledged" required/>';
		echo '<label for="acknowledgeRenewal" class="checkbox" style="line-height: 1em; padding-left: 5px;">I acknowledge the required $85 re-certification fee is annual and is charged on the anniversary date. I understand that CMS annual compliance and certification is a mandatory requirement of the CMSS credential. </label>';
		echo ' <a onclick="window.open(\'https://theacmss.org/storePolicies.html\',\'_blank\',\'height=800, width=600\');">To learn more about ACMSS CMSS Certification and Compliance Policies, click here.</a>';
		
		
		// echo ' <a onclick="window.open(\'https://theacmss.org/storePolicies.html\',\'_blank\',\'height=800, width=600\');">Click to view the ACMSS Store Policies.</a></label>';
		
		// echo '<label for="acknowledgeRenewal" class="checkbox">I Acknowledge that ACMSS membership is renewed yearly and that all material presented within the ACMSS website is proprietary intellectual property of ACMSS. <a onclick="window.open(\'https://theacmss.org/storePolicies.html\',\'_blank\',\'height=800, width=600\');">Click to view the ACMSS Store Policies.</a></label>';
		echo '</p>';
		
		echo '<p id="acmssPolicies" class="form-row terms" style="display: none;">';
		// echo '<input type="checkbox" class="input_checkbox" name="acknowledgeRenewal" id="acknowledgeRenewal" value="acknowledged" required/>';
		// echo '<label for="acknowledgeRenewal" class="checkbox" style="line-height: 1em; padding-left: 5px;">I acknowledge the required $85 re-certification fee is annual and is charged on the anniversary date. I understand that CMS annual compliance and certification is a mandatory requirement of the CMSS credential. </label>';
		echo ' <a onclick="window.open(\'https://theacmss.org/storePolicies.html\',\'_blank\',\'height=800, width=600\');">To learn more about ACMSS CMSS Certification and Compliance Policies, click here.</a>';
		
		
		// echo ' <a onclick="window.open(\'https://theacmss.org/storePolicies.html\',\'_blank\',\'height=800, width=600\');">Click to view the ACMSS Store Policies.</a></label>';
		
		// echo '<label for="acknowledgeRenewal" class="checkbox">I Acknowledge that ACMSS membership is renewed yearly and that all material presented within the ACMSS website is proprietary intellectual property of ACMSS. <a onclick="window.open(\'https://theacmss.org/storePolicies.html\',\'_blank\',\'height=800, width=600\');">Click to view the ACMSS Store Policies.</a></label>';
		echo '</p>';
		
		
		echo '<BR><BR><BR>';
		
		
		
	}
	
	function addPracticeInfo() {
		
		echo '<style>';
		echo '.column { float: left; width: 33.33%; } ';
		echo '.row:after { content: "" !important; display: table !important; clear: both !important; }';
		echo '@media screen and (max-width: 600px) { .column { width: 100%; } }';		
		echo '</style>';
		
		// echo '<span style="">Note: If under the National Certification CMSS Partnership (NCCP) and compliance program with ACMSS purchasers will see their contractual price after clicking the "Proceed to Checkout" button and submitting their order.</span><BR><BR>';
		
		echo '<img style="display: block; margin-bottom: 10px;" src="/wp-content/uploads/2018/06/logo-national-partner.png">';
		echo '<div style="width: 100%; margin: 0px auto 15px; padding-left: 2px; background-color: #ffffff; color: black;">';
		echo '<span style="font-weight: bold; font-size: 24px; color: black;">National Certification & Compliance Partners</span><BR><BR>';
		
		echo '<div class="row">';
		echo '<div class="column">';
		echo '<input type=radio id="radio_training" name=certifierName value="Training"> Scribe Specialist Clinical Training & Credentialing Program<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_citymd" name=certifierName value="CityMD"> CityMD<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_mds" name=certifierName value="MDS"> MDS of Kansas<BR><BR>';
		echo '</div>';
		echo '</div>';
		
		echo '<div class="row">';
		echo '<div class="column">';
		echo '<input type=radio id="radio_scholarship" name=certifierName value="Scholarship"> ACCDO Scholarship Program<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_essen" name=certifierName value="Essen"> Essen Medical Associates, PC<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_proscribe" name=certifierName value="ProScribe"> ProScribe<BR><BR>';
		echo '</div>';
		echo '</div>';
		
		echo '<div class="row">';
		echo '<div class="column">';
		echo '<input type=radio id="radio_aaucm" name=certifierName value="AAUCM"> AAUCM<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_fairview" name=certifierName value="FairviewClinic"> Fairview Clinic, PC<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_scribeamerica" name=certifierName value="ScribeAmerica"> ScribeAmerica<BR><BR>';
		echo '</div>';
		echo '</div>';
		
		echo '<div class="row">';
		echo '<div class="column">';
		echo '<input type=radio id="radio_asoa" name=certifierName value="ASOA"> ASOA<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_floridaeye" name=certifierName value="FloridaEye"> Florida Eye Clinic<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_aceso" name=certifierName value="ACESO"> ACESO Scribes<BR><BR>';
		echo '</div>';
		echo '</div>';
		
		echo '<div class="row">';
		echo '<div class="column">';
		echo '<input type=radio id="radio_caahep" name=certifierName value="CAAHEP"> CAAHEP<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_houstoncommcollege" name=certifierName value="HoustonCommColl"> Houston Community College<BR><BR>';
		echo '</div>';
		echo '<div class="column">';
		echo '<input type=radio id="radio_other" name=certifierName value="Other" checked onchange="testScript();"> Other: ';
		echo '<input type=text name=certifierOtherName style="width: 300px;" placeholder="Fill in" required selected>';
		echo '</div>';
		echo '</div>';
		
		echo '</div>';
		
		
	}
	
	// show notice if customer does not tick checkbox
	// add_action('woocommerce_before_checkout_process','purchaserNotApproveRenewal');
	// add_action('woocommerce_checkout_process','purchaserNotApproveRenewal');
	add_action('woocommerce_checkout_process','processPracticeInfo',1);
	
	function purchaserNotApproveRenewal() {
		// error_log( 'functions.php :: 159 :: acknowledgeRenewal Value: ' . $_POST['acknowledgeRenewal'] . ' isset: ' . isset($_POST['acknowledgeRenewal']));
		$checkboxYearlyRenewalValue = $_POST['acknowledgeRenewal'] == 'on' ? 'on' : 'off' ;
		if( (isset($_POST['acknowledgeRenewal'])) && ($checkboxYearlyRenewalValue != 'on') ) {
			wc_add_notice( __('Please Acknowledge Yearly Renewal Policy'), 'error');
		}
		elseif( $checkboxYearlyRenewalValue == 'on' ) {
			// write to DB
			$userID = get_current_user_id();
			add_user_meta($userID,'_acmssAffirmYearlyRenewal','1');
		}
	}
	function processPracticeInfo() {
		// error_log( 'functions.php :: 169 :: certifierName Value: ' . $_POST['certifierName'] );
		$userID = get_current_user_id();
		add_user_meta($userID,'_acmssNationalCertifierName',$_POST['certifierName']);
		add_user_meta($userID,'_acmssCertifierOtherName',$_POST['certifierOtherName']);
	}
	
	/* Plugin Name: First name plus last name as default display name. */
	add_action( 'user_register', 'display_name_first_last' );

	function display_name_first_last( $user_id ) {
    	// $data = get_userdata( $user_id );
    	// error_log('functions.php :: 281 :: $data: ' . print_r($data, true));
    	// $userMeta = get_user_meta($user_id);
    	// error_log('functions.php :: 283 :: $userMeta: ' . print_r($userMeta, true));
    	// error_log('functions.php :: 284 :: $_POST: ' . print_r($_POST, true));
    	$fName = $_POST['billing_first_name'];
    	$lName = $_POST['billing_last_name'];
    	// check if this data are available in your real code!
    	wp_update_user( 
        	array (
            	'ID' => $user_id, 
            	'display_name' => "$fName $lName"
        	) 
    	);
	}
	
	
		
	
?>	