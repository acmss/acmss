<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Apicona
 * @since Apicona 1.0
 */

	function quiz_button_text( $translation, $text = '', $domain = '' ) {
		switch ($translation) {
			case 'Start quiz':
				$translation = 'Start Certification';
				break;
			case 'Restart quiz':
				$translation = 'Restart Certification';
				break;
		}

			return $translation;
	}
	add_filter( 'gettext', 'quiz_button_text' );

	get_header();

	the_post();
?>
<div class="container">
	<div class="row">		
		<div id="primary" class="content-area col-md-12 col-lg-12 col-sm-12 col-xs-12">
			<div id="content" class="site-content" role="main">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="postcontent">
						<header class="entry-header">
							<h2 class="entry-title"><?php the_title(); ?></h2>
						</header>

						<?php the_content(); ?>

						<footer class="entry-meta">
							<div class="footer-entry-meta">
								<?php edit_post_link( __( 'Edit', 'apicona' ), '<span class="edit-link">', '</span>' ); ?>
							</div>
						</footer>
					</div>
					<div class="clearfix"></div>
				</article>
			</div>
		</div>
	</div>
</div>

<?php get_footer();