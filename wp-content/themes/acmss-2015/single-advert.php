<?php
/**
 * The template for displaying single WPAdverts
 *
 */

get_header();

// Checking if Blog page
$primaryclass = 'col-md-12 col-lg-12 col-sm-12 col-xs-12';
global $apicona;
$sidebar = $apicona['sidebar_blog']; // Global settings

$sidebarposition = get_post_meta( get_the_ID(), '_kwayy_post_options_sidebarposition', true);

// Page settings
if( isset($sidebarposition) && trim($sidebarposition) != '' ){
	$sidebar = $sidebarposition;
}

// Primary Content class
$primaryclass = setPrimaryClass($sidebar);


?>
<div class="container">
<div class="row">		

	<div id="primary" style="width: 90% !important;" class="content-area <?php echo $primaryclass; ?>">
		<div id="content" class="site-content" role="main">
<!-- Hi -->
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php apicona_post_nav(); ?>
				<?php comments_template(); ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->


	
</div><!-- .row -->
</div><!-- .container -->
<?php get_footer(); ?>