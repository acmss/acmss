<?php
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;	
	
	$quizTitles = array(
		"2614" =>"MSCAT Certification (Emergency Medicine)",
		"10651"=>"MSCAT Certification (Oncology)",
		"4955" =>"MSCAT Certification (Internal Medicine)",
		"4954" =>"MSCAT Certification (Primary Care)",
		"2548" =>"MSCAT Certification (Dermatology)",
		"2547" =>"MSCAT Certification (Vascular Care)",
		"2546" =>"MSCAT Certification (Urgent Care)",
		"2336" =>"MSCAT Certification (Ophthalmology)",
		"1758" =>"MSCAT Certification (General)",
		"11837"=>"CSE Ophthalmology Level 1",
		"11836"=>"CSE Ophthalmology Level 2",
		"12213"=>"CSE E&M",
		"13482"=>"CSE, CDS",
		"13480"=>"CSE, CPOE",
		"11840"=>"CSE, HIPAA Level 1",
		"11841"=>"CSE Emergency Medicine - Cardiac",
		"1568" =>"HIPAA for Medical Scribes",
		"4953" =>"Reporting and Compliance",
		"11839"=>"CSE Reporting, Compliance, and HIPAA"
	);

	$annualContractDates = array(
		"ProScribe, LLC"=>"Oct 15, 2017"
	
	
	
	
	);
	
	
	// who am I
	$userID = get_current_user_id();
	
	// error_log('Hi from getAdminUserInfo -- my userID is: ' . $userID);
	
	$organizationName = ACMSS()->get_org_name( $userId );
	// error_log('Organization Name: ' . $organizationName);
	if( !empty($organizationName) ) {
		
		// filename 
		$fileName = "websiteData_" . date('Ymd') . '.csv';		
		error_log('fileName: ' . $fileName);
		
		header("Content-Disposition: attachment; filename=\"$fileName\"");
		header("Content-Type: text/csv");
		
		// $fileHandle = fopen($fileName,"w");
		$fileHandle = fopen("php://output","w");
		
		// header / column titles
		$columnTitles = array(
			'First Name',
			'Last Name',
			'Annual Contract Date',
			'Employee to the MMA',
			'CMS Compliance',
			'CMS Hours',
			'Date MSCAT #1 Taken',
			'MSCAT Certification 1',
			'Pass/Fail',
			'%',
			'Date MSCAT #2 Taken',
			'MSCAT Certification 2',
			'Pass/Fail',
			'%',
			'Date MSCAT #3 Taken',
			'MSCAT Certification 3',
			'Pass/Fail',
			'%',
			'Date MSCAT #4 Taken',
			'MSCAT Certification 4',
			'Pass/Fail',
			'%',
			'Date MSCAT #5 Taken',
			'MSCAT Certification 5',
			'Pass/Fail',
			'%'
		
		);
		
		fputcsv($fileHandle,$columnTitles,',','"');
	
		
		// get userIDs
		$user_ids = $wpdb->get_col( "SELECT DISTINCT g1.user_id FROM wp_groups_user_group g1 WHERE g1.group_id IN (SELECT group_id FROM wp_groups_group WHERE name = '$organizationName');");
		
		error_log( 'userIDs: ' . print_r($user_ids,true) );

		// output organization name on a separate line
		// $outputOrganzationName = array($organizationName);
		// fputcsv($fileHandle,$outputOrganzationName,',','"');
		
		// get each user record
		foreach($user_ids as $userID) {
			$userMeta = get_user_meta($userID);
			// error_log( 'userMeta: ' . print_r($userMeta,true) );
			$userData = get_userdata($userID);
			// error_log( 'userData: ' . print_r($userData,true) );
			
			// name
			$firstName = $userMeta['first_name'][0];
			$lastName  = $userMeta['last_name'][0];
			// error_log( 'firstName: ' . $firstName . ' lastName: ' . $lastName);
			
			// contract date
			if($organizationName == 'ProScribe, LLC'){
				$contractDate = $annualContractDates['ProScribe, LLC'];
			}
			else {
				$contractDate = ' ';
			}
			
			
			
			
			
			
			
			
			// registration date
			$regDate = $userData->user_registered;
			// error_log( 'RegistrationDate: ' . $regDate);
			$dateParts = explode(' ', $regDate);
			$registrationDate = $dateParts[0];
			
			// current certification / subscription
			$userHasSubscription = WC_Subscriptions_Manager::user_has_subscription($userID) ? 'Yes' : 'No' ;
			// error_log('user has subscription: ' . $userHasSubscription);
			$userHasSubscriptionString = ($userHasSubscription == 'Yes') ? 'User has a subscription' : 'User does NOT have a subscription' ;
			
			// CME hours
			$continuingEducation = unserialize($userMeta['_continuing_ed'][0]) ? : 'None' ;	
			// error_log('continuing education: ' . print_r($continuingEducation,true) );	
			// $list = ACMSS()->continuing_ed_list();
			// if($continuingEducation != 'None') {
				// $output = array(' ',' ','Continuing Education');
				// fputcsv($fileHandle,$output,',','"');
			// }
			
			$totalCSEhours = 0;
			foreach($continuingEducation as $record) {
				// $certificationDate = date("F j, Y",strtotime($record['date']));
				// $certifcationTopic= $list[$record['topic']];
				$certificationHours = $record['hours'];
				
				// $output = array("  ","  ",$certifcationTopic,' ',' ',$certificationDate,$certificaitonHours);
				// fputcsv($fileHandle,$output,',','"');
				$totalCSEhours = $totalCSEhours + $certificationHours;
				// error_log('totalCSEhours: ' . $totalCSEhours);
			}
		
			// $output = array(' ','Renewal Date and CSE hours'); updated 12/13/17 kls
			// $output = array(' ',' ','CSE hours');
			// fputcsv($fileHandle,$output,',','"');			
			// error_log('Total CSE hours: ' . $totalCSEhours);
		
			// certifications
			$quizzes = unserialize($userMeta['_sfwd-quizzes'][0]);
			// error_log('quizzes: ' . print_r($quizzes,true) );
			
			if($userID == 8479){
				error_log('getAdminUserInfo.php :: 168 :: $quizzes: ' . print_r($quizzes,true) );
			}
			
			// $quiz0 = $quizzes[0];
			$quiz1 = $quizzes[0];
			$quiz2 = $quizzes[1];
			$quiz3 = $quizzes[2];
			$quiz4 = $quizzes[3];
			$quiz5 = $quizzes[4];
			
			// error_log('quiz1: ' . print_r($quiz1,true) );
			// error_log('quiz2: ' . print_r($quiz2,true) );
			// error_log('quiz3: ' . print_r($quiz3,true) );
			// error_log('quiz4: ' . print_r($quiz4,true) );
			// error_log('quiz5: ' . print_r($quiz5,true) );
			
			// quiz 1
			$MSCAT1taken = $quiz1[time] ? date("F j, Y",$quiz1['time']) : '';
			$MSCATquizID = $quiz1[quiz];
			$MSCAT1title = $quiz1[quiz] ? $quizTitles[$MSCATquizID] : '';
			
			// $MSCAT1status = $quiz1[pass] > 0 ? $quiz[quiz] == 1 ? 'PASS' : 'FAIL' : '' ;
			if($quiz1[quiz] == ''){ $MSCAT1status = '';}
			elseif($quiz1[pass] == 1){ $MSCAT1status = 'PASS';}
			else{ $MSCAT1status = 'FAIL';}
			
			$MSCAT1percentage = $quiz1[percentage] ? : '';
			
			// quiz 2
			$MSCAT2taken = $quiz2[time] ? date("F j, Y",$quiz1['time']) : '';
			$MSCATquizID = $quiz2[quiz];
			$MSCAT2title = $quiz2[quiz] ? $quizTitles[$MSCATquizID] : '';
			
			// $MSCAT2status = $quiz2[pass] ? $quiz2[quiz] == true ? 'PASS' : 'FAIL' : '';
			if($quiz2[quiz] == ''){ $MSCAT2status = '';}
			elseif($quiz2[pass] == 1){ $MSCAT2status = 'PASS';}
			else{ $MSCAT2status = 'FAIL';}
			
			$MSCAT2percentage = $quiz2[percentage] ? : '';
			
			// quiz 3
			$MSCAT3taken = $quiz3[time] ? date("F j, Y",$quiz1['time']) : '';
			$MSCATquizID = $quiz3[quiz];
			$MSCAT3title = $quiz3[quiz] ? $quizTitles[$MSCATquizID] : '';
			
			// $MSCAT3status = $quiz3[pass] ? $quiz3[quiz] == true ? 'PASS' : 'FAIL' : '';
			if($quiz3[quiz] == ''){ $MSCAT3status = '';}
			elseif($quiz3[pass] == 1){ $MSCAT3status = 'PASS';}
			else{ $MSCAT3status = 'FAIL';}
			
			$MSCAT3percentage = $quiz3[percentage] ? : '';
			
			// quiz 4
			$MSCAT4taken = $quiz4[time] ? date("F j, Y",$quiz1['time']) : '';
			$MSCATquizID = $quiz4[quiz];
			$MSCAT4title = $quiz4[quiz] ? $quizTitles[$MSCATquizID] : '';
			
			// $MSCAT4status = $quiz4[pass] ? $quiz4[quiz] == true ? 'PASS' : 'FAIL' : '';
			if($quiz4[quiz] == ''){ $MSCAT4status = '';}
			elseif($quiz4[pass] == 1){ $MSCAT4status = 'PASS';}
			else{ $MSCAT4status = 'FAIL';}
			
			$MSCAT4percentage = $quiz4[percentage] ? : '';
			
			// quiz 5
			$MSCAT5taken = $quiz5[time] ? date("F j, Y",$quiz1['time']) : '';
			$MSCATquizID = $quiz5[quiz];
			$MSCAT5title = $quiz5[quiz] ? $quizTitles[$MSCATquizID] : '';
			
			// $MSCAT5status = $quiz5[pass] ? $quiz5[quiz] == true ? 'PASS' : 'FAIL' : '';
			if($quiz5[quiz] == ''){ $MSCAT5status = '';}
			elseif($quiz5[pass] == 1){ $MSCAT5status = 'PASS';}
			else{ $MSCAT5status = 'FAIL';}
			
			$MSCAT5percentage = $quiz5[percentage] ? : '';




/*
			// output each certification		
			for( $i = 0; $i < 5; $i++ ) {
				// quiz ID
				$quizData = $quizzes[$i];
				error_log ('getAdminUserInfo.php :: 159 :: quizData for quizID: ' . $i . ' -- ' . print_r($quizData,true));		
		
				// if no ID go to next record
				// if( !$quizID ) { continue; }
		
				// quiz title
				$quizNumber = $quizID;
				$quizName = $quizTitles[$quizNumber];
				
				// reset cseRenewalDate 
				// $cseRenewalDate = '';
				// get MSCAT pass date 
				// error_log('strpos MSCAT: ' . strpos($quizName,"MSCAT"));
				if( strpos($quizName,'MSCAT') > -1 ) {
					$cseStartDate = date("F j, Y",$quiz['time']);
					$cseStartMonthDay = date("F j,",$quiz['time']);
					$cseStartYear = date("Y",$quiz['time']);
					// error_log('cseStartDate: ' . $cseStartDate);
					$cseRenewalYear = $cseStartYear + 3;
					$cseRenewalDate = $cseStartMonthDay . ' ' . $cseRenewalYear;
					// error_log('cseReneewalDate: ' . $cseRenewalDate);
				}
				
				// echo ' ' . $quizName;
				// echo ' - ';
				// percentage / grade
				// echo $quiz['percentage'] . '% ';
				// pass / fail status
				$quizPassStatus = $quiz['pass'] ? '<span style="font-weight: bold; color: green;">Pass</span>' : '<span style="font-weight: bold; color: red;">Fail</span>';
				// echo ' ' . $quizPassStatus;
				
				$quizPass = $quiz['pass'] ? 'Pass' : 'Fail';

				// date / time taken
				$quizTime = $quiz['time'];
				// echo ' ' . date("r",$quizTime);
				// echo ' ' . date("F d, Y",$quizTime);		

				if($quizName == '') {$quizName = 'No Certs Attempted';}
				// echo '<BR>';
				$outputData = array('   ','   ',$quizName,$quiz['percentage'],$quizPass,date("F j, Y",$quiz['time']));
				error_log('getAdminUserInfo.php :: 201 :: quiz info: ' . print_r($outputData,true));
				// write to csv file 
				// fputcsv($fileHandle,$outputData,',','"');
		}
*/		
		
	
		
		$columnData = array(
			$firstName,
			$lastName,
			$contractDate,
			$registrationDate,
			$userHasSubscriptionString,
			$totalCSEhours,
			$MSCAT1taken,
			$MSCAT1title,
			$MSCAT1status,
			$MSCAT1percentage,
			$MSCAT2taken,
			$MSCAT2title,
			$MSCAT2status,
			$MSCAT2percentage,
			$MSCAT3taken,
			$MSCAT3title,
			$MSCAT3status,
			$MSCAT3percentage,
			$MSCAT4taken,
			$MSCAT4title,
			$MSCAT4status,
			$MSCAT4percentage,
			$MSCAT5taken,
			$MSCAT5title,
			$MSCAT5status,
			$MSCAT5percentage,
			
		);
		
		fputcsv($fileHandle,$columnData,',','"');
	
	
	
	
	}
	fclose($fileHandle);
	
	}

?>