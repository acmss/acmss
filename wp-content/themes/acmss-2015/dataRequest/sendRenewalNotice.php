<?php
	// program to send renewal emails to subscribers 15 days before their accounts require renewal
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;
	
	$offset = $_GET['offset'];
	$limit  = $_GET['limit'];
	
	// set test mode 
	$testMode = 0;

	// get upper and lower bounds for comparison
	
	$currentTimestamp = time();
	$secondsInDay = 86400;
	
	if($testMode == 1) {	
		error_log( 'currentTimestamp: ' . $currentTimestamp );
	}
	
	$lowerBoundOffset = 14; // rounds up a day
	$upperBoundOffset = 15;
	$lowerBound = $currentTimestamp + ($lowerBoundOffset * $secondsInDay);
	$upperBound = $currentTimestamp + ($upperBoundOffset * $secondsInDay);
	// $upperBound = $currentTimestamp + 29 * $secondsInDay; **** for testing ****
	
	if($testMode == 1) {
		error_log( 'lowerBound: ' . $lowerBound );
		error_log( 'upperBound: ' . $upperBound );
		
		echo 'lower bounds: ' . $lowerBound . ' ' . date("r",$lowerBound) . ' upper bounds: ' . $upperBound . ' ' . date("r",$upperBound) . "\n\r\n\r";
	
	}
	
	// get all posts records where post_type = 'shop_subscription' and post_status = 'active'
	$postsArray = $wpdb->get_results ( " SELECT ID FROM  $wpdb->posts WHERE post_type = 'shop_subscription' AND post_status = 'wc-active' LIMIT $offset,$limit" );
	
	/*
	echo 'postsArray: ' . "\n\r"; 
	print_r($postsArray);
	echo "\n\r";
	*/
	
	// get all subscriber userIDs
	
	$userIDstack = array();
	$idsOfUsersToSendReminder = array();
	
	foreach( $postsArray as $post ) {
		$postID = $post->ID;
		// echo 'postID: ' . $postID . '<BR>';
		$row = $wpdb->get_results( "SELECT meta_value FROM $wpdb->postmeta WHERE post_id = " . $postID . " AND meta_key = '_customer_user' " );
		// echo 'row: ' . print_r($row) . '<BR>';
		$userID = $row[0]->meta_value;
		// echo 'userID: ' . $userID . '<BR>';
		// push $userID on $userIDstack
		array_push($userIDstack,$userID);
		
	}
	
	// echo 'userIDstack: ' . print_r($userIDstack) . '<BR>';
	$arrayLength = count($userIDstack);
	error_log( 'length of array: ' . $arrayLength );
	
	// filter stack using array_unique
	
	$userIDstack = array_unique($userIDstack);
	// echo 'userIDstack: ' . print_r($userIDstack) . '<BR>';
	$arrayLength = count($userIDstack);
	error_log( 'length of array: ' . $arrayLength );
	
	/*
	echo 'UserID Stack:' . "\n\r";
	print_r($userIDstack);
	echo "\n\r\n\r";
	*/
		
	// iterate though $userIDstack pushing ids of users needing reminder email onto stack
	foreach( $userIDstack as $userID ) {
	
		$subscriptions = wcs_get_users_subscriptions($userID);
		// error_log('subscriptions: ' . print_r($subscriptions,true));
		$nextPaymentDatesArray = array();
		// echo 'userID: ' . $userID;
		foreach($subscriptions as $subscriptionID => $subscription) {
			// error_log('orderNumber: ' . $subscription->get_order_number());
			// echo 'nextPaymentDate: ' . $subscription->get_date_to_display( 'next_payment' ) . "\n\r";
			$nextPaymentDate = strtotime($subscription->get_date_to_display( 'next_payment' )) ?: 0;
			// error_log('nextPaymentDate timestamp: ' . $nextPaymentDate);
			// echo ' -- nextPaymentDate: ' . $subscription->get_date_to_display( 'next_payment' ) . ' -- ' . $nextPaymentDate . "\n\r";
			if($nextPaymentDate > 0) {
				array_push($nextPaymentDatesArray,$nextPaymentDate);
			}
			$nextPaymentDate = 0;			
		}
		asort($nextPaymentDatesArray);
		
		if($testMode == 1) {
			echo "\n\r\n\r";
			print_r($nextPaymentDatesArray);
			echo "\n\r\n\r";
		}
		
		$sortedNextPaymentDate = array_shift($nextPaymentDatesArray);
		
		// echo 'sortedNextPaymentDate: ' . $sortedNextPaymentDate . '<BR>';
	
		// compare $sortedNextPaymentDate with range of timestamps for inclusion in email notification
		// if in range push on $idsOfUsersToSendReminder
		if( ($sortedNextPaymentDate >= $lowerBound) && ($sortedNextPaymentDate <= $upperBound) ) {
			// push on stack
			array_push($idsOfUsersToSendReminder,$userID,$sortedNextPaymentDate);
			// echo ' send Reminder' . "\n\r";
		}
		else {
			// echo "\n\r";
		}	
	}
	
	
	
	// test mode
	if($testMode == 1) {
		
		print_r($idsOfUsersToSendReminder);
		echo "\n\r\n\r";
		
		
		
		
		//$idsOfUsersToSendReminder = array(5627,00000,5856,000000,5852,00000);
	}
	
	
	error_log( 'idsOfUserToSendReminder: ' . print_r($idsOfUsersToSendReminder,true));
	
	// set headers for html email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset-UTF8" .  "\r\n";
	$headers .= "From: <support@theACMSS.org>" . "\r\n";
	// email subject
	$subject  = "Renewal Reminder Email";
		
	// iterate through stack formatting and sending reminder emails
	
	for( $i = 0; $i < count($idsOfUsersToSendReminder); $i += 2 ) {
		$userID = $idsOfUsersToSendReminder[$i];
		$timestamp = $idsOfUsersToSendReminder[$i + 1];
		error_log( 'userID: ' . $userID . ' timestamp: ' . $timestamp );
		$fName = get_user_meta($userID,'first_name',true);
		$lName = get_user_meta($userID,'last_name',true);
		$email = get_the_author_meta('user_email',$userID);
		$renewalDateString = date("F j, Y",$timestamp);
						
		echo( 'Name: ' . $fName . ' ' . $lName . ' email: ' . $email . ' renewalDate: ' . $renewalDateString . "<BR>" );
		
		// send email to user
		
		// build $message
		$message = '';
		$message .= 'Hi ' .  $fName . ' ' . $lName . ',<BR><BR>';
		$message .= 'It appears your membership requires renewal within the next two weeks. Please renew today, if there are changes to your account personnel number contact <a href="mailto:developer@theacmss.org">Customer Service</a> for account review at:<BR><BR>';
		$message .= 'Phone: (800) 987-3692<BR>';
		$message .= 'Email: support@acmss.zendesk.com<BR><BR>';
		$message .= 'To meet CMS regulatory guidance and compliance, each individual receives a new printable CMS Compliance Certificate, annually, available in account. Individuals need to keep a copy for themselves and provide a new copy to Practice Administrator and/or Physician to meet audit compliance. Individual certificates include unique Meaningful Use CEHRT identification number.<BR><BR>';
		// $message .= 'CMS recognizes CMSS certification and credentials to enable Physicians and Practice Administrators to achieve national compliance and innovation through appropriate Physician/CMSS Team utilization, building our 21st Century patient-centered preventive wellness healthcare.<BR><BR>';
		$message .= 'Thank you for being a valued member of ACMSS. We look forward to growing and building high-calibur, innovative, physician/CMSS Teams across America, in compliance with CMS.<BR><BR>';
		// $message .= 'ACMSS innovates and customizes new care models specifically designed around your practice and population health needs, meeting MU and revolutionizing care through MACRA.';
		$message .= ' Learn more, make plans, and let\'s begin with strategic <a href="https://theacmss.org/consulting-questionnaire">Customized Consulting</a> planning with your practice today!<BR><BR>';
		$message .= 'ACMSS Staff & Team<BR>';
		$message .= '(800) 987-3692<BR>';
		$message .= 'TheACMSS.org';
		
		$to = $email;
		
		echo('to: ' . $to . '<BR> subject: ' . $subject . '<BR> message: ' . $message . '<BR> headers: ' . $headers . '<BR><BR>');
		
		// override $to
		// $to = 'kurt@www-masters.com';
		
		// send email
		mail($to,$subject,$message,$headers);
		

		
	}
	
	echo("Emails sent:<BR><BR>");
	for( $i = 0; $i < count($idsOfUsersToSendReminder); $i += 2 ) {
		$userID = $idsOfUsersToSendReminder[$i];
		$fName = get_user_meta($userID,'first_name',true);
		$lName = get_user_meta($userID,'last_name',true);
		$recordNumber = ($i / 2) + 1;
		echo('Record ' . $recordNumber . ' Name: ' . $fName . ' ' . $lName . "<BR>");
	
	}
	

	error_log('sendRenewalNotice.php has finished running');
	
	
	
	
	
	


?>