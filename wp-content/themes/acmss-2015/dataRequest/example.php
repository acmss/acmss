<?php

require_once('../../../../wp-load.php');
global $wp,$wpdb;

require_once( 'lib/woocommerce-api.php' );

$options = array(
	'debug'           => true,
	'return_as_array' => false,
	'validate_url'    => false,
	'timeout'         => 30,
	'ssl_verify'      => false,
);


/*
$data = array (
	"subscription" => array (
		"billing_period" 	=> "month",
		"billing_interval" 	=> 1,
		"start_date"		=> "2015-05-10 08:30:00",
		"next_payment_date"	=> "2015-06-10 08:30:00"
	),
	"payment_details" => array (
		"method_id"			=> "stripe",
		"method_title"		=> "Stripe (Credit Card)",
		"post_meta"			=> array (
			"_stripe_customer_id"	=> "cus_T35TCU57M3R",
			"_stripe_card_id"		=> "card_T35TC4RD"
		),	
    ),
    "billing_address" => array (
    	"first_name"		=> "John",
    	"last_name"			=> "Doe",
    	"address_1"			=> "969 Market",
    	"address_2"			=> "",
    	"city"				=> "San Francisco",
    	"state"				=> "CA",
    	"postcode"			=> "94103",
    	"country"			=> "US",
    	"email"				=> "john.doe@example.com",
    	"phone"				=> "(555) 555-5555"
    ),
    "shipping_address" => array (
    	"first_name"		=> "John",
    	"last_name"			=> "Doe",
    	"address_1"			=> "969 Market",
    	"address_2"			=> "",
    	"city"				=> "San Francisco",
    	"state"				=> "CA",
    	"postcode"			=> "94103",
    	"country"			=> "US"
    ),	
    "customer_id"			=> 2,
    "line_items" => array (
    	array (
    		"product_id"	=> 546,
    		"quantity"		=> 2
    	),	
      	array (
      		"product_id"	=> 613,
      		"quantity"		=> 1,
      		"variations" array (
      			"pa_color"	=> "Black"
      		),	
        ),
    ),
    "shipping_lines" => array (
    	array (
    		"method_id"		=> "flat_rate",
    		"method_title"	=> "Flat Rate",
    		"total"			=> 10
    	),
    ),		
 );
 */
 
/*
$data = array (
	"subscription" => array (
		"billing_period" 	=> "month",
		"billing_interval" 	=> 1,
		"status"			=> "active",
		"start_date"		=> "2016-12-17 08:30:00",
		"next_payment_date"	=> "2017-01-17 08:30:00",
		"customer_id"		=> 5856,
		"payment_details" => array (
			"method_id"			=> "manual",
			"method_title"		=> "Manual Renewal"
    	),
    	"billing_address" => array (
    		"first_name"		=> "Bozo",
    		"last_name"			=> "the Clown",
    		"address_1"			=> "969 Market",
    		"address_2"			=> "",
    		"city"				=> "San Francisco",
    		"state"				=> "CA",
    		"postcode"			=> "94103",
    		"country"			=> "US",
    		"email"				=> "bozo.the.clown@example.com",
    		"phone"				=> "(555) 555-5555"
    	),
    	"shipping_address" => array (
    		"first_name"		=> "Bozo",
    		"last_name"			=> "II",
    		"address_1"			=> "969 Market",
    		"address_2"			=> "",
    		"city"				=> "San Francisco",
    		"state"				=> "CA",
    		"postcode"			=> "94103",
    		"country"			=> "US"
    	),
    	"line_items" => array (
    		array (
    			"product_id"	=> 11500,
    			"quantity"		=> 1
    		)
    	)
    )			
 );
 */
 
 // get userIDs
 

 // $userIDs = $wpdb->get_col( "SELECT ID FROM wp_users ORDER BY ID LIMIT 6390,20;" );
 // $userIDs = $wpdb->get_col( "SELECT ID FROM wp_users ORDER BY ID;" );

 // echo 'userIDs: ' . print_r( $userIDs );
/*
 foreach($userIDs as $userID) {
 	echo 'userID: ' . $userID;
 	// echo "<BR>";
 	$fName = get_user_meta($userID, 'first_name', true);
 	$lName = get_user_meta($userID, 'last_name', true);
 	
 	if( $fName == '' && $lName == '' ) {
 		echo 'no first or last name';
 		//update_user_meta($userID,'last_name','Unknown','');
 	}
 	
 	$userInfo = get_userdata($userID);
 	$email = $userInfo->user_email;
 	echo ' <span style="color:red;">' . $fName . ' ' . $lName . '</span> ' . $email . '<BR>';
}
*/ 

 $customerID = 5627;
 $data = array (
	"subscription" => array (
		// "billing_period" 	=> "year",
		// "billing_interval" 	=> 1,
		// "status"			=> "active",
		// "start_date"		=> "2016-12-17 08:30:00",
		// "next_payment_date"	=> "2018-06-01 08:30:00",
		// "customer_id"		=> $customerID,
		// "note"				=> "Record adjusted via program 3",
		// "payment_details" => array (
			// "method_id"			=> "manual",
			// "method_title"		=> "Manual Renewal"
    	// ),
    	"line_items" => array (
    		array (
    			"id"			=> 16549,
    			"product_id"	=> 1743,
    			"quantity"		=> 25
    		)
    	)
    )			
 );

/*
$data = array(
	"subscription" => array(
		"customer_id"=> 5627,
		"line_items" => array(
			"id"			=> 11504,
			"product_id"	=> 1743,
			"quantity" 		=> 10
		)
	)
);
*/ 
 

try {

	$client = new WC_API_Client( 'https://theacmss.org', 'ck_d9374ac6271db98513069d1d054f12344287519a', 'cs_5a594c40ef64271c0dae5401cc13c412f9393a7c', $options );

	// coupons
	//print_r( $client->coupons->get() );
	//$coupon_id = 20578;
	//print_r( $client->coupons->get( $coupon_id ) );
	//print_r( $client->coupons->get_by_code( 'coupon-code' ) );
	//print_r( $client->coupons->create( array( 'code' => 'test-coupon', 'type' => 'fixed_cart', 'amount' => 10 ) ) );
	//print_r( $client->coupons->update( $coupon_id, array( 'description' => 'new description' ) ) );
	//print_r( $client->coupons->delete( $coupon_id ) );
	//print_r( $client->coupons->get_count() );

	// custom
	//$client->custom->setup( 'webhooks', 'webhook' );
	//print_r( $client->custom->get( $params ) );

	// customers
	//print_r( $client->customers->get() );
	//print_r( $client->customers->get( 5627 ) );
	//print_r( $client->customers->get_by_email( 'kurt@www-masters.com' ) );
	//print_r( $client->customers->create( array( 'email' => 'woothemes@mailinator.com' ) ) );
	//print_r( $client->customers->update( $customer_id, array( 'first_name' => 'John', 'last_name' => 'Galt' ) ) );
	//print_r( $client->customers->delete( $customer_id ) );
	//print_r( $client->customers->get_count( array( 'filter[limit]' => '-1' ) ) );
	//print_r( $client->customers->get_orders( $customer_id ) );
	//print_r( $client->customers->get_downloads( $customer_id ) );
	//$customer = $client->customers->get( 5627 );
	//echo "<BR><BR>" . 'Customer: ' . $customer->customer->first_name . ' ' . $customer->customer->last_name;
	//$customer->customer->last_name = 'New Last Name';
	//print_r( $client->customers->update( $customer_id, (array) $customer ) );

	// index
	//print_r( $client->index->get() );

	// orders
	//print_r( $client->orders->get() );
	//print_r( $client->orders->get( 20156 ) );
	//print_r( $client->orders->update_status( $order_id, 'pending' ) );
	
	// subscriptions
	//print_r( $client->subscriptions->get( 29968 ) );
	//print_r( $client->subscriptions->get_count());
	//$subscriptionCountObject = $client->subscriptions->get_count();
	//echo 'Number of subscriptions: ' . $subscriptionCountObject->count;
	//print_r( $client->subscriptions->create( $data ) );
	//print_r( $client->subscriptions->delete( $subscription_id ) );
	$subscription_id = 29968;
	print_r( $client->subscriptions->update( $subscription_id, $data ) );
	
/*	
 foreach($userIDs as $userID) {
 	echo 'userID: ' . $userID;
 	// echo "<BR>";
 	$fName = get_user_meta($userID, 'first_name', true);
 	$lName = get_user_meta($userID, 'last_name', true);
 	echo ' ' . $fName . ' ' . $lName . '<BR>';


 	if(wcs_get_users_subscriptions($userID)) {
 		echo ' has subscription.<BR>';
 	}
 	else {
 		echo ' has NO subscription.<BR>';
 		
 		 $data = array (
			"subscription" => array (
				"billing_period" 	=> "year",
				"billing_interval" 	=> 1,
				"status"			=> "active",
				"start_date"		=> "2016-12-17 08:30:00",
				"next_payment_date"	=> "2017-10-01 08:30:00",
				"customer_id"		=> $userID,
				"note"				=> "Record added via program",
				"payment_details" => array (
					"method_id"			=> "manual",
					"method_title"		=> "Manual Renewal"
				),
				"line_items" => array (
					array (
						"product_id"	=> 11500,
						"quantity"		=> 1
					)
				)
			)			
		 );
 		// $client->subscriptions->create( $data );
 		echo 'subscription created. <BR>';
 	}
 }	
*/	
	

	// order notes
	//print_r( $client->order_notes->get( $order_id ) );
	//print_r( $client->order_notes->create( $order_id, array( 'note' => 'Some order note' ) ) );
	//print_r( $client->order_notes->update( $order_id, $note_id, array( 'note' => 'An updated order note' ) ) );
	//print_r( $client->order_notes->delete( $order_id, $note_id ) );

	// order refunds
	//print_r( $client->order_refunds->get( $order_id ) );
	//print_r( $client->order_refunds->get( $order_id, $refund_id ) );
	//print_r( $client->order_refunds->create( $order_id, array( 'amount' => 1.00, 'reason' => 'cancellation' ) ) );
	//print_r( $client->order_refunds->update( $order_id, $refund_id, array( 'reason' => 'who knows' ) ) );
	//print_r( $client->order_refunds->delete( $order_id, $refund_id ) );

	// products
	//print_r( $client->products->get() );
	//print_r( $client->products->get( $product_id ) );
	//print_r( $client->products->get( $variation_id ) );
	//print_r( $client->products->get_by_sku( '1000' ) );
	//print_r( $client->products->create( array( 'title' => 'Test Product', 'type' => 'simple', 'regular_price' => '9.99', 'description' => 'test' ) ) );
	//print_r( $client->products->update( $product_id, array( 'title' => 'Yet another test product' ) ) );
	//print_r( $client->products->delete( $product_id, true ) );
	//print_r( $client->products->get_count() );
	//print_r( $client->products->get_count( array( 'type' => 'simple' ) ) );
	//print_r( $client->products->get_categories() );
	//print_r( $client->products->get_categories( $category_id ) );

	// reports
	//print_r( $client->reports->get() );
	//print_r( $client->reports->get_sales( array( 'filter[date_min]' => '2014-07-01' ) ) );
	//print_r( $client->reports->get_top_sellers( array( 'filter[date_min]' => '2014-07-01' ) ) );

	// webhooks
	//print_r( $client->webhooks->get() );
	//print_r( $client->webhooks->create( array( 'topic' => 'coupon.created', 'delivery_url' => 'http://requestb.in/' ) ) );
	//print_r( $client->webhooks->update( $webhook_id, array( 'secret' => 'some_secret' ) ) );
	//print_r( $client->webhooks->delete( $webhook_id ) );
	//print_r( $client->webhooks->get_count() );
	//print_r( $client->webhooks->get_deliveries( $webhook_id ) );
	//print_r( $client->webhooks->get_delivery( $webhook_id, $delivery_id );

	// trigger an error
	//print_r( $client->orders->get( 0 ) );

} catch ( WC_API_Client_Exception $e ) {

	echo $e->getMessage() . PHP_EOL;
	echo $e->getCode() . PHP_EOL;

	if ( $e instanceof WC_API_Client_HTTP_Exception ) {

		print_r( $e->get_request() );
		print_r( $e->get_response() );
	}
}
