<?php
	// program to send notice emails to non-subscribers
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;

	// get upper and lower bounds for comparison
	
	$currentTimestamp = time();
	$secondsInDay = 86400;
	// error_log( 'currentTimestamp: ' . $currentTimestamp );
	$lowerBound = $currentTimestamp + 15 * $secondsInDay;
	$upperBound = $currentTimestamp + 16 * $secondsInDay;
	
	$cutOffDateString = 'Sept 1, 2016';
	$cutOffDateTimestamp = strtotime($cutOffDateString);
	echo('cutOffDateString: ' . $cutOffDateString . ' cutOffDateTimestamp: ' . $cutOffDateTimestamp . '<BR>');
	// $upperBound = $currentTimestamp + 29 * $secondsInDay; **** for testing ****
	// error_log( 'lowerBound: ' . $lowerBound );
	// error_log( 'upperBound: ' . $upperBound );

/*	
	// get all posts records where post_type = 'shop_subscription' and post_status = 'active'
	$postsArray = $wpdb->get_results ( " SELECT * FROM  $wpdb->posts WHERE post_type = 'shop_subscription' AND post_status = 'wc-active' " );
	// echo 'postsArray: ' . print_r($postsArray);
	
	// get all subscriber userIDs
	
	$userIDstack = array();
	$idsOfUsersToSendReminder = array();
	
	foreach( $postsArray as $post ) {
		$postID = $post->ID;
		// echo 'postID: ' . $postID . '<BR>';
		$row = $wpdb->get_results( "SELECT meta_value FROM $wpdb->postmeta WHERE post_id = " . $postID . " AND meta_key = '_customer_user' " );
		// echo 'row: ' . print_r($row) . '<BR>';
		$userID = $row[0]->meta_value;
		// echo 'userID: ' . $userID . '<BR>';
		// push $userID on $userIDstack
		array_push($userIDstack,$userID);
		
	}
*/

	$userIDstack = array();
	$idsOfUsersToSendReminder = array();
	
	// acquire all userIDs
	$userIDarray = $wpdb->get_results( "SELECT ID FROM $wpdb->users LIMIT 0,100" );
	// echo 'user IDs array: ' . print_r($userIDarray,true);
	
	foreach( $userIDarray as $userIDobject ) {
		$userID = $userIDobject->ID;
		// echo 'userID: ' . $userID . '<BR>';
		array_push($userIDstack,$userID);
	}
	
	// echo '<BR><BR>userID stack; ' . print_r($userIDstack,true) . '<BR><BR>';

		
	// echo 'userIDstack: ' . print_r($userIDstack) . '<BR>';
	$arrayLength = count($userIDstack);
	// echo 'length of array: ' . $arrayLength . '<BR>';
	
	// filter stack using array_unique
	
	$userIDstack = array_unique($userIDstack);
	// echo 'userIDstack: ' . print_r($userIDstack) . '<BR>';
	$arrayLength = count($userIDstack);
	// echo( 'length of array: ' . $arrayLength . '<BR>' );
		
	// iterate though $userIDstack pushing ids of users needing reminder email onto stack
	foreach( $userIDstack as $userID ) {
		// echo('userID: ' . $userID . '<BR>');
		$subscriptions = wcs_get_users_subscriptions($userID);
		// echo('subscriptions: ' . print_r($subscriptions,true));
		$nextPaymentDatesArray = array();
		foreach($subscriptions as $subscriptionID => $subscription) {
			// echo('orderNumber: ' . $subscription->get_order_number() . '<BR>');
			// echo('nextPaymentDate: ' . $subscription->get_date_to_display( 'next_payment' ) . '<BR>');
			$nextPaymentDate = strtotime($subscription->get_date_to_display( 'next_payment' ));
			// echo('nextPaymentDate timestamp: ' . $nextPaymentDate . '<BR>');
			array_push($nextPaymentDatesArray,$nextPaymentDate);
			// echo '<BR><BR>';			
		}
		asort($nextPaymentDatesArray);
		$sortedNextPaymentDate = array_pop($nextPaymentDatesArray);
		
		// echo 'sortedNextPaymentDate: ' . $sortedNextPaymentDate . '<BR>';
	
		// compare $sortedNextPaymentDate with range of timestamps for inclusion in email notification
		// if in range push on $idsOfUsersToSendReminder
		/*
		if( ($sortedNextPaymentDate >= $lowerBound) && ($sortedNextPaymentDate <= $upperBound) ) {
			// push on stack
			array_push($idsOfUsersToSendReminder,$userID,$sortedNextPaymentDate);
		}
		*/
		// 
		if($sortedNextPaymentDate < $cutOffDateTimestamp) {
			array_push($idsOfUsersToSendReminder,$userID);
		}
		// echo '<BR><BR>';
	}
	
	$arrayLength = count($idsOfUsersToSendReminder);
	echo( 'length of array: ' . $arrayLength . '<BR>' );
	echo( 'idsOfUserToSendReminder: ' . print_r($idsOfUsersToSendReminder,true) . '<BR><BR>');
	
	// iterate through stack formatting and sending reminder emails
	
	for( $i = 0; $i < (count($idsOfUsersToSendReminder) ); $i ++ ) {
		$userID = $idsOfUsersToSendReminder[$i];
		// $timestamp = $idsOfUsersToSendReminder[$i + 1];
		// echo( 'userID: ' . $userID . ' ');
		echo($userID . ',');
		$fName = get_user_meta($userID,'first_name',true);
		$lName = get_user_meta($userID,'last_name',true);
		$email = get_the_author_meta('user_email',$userID);
		// $renewalDateString = date("F j, Y",$timestamp);
		// echo( 'fName: ' . $fName . ' lName: ' . $lName . ' email: ' . $email . '<BR>' );
		echo( '"' . $fName . '","' . $lName . '","' . $email . '"<BR>' );
	}
	

	// $message = "sendRenewalNotice.php has finished execution.";
	
	// $message = wordwrap($message,70);
	
	// mail("kurt@www-masters.com","Program Execution Complete",$message);
	
	
	
	
	
	


?>