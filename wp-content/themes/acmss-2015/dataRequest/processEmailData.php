<?php
	/*
	* program to process data sent from www-masters.com
	*
	*
	*/
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;
	// global $wpdb;

	
	error_log('post array: ' . print_r($_POST,true));
	
	$userName	= $_POST['username'];
	$domain		= $_POST['domain'];
	$name		= $_POST['name'];
	
	$postFileName = $_POST['file'];
	error_log('postFileName: ' . $postFileName);
	

	// $filesArrayString = print_r($_FILE,true);
	// error_log('file attachment upload: ' . $filesArrayString);
	
	error_log('FILES: ' . $_FILES);
	error_log('print_r FILES: ' . print_r($_FILES,true));
	
	$targetFile = basename($_FILES["file"]["name"]);
	error_log('targetFile: ' . $targetFile);
	
	$targetFileArray = explode('--',$targetFile);
	

	// email address
	$sendersEmailAddress = $userName . '@' . $domain;
	error_log('email address: ' . $sendersEmailAddress);
		
	if( $user = get_user_by( 'email', $sendersEmailAddress ) ) {
		// error_log('user: ' . print_r($user,true));
		error_log('userID: ' . $user->ID);
	
		// affidavit type
		$fileName = $targetFileArray[1];
		error_log('fileName: ' . $fileName);
	
		switch ( $fileName ) {
			case 'cmssAffidavitTwoStep.pdf':
				error_log('file is two step');
				$fileNameFragment = ' Eligibility Affidavit - CMSS Two Step - signed';
			
				if($user->ID) {
					update_user_meta( $user->ID, '_signed_cmsa_affidavit', true);
					update_user_meta( $user->ID, '_signed_mscat_affidavit', true);
					ACMSS()->add_user_cap($user->ID,'apprenticed_member');
					ACMSS()->remove_user_cap($user->ID,'certified_member');
					Groups_User_Group::create( array( 'user_id' => $user->ID, 'group_id' => 2 ) );
					Groups_User_Group::delete( $user->ID, 17 );
					// ACMSS()->emails()->send_email( 'cmss-congratulations', $user->ID );
					$responseEmailFragment = ' processed a two step affidavit';
				
				}
				$affidavit_type = 'cmss: two step';
			
				break;
		
			case 'cmssAffidavitOneStepRevJun27.pdf';
				error_log('file is one step');	
				$fileNameFragment = ' Eligibility Affidavit - CMSS One Step - signed';
			
				if($user->ID) {
					update_user_meta( $user->ID, '_signed_cmss_affidavit', true);
					update_user_meta( $user->ID, '_signed_mscat_affidavit', true);
					ACMSS()->add_user_cap($user->ID,'certified_member');
					ACMSS()->remove_user_cap($user->ID,'apprenticed_member');
					Groups_User_Group::create( array( 'user_id' => $user->ID, 'group_id' => 17 ) );
					Groups_User_Group::delete( $user->ID, 2 );
					// ACMSS()->emails()->send_email( 'cmss-congratulations', $user->ID );
					$responseEmailFragment = ' processed a one step affidavit';
				}
				$affidavit_type = 'cmss: one step';
			
				break;
			
			default:
				error_log('file is not determined');
				$responseEmailFragment = ' something went wrong!';
				break;	
		}
	
	
		$filename = ACMSS()->get_file_save_dir("affidavits/{$affidavit_type}") . "/{$user->ID} - {$fileNameFragment}.pdf";
		error_log('filename: ' . $filename);
	
		move_uploaded_file($_FILES["file"]["tmp_name"],$filename);
	
	}	
	
	// echo $testData;
	
	$responseEmailFragment = $responseEmailFragment ? $responseEmailFragment : ' user did NOT use a registered email address';
	$userIDforResponse = $user->ID ? $user->ID : ' Not a valid User ';
	
	echo 'Senders Email Addres: ' . $sendersEmailAddress . ' userID: ' . $userIDforResponse . ' Action: ' . $responseEmailFragment;



?>