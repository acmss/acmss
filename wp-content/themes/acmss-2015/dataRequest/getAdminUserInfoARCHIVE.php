<?php
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;
	// global $wpdb;
	
	
	$quizTitles = array(
							"2614" =>"MSCAT Certification (Emergency Medicine)",
							"10651"=>"MSCAT Certification (Oncology)",
							"4955" =>"MSCAT Certification (Internal Medicine)",
							"4954" =>"MSCAT Certification (Primary Care)",
							"2548" =>"MSCAT Certification (Dermatology)",
							"2547" =>"MSCAT Certification (Vascular Care)",
							"2546" =>"MSCAT Certification (Urgent Care)",
							"2336" =>"MSCAT Certification (Ophthalmology)",
							"1758" =>"MSCAT Certification (General)",
							"11837"=>"CSE Ophthalmology Level 1",
							"11836"=>"CSE Ophthalmology Level 2",
							"12213"=>"CSE E&M",
							"13482"=>"CSE, CDS",
							"13480"=>"CSE, CPOE",
							"11840"=>"CSE, HIPAA Level 1",
							"11841"=>"CSE Emergency Medicine - Cardiac",
							"1568" =>"HIPAA for Medical Scribes",
							"4953" =>"Reporting and Compliance",
							"11839"=>"CSE Reporting, Compliance, and HIPAA"
						);

	
	// who am I
	$userID = get_current_user_id();
	
	
	error_log('Hi from getAdminUserInfo -- my userID is: ' . $userID);
	/*
	$existing = ACMSS()->get_member_list( get_current_user_id(), true );
	if( empty($existing) ) {
		$existing = '<p>You have not yet created any memberships.</p>';
	}
	
	echo print_r($existing, true);
	// echo 'someone from the existing array: ' . $existing[0];
	echo '<BR><BR>';
	*/

	
	$organizationName = ACMSS()->get_org_name( $userId );
	error_log('Organization Name: ' . $organizationName);
	if( !empty($organizationName) ) {
		$user_ids = $wpdb->get_col( "SELECT DISTINCT g1.user_id FROM wp_groups_user_group g1 WHERE g1.group_id IN (SELECT group_id FROM wp_groups_group WHERE name = '$organizationName');");
		
		error_log( 'userIDs: ' . print_r($user_ids,true) );
		/*
		if( count($user_ids) > 0 ) {
			$args = array(
				'blog_id'		=> $GLOBALS['blog_id'],
				'include'		=> $user_ids,
				'orderby'		=> 'login',
				'order'			=> 'ASC',
				'fields'		=> array( 'ID', 'display_name' )
			);

		$members = get_users( $args );
		
		echo print_r($members,true);
		echo '<BR><BR>';

		}
		*/
		
		// filename 
		$fileName = "websiteData_" . date('Ymd') . '.csv';		
		error_log('fileName: ' . $fileName);
		
		header("Content-Disposition: attachment; filename=\"$fileName\"");
		header("Content-Type: text/csv");
		
		// $fileHandle = fopen($fileName,"w");
		$fileHandle = fopen("php://output","w");
		
		// header / column titles
		$output = array("Name","Description","%","Pass/Fail","Date","CSE hours");
		fputcsv($fileHandle,$output,',','"');
		
		// output organization name on a separate line
		$outputOrganzationName = array($organizationName);
		fputcsv($fileHandle,$outputOrganzationName,',','"');
		
		// get each user record
		foreach($user_ids as $userID) {
			$userMeta = get_user_meta($userID);
			// error_log( 'userMeta: ' . print_r($userMeta,true) );
			// output name
			$name = $userMeta['first_name'][0] . ' ' . $userMeta['last_name'][0];
			error_log( 'Name: ' . $name);
			$outputName = array($name);
			fputcsv($fileHandle,$outputName,',','"');
			
			// echo 'certifications: ' . $userMeta['_sfwd-quizzes'][0] . '<BR>';
			$quizzes = unserialize($userMeta['_sfwd-quizzes'][0]) ? : 'None' ;
			error_log('quizzes: ' . print_r($quizzes,true) );
			// output each certification		
			foreach( $quizzes as $quiz ) {
				// quiz ID
				// echo $quiz['quiz'] . ' ';		
		
				// if no ID go to next record
				if(!$quiz['quiz']) { continue; }
		
		
				// quiz title
				$quizNumber = $quiz['quiz'];
				$quizName = $quizTitles[$quizNumber];
				
				// reset cseRenewalDate 
				// $cseRenewalDate = '';
				// get MSCAT pass date 
				error_log('strpos MSCAT: ' . strpos($quizName,"MSCAT"));
				if( strpos($quizName,'MSCAT') > -1 ) {
					$cseStartDate = date("F j, Y",$quiz['time']);
					$cseStartMonthDay = date("F j,",$quiz['time']);
					$cseStartYear = date("Y",$quiz['time']);
					error_log('cseStartDate: ' . $cseStartDate);
					$cseRenewalYear = $cseStartYear + 3;
					$cseRenewalDate = $cseStartMonthDay . ' ' . $cseRenewalYear;
					error_log('cseReneewalDate: ' . $cseRenewalDate);
				}
				
				// echo ' ' . $quizName;
				// echo ' - ';
				// percentage / grade
				// echo $quiz['percentage'] . '% ';
				// pass / fail status
				$quizPassStatus = $quiz['pass'] ? '<span style="font-weight: bold; color: green;">Pass</span>' : '<span style="font-weight: bold; color: red;">Fail</span>';
				// echo ' ' . $quizPassStatus;
				
				$quizPass = $quiz['pass'] ? 'Pass' : 'Fail';

				// date / time taken
				$quizTime = $quiz['time'];
				// echo ' ' . date("r",$quizTime);
				// echo ' ' . date("F d, Y",$quizTime);		

				if($quizName == '') {$quizName = 'No Certs Attempted';}
				// echo '<BR>';
				$outputData = array('   ',$quizName,$quiz['percentage'],$quizPass,date("F j, Y",$quiz['time']));
				// echo print_r($outputData,true) . '<BR><BR>';
				// write to csv file 
				fputcsv($fileHandle,$outputData,',','"');
		}
		$continuingEducation = unserialize($userMeta['_continuing_ed'][0]) ? : 'None' ;	
		error_log('continuing education: ' . print_r($continuingEducation,true) );	
		$list = ACMSS()->continuing_ed_list();
		if($continuingEducation != 'None') {
			$output = array(' ','Continuing Education');
			fputcsv($fileHandle,$output,',','"');
		}
		$totalCSEhours = 0;
		foreach($continuingEducation as $record) {
			$certificationDate = date("F j, Y",strtotime($record['date']));
			$certifcationTopic= $list[$record['topic']];
			$certificaitonHours = $record['hours'];
			
			$output = array("  ",$certifcationTopic,' ',' ',$certificationDate,$certificaitonHours);
			fputcsv($fileHandle,$output,',','"');
			$totalCSEhours = $totalCSEhours + $certificaitonHours;
			error_log('totalCSEhours: ' . $totalCSEhours);
		}
		
		// $output = array(' ','Renewal Date and CSE hours'); updated 12/13/17 kls
		$output = array(' ','CSE hours');
		fputcsv($fileHandle,$output,',','"');
		
		$renewalDateString = $cseRenewalDate ? 'their renewal date is ' . $cseRenewalDate : 'they have NOT certified';
		
		// $output = array(' ','User has ' . $totalCSEhours . ' CSE hour(s) and ' . $renewalDateString);
		$output = array(' ','User has ' . $totalCSEhours . ' CSE hour(s)');
		fputcsv($fileHandle,$output,',','"');
		$cseRenewalDate = '';
		$userHasSubscription = WC_Subscriptions_Manager::user_has_subscription($userID) ? 'Yes' : 'No' ;
		error_log('user has subscription: ' . $userHasSubscription);

		$output = array(' ','Subscription');
		fputcsv($fileHandle,$output,',','"');
		
		$outputString = ($userHasSubscription == 'Yes') ? 'User has a subscription' : 'User does NOT have a subscription' ;
		
		$output = array(' ',$outputString);
		fputcsv($fileHandle,$output,',','"');
		
		
		}
	}
	
	

	fclose($fileHandle);

?>