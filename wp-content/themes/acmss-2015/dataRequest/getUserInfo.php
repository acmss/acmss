<?php 
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;
	// global $wpdb;
	require_once('lib/woocommerce-api.php');
	
	$options = array(
		'debug'           => true,
		'return_as_array' => false,
		'validate_url'    => false,
		'timeout'         => 30,
		'ssl_verify'      => false,
	);
	$client = new WC_API_Client( 'https://theacmss.org', 'ck_d9374ac6271db98513069d1d054f12344287519a', 'cs_5a594c40ef64271c0dae5401cc13c412f9393a7c', $options );

	
	$quizTitles = array(
							"2614" =>"MSCAT Certification (Emergency Medicine)",
							"10651"=>"MSCAT Certification (Oncology)",
							"4955" =>"MSCAT Certification (Internal Medicine)",
							"4954" =>"MSCAT Certification (Primary Care)",
							"2548" =>"MSCAT Certification (Dermatology)",
							"2547" =>"MSCAT Certification (Vascular Care)",
							"2546" =>"MSCAT Certification (Urgent Care)",
							"2336" =>"MSCAT Certification (Ophthalmology)",
							"1758" =>"MSCAT Certification (General)",
							"11837"=>"CSE Ophthalmology Level 1",
							"11836"=>"CSE Ophthalmology Level 2",
							"12213"=>"CSE E&M",
							"13482"=>"CSE, CDS",
							"13480"=>"CSE, CPOE",
							"11840"=>"CSE, HIPAA Level 1",
							"11841"=>"CSE Emergency Medicine - Cardiac",
							"1568" =>"HIPAA for Medical Scribes",
							"4953" =>"Reporting and Compliance",
							"11839"=>"CSE Reporting, Compliance, and HIPAA"
						);
	
	// format output page using css
	echo '<style>body { font-family: avenir; font-size: 12px; }</style>';

	// echo 'wpdb: ' . var_dump( $wpdb );
	// echo 'host info: ' . $wpdb->host_info();
	// echo '<BR><BR>';
	// echo 'wp: ' . var_dump( $wp );

	
	
	$userID = $_GET['userID'];
	$adminID = $_GET['adminID'];
	
	// echo 'Hello World';
	// echo '<BR>';
	// echo 'userID: ' . $userID;
	
	// echo '<BR><BR>';
	
	$result1 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'first_name'", ARRAY_A);
	// echo print_r($firstName);
	$result2 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'last_name'", ARRAY_A);
	$result3 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = '_sfwd-quizzes'", ARRAY_A);
	$result4 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = '_member_org'", ARRAY_A);
	
	$result5 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_first_name'", ARRAY_A);
	$result6 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_last_name'", ARRAY_A);
	$result7 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_company'", ARRAY_A);
	$result8 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_address_1'", ARRAY_A);
	$result9 =  $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_address_2'", ARRAY_A);
	$result10 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_city'", ARRAY_A);
	$result11 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_state'", ARRAY_A);
	$result12 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_postcode'", ARRAY_A);
	$result13 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_phone'", ARRAY_A);
	$result14 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'billing_email'", ARRAY_A);
	
	$result15 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_first_name'", ARRAY_A);
	$result16 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_last_name'", ARRAY_A);
	$result17 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_company'", ARRAY_A);
	$result18 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_address_1'", ARRAY_A);
	$result19 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_address_2'", ARRAY_A);
	$result20 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_city'", ARRAY_A);
	$result21 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_state'", ARRAY_A);
	$result22 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_postcode'", ARRAY_A);
	$result23 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_phone'", ARRAY_A);
	$result24 = $wpdb->get_row ( " SELECT * FROM  $wpdb->usermeta WHERE user_id = " . $userID . " AND meta_key = 'shipping_email'", ARRAY_A);
	
	
	
	
	// echo print_r($result3);
	// echo '<BR><BR>';
	// echo $result3['meta_value'];
	// echo '<BR><BR>';
	$quizzes = unserialize($result3['meta_value']);
	// echo print_r($quizzes);
	// echo '<BR><BR>';
	
	
	
	$name = $result1['meta_value'] . ' ' . $result2['meta_value'];
	$lowerCaseNameString = strtolower($name);
	$ucfNamelName = ucwords($lowerCaseNameString);
	$name = $ucfNamelName;
		
	// echo 'Name: ' . $name;
	// echo '<BR><BR>';
	
	$practiceName    = $result4['meta_value'] ?: "Not Recorded" ;
	
	// lookup groupID from $practiceName
	$result25 = $wpdb->get_row ( " SELECT * FROM  wp_groups_group WHERE name = '$practiceName'", ARRAY_A);
	error_log('getUserInfo.php :: 99 :: $result25: ' . print_r($result25,true));
	$groupID = $result25['group_id'];
	error_log('getUserInfo.php :: 101 :: groupID: ' . $groupID);

	$billingName     = $result5['meta_value'] . ' ' . $result6['meta_value'];
	$billingCompany  = $result7['meta_value'];
	$billingAddress1 = $result8['meta_value'];
	$billingAddress2 = $result9['meta_value'];
	$billingCity     = $result10['meta_value'];
	$billingState    = $result11['meta_value'];
	$billingZip      = $result12['meta_value'];
	$billingPhone    = $result13['meta_value'];
	$billingEmail    = $result14['meta_value'];

	$shippingName     = $result15['meta_value'] . ' ' . $result16['meta_value'];
	$shippingCompany  = $result17['meta_value'];
	$shippingAddress1 = $result18['meta_value'];
	$shippingAddress2 = $result19['meta_value'];
	$shippingCity     = $result20['meta_value'];
	$shippingState    = $result21['meta_value'];
	$shippingZip      = $result22['meta_value'];
	// $shippingPhone    = $result23['meta_value'];
	// $shippingEmail    = $result24['meta_value'];
	
	$subscriptionRenewalBool = false;
	$subscriptions = wcs_get_users_subscriptions($userID);
	error_log('getUserInfo.php :: 138 :: $subscriptions array: ' . print_r($subscriptions,true));
	$subscription = array_shift($subscriptions);
	$subscriptionID = $subscription->id;
	error_log('getUserInfo.php :: 141 :: subscriptionID: ' . $subscriptionID);
	$subscriptionRenewalString = get_post_meta($subscriptionID,'_requires_manual_renewal',true);
	$subscriptionRenewalDate   = get_post_meta($subscriptionID,'_schedule_next_payment',true);
	$subscriptionRenewalDate = array_shift(explode(' ', $subscriptionRenewalDate));
	error_log('getUserInfo.php :: 145 :: subscriptionRenewalString: ' . $subscriptionRenewalString);
	$subscriptionRenewalBool = $subscriptionRenewalString == 'false' ? false : true ;
	error_log('getUserInfo.php :: 147 :: subscriptionRenewalBool: ' . $subscriptionRenewalBool);
	$subscriber = $subscriptionID ? true : false;
	
	// $subscriptionObject = $client->subscriptions->get( $subscriptionID );
	$subscriptionStatus = $subscriptionObject->subscription->status;
	error_log('getUserInfo.php :: 152 :: subscription status: ' . $subscriptionStatus);

	echo "<span style='font-size: 24px; font-weight: bold;'>$name</span><BR><BR>";
	
	if(count($practiceName) > 1) {
		echo "Practice Name: $practiceName<BR>";
	}
	
	echo "ACMSS Member Status: ";
	
	if( strlen($subscriptionStatus) > 2) {
		echo $subscriptionStatus . ' -- ';
	}
	
	if($subscriber == false) {
		echo "Not a Member";
	}
	elseif ($subscriptionRenewalBool != 1) {
		echo "Member with Automatic Renewal On";
	}
	else {
		echo "Member with Manual Renewal";
	}
	echo "<BR>";
	echo "ACMSS Membership Renewal Date: " . $subscriptionRenewalDate;
	
	echo "<BR>";
	
	$emrNumber = strtoupper(str_pad(dechex($userID + 100001),8,'0', STR_PAD_LEFT));
	echo 'MU Number: ' . $emrNumber;
	
	echo "<BR>";
	
	/*
	echo 'Certification Exam Passed on: ';
	echo "<BR>";
	
	echo 'Certification Renewal Date: ' . $subscriptionRenewalDate;
	echo "<BR>";
	*/
	
	if( 0 ) {
		echo "Billing Information<BR>";
		echo "&nbsp;Name: <span style='font-weight: bold;'>$billingName</span><BR>";
		echo "&nbsp;Company: <span style='font-weight: bold;'>$billingCompany</span><BR>";
		echo "&nbsp;Address: <span style='font-weight: bold;'>$billingAddress1</span><BR>";
		if( strlen($billingAddress2) > 1 ) {
			echo "&nbsp;Address: <span style='font-weight: bold;'>$billingAddress2</span><BR>";
		}	
		echo "&nbsp;City: <span style='font-weight: bold;'>$billingCity</span> State: <span style='font-weight: bold;'>$billingState</span><BR>";
		echo "&nbsp;Phone: <span style='font-weight: bold;'>$billingPhone</span> Email: <span style='font-weight: bold;'>$billingEmail</span><BR>";
	}
	/*
	echo '<BR><BR>';
	echo '<h2>Certification History</h2>';
	echo $name;
	
	if($quizzes == '') {
		echo ' has NOT taken any quizzes.';
	}
	else {
	
		echo ' has taken the following certifications:<BR><BR>';
		foreach( $quizzes as $quiz ){
			// quiz ID
			// echo $quiz['quiz'] . ' ';		
		
			// if no ID go to next record
			if($quiz['quiz'] == '') { continue; }
		
		
			// quiz title
			$quizNumber = $quiz['quiz'];
			$quizName = $quizTitles[$quizNumber];
			echo ' ' . $quizName;
			echo ' - ';
			// percentage / grade
			echo $quiz['percentage'] . '% ';
			// pass / fail status
			$quizPassStatus = $quiz['pass'] ? '<span style="font-weight: bold; color: green;">Pass</span>' : '<span style="font-weight: bold; color: red;">Fail</span>';
			echo ' ' . $quizPassStatus;

			// date / time taken
			$quizTime = $quiz['time'];
			// echo ' ' . date("r",$quizTime);
			echo ' ' . date("F d, Y",$quizTime);		

			echo '<BR>';
		}
	}
	
	echo "<style>label {display: inline-block; width: 80px; text-align: right;}</style>";
	
	echo '<BR><BR>';
	echo '<button style="display: block; margin: 0 auto; background: #eee;" onclick="window.close();">Close Window</button>';
	*/
	echo "<style>label {display: inline-block; width: 80px; text-align: right;}</style>";
	
	echo '<BR>';
	
	echo '<form method=POST action="https://theacmss.org/wp-content/themes/acmss-2015/dataRequest/updateAdminUserInfo.php">';
	
	echo "<input type=hidden name=userID value=$userID>";
	echo "<input type=hidden name=subscriptionID value=$subscriptionID>";
	echo "<input type=hidden name=groupID value=$groupID>";
	echo "<input type=hidden name=adminID value=$adminID>";
	
	echo "<h3>Billing Information</h3>";
	echo "<label for='billing_name'>Name</label><input type=text name=billing_name value='$billingName'><BR>";
	echo "<label for='billing_company'>Company</label><input type=text name=billing_company value='$billingCompany'><BR>";
	echo "<label for='billing_address_1'>Address</label><input type=text name=billing_address_1 value='$billingAddress1'><BR>";
	if(strlen($billingAddress2) > 1) {
		echo "<label for='billing_address_2'>Address</label><input type=text name=billing_address_2 value='$billingAddress2'><BR>";
	}
	echo "<label for='billing_city'>City</label><input type=text name=billing_city value='$billingCity'><BR>";
	echo "<label for='billing_state'>State</label><input type=text name=billing_state value='$billingState'><BR>";
	echo "<label for='billing_zip'>Zip</label><input type=text name=billing_zip value='$billingZip'><BR>";
	echo "<label for='billing_phone'>Phone</label><input type=text name=billing_phone value='$billingPhone'><BR>";
	echo "<label for='billing_email'>Email</label><input type=text name=billing_email value='$billingEmail'><BR>";
		
	
	echo "<h3>Shipping Information</h3>";
	echo "<label for='shipping_name'>Name</label><input type=text name=shipping_name value='$shippingName'><BR>";
	echo "<label for='shipping_company'>Company</label><input type=text name=shipping_company value='$shippingCompany'><BR>";
	echo "<label for='shipping_address_1'>Address</label><input type=text name=shipping_address_1 value='$shippingAddress1'><BR>";
	if(strlen($shippingAddress2) > 1) {
		echo "<label for='shipping_address_2'>Address</label><input type=text name=shipping_address_2 value='$shippingAddress2'><BR>";
	}
	echo "<label for='shipping_city'>City</label><input type=text name=shippiing_city value='$shippingCity'><BR>";
	echo "<label for='shipping_state'>State</label><input type=text name=shipping_state value='$shippingState'><BR>";
	echo "<label for='shipping_zip'>Zip</label><input type=text name=shipping_zip value='$shippingZip'><BR>";
	// echo "<label for='shipping_phone'>Phone</label><input type=text name=shipping_phone value='$shippingPhone'><BR>";
	// echo "<label for='shipping_email'>Email</label><input type=text name=shipping_email value='$shippingEmail'><BR>";
	
	// checkbox for disabling auto renewal
	if($subscriptionRenewalBool == 0) {
		// echo "disable auto renewal";
		echo "<input style='padding: 0 10px 0 40px; width: 15px;' type=checkbox name=disableAutoRenewal value=true><label style='text-align: left; display: inline-block; width: 200px; margin-top: 30px;' for='disableAutoRenewal'> Disable Auto Renewal </label><BR>";
	}
	
	// checkbox to indicate removal of this user from the current group 
	if($adminID != $userID) {
		// echo "<h2>Note: You can NOT delete the Practice Administrator.</h2>";
		echo "<input style='margin-top: 15px; padding: 0 10px 0 40px; width: 15px;' type=checkbox name=removeUserFromGroup value=true>
		<label style='text-align: left; display: inline-block; width: 200px; margin-top: 0px;' for='removeUserFromGroup'> 
			Remove User From Group
		</label><BR>";
	}
	
	
	echo "<BR><BR>";
	echo "<button  style='display: inline-block; margin: 0 auto; background: #eee;' type=submit>Update Record</button>";
	
	echo '<button style="display: inline-block; margin: 0 auto; background: #eee;" onclick="window.close();">Do NOT update record</button>';
	
	echo '</form>';
	
	echo '<BR><BR>';
	
	$returnedFromCall = woocommerce_get_template('myaccount/my-orders.php');
	error_log($returnedFromCall);
	
	
		echo '<BR><BR>';
	echo '<h2>Certification History</h2>';
	echo $name;
	
	if($quizzes == '') {
		echo ' has NOT taken any certifications.';
	}
	else {
	
		echo ' has taken the following certifications:<BR><BR>';
		foreach( $quizzes as $quiz ){
			// quiz ID
			// echo $quiz['quiz'] . ' ';		
		
			// if no ID go to next record
			if($quiz['quiz'] == '') { continue; }
		
		
			// quiz title
			$quizNumber = $quiz['quiz'];
			$quizName = $quizTitles[$quizNumber];
			echo ' ' . $quizName;
			echo ' - ';
			// percentage / grade
			echo $quiz['percentage'] . '% ';
			// pass / fail status
			$quizPassStatus = $quiz['pass'] ? '<span style="font-weight: bold; color: green;">Pass</span>' : '<span style="font-weight: bold; color: red;">Fail</span>';
			echo ' ' . $quizPassStatus;

			// date / time taken
			$quizTime = $quiz['time'];
			// echo ' ' . date("r",$quizTime);
			echo ' ' . date("F d, Y",$quizTime);		

			echo '<BR>';
		}
	}
	
	echo "<style>label {display: inline-block; width: 80px; text-align: right;} input {margin-left: 3px; width: 300px;} th {text-align: left;} td.order-actions {display: none;} .order {font-size: 14px;} .order-number {width: 70px;} .order-number a {text-decoration: none; color: black;} .order-date {width: 170px;} .order-status {width: 120px;} .order-total {width: 150px;}</style>";
	
	echo '<BR><BR>';
	// echo '<button style="display: block; margin: 0 auto; background: #eee;" onclick="window.close();">Close Window</button>';
	
	// if($adminID == 5627) {
		echo "<h2>Assigned Coursework and Assessments</h2>";
		echo $name . ' has the following assigned coursework and assessments: ' . '<BR>';
		echo ACMSS_Entitlements::user_has_entitlement('quiz', 1758, $userID) . ' MSCAT certification assessments<BR>';
		echo ACMSS_Entitlements::user_has_entitlement('quiz', 1568, $userID) . ' HIPAA certification assessments<BR><BR>';
		echo "In the event that $name has not accessed the assigned coursework and assessments within 30 days of purchase the coursework and assessments will be returned to the practice administrator for reassignment.<BR><BR>";
	
	//}
		
	
	
	// display certs
	error_log('getUserInfo.php :: 326 :: userID: ' . $userID);
	if( $results = get_user_meta( $userID, '_sfwd-quizzes', true ) ) {
		$quizzesArray = array();
		$links = '';
		foreach( $results as $result ) {
			if( !in_array( $result['quiz'], $quizzesArray ) ) {
				if( $result['pass'] == 1 ) {
					error_log('getUserInfo.php :: 333 :: $result: ' . print_r($result,true));
					// $post = get_post( $result['quiz'] );
					// $link = learndash_certificate_details( $post, $userID );
					$link = learndash_certificate_details( $result['quiz'], $userID );
					$post = get_post( $result['quiz'] );
					// error_log();
					
					
					
					
					$links .= sprintf( '<a style="text-decoration: none; color: black;" href="%s" target="_blank">Click to view or print your %s</a><br>', $link['certificateLink'] . '&userID=' . $userID . '&user_id=' . $userID, $post->post_title );
					$quizzesArray[] = $result['quiz'];
					
					// $newLink = learndash_get_certificate_link( $result['quiz'], $userID );
					// echo $newLink;
					
					
				}
			}
		}

	}
		
	error_log('getUserInfo.php :: 342 :: strlen($links): ' . strlen($links));
	if( strlen( $links ) < 5 ) {
		$links = '<p>You have not passed any assessments yet.</p>';
	}
	/*
	if($adminID != 6623) {
		$links = '<p>Coming Soon!</p>';
	}
	*/	
	echo "<h2>Assessment Certificates</h2>$links";
	echo "<BR><BR>";
	
	


?>