<?php 
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;
	// global $wpdb;
	
	// format output page using css
	echo '<style>body { font-family: avenir; font-size: 12px; }</style>';

	// echo 'wpdb: ' . var_dump( $wpdb );
	// echo 'host info: ' . $wpdb->host_info();
	// echo '<BR><BR>';
	// echo 'wp: ' . var_dump( $wp );

	
	
	$userID = $_GET['userID'];
	
	$existingMembers = ACMSS()->get_member_list_plain ( get_current_user_id(), true );
	
	echo '<h2>Select Practice Adminstrator</h2>';
	// echo '<BR>';
	// echo 'userID: ' . $userID;
	
	
	// echo $existingMembers;
	//print_r($existingMembers);
	
	echo "Below is a list of the members of your group. The current practice administrator is selected.";
	echo "<BR><BR>";
	echo "To change practice administrator select a different member and click 'Save'.";
	echo "<BR>";
	
	echo "<form action='https://theacmss.org/wp-content/themes/acmss-2015/dataRequest/updatePracticeAdmin.php' method='POST' style='padding-top: 15px;'>";
	echo "<input type=hidden name=userID value=$userID>";
	
	foreach($existingMembers as $existingMember) {
		// echo 'getPracticeMembers.php :: 30 :: ID: ' . $existingMember->ID . ' display_name: ' . $existingMember->display_name . '<BR>';
		$selected = '';
		if($existingMember->ID == $userID) {
			$selected = ' checked ';	
		}	
		echo "<input type=radio name=practiceAdmin value=$existingMember->ID $selected> $existingMember->display_name<BR>";
	}
	echo "<BR><BR>";
	// echo "<input type=submit value='Save'>";
	
	echo "<button style='display: inline-block; margin: 0 auto; background: #eee;' type=submit>Update Practice Administrator</button>";
	echo "<button style='display: inline-block; margin: 0 auto; background: #eee;' onclick='window.close();'>Do NOT Update Practice Administrator</button>";
	echo "</form>";
	
	echo '<BR><BR>';
	

?>