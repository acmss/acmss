<?php

	require_once('../../../../wp-load.php');
	global $wp,$wpdb;

	$userID = $_POST['userID'];
	error_log('updatePracticeAdmin.php :: 7 :: userID: ' . $userID);
	$updatedPracticeAdmin = $_POST['practiceAdmin'];
	error_log('updatePracticeAdmin.php :: 9 :: updatedPracticeAdmin: ' . $updatedPracticeAdmin);
	
	$wpdb->update(
		$wpdb->usermeta,
			array(
				'user_id' => $updatedPracticeAdmin
			),
			array(
				'user_id' => $userID,
				'meta_key' => '_assignable'
			)
	);

	$wpdb->update(
		$wpdb->usermeta,
			array(
				'user_id' => $updatedPracticeAdmin
			),
			array(
				'user_id' => $userID,
				'meta_key' => '_org_name'
			)
	);

	echo "<span style='display: block; margin-top: 125px; font-size: 24px; font-weight: bold; font-family: avenir,sans-serif; text-align: center;'>Update Completed</span>";
	echo '<button style="display: block; margin: 0 auto; background: #eee;" onclick="window.close();">Close Window</button>';



?>