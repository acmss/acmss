<?php 
	
	require_once('../../../../wp-load.php');
	global $wp,$wpdb;
	// global $wpdb;
	
	$groupID = $_GET['groupID'];
	
	echo '<style>body { font-family: avenir; font-size: 12px; font-size: 14px; line-height: 1.25em;}</style>';
	
	echo '
	<script>
	
	function openWindow(userID) {
		console.log("userID: " . userID);
		window.open("https://theacmss.org/wp-content/themes/acmss-2015/dataRequest/getUserInfo.php?userID="+userID,"userEntity","left=700,top=50,height=600,width=600,status=no,scroolbars=yes");
	}
	
	</script>
	';
	
	$practiceName = $wpdb->get_col( "SELECT name FROM wp_groups_group WHERE group_id = $groupID" );
	// print_r($practiceName);
	$practiceName = $practiceName?: 'Not Recorded' ;
	
	echo '<h1 style="line-height: 1.1em;">' . $practiceName[0] . '</h1>';
		
	// echo 'Hello World';
	// echo '<BR>';
	// echo 'groupID: ' . $groupID;
	
	// echo '<BR><BR>';
	
	$userIDs = $wpdb->get_col( "SELECT user_id FROM wp_groups_user_group WHERE group_id = $groupID" );
	// print_r($userIDs);
	
	$args = array(
		'blog_id'		=> $GLOBALS['blog_id'],
		'include'		=> $userIDs,
		'orderby'		=> 'display_name',
		'fields'		=> array( 'ID','display_name' )
	
	);
	
	$members = get_users($args);
	// print_r($members);
	
	// find admin
	$admins = array();
	foreach($members as $member) {
		$practiceAdmin = get_usermeta($member->ID,'_org_name',true) ? true : false ;
		if($practiceAdmin === true) {
			array_push($admins,$member->ID);
		}
	}
	
	// print_r($admins);
	
		
	if(count($admins) > 1) {
		echo '<h3>Practice Admins: </h3>';
	}
	else {	
		echo '<h3>Practice Admin: </h3>';
	}
	
	
	
	foreach($admins as $admin) {
		$adminfName 			= get_usermeta($admin,'first_name',true);
		$adminlName 			= get_usermeta($admin,'last_name',true);
		$adminBillingAddress 	= get_usermeta($admin,'billing_address_1',true);
		$adminBillingCity		= get_usermeta($admin,'billing_city',true);
		$adminBillingState		= get_usermeta($admin,'billing_state',true);
		$adminBillingZip		= get_usermeta($admin,'billing_postcode',true);
		$adminBillingPhone		= get_usermeta($admin,'billing_phone',true);
		$adminBillingEmail		= get_usermeta($admin,'billing_email',true);
		
		echo $adminfName . ' ' . $adminlName . '<BR>';
		echo strlen($adminBillingAddress) > 5 ? $adminBillingAddress . '<BR>' : '';
		echo strlen($adminBillingCity) > 5 ? $adminBillingCity . ', ' : '' ; 
		echo strlen($adminBillingState) > 1 ? $adminBillingState . ' ' : ''; 
		echo strlen($adminBillingZip) > 1 ? $adminBillingZip . '<BR>' : '<BR>';
		echo strlen($adminBillingPhone) > 3 ? 'Phone: ' . $adminBillingPhone . '<BR>' : '';
		echo strlen($adminBillingEmail) > 5 ? 'Email: ' . $adminBillingEmail . '<BR>' : '';
		
		
		//echo '<BR>';
	}
	
	echo '<h3>Practice Members:</h3>';
	echo '<small>(Click on the names for further information)</small><BR><BR>';
	foreach($members as $member) {
		
		$placedOrder = get_usermeta($member->ID,'_order_count',true) > 0 ? ' -- has ordered' : '';
		
		$lName = get_usermeta($member->ID,'last_name',true); 
		$fName = get_usermeta($member->ID,'first_name',true); 
		
		echo '<a onclick="openWindow(' . $member->ID . ')">' . $fName . ' ' . $lName . '</a>' . $placedOrder . '<BR>';
		
		// echo $member->ID . ' ' . $fName . ' ' . $lName . '<BR>';
	}

?>