<?php

	require_once('../../../../wp-load.php');
	global $wp,$wpdb;
	require_once( 'lib/woocommerce-api.php' );

	$userID = $_POST['userID'];
	error_log('updateAdminUserInfo.php :: userID: ' . $userID);
	$subscriptionID = $_POST['subscriptionID'];
	$groupID = $_POST['groupID'];
	error_log('updateAdminUserInfo.php :: 10 :: groupID: ' . $groupID);
	$adminID = $_POST['adminID'];
	echo 'adminID: ' . $adminID . '<BR><BR>';
	
	$billingName     = $_POST['billing_name'];	
	$billingCompany  = $_POST['billing_company'];
	$billingAddress1 = $_POST['billing_address_1'];
	$billingAddress2 = $_POST['billing_address_2'];
	$billingCity     = $_POST['billing_city'];
	$billingState    = $_POST['billing_state'];
	$billingZip      = $_POST['billing_zip'];
	$billingPhone    = $_POST['billing_phone'];
	$billingEmail    = $_POST['billing_email'];
	
	$shippingName     = $_POST['shipping_name'];
	$shippingCompany  = $_POST['shipping_company'];
	$shippingAddress1 = $_POST['shipping_address_1'];
	$shippingAddress2 = $_POST['shipping_address_2'];
	$shippingCity     = $_POST['shippiing_city'];
	$shippingState    = $_POST['shipping_state'];
	$shippingZip      = $_POST['shipping_zip'];
	// $shippingPhone    = $_POST['shipping_phone'];
	// $shippingEmail    = $_POST['shipping_email'];
	
	error_log('updateAdminUserInfo.php :: 32 :: billingName: ' . $billingName);
	
	$disableAutoRenewal = $_POST['disableAutoRenewal'] ?: 'false';
	error_log('updateAdminUserInfo.php :: 33 :: disableAutoRenewal: ' . $disableAutoRenewal);
	$removeUserFromGroup = $_POST['removeUserFromGroup'] ?: 'false';
	error_log('updateAdminUserInfo.php :: 35 :: removeUserFromGroup: ' . $removeUserFromGroup);
	
	$billingNameArray = explode(' ', $billingName);
	error_log('updateAdminUserInfo.php :: 40 :: billingNameArray: ' . print_r($billingNameArray,true));
	$billingFirstName = array_shift($billingNameArray);
	error_log('updateAdminUserInfo.php :: 42 :: billingFirstName: ' . $billingFirstName);
	if($billingFirstName == '') {
		$billingFirstName = array_shift($billingNameArray);
	}
	$billingLastName  = array_pop($billingNameArray);
	error_log('updateAdminUserInfo.php :: 43 :: billingFirstName: ' . $billingFirstName . ' billingLastName: ' . $billingLastName);
	$shippingNameArray = explode(' ',$shippingName);
	$shippingFirstName = array_shift($shippingNameArray);
	$shippingLastName  = array_pop($shippingNameArray);
	
	
	function updateUserMeta($wpdb,$userID,$metaKeyString,$metaValueString) {
		error_log('updateAdminUserInfo.php :: 46 :: userID: ' . $userID . ' metaKeyString: ' . $metaKeyString . ' metaValueString: ' . $metaValueString);
		$updateResult = $wpdb->update(
			$wpdb->usermeta,
			array(
				'meta_value' => $metaValueString
			),
			array(
				'user_id' => $userID,
				'meta_key' => $metaKeyString
			)
		);
		error_log('updateAdminUserInfo.php :: 61 :: updateResult: ' . $updateResult);
		if($updateResult == 0) {
			$wpdb->insert(
				$wpdb->usermeta,
				array(
					'meta_value' => $metaValueString,
					'user_id' => $userID,
					'meta_key' => $metaKeyString
				)
			);
		}
		
	}

	updateUserMeta($wpdb,$userID,'billing_first_name',$billingFirstName);
	updateUserMeta($wpdb,$userID,'billing_last_name',$billingLastName);
	updateUserMeta($wpdb,$userID,'billing_company',$billingCompany);
	updateUserMeta($wpdb,$userID,'billing_address_1',$billingAddress1);
	updateUserMeta($wpdb,$userID,'billing_address_2',$billingAddress2);
	updateUserMeta($wpdb,$userID,'billing_city',$billingCity);
	updateUserMeta($wpdb,$userID,'billing_state',$billingState);
	updateUserMeta($wpdb,$userID,'billing_postcode',$billingZip);
	updateUserMeta($wpdb,$userID,'billing_phone',$billingPhone);
	updateUserMeta($wpdb,$userID,'billing_email',$billingEmail);
	
	updateUserMeta($wpdb,$userID,'shipping_first_name',$shippingFirstName);
	updateUserMeta($wpdb,$userID,'shipping_last_name',$shippingLastName);
	updateUserMeta($wpdb,$userID,'shipping_company',$shippingCompany);
	updateUserMeta($wpdb,$userID,'shipping_address_1',$shippingAddress1);
	updateUserMeta($wpdb,$userID,'shipping_address_2',$shippingAddress2);
	updateUserMeta($wpdb,$userID,'shipping_city',$shippingCity);
	updateUserMeta($wpdb,$userID,'shipping_state',$shippingState);
	updateUserMeta($wpdb,$userID,'shipping_postcode',$shippingZip);
	// updateUserMeta($wpdb,$userID,'shipping_phone',$shippingPhone);
	// updateUserMeta($wpdb,$userID,'shipping_email',$shippingEmail);
	
	if($disableAutoRenewal == 'true') {
		$wpdb->update(
			$wpdb->postmeta,
			array(
				'meta_value' => $disableAutoRenewal
			),
			array(
				'post_id' => $subscriptionID,
				'meta_key' => '_requires_manual_renewal'
			)
		);
	}
	
	if($removeUserFromGroup == 'true') {
		error_log('remove user from group');
		error_log('userID: ' . $userID . ' == groupID: ' . $groupID);
		
		// reclaim entitlements
		$availableMSCAT = ACMSS_Entitlements::user_has_entitlement('quiz', 1758, $userID);
		$availableHIPAA = ACMSS_Entitlements::user_has_entitlement('quiz', 1568, $userID);
		if( ($availableMSCAT > 2) && ($availableHIPAA > 2) ) {
			echo '<span style="display: block; margin-top: 100px; text-align: center">can recover assets</span>';
			// recover assets
			// entitlements
			ACMSS_Entitlements::add_entitlement('quiz', 1758, -3, $userID);
			ACMSS_Entitlements::add_entitlement('quiz', 1568, -3, $userID);
			ACMSS_Entitlements::add_assignable(1508, 1, $adminID);
			ACMSS_Entitlements::add_assignable(1550, 1, $adminID); 
			// remove manual access
			ACMSS_Core::remove_user_cap($userID, 'read_mscat_manual');
			// membership
			if($subscriptionID > 1) {
				$options = array(
					'debug'           => true,
					'return_as_array' => false,
					'validate_url'    => false,
					'timeout'         => 30,
					'ssl_verify'      => false,
				);
			
				try {
					$client = new WC_API_Client( 'https://theacmss.org', 'ck_d9374ac6271db98513069d1d054f12344287519a', 'cs_5a594c40ef64271c0dae5401cc13c412f9393a7c', $options );
					$client->subscriptions->delete( $subscriptionID );
				}
				catch ( WC_API_Client_Exception $e ) {
					echo $e->getMessage() . PHP_EOL;
					echo $e->getCode() . PHP_EOL;
				
					if ( $e instanceof WC_API_Client_HTTP_Exception ) {
						print_r( $e->get_request() );
						print_r( $e->get_response() );
					}
				}
			}	
		}
		// remove user from group / practice
		else {
			echo '<span style="display: block; margin-top: 100px; text-align: center;">can NOT recover assets</span>';
			// delete assets
			ACMSS_Entitlements::add_entitlement('quiz', 1758, -($availableMSCAT), $userID);
			ACMSS_Entitlements::add_entitlement('quiz', 1568, -($availableHIPAA), $userID);
			// remove manual access
			ACMSS_Core::remove_user_cap($userID, 'read_mscat_manual');
			// disable membership
			if($subscriptionID > 1) {
				$options = array(
					'debug'           => true,
					'return_as_array' => false,
					'validate_url'    => false,
					'timeout'         => 30,
					'ssl_verify'      => false,
				);
			
				try {
					$client = new WC_API_Client( 'https://theacmss.org', 'ck_d9374ac6271db98513069d1d054f12344287519a', 'cs_5a594c40ef64271c0dae5401cc13c412f9393a7c', $options );
					$client->subscriptions->delete( $subscriptionID );
				}
				catch ( WC_API_Client_Exception $e ) {
					echo $e->getMessage() . PHP_EOL;
					echo $e->getCode() . PHP_EOL;
				
					if ( $e instanceof WC_API_Client_HTTP_Exception ) {
						print_r( $e->get_request() );
						print_r( $e->get_response() );
					}
				}
			}				
		}
		
		// remove user from group
		Groups_User_Group::delete( $userID, $groupID );
	}
	
	echo "<span style='display: block; margin-top: 25px; font-size: 24px; font-weight: bold; font-family: avenir,sans-serif; text-align: center;'>Update Completed</span>";
	echo '<button style="display: block; margin: 0 auto; background: #eee;" onclick="window.close();">Close Window</button>';
	


?>