
<?php if( current_user_can('acmss_member') || current_user_can('certified_academic_partner') || current_user_can('corporate_partner') ) { ?>
<?php
	error_log('acmss-membership-details.php :: 3');
	foreach( array( '_member_org', '_member_tel' ) as $key ) {
		if( isset( $_POST[$key] ) ) {
			update_user_meta( (int) get_current_user_id(), $key, (string) $_POST[$key] );
		} 
	}

	$meta = get_metadata( 'user', get_current_user_id() );

	global $wpdb;
	wp_enqueue_script( 'jquery-ui-autocomplete' );
	$group_table = _groups_get_tablename( 'group' );
	$org_groups = $wpdb->get_results( "SELECT group_id, name FROM $group_table WHERE parent_id = 16" );
?>

	<hr/>

	<script type="text/javascript">var acmss_groups = <?php echo json_encode($org_groups); ?>;</script>

	<style type="text/css">
		/* highlight results */
		.ui-autocomplete span.hl_results {
		    background-color: #ffff66;
		}
		 
		/* loading - the AJAX indicator */
		.ui-autocomplete-loading {
		    background: white url('../img/ui-anim_basic_16x16.gif') right center no-repeat;
		}
		 
		/* scroll results */
		.ui-autocomplete {
		    max-height: 250px;
		    overflow-y: auto;
		    /* prevent horizontal scrollbar */
		    overflow-x: hidden;
		    /* add padding for vertical scrollbar */
		    padding-right: 5px;
		}
		 
		.ui-autocomplete li {
		    font-size: 16px;
		}
		 
		/* IE 6 doesn't support max-height
		* we use height instead, but this forces the menu to always be this tall
		*/
		* html .ui-autocomplete {
		    height: 250px;
		}
	</style>

	<h2>ACMSS Membership Details</h2>

	<form method="post" action="">
		<div class="editfield">
			<div style="width: 350px; display: inline-block; text-align: right;">
				<label for="_member_org">Name of Organization or Employer (required): </label>
			</div>
			<div style="width: 350px; display: inline-block;">
				<input style="margin-left: 10px; width: 300px;" name="_member_org" id="txtMemberOrg" value="<?php echo $meta['_member_org'][0]; ?>" style="width: 250px;" type="text" placeholder='Please Add Name'/>
			</div>	
			<p class="description"></p>
		</div>

		<div class="editfield">
			<div style="width: 350px; display: inline-block; text-align: right;">	
				<label for="_member_tel">Contact Telephone Number: </label>
			</div>	
			<div style="width: 350px; display: inline-block;">	
				<input style="margin-left: 10px; width: 300px;" name="_member_tel" id="txtMemberPhone" value="<?php echo $meta['_member_tel'][0]; ?>" style="width: 250px;" type="text">
			</div>
			<p class="description"></p>
		</div>

		<input style="margin-left: 250px; margin-bottom: 20px;" type="submit" value="Update" />
	</form>
<?php }

	if( $results = get_user_meta( get_current_user_id(), '_sfwd-quizzes', true ) )  {
		$quizzes = array();
		$links = '';
		foreach( $results as $result ) {
			if( !in_array( $result['quiz'], $quizzes) ) {
				if( $result['pass'] == 1 ) {
					error_log('79 :: acmss-membership-details result: ' . print_r($result,true));
					$link = learndash_certificate_details( $result['quiz'] );
					$post = get_post($result['quiz']);
					error_log('81 :: acmss-membership-details link: ' . print_r($link,true));
					$links .= sprintf( '<p><a href="%s">Click to print your %s</a></p>', $link['certificateLink'], $post->post_title );
					$quizzes[] = $result['quiz'];
				}
			}
		}

		if( empty($links) ) {
			$links = '<p>You have not passed any assessments yet.</p>';
		}
		error_log('91 :: acmss-membership-details quizzes: ' . print_r($quizzes,true));
		$update_pathway = do_shortcode('[cmss-affidavit-prompt]');

		echo "<hr/><h2>Assessment Certificates</h2>$update_pathway$links";
	}

		// display CSE units
		if( (get_current_user_id() == 5627 ) || (get_current_user_id() == 4 ) ) {
			error_log('100 :: it is me');
			
			if( $results = get_user_meta( get_current_user_id(), '_sfwd-quizzes', true ) )  {
				$quizzes = array();
				$CSEs = '';
				foreach( $results as $result ) {
					if( !in_array( $result['quiz'], $quizzes) ) {
						if( $result['pass'] == 1 ) {
							
							error_log('108 :: acmss-membership-details result: ' . print_r($result,true));
							
							error_log('110 :: result quiz: ' . $result['quiz']);
							$post = get_post( $result['quiz'] );
							$cseTitle = $post->post_title;
							error_log('113 :: cseTitle: ' . $cseTitle);
							
							error_log('115 :: result quiz timestamp: ' . $result['time']);
							$cseDate = date('m/d/Y', $result['time']);
							error_log('117 :: cseDate: ' . $cseDate);

							if($result['quiz'] != 2614){
								$CSEs .= '<td>' . $cseTitle . '<td style="text-align: center;">' . $cseDate . '<td style="text-align: center;">' . '1' . '</tr>';
							}
							
							$quizzes[] = $result['quiz'];
						}
					}
				}

				if( empty($CSEs) ) {
					$CSEs = '<p>You do not have any Continuing Education Units.</p>';
				}
				error_log('119 :: acmss-membership-details quizzes: ' . print_r($quizzes,true));

				// echo "<hr/><h2>Continuing Education Units</h2>$CSEs";
				echo "<BR><BR>";
				echo "<h2>Continuing Education Units</h2>";
				echo "<table style='width: 80%;'>";
				echo "<tr>";
				echo "<td style='font-weight: bold; color: black;'>Title</td>";
				echo "<td style='font-weight: bold; color: black; text-align: center;'>Date</td>";
				echo "<td style='font-weight: bold; color: black; text-align: center;'>Hours Earned</td>";
				echo "</tr>";
				echo $CSEs;
				echo "</table>";
			}

			
		}
		
		// partner logo
		
		if( current_user_can('certified_academic_partner') 
			|| current_user_can('corporate_partner') 
			|| current_user_can('sponsor_partner') 
			|| current_user_can('administrator') ) {

			if( current_user_can('certified_academic_partner') ) {
				$logo = 'partner-logo-academic.png';
			} else {
				$logo = 'partner-logo-corporate.png';
				$path = ABSPATH.'wp-content/plugins/acmss/images/partner-graphics/';

				$user = new WP_User( get_current_user_id() );

				foreach( $user->capabilities as $cap ) {
					$file = str_replace( '_', '-', $cap ) . '.png';

					if( file_exists($path.$file) ) {
						$logo = $file;
						break;
					}
				}
			}

			echo sprintf( '<hr/><h2>Partner Logo</h2><p>As a partner of the ACMSS, you are entitled to display the following logo on your website.</p><p><img src="%s/images/partner-graphics/%s" /></p>', plugins_url( 'acmss' ), $logo );
		}
		
		echo sprintf('<hr/><h2>Webinar Member Discount Coupon Code</h2><p>As a membership benefit use <b style="color: black;">\'ACMSS Memberhip Webinar\'</b> to attend ACMSS Webinars for $30.00 instead of the regular price of $35.00.</p>');

		if( current_user_can('corporate_partner') || current_user_can('administrator') ) {
			if( current_user_can('corporate_platinum_plus') ) {
				$discount = '100%';

			} else if( current_user_can('corporate_platinum') ) {
				$discount = '50%';

			} else if( current_user_can('corporate_diamond') ) {
				$discount = '40%';

			} else if( current_user_can('corporate_gold') ) {
				$discount = '30%';

			} else if( current_user_can('corporate_silver') ) {
				$discount = '20%';

			} else if( current_user_can('corporate_bronze') ) {
				$discount = '10%';

			} else {
				$discount = '5%';
			}

			echo sprintf( '<hr/><h2>Store Discount</h2><p>Your account entitles you to a discount of %s on items other than membership when bought from our online store. Your discount will be applied automatically, so you do not need to do anything extra during checkout.</p>', $discount );
		}
?>
	<hr/>