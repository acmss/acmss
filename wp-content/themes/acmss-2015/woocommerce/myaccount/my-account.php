<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( function_exists('wc_print_notices') ) wc_print_notices(); 

$userID = get_current_user_id();

$lapsedMember = ( user_can( $userID, 'lapsed_member' ) ) ? 1 : 0 ;
// error_log('my-account.php :: 15 lapsedMember: ' . $lapsedMember);

// $lapsedMember = false;

// get subscripton / membership info
$subscriptions = wcs_get_users_subscriptions($userID);
// error_log('my-account.php :: 23 subscriptions: ' . print_r($subscriptions,true));
foreach($subscriptions as $subscriptionID => $subscription) {
	$nextPaymentDate = strtotime($subscription->get_date_to_display( 'next_payment' ));
	$subscriptionStatus = $subscription->get_status();
	// error_log('my-account.php :: 27 :: get_status: ' . $subscriptionStatus);
	$nextPaymentDatesArray[$nextPaymentDate] = $subscriptionStatus;
	
}
// error_log('my-account :: 31 :: nextPaymentDatesArray: ' . print_r($nextPaymentDatesArray,true));
ksort($nextPaymentDatesArray);
// error_log('my-account.php :: 33 :: nextPaymentDatesArray: ' . print_r($nextPaymentDatesArray,true));
$keyValue = '';
$membershipStatus= '';
foreach($nextPaymentDatesArray as $key => $value ){
	$keyValue = $key;
	$membershipStatus = $value;
	// error_log('my-account :: 39 :: key: ' . $keyValue . ' value: ' . $membershipStatus); 
}

// error_log('my-account.php :: 42 :: keyValue: ' . $keyValue . ' status: ' . $membershipStatus);
$membershipStatus = $membershipStatus == 'active' ? 'active' : 'inactive' ;
$membershipStatus = $membershipStatus == 'inactive' || $lapsedMember == true ? 'inactive' : 'active' ;

if( current_user_can('administrator') ) $membershipStatus = 'active';

$group_id = 346;
$isMemberOfGroup = Groups_User_Group::read( $userID, $group_id ) ? true : false ;
error_log('my-account.php :: 49 :: isMemberOfGroup: ' . $isMemberOfGroup );

// error_log('my-account.php :: 46 :: membershipStatus: ' . $membershipStatus);

global $wpdb;
$user_group_table = _groups_get_tablename( 'user_group' );
$userGroups = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $user_group_table WHERE user_id = $userID ") );
error_log('my-account.php :: 56 :: userGroups: ' . print_r($userGroups, true));

$orgName = get_usermeta($userID,'_member_org') ?: 'None Indicated';
if($orgName == 'None Indicated') {
	$updateMemberOrg = 'Please add the Name of your Organization or Employer below. If an independent or self employed scribe please input \'Self\'. &nbsp; After updating your account record you will need to refresh your browser or log out and log back in.';
}

?>
<style>
a.cancel {display: none !important;}
</style>

<p class="myaccount_user">
	<?php
	if( $membershipStatus != 'active' ) {
		echo "Hello, <strong>$current_user->display_name</strong>.";
	}
	else {
		echo"Hello, <strong>$current_user->display_name</strong>. You have indicated that work for <strong>$orgName</strong>. $updateMemberOrg From your account dashboard you can view your recent orders, manage your shipping and billing addresses and <a href='/my-account/edit-account'>change your password</a>.";
	}	
	?>
</p>


<?php 
	if( $membershipStatus != 'active' ) {
		error_log('my-account :: 35 member lapsed');
		
		echo <<< HTML
<div class="expired-alert">
	<p>In preparation for MACRA, and ongoing account management, ACMSS would like to review your account(s) for compliance of Certified Medical Scribe Specialists. Please contact Customer Service to review at (800) 987-3692 or email ACMSS at support@acmss.zendesk.com.</p>
	<p>Thank you for being a valued member of ACMSS. We look forward to growing, servicing your account, and creating our bright and healthy futures together!</p>

	<p><a href="/store/membership-renewal">Click here to renew your membership</a></p>
</div>	
HTML;
	}

?>


<?php do_action( 'woocommerce_before_my_account' ); ?>

<?php 
	if( $membershipStatus == 'active' ) {
		// if member lapsed then this disables seeing acmss-membership-details.php
		woocommerce_get_template( 'myaccount/acmss-membership-details.php' ); 
	}
?>

<?php 
	$list = ACMSS()->continuing_ed_list();
	// error_log('130 :: list: ' . print_r($list,true));
	// $list = array(6=>"Clinical Medicine (Ophthalmology)",11=>"Clinical Documentation (Compliance, E&M)", 4=>"HIPAA/Medicolegal");
	error_log('32 :: list: ' . print_r($list,true));
	foreach ($list as $key => $value) {
		// echo sprintf( '<option value="%s">%s</option>', $key, $value );
	}
	error_log('my-account.php :: 105 :: membershipStatus: ' . $membershipStatus . ' current user id: ' . get_current_user_id() . ' user_id: ' . $user_id );
	if( $membershipStatus == 'active' ) {
		// display user input Continuing Education Hours
		if( ($ce = get_user_meta( get_current_user_id(), '_continuing_ed',true )) && ( !$user_id == 5627 ) ) {
			error_log('my-account.php :: 108 :: $ce: ' . print_r($ce,true));
			echo '<h2>Continuing Education</h2>';
			echo '<table id="user_ce" style="width: 80%;">';
			echo '<thead><tr><th>Date</th><th>Topic</th><th>Hours</th><th></th></tr></thead>';

			$i = 0;
			foreach( $ce as $record ) {
				echo sprintf( '<tr><td>%s</td><td>%s</td><td>%s</td><td><form method="post"><input type="hidden" name="ce_nonce" value="%s" /><input type="hidden" name="delete_id" value="%s" /><!--<input class="delete_id" type="submit" value="Delete" />--></form></tr>', $record['date'], $list[$record['topic']], $record['hours'], $nonce, $i );
				$i++;
			}

			echo '</table>';
		} else {
			echo '<p style="margin-top: 45px;">You have no hours recorded.</p>';
		}
	}
	
	// display CMSS affidavit status 
	$cmssAffidavitChecked = checked( true, get_user_meta( $userID, '_signed_corporate_member_affidavit', true ), false );
	error_log('my-account :: 82 cmssAffidavitChecked: ' . $cmssAffidavitChecked);
	$cmssLogoChecked = checked( true, get_user_meta( $userID, '_corporate_logo_received', true ), false );
	$cmssCurriculumSubmitted = checked( true, get_user_meta( $userID, '_corporate_curriculum_received', true), false );
	$cmssCurriculumApproved  = checked( true, get_user_meta( $userID, '_corporate_curriculum_approved', true), false );

	// KH asked me to remove Corporate Member Partner Status on 3/29/17
	/*
	if( user_can($userID, 'corporate_partner') ) {
		error_log('my-account :: 88 userID: ' . $userID . ' corporate partner -- yes');
		echo '<h2>Corporate Member Partner Status</h2>';
		if($cmssAffidavitChecked != '') {
			echo 'Corporate CMSS Affidavit received';
		}
		else {
			echo 'Corporate CMSS Affidavit Required - please upload!';
		}
		echo '<br>';
		if($cmssLogoChecked != '') {
			echo 'Corporate Logo received';
		}
		else
		{
			echo 'Corporate Logo Required - please upload!';
		}
		echo '<br><br>';
	}
	else {
		error_log('my-account :: 95 userID: ' . $userID . ' corporate partner -- no');
	}
	*/	


?>


<?php woocommerce_get_template( 'myaccount/my-downloads.php' ); ?>

<?php 
	if( $membershipStatus == 'active' ) {
		woocommerce_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); 
	}
?>

<?php woocommerce_get_template( 'myaccount/my-address.php' ); ?>




<?php do_action( 'woocommerce_after_my_account' ); ?>