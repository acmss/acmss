<?php
/**
 * Customer processing renewal order email
 *
 * @author  Brent Shepherd
 * @package WooCommerce_Subscriptions/Templates/Emails
 * @version 1.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php esc_html_e( 'Your CMS Recertification Compliance renewal order has been received and is now being processed. Your order details are shown below for your reference:', 'woocommerce-subscriptions' ); ?></p>

<?php do_action( 'woocommerce_subscriptions_email_order_details', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email ); ?>


<img style="width:150px; display: block; margin: 0 auto;" src="https://theacmss.org/wp-content/uploads/2017/11/logo2aTransparentBKG.png">
<div style="font-weight: bold; text-align: center; margin: -40px auto 12px;">American College of Clinical Documentation Outcomes</div>
<div style=""><a href="https://www.guidestar.org/profile/82-3245967/" target="_blank">American College of Clinical Documentation Outcomes</a>, Inc., (ACCDO) is a California based, <a href="https://theacmss.org/wp-content/uploads/2018/05/501c3Status.pdf" target="_blank">501(c)(3)</a>, IRS recognized tax-exempt national non-profit public benefit corporation organized for charitable purposes.
<BR><BR>
ACCDO is organized to promote the meaningful utilization of Certified Electronic Health Records Technology in achieving greater health outcomes in integrative medicine, functional medicine, preventive medicine and precision medicine, and to achieve meaningful wellness through the responsible uptake and adoption of Certified Medical Scribe Specialists.</div>
<div style="text-align: center;" class="btn">
<a href="https://www.guidestar.org/profile/82-3245967/"><button style="padding: 7px; background-color: #000080; color: white; font-size: 16px; font-weight: bold; margin-top:15px; border-radius: 10px;">Make Donation</button></a>
</div>


<?php do_action( 'woocommerce_email_footer', $email ); ?>