<?php
/**
 * Admin failed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/admin-failed-order.php
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.5.0
 */

 if ( ! defined( 'ABSPATH' ) ) {
 	exit;
 }

 /**
  * @hooked WC_Emails::email_header() Output the email header
  */
 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

 <p><?php printf( __( 'Payment for order #%1$s from %2$s has failed. The order was as follows:', 'woocommerce' ), $order->get_order_number(), $order->get_formatted_billing_full_name() ); ?></p>

 <?php

 /**
  * @hooked WC_Emails::order_details() Shows the order details table.
  * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
  * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
  * @since 2.5.0
  */
 do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

 /**
  * @hooked WC_Emails::order_meta() Shows order meta data.
  */
 do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

 /**
  * @hooked WC_Emails::customer_details() Shows customer details
  * @hooked WC_Emails::email_address() Shows email address
  */
 do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
?>

<img style="width:150px; display: block; margin: 0 auto;" src="https://theacmss.org/wp-content/uploads/2017/11/logo2aTransparentBKG.png">
<div style="font-weight: bold; text-align: center; margin: -40px auto 12px;">American College of Clinical Documentation Outcomes</div>
<div style=""><a href="https://www.guidestar.org/profile/82-3245967/" target="_blank">American College of Clinical Documentation Outcomes</a>, Inc., (ACCDO) is a California based, <a href="https://theacmss.org/wp-content/uploads/2018/05/501c3Status.pdf" target="_blank">501(c)(3)</a>, IRS recognized tax-exempt national non-profit public benefit corporation organized for charitable purposes.
<BR><BR>
ACCDO is organized to promote the meaningful utilization of Certified Electronic Health Records Technology in achieving greater health outcomes in integrative medicine, functional medicine, preventive medicine and precision medicine, and to achieve meaningful wellness through the responsible uptake and adoption of Certified Medical Scribe Specialists.</div>
<div style="text-align: center;" class="btn">
<a href="https://www.guidestar.org/profile/82-3245967/"><button style="padding: 7px; background-color: #000080; color: white; font-size: 16px; font-weight: bold; margin-top:15px; border-radius: 10px;">Make Donation</button></a>
</div>

<?php
 /**
  * @hooked WC_Emails::email_footer() Output the email footer
  */
 do_action( 'woocommerce_email_footer', $email );