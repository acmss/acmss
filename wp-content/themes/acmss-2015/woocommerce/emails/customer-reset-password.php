<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( 'Someone requested that the password be reset for the following account:', 'woocommerce' ); ?></p>
<p><?php printf( __( 'Username: %s', 'woocommerce' ), $user_login ); ?></p>
<p><?php _e( 'If this was a mistake, just ignore this email and nothing will happen.', 'woocommerce' ); ?></p>
<p><?php _e( 'To reset your password, visit the following address:', 'woocommerce' ); ?></p>
<p>
	<a class="link" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>">
			<?php _e( 'Click here to reset your password', 'woocommerce' ); ?></a>
</p>
<p></p>

<img style="width:150px; display: block; margin: 0 auto;" src="https://theacmss.org/wp-content/uploads/2017/11/logo2aTransparentBKG.png">
<div style="font-weight: bold; text-align: center; margin: -40px auto 12px;">American College of Clinical Documentation Outcomes</div>
<div style=""><a href="https://www.guidestar.org/profile/82-3245967/" target="_blank">American College of Clinical Documentation Outcomes</a>, Inc., (ACCDO) is a California based, <a href="https://theacmss.org/wp-content/uploads/2018/05/501c3Status.pdf" target="_blank">501(c)(3)</a>, IRS recognized tax-exempt national non-profit public benefit corporation organized for charitable purposes.
<BR><BR>
ACCDO is organized to promote the meaningful utilization of Certified Electronic Health Records Technology in achieving greater health outcomes in integrative medicine, functional medicine, preventive medicine and precision medicine, and to achieve meaningful wellness through the responsible uptake and adoption of Certified Medical Scribe Specialists.</div>
<div style="text-align: center;" class="btn">
<a href="https://www.guidestar.org/profile/82-3245967/"><button style="padding: 7px; background-color: #000080; color: white; font-size: 16px; font-weight: bold; margin-top:15px; border-radius: 10px;">Make Donation</button></a>
</div>

<?php do_action( 'woocommerce_email_footer', $email ); ?>