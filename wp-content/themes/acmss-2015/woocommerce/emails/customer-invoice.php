<?php
/**
 * Customer invoice email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-invoice.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates/Emails
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Executes the e-mail header.
 *
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php if ( $order->has_status( 'pending' ) ) : ?>
	<p>
	<?php
	printf(
		wp_kses(
			/* translators: %1s item is the name of the site, %2s is a html link */
			__( 'An order has been created for you on %1$s. %2$s', 'woocommerce' ),
			array(
				'a' => array(
					'href' => array(),
				),
			)
		),
		esc_html( get_bloginfo( 'name', 'display' ) ),
		'<a href="' . esc_url( $order->get_checkout_payment_url() ) . '">' . esc_html__( 'Pay for this order', 'woocommerce' ) . '</a>'
	);
	?>
	</p>
<?php endif; ?>

<?php

/**
 * Hook for the woocommerce_email_order_details.
 *
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );



/**
 * Hook for the woocommerce_email_order_meta.
 *
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

?>
<div style="margin-bottom: 30px;">
<a style="font-size: 18px; font-weight: bold; color: #000080;" href="https://theacmss.org/wp-content/uploads/2018/05/Certification-Re-Certification-Compliance-Personnel-Terms-Conditions.pdf" target="_blank">Certification | Recertification Licensure</a><BR>
<a style="font-size: 18px; font-weight: bold; color: #000080;" href="https://theacmss.org/wp-content/uploads/2018/05/Certification-Re-Certification-Compliance-Personnel-Terms-Conditions.pdf" target="_blank">Terms & Conditions</a>
</div>

<?php

/**
 * Hook for woocommerce_email_customer_details.
 *
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
?>

<img style="width:150px; display: block; margin: 0 auto;" src="https://theacmss.org/wp-content/uploads/2017/11/logo2aTransparentBKG.png">
<div style="font-weight: bold; text-align: center; margin: -40px auto 12px;">American College of Clinical Documentation Outcomes</div>
<div style=""><a href="https://www.guidestar.org/profile/82-3245967/" target="_blank">American College of Clinical Documentation Outcomes</a>, Inc., (ACCDO) is a California based, <a href="https://theacmss.org/wp-content/uploads/2018/05/501c3Status.pdf" target="_blank">501(c)(3)</a>, IRS recognized tax-exempt national non-profit public benefit corporation organized for charitable purposes.
<BR><BR>
ACCDO is organized to promote the meaningful utilization of Certified Electronic Health Records Technology in achieving greater health outcomes in integrative medicine, functional medicine, preventive medicine and precision medicine, and to achieve meaningful wellness through the responsible uptake and adoption of Certified Medical Scribe Specialists.</div>
<div style="text-align: center;" class="btn">
<a href="https://www.guidestar.org/profile/82-3245967/"><button style="padding: 7px; background-color: #000080; color: white; font-size: 16px; font-weight: bold; margin-top:15px; border-radius: 10px;">Make Donation</button></a>
</div>


<?php
/**
 * Executes the email footer.
 *
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );

?>