<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>

<div class="container">
	<div class="row">
		<h2 class="text-center">CMS Certification Compliance</h2>
		<style>li.post-41039 .productbox . productimagebox { width: 150px !important; }</style>
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		<?php
			do_action( 'woocommerce_before_main_content' );
		?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php do_action( 'woocommerce_archive_description' ); ?>

		<?php
			$meta_query   = WC()->query->get_meta_query();
			$meta_query[] = array(
				'key'   => '_featured',
				'value' => 'yes'
			);

			$query_args = array(
				'post_type'           => 'product',
				'post_status'         => 'publish',
				//'ignore_sticky_posts' => true,
				'posts_per_page'      => -1,
				'orderby'             => 'menu_order',
				'order'               => 'ASC',
				'meta_query'          => $meta_query
			);
			
			$products = new WP_Query( $query_args );
			
			// error_log('archive-products.php :: 52 :: products: ' . var_dump($products));

			if ( $products->have_posts() ) {
				do_action( 'woocommerce_before_shop_loop' );
				
				woocommerce_product_loop_start();
				
				woocommerce_product_subcategories();

				while ( $products->have_posts() ) {
					$products->the_post();

					wc_get_template_part( 'content', 'product' );
					
					
					// error_log('archive-products.php :: 67 :: the post: ' . $products->the_post());
					// error_log('archive-products.php :: 68 :: content - product -- ' . wc_get_template_part( 'content', 'product' ) );

				}

				woocommerce_product_loop_end();
				//do_action( 'woocommerce_after_shop_loop' );

			} elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) {

				wc_get_template( 'loop/no-products-found.php' );

			}

			do_action( 'woocommerce_after_main_content' );
		?>
		</div>
	</div>
</div>

<?php get_footer( 'shop' );