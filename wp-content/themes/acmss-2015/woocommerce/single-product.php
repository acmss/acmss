<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>
<div class="container containerProd">
  <div class="row">
<!-- <div class="col-md-9 col-lg-9 col-xs-12"> -->
<!--
<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">

    <ul class="sidebarProd">
        <li><a href="<?php echo home_url(); ?>/store/acmss-individual-membership/">individual package</a></li>
        <li><a href="<?php echo home_url(); ?>/store/acmss-corporate-membership/">corporate package</a></li>
        <li><a href="<?php echo home_url(); ?>/store/acmss-academic-membership/">academic package</a></li>
        <li><a href="<?php echo home_url(); ?>/store/acmss-practice-membership/">practice package</a></li>
    </ul>

</div>
-->
<!-- <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12"> -->
<div class="col-md-12 col-lg-12 col-sm-8 col-xs-12">
    <?php
		if( get_the_id() != 30804 ) { 	
			echo '<div style="color: red; font-size: 30px; font-weight: bold; line-height: 1.25em; margin-bottom: 25px; display: none;">
				Physicians / Administrators: ACMSS Enables Volume Purchase Discounts and Simple Invoice Agreements to Meet CMS CEHRT Personnel Requirements
			</div>';
		}	
    ?>
    
    <?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
    
    <?php while ( have_posts() ) : the_post(); ?>
    
    <?php wc_get_template_part( 'content', 'single-product' ); ?>
    
    <?php endwhile; // end of the loop. ?>
    
    <?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
    
<!-- div .col-md-9 col-lg-9 col-sm-8 col-xs-12 -->
</div><!-- .col-md-9 col-lg-9 col-sm-8 col-xs-12 -->
    
    <?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
    
  </div>
</div>
<!-- .row -->

<?php get_footer( 'shop' ); ?>