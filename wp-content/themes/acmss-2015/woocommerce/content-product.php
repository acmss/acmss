<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $apicona;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$columns = ( isset($apicona['woocommerce-column']) && trim($apicona['woocommerce-column'])!='' ) ? trim($apicona['woocommerce-column']) : 4;
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', $columns );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
switch( $woocommerce_loop['columns'] ){
	case '4':
		$classes[] = 'col-xs-12 col-sm-6 col-md-3 col-lg-3';
		break;

	case '2':
		$classes[] = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
		break;

	case '3':
	default:
		$classes[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
		break;
}
	
?>
<li <?php post_class( $classes ); ?>>
	<div class="productbox">
		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		
		<?php
		$titleString = get_the_title();
		error_log('titleString: ' . $titleString);
		if( $titleString == 'Purchase Certifications'){
			echo '<div style="font-weight: bold; text-align: center; font-size: 24px; color: red; margin: -20px 0 10px;">Official CMS Certification Package</div>';
		}
		?>

		<a href="<?php the_permalink(); ?>">
			<div class="productimagebox">
				<?php
					do_action( 'woocommerce_before_shop_loop_item_title' );

					do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
        </a>
        
        <?php
        if( $titleString == 'Purchase Certifications &#038; Memberships'){
			// echo '<div style="font-weight: bold; text-align: center; font-size: 24px; color: red; margin: 4px 0 0px;">You Asked - We Listened!</div>';
		}
        ?>
        
		<div class="productcontent">
			<h3><?php the_title(); ?></h3>
			<?php
				do_action( 'woocommerce_after_shop_loop_item_title' );
			?>
			<!-- hi -->
			
			<?php
				// this makes the 'buy now' disappear for consulting and speaking 10/24/16 kls
				$urlString = get_permalink();
				if( !($titleString == 'Scribe Corporate Consulting' || $titleString == 'Public Speaking') ) { 
					echo('<a href=' . $urlString . ' class="cart_button">Buy Now</a>');
				}
			
			?>
			<!-- <a href="<?php the_permalink(); ?>" class="cart_button">Buy Now</a> -->
			
		</div>
	</div>
</li>