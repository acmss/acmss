<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	global $product, $variation;

	// Ensure visibility
	if ( !$product || !$product->is_visible() || !$variation['variation_is_visible'] || !$variation['variation_is_active'] )
		return;

	if( !has_post_thumbnail($variation['variation_id']) )
		return;

	// Extra post classes
	$classes = array();
	$classes[] = 'col-xs-12 col-sm-6 col-md-3 col-lg-3 acmss-product variation';
	$attributes = array();

	$href = "/store/?add-to-cart={$product->post->ID}&variation_id={$variation['variation_id']}";
	foreach( $variation['attributes'] as $key => $name ) {
		$href .= "&$key=$name"; 
		$attributes[] = ucwords( str_replace( 'attribute_', '', "$key: $name") );
	}
?>
<li <?php post_class( $classes ); ?>>
	<div class="productbox">
		
		<div class="productimagebox">
			<a href="<?php the_permalink(); ?>">
				<?php echo get_the_post_thumbnail( $variation['variation_id'] ); ?>
			</a>
			
			<a href="<?php echo $href; ?>" rel="nofollow" data-product_id="<?php echo $product->ID; ?>" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add to Cart</a>
		</div>

		<div class="productcontent">
			<h3><?php the_title(); echo ' - '; echo implode( ', ', $attributes); ?></h3>
			<?php echo $variation['price_html']; ?>
		</div>
	</div>		
</li>