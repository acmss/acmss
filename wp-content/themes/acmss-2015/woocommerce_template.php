<?php

/**
 * Template Name: woocommerce template
 */
//get_header();

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

if (function_exists('wc_print_notices'))
    wc_print_notices();
/*
  $args = array(
  'post_type' => array('shop_subscription'),
  'post_status' => array('wc-pending-cancel', 'wc-cancelled', 'wc-on-hold', 'wc-pending'),
  'offset' => -1,
  //'post__in' => array(7946),
  'posts_per_page' => -1
  );
  $the_query = new WP_Query($args);
  $data = array();
  //var_dump($the_query);
  if ($the_query->have_posts()) :
  while ($the_query->have_posts()) : $the_query->the_post();
  /*  $arr = array();
  $post_id = get_the_id();
  $status = get_post_status($post_id);
  if ($status == 'wc-pending-cancel') {
  $st = 'Pending Cancellation';
  }
  if ($status == 'wc-pending') {
  $st = 'Pending';
  }
  if ($status == 'wc-cancelled') {
  $st = 'Cancelled';
  }
  if ($status == 'wc-on-hold') {
  $st = 'On hold';
  }
  $user_id = get_post_meta($post_id, '_customer_user', TRUE);
  $user = get_user_by('id', $user_id);
  $first_name = $user->first_name;
  $last_name = $user->last_name;
  $email_id = $user->user_email;
  $arr[] = $first_name;
  $arr[] = $last_name;
  $arr[] = $email_id;
  $arr[] = $st;
  $arr[] = get_the_date();
  array_push($data, $arr);

  endwhile;


  wp_reset_postdata();
  endif;

  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=test.csv');

  // create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');



  $var_rows_data_header = array(
  'First name',
  'Last name',
  'Email Addresss',
  'Status',
  'Date',
  );

  fputcsv($output, $var_rows_data_header);
  foreach ($data as $key => $value) {
  fputcsv($output, $value);
  }
  fclose($output);

  ?>

  <?php

  $user = get_user_by('id', '5639');
  $user_last = get_user_meta(5639);
  $args = array(
  //'meta_key'     => 'paying_customer',
  //'meta_value'   => '1',
  //'meta_compare' => '=',
  'order' => 'ASC',
  'orderby' => 'display_name',
  );
  $user_query = new WP_User_Query($args);
  $data = array();
  //var_dump($the_query);
  if (!empty($user_query->results)) {
  foreach ($user_query->results as $users) {
  $arr = array();
  $id = $users->ID;
  $role = $users->roles;
  $pac = 'NO';
  $pc = get_user_meta($id, 'paying_customer', TRUE);
  if (isset($pc) && $pc == 1) {
  $pac = 'YES';
  }
  if ('acmss_member' == $role[0]) {
  $r = 'ACMSS Member';
  }
  if ('acmss_member_lapsed' == $role[0]) {
  $r = 'ACMSS Member (Lapsed)';
  }
  if ('student_member' == $role[0]) {
  $r = 'ACMSS Student Member';
  }
  if ('certified_academic_partner_administrator' == $role[0]) {
  $r = 'Academic Administrator';
  }
  if ('corporate_partner_administrator' == $role[0]) {
  $r = 'Corporate Administrator';
  }
  if ('quiz_administrator' == $role[0]) {
  $r = 'Quiz Administrator';
  }

  //var_dump($role);

  $user = get_user_by('id', $id);
  $first_name = $user->first_name;
  $last_name = $user->last_name;
  $email_id = $user->user_email;
  $arr[] = $first_name;
  $arr[] = $last_name;
  $arr[] = $email_id;
  $arr[] = $pac;
  $arr[] = $r;
  $arr[] = date_format(date_create($users->user_registered), 'l jS F Y');
  array_push($data, $arr);
  }
  }
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=quiz_administrator_yes.csv');

  // create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');



  $var_rows_data_header = array(
  'First name',
  'Last name',
  'Email Addresss',
  'Paying customer',
  'Customer type',
  'User registered'
  );

  fputcsv($output, $var_rows_data_header);
  foreach ($data as $key => $value) {
  fputcsv($output, $value);
  }
  fclose($output);



  ?>

  <?php


 */
$args = array(
    'order' => 'ASC',
    'orderby' => 'display_name',
);
$user_query = new WP_User_Query($args);
$data = array();
//var_dump($the_query);
if (!empty($user_query->results)) {
    foreach ($user_query->results as $users) {
        $arr = array();
        $id = $users->ID;
        $pc = get_user_meta($id, 'paying_customer', TRUE);
        if (isset($pc) && $pc == 1) {
            $ids[] = $id;
        }
        //var_dump($the_query);
    }
}

//foreach ($ids as $id) {
 $time = strtotime("-1 year", time());
  $date = date("Y-m-d", $time); 
$args = array(
    'post_type' => array('shop_subscription'),
    'post_status' => array('wc-pending-cancel', 'wc-cancelled', 'wc-on-hold', 'wc-active', 'wc-pending'),
    'offset' => -1,
    'orderby' => 'date',
    'order' => 'ASC',
    'date_query' => array(
        'before' => $date 
      ),
   'posts_per_page' => -1,
        /* 'meta_query' => array(
          'meta_key' => '_customer_user',
          'value' => $ids,
          ), */
);
$the_query = new WP_Query($args);

if ($the_query->have_posts()) :
    while ($the_query->have_posts()) : $the_query->the_post();
        $arr = array();
        $post_id = get_the_id();
        $status = get_post_status($post_id);
        if ($status == 'wc-active') {
            $st = 'Active';
        }
        if ($status == 'wc-pending-cancel') {
            $st = 'Pending Cancellation';
        }
        if ($status == 'wc-pending') {
            $st = 'Pending';
        }
        if ($status == 'wc-cancelled') {
            $st = 'Cancelled';
        }
        if ($status == 'wc-on-hold') {
            $st = 'On hold';
        }
        $user_id = get_post_meta($post_id, '_customer_user', TRUE);
        $user = get_user_by('id', $user_id);
        $first_name = $user->first_name;
        $last_name = $user->last_name;
        $email_id = $user->user_email;
        $arr[] = $user_id;
        $arr[] = $first_name;
        $arr[] = $last_name;
        $arr[] = $email_id;
        $arr[] = $st;
        $arr[] = get_the_date();
        array_push($data, $arr);
    endwhile;
    wp_reset_postdata();
endif;

usort($data, sort_by_order);

function sort_by_order($a, $b) {
    return $a[0] - $b[0];
}

$sub = array();
$data1 = $data;
//print_r($data);
//die();
foreach ($data as $val) {
    $id=$val[0];
    if(!isset($sub[$id])) {
        $sub[$id]=array($val[0], $val[1], $val[2], $val[3]);
    }
    array_push($sub[$id], $val[5]);
    array_push($sub[$id], $val[4]);
   
}
//print_r($sub);
//die();
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=subscription_active.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');



$var_rows_data_header = array(
    'User id',
    'First name',
    'Last name',
    'Email Addresss',
    'Date',
    'Status',
);

fputcsv($output, $var_rows_data_header);
foreach ($sub as $key => $value) {
    fputcsv($output, $value);
}
fclose($output);


//get_footer();
?>