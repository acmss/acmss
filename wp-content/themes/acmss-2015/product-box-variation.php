<?php 

	global $variation;

	foreach( $variation['attributes'] as $key => $name ) {
		$href .= "&$key=$name"; 
		$attributes[] = ucwords( str_replace( 'attribute_', '', "$key: $name") );
	}

?>
<div class="product-box" data-product-id="<?php echo $variation['variation_id']; ?>">
	<h2><?php the_title(); echo ' - '; echo implode( ', ', $attributes); ?></h2>
</div>